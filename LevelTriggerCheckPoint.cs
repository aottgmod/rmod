using UnityEngine;

public class LevelTriggerCheckPoint : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (!other.gameObject.CompareTag("Player"))
            return;
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer || other.gameObject.GetComponent<HERO>().photonView.isMine)
            FengGameManagerMKII.MKII.checkpoint = gameObject;
    }

    private void Start() {}
}