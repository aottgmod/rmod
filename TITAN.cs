using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using mItems;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using MonoBehaviour = Photon.MonoBehaviour;
using Random = UnityEngine.Random;

public class TITAN : MonoBehaviour
{
    private static Dictionary<string, int> attacks;
    private static Dictionary<string, int> attacks1;
    private static Dictionary<string, int> grab_anims;

    private Vector3 abnorma_jump_bite_horizon_v;
    public AbnormalType abnormalType;
    public int activeRad = 0x7fffffff;
    private float angle;
    public bool colliderEnabled;
    public bool isHooked;
    public bool isLook;
    public TitanTrigger myTitanTrigger;
    public List<Collider> baseColliders;
    public bool asClientLookTarget;
    private string attackAnimation;
    private float attackCheckTime;
    private float attackCheckTimeA;
    private float attackCheckTimeB;
    private int attackCount;
    public float attackDistance = 13f;
    private bool attacked;
    private float attackEndWait;
    public float attackWait = 1f;
    private float between2;
    public float chaseDistance = 80f;
    public ArrayList checkPoints = new ArrayList();
    public TITAN_CONTROLLER controller;
    public GameObject currentCamera;
    private Transform currentGrabHand;
    private float desDeg;
    private float dieTime;
    private string fxName;
    private Vector3 fxPosition;
    private Quaternion fxRotation;
    private float getdownTime;
    public GameObject grabbedTarget;
    public GameObject grabTF;
    private float gravity = 120f;
    private bool grounded;
    public bool hasDie;
    private bool hasDieSteam;
    private Transform head;
    private Vector3 headscale = Vector3.one;
    private string hitAnimation;
    private float hitPause;
    public bool isAlarm;
    private bool isAttackMoveByCore;
    private bool isGrabHandLeft;
    private bool leftHandAttack;
    public GameObject mainMaterial;
    private float maxStamina = 320f;
    public float maxVelocityChange = 10f;
    public static float minusDistance = 99999f;
    public static GameObject minusDistanceEnemy;
    public int myDifficulty;
    public float myDistance;
    public GROUP myGroup = GROUP.T;
    public GameObject myHero;
    public float myLevel = 1f;
    private Transform neck;
    private bool needFreshCorePosition;
    private string nextAttackAnimation;
    public bool nonAI;
    private bool nonAIcombo;
    private Vector3 oldCorePosition;
    private Quaternion oldHeadRotation;
    public PVPcheckPoint PVPfromCheckPt;
    private float random_run_time;
    private float rockInterval;
    private string runAnimation;
    private float sbtime;
    private Vector3 spawnPt;
    public float speed = 7f;
    private float stamina = 320f;
    private TitanState state;
    private int stepSoundPhase = 2;
    private bool stuck;
    private float stuckTime;
    private float stuckTurnAngle;
    private Vector3 targetCheckPt;
    private Quaternion targetHeadRotation;
    private float targetR;
    private float tauntTime;
    private GameObject throwRock;
    private string turnAnimation;
    private float turnDeg;
    private GameObject whoHasTauntMe;

    private Transform myTransform;
    private Rigidbody myRigidbody;

    /*private TITAN()
        : base(SPECIES.TITAN)
    {
    }*/

    private void attack(string type)
    {
        state = TitanState.attack;
        attacked = false;
        isAlarm = true;
        if (attackAnimation == type)
        {
            attackAnimation = type;
            playAnimationAt("attack_" + type, 0f);
        }
        else
        {
            attackAnimation = type;
            playAnimationAt("attack_" + type, 0f);
        }
        nextAttackAnimation = null;
        fxName = null;
        isAttackMoveByCore = false;
        attackCheckTime = 0f;
        attackCheckTimeA = 0f;
        attackCheckTimeB = 0f;
        attackEndWait = 0f;
        fxRotation = Quaternion.Euler(270f, 0f, 0f);
        var key = type;
        if (key != null)
        {
            int num;
            if (attacks1 == null)
            {
                attacks1 = new Dictionary<string, int>(22)
                {
                    { "abnormal_getup", 0 }, { "abnormal_jump", 1 }, { "combo_1", 2 }, { "combo_2", 3 }, { "combo_3", 4 }, { "front_ground", 5 },
                    { "kick", 6 }, { "slap_back", 7 }, { "slap_face", 8 }, { "stomp", 9 }, { "bite", 10 }, { "bite_l", 11 }, { "bite_r", 12 },
                    { "jumper_0", 13 }, { "crawler_jump_0", 14 }, { "anti_AE_l", 15 }, { "anti_AE_r", 16 }, { "anti_AE_low_l", 17 },
                    { "anti_AE_low_r", 18 }, { "quick_turn_l", 19 }, { "quick_turn_r", 20 }, { "throw", 21 }
                };
            }
            if (attacks1.TryGetValue(key, out num))
            {
                switch (num)
                {
                    case 0:
                        attackCheckTime = 0f;
                        fxName = string.Empty;
                        break;
                    case 1:
                        nextAttackAnimation = "abnormal_getup";
                        if (!nonAI)
                            attackEndWait = myDifficulty <= 0 ? Random.Range(1f, 4f) : Random.Range(0f, 1f);
                        else
                            attackEndWait = 0f;
                        attackCheckTime = 0.75f;
                        fxName = "boom4";
                        fxRotation = Quaternion.Euler(270f, myTransform.rotation.eulerAngles.y, 0f);
                        break;
                    case 2:
                        nextAttackAnimation = "combo_2";
                        attackCheckTimeA = 0.54f;
                        attackCheckTimeB = 0.76f;
                        nonAIcombo = false;
                        isAttackMoveByCore = true;
                        leftHandAttack = false;
                        break;
                    case 3:
                        if (abnormalType != AbnormalType.TYPE_PUNK)
                            nextAttackAnimation = "combo_3";
                        attackCheckTimeA = 0.37f;
                        attackCheckTimeB = 0.57f;
                        nonAIcombo = false;
                        isAttackMoveByCore = true;
                        leftHandAttack = true;
                        break;
                    case 4:
                        nonAIcombo = false;
                        isAttackMoveByCore = true;
                        attackCheckTime = 0.21f;
                        fxName = "boom1";
                        break;
                    case 5:
                        fxName = "boom1";
                        attackCheckTime = 0.45f;
                        break;
                    case 6:
                        fxName = "boom5";
                        fxRotation = myTransform.rotation;
                        attackCheckTime = 0.43f;
                        break;
                    case 7:
                        fxName = "boom3";
                        attackCheckTime = 0.66f;
                        break;
                    case 8:
                        fxName = "boom3";
                        attackCheckTime = 0.655f;
                        break;
                    case 9:
                        fxName = "boom2";
                        attackCheckTime = 0.42f;
                        break;
                    case 10:
                        fxName = "bite";
                        attackCheckTime = 0.6f;
                        break;
                    case 11:
                        fxName = "bite";
                        attackCheckTime = 0.4f;
                        break;
                    case 12:
                        fxName = "bite";
                        attackCheckTime = 0.4f;
                        break;
                    case 13:
                        abnorma_jump_bite_horizon_v = Vector3.zero;
                        break;
                    case 14:
                        abnorma_jump_bite_horizon_v = Vector3.zero;
                        break;
                    case 15:
                        attackCheckTimeA = 0.31f;
                        attackCheckTimeB = 0.4f;
                        leftHandAttack = true;
                        break;
                    case 16:
                        attackCheckTimeA = 0.31f;
                        attackCheckTimeB = 0.4f;
                        leftHandAttack = false;
                        break;
                    case 17:
                        attackCheckTimeA = 0.31f;
                        attackCheckTimeB = 0.4f;
                        leftHandAttack = true;
                        break;
                    case 18:
                        attackCheckTimeA = 0.31f;
                        attackCheckTimeB = 0.4f;
                        leftHandAttack = false;
                        break;
                    case 19:
                        attackCheckTimeA = 2f;
                        attackCheckTimeB = 2f;
                        isAttackMoveByCore = true;
                        break;
                    case 20:
                        attackCheckTimeA = 2f;
                        attackCheckTimeB = 2f;
                        isAttackMoveByCore = true;
                        break;
                    case 21:
                        isAlarm = true;
                        chaseDistance = 99999f;
                        break;
                }
            }
        }
        needFreshCorePosition = true;
    }

    private void Awake()
    {
        myTransform = transform;
        myRigidbody = rigidbody;

        rigidbody.freezeRotation = true;
        rigidbody.useGravity = false;
    }

    public void beLaughAttacked()
    {
        if (hasDie || abnormalType == AbnormalType.TYPE_CRAWLER)
            return;
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
            photonView.RPC("laugh", PhotonTargets.All, 0f);
        else if (state == TitanState.idle || state == TitanState.turn || state == TitanState.chase)
            laugh();
    }

    public void beTauntedBy(GameObject target, float tauntTime)
    {
        whoHasTauntMe = target;
        this.tauntTime = tauntTime;
        isAlarm = true;
    }

    private void chase()
    {
        state = TitanState.chase;
        isAlarm = true;
        crossFade(runAnimation, 0.5f);
    }

    private GameObject checkIfHitCrawlerMouth(Transform head, float rad)
    {
        var num = rad * myLevel;
        foreach (var player in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (player.GetComponent<TITAN_EREN>() != null || (player.GetComponent<HERO>() != null && player.GetComponent<HERO>().isInvincible()))
                continue;
            var hitbox = player.GetComponent<CapsuleCollider>().height * 0.5f;
            if (Vector3.Distance(player.transform.position + Vector3.up * hitbox, head.position - Vector3.up * 1.5f * myLevel) < num + hitbox)
                return player;
        }
        return null;
    }

    private GameObject checkIfHitHand(Transform hand)
    {
        var num = 2.4f * myLevel;
        foreach (var col in Physics.OverlapSphere(hand.GetComponent<SphereCollider>().transform.position, num + 1f))
        {
            if (!col.transform.root.CompareTag("Player"))
                continue;
            var go = col.transform.root.gameObject;
            var eren = go.GetComponent<TITAN_EREN>();
            if (eren != null)
            {
                if (!eren.isHit)
                    eren.hitByTitan();
            }
            else if (go.GetComponent<HERO>() != null && !go.GetComponent<HERO>().isInvincible())
                return go;
        }
        return null;
    }

    private GameObject checkIfHitHead(Transform head, float rad)
    {
        var num = rad * myLevel;
        foreach (var obj2 in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (obj2.GetComponent<TITAN_EREN>() != null || (obj2.GetComponent<HERO>() != null && obj2.GetComponent<HERO>().isInvincible()))
                continue;
            var num3 = obj2.GetComponent<CapsuleCollider>().height * 0.5f;
            if (Vector3.Distance(obj2.transform.position + Vector3.up * num3, head.position + Vector3.up * 1.5f * myLevel) < num + num3)
                return obj2;
        }
        return null;
    }

    private void crossFade(string aniName, float time)
    {
        animation.CrossFade(aniName, time);
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine)
            photonView.RPC("netCrossFade", PhotonTargets.Others, aniName, time);
    }

    public bool die()
    {
        if (hasDie)
            return false;
        hasDie = true;
        FengGameManagerMKII.MKII.oneTitanDown(string.Empty);
        dieAnimation();
        return true;
    }

    private void dieAnimation()
    {
        if (animation.IsPlaying("sit_idle") || animation.IsPlaying("sit_hit_eye"))
            crossFade("sit_die", 0.1f);
        else if (abnormalType == AbnormalType.TYPE_CRAWLER)
            crossFade("crawler_die", 0.2f);
        else if (abnormalType == AbnormalType.NORMAL)
            crossFade("die_front", 0.05f);
        else if (animation.IsPlaying("attack_abnormal_jump") && animation["attack_abnormal_jump"].normalizedTime > 0.7f
            || animation.IsPlaying("attack_abnormal_getup") && animation["attack_abnormal_getup"].normalizedTime < 0.7f
            || animation.IsPlaying("tired"))
            crossFade("die_ground", 0.2f);
        else
            crossFade("die_back", 0.05f);
    }

    public void dieBlow(Vector3 attacker, float hitPauseTime)
    {
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
        {
            dieBlowFunc(attacker, hitPauseTime);
            if (GameObject.FindGameObjectsWithTag("titan").Length <= 1)
                IN_GAME_MAIN_CAMERA.maincamera.gameOver = true;
        }
        else
            photonView.RPC("dieBlowRPC", PhotonTargets.All, attacker, hitPauseTime);
    }

    public void dieBlowFunc(Vector3 attacker, float hitPauseTime)
    {
        if (hasDie)
            return;
        myTransform.rotation = Quaternion.Euler(0f, Quaternion.LookRotation(attacker - myTransform.position).eulerAngles.y, 0f);
        hasDie = true;
        hitAnimation = "die_blow";
        hitPause = hitPauseTime;
        playAnimation(hitAnimation);
        animation[hitAnimation].time = 0f;
        animation[hitAnimation].speed = 0f;
        needFreshCorePosition = true;
        FengGameManagerMKII.MKII.oneTitanDown(string.Empty);
        if (!photonView.isMine)
            return;
        if (grabbedTarget != null)
            grabbedTarget.GetPhotonView().RPC("netUngrabbed", PhotonTargets.All);
        if (!nonAI)
            return;
        currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(null);
        currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setSpectorMode(true);
        currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().gameOver = true;
        var propertiesToSet = new Hashtable
        {
            { PhotonPlayerProperty.dead, true }, { PhotonPlayerProperty.deaths, (int)PhotonNetwork.player.customProperties[PhotonPlayerProperty.deaths] + 1 }
        };
        PhotonNetwork.player.SetCustomProperties(propertiesToSet);
    }

    [RPC]
    private void dieBlowRPC(Vector3 attacker, float hitPauseTime)
    {
        if (!photonView.isMine)
            return;
        var vector = attacker - myTransform.position;
        if (vector.magnitude < 80f)
            dieBlowFunc(attacker, hitPauseTime);
    }

    public void dieHeadBlow(Vector3 attacker, float hitPauseTime)
    {
        if (abnormalType == AbnormalType.TYPE_CRAWLER)
            return;
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
        {
            dieHeadBlowFunc(attacker, hitPauseTime);
            if (GameObject.FindGameObjectsWithTag("titan").Length <= 1)
                IN_GAME_MAIN_CAMERA.maincamera.gameOver = true;
        }
        else
            photonView.RPC("dieHeadBlowRPC", PhotonTargets.All, attacker, hitPauseTime);
    }

    public void dieHeadBlowFunc(Vector3 attacker, float hitPauseTime)
    {
        if (hasDie)
            return;
        playSound("snd_titan_head_blow");
        myTransform.rotation = Quaternion.Euler(0f, Quaternion.LookRotation(attacker - myTransform.position).eulerAngles.y, 0f);
        hasDie = true;
        hitAnimation = "die_headOff";
        hitPause = hitPauseTime;
        playAnimation(hitAnimation);
        animation[hitAnimation].time = 0f;
        animation[hitAnimation].speed = 0f;
        FengGameManagerMKII.MKII.oneTitanDown(string.Empty);
        needFreshCorePosition = true;
        var blood = IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine
            ? PhotonNetwork.Instantiate("bloodExplore", head.position + Vector3.up * 1f * myLevel, Quaternion.Euler(270f, 0f, 0f), 0)
            : (GameObject)Instantiate(CacheResources.Load("bloodExplore"), head.position + Vector3.up * 1f * myLevel, Quaternion.Euler(270f, 0f, 0f));
        blood.transform.localScale = myTransform.localScale;
        blood = IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine
            ? PhotonNetwork.Instantiate("bloodsplatter", head.position, Quaternion.Euler(270f + neck.rotation.eulerAngles.x, neck.rotation.eulerAngles.y, neck.rotation.eulerAngles.z), 0)
            : (GameObject)Instantiate(CacheResources.Load("bloodsplatter"), head.position, Quaternion.Euler(270f + neck.rotation.eulerAngles.x, neck.rotation.eulerAngles.y, neck.rotation.eulerAngles.z));
        blood.transform.localScale = myTransform.localScale;
        blood.transform.parent = neck;
        blood = IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine ? PhotonNetwork.Instantiate("FX/justSmoke", neck.position, Quaternion.Euler(270f, 0f, 0f), 0) : (GameObject)Instantiate(CacheResources.Load("FX/justSmoke"), neck.position, Quaternion.Euler(270f, 0f, 0f));
        blood.transform.parent = neck;
        if (!photonView.isMine)
            return;
        if (grabbedTarget != null)
            grabbedTarget.GetPhotonView().RPC("netUngrabbed", PhotonTargets.All);
        if (!nonAI)
            return;
        var c = currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>();
        c.setMainObject(null);
        c.setSpectorMode(true);
        c.gameOver = true;
        var propertiesToSet = new Hashtable
        {
            { PhotonPlayerProperty.dead, true }, { PhotonPlayerProperty.deaths, (int)PhotonNetwork.player.customProperties[PhotonPlayerProperty.deaths] + 1 }
        };
        PhotonNetwork.player.SetCustomProperties(propertiesToSet);
    }

    [RPC]
    private void dieHeadBlowRPC(Vector3 attacker, float hitPauseTime)
    {
        if (!photonView.isMine)
            return;
        var vector = attacker - myTransform.position;
        if (vector.magnitude < 80f)
            dieHeadBlowFunc(attacker, hitPauseTime);
    }

    private void eat()
    {
        state = TitanState.eat;
        attacked = false;
        if (isGrabHandLeft)
        {
            attackAnimation = "eat_l";
            crossFade("eat_l", 0.1f);
        }
        else
        {
            attackAnimation = "eat_r";
            crossFade("eat_r", 0.1f);
        }
    }

    private void eatSet(GameObject grabTarget)
    {
        if ((IN_GAME_MAIN_CAMERA.IsSingleplayer || (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine)) && grabTarget.GetComponent<HERO>().isGrabbed)
            return;
        grabToRight();
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine)
        {
            photonView.RPC("grabToRight", PhotonTargets.Others);
            grabTarget.GetPhotonView().RPC("netPlayAnimation", PhotonTargets.All, "grabbed");
            grabTarget.GetPhotonView().RPC("netGrabbed", PhotonTargets.All, photonView.viewID, false);
        }
        else
        {
            grabTarget.GetComponent<HERO>().grabbed(gameObject, false);
            grabTarget.GetComponent<HERO>().animation.Play("grabbed");
        }
    }

    private void eatSetL(GameObject grabTarget)
    {
        if ((IN_GAME_MAIN_CAMERA.IsSingleplayer || (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine)) && grabTarget.GetComponent<HERO>().isGrabbed)
            return;
        grabToLeft();
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine)
        {
            photonView.RPC("grabToLeft", PhotonTargets.Others);
            grabTarget.GetPhotonView().RPC("netPlayAnimation", PhotonTargets.All, "grabbed");
            grabTarget.GetPhotonView().RPC("netGrabbed", PhotonTargets.All, photonView.viewID, true);
        }
        else
        {
            grabTarget.GetComponent<HERO>().grabbed(gameObject, true);
            grabTarget.GetComponent<HERO>().animation.Play("grabbed");
        }
    }

    private bool executeAttack(string decidedAction)
    {
        var key = decidedAction;
        if (key == null)
            return false;
        int num;
        if (attacks == null)
        {
            attacks = new Dictionary<string, int>
            {
                { "grab_ground_front_l", 0 }, { "grab_ground_front_r", 1 }, { "grab_ground_back_l", 2 }, { "grab_ground_back_r", 3 },
                { "grab_head_front_l", 4 }, { "grab_head_front_r", 5 }, { "grab_head_back_l", 6 }, { "grab_head_back_r", 7 },
                { "attack_abnormal_jump", 8 }, { "attack_combo", 9 }, { "attack_front_ground", 10 }, { "attack_kick", 11 },
                { "attack_slap_back", 12 }, { "attack_slap_face", 13 }, { "attack_stomp", 14 }, { "attack_bite", 15 },
                { "attack_bite_l", 16 }, { "attack_bite_r", 17 }
            };
        }
        if (attacks.TryGetValue(key, out num))
        {
            switch (num)
            {
                case 0:
                    grab("ground_front_l");
                    return true;
                case 1:
                    grab("ground_front_r");
                    return true;
                case 2:
                    grab("ground_back_l");
                    return true;
                case 3:
                    grab("ground_back_r");
                    return true;
                case 4:
                    grab("head_front_l");
                    return true;
                case 5:
                    grab("head_front_r");
                    return true;
                case 6:
                    grab("head_back_l");
                    return true;
                case 7:
                    grab("head_back_r");
                    return true;
                case 8:
                    attack("abnormal_jump");
                    return true;
                case 9:
                    attack("combo_1");
                    return true;
                case 10:
                    attack("front_ground");
                    return true;
                case 11:
                    attack("kick");
                    return true;
                case 12:
                    attack("slap_back");
                    return true;
                case 13:
                    attack("slap_face");
                    return true;
                case 14:
                    attack("stomp");
                    return true;
                case 15:
                    attack("bite");
                    return true;
                case 16:
                    attack("bite_l");
                    return true;
                case 17:
                    attack("bite_r");
                    return true;
            }
        }
        return false;
    }

    private void findNearestFacingHero()
    {
        var heroes = GameObject.FindGameObjectsWithTag("Player");
        GameObject obj2 = null;
        var positiveInfinity = float.PositiveInfinity;
        var position = myTransform.position;
        var num3 = abnormalType != AbnormalType.NORMAL ? 180f : 100f;
        foreach (var h in heroes)
        {
            var vector2 = h.transform.position - position;
            var sqrMagnitude = vector2.sqrMagnitude;
            if (!(sqrMagnitude < positiveInfinity))
                continue;
            var vector3 = h.transform.position - myTransform.position;
            var current = -Mathf.Atan2(vector3.z, vector3.x) * 57.29578f;
            var f = -Mathf.DeltaAngle(current, gameObject.transform.rotation.eulerAngles.y - 90f);
            if (!(Mathf.Abs(f) < num3))
                continue;
            obj2 = h;
            positiveInfinity = sqrMagnitude;
        }
        if (obj2 == null)
            return;
        var hero = myHero;
        myHero = obj2;
        if (hero != myHero && IN_GAME_MAIN_CAMERA.IsMultiplayer && PhotonNetwork.isMasterClient)
        {
            if (myHero == null)
                photonView.RPC("setMyTarget", PhotonTargets.Others, -1);
            else
                photonView.RPC("setMyTarget", PhotonTargets.Others, myHero.GetPhotonView().viewID);
        }
        tauntTime = 5f;
    }

    private void findNearestHero()
    {
        var hero = myHero;
        myHero = getNearestHero();
        if (myHero != hero && IN_GAME_MAIN_CAMERA.IsMultiplayer && PhotonNetwork.isMasterClient)
        {
            if (myHero == null)
                photonView.RPC("setMyTarget", PhotonTargets.Others, -1);
            else
                photonView.RPC("setMyTarget", PhotonTargets.Others, myHero.GetPhotonView().viewID);
        }
        oldHeadRotation = head.rotation;
    }

    private void FixedUpdate()
    {
        if ((IN_GAME_MAIN_CAMERA.isPausing && IN_GAME_MAIN_CAMERA.IsSingleplayer) || !IN_GAME_MAIN_CAMERA.IsSingleplayer && !photonView.isMine)
            return;
        rigidbody.AddForce(new Vector3(0f, -gravity * rigidbody.mass, 0f));
        if (needFreshCorePosition)
        {
            oldCorePosition = myTransform.position - myTransform.Find("Amarture/Core").position;
            needFreshCorePosition = false;
        }
        if (hasDie)
        {
            if (hitPause <= 0f && animation.IsPlaying("die_headOff"))
                rigidbody.velocity = (myTransform.position - myTransform.Find("Amarture/Core").position - oldCorePosition) / Time.deltaTime + Vector3.up * rigidbody.velocity.y;
            oldCorePosition = myTransform.position - myTransform.Find("Amarture/Core").position;
        }
        else if (state == TitanState.attack && isAttackMoveByCore || state == TitanState.hit)
        {
            rigidbody.velocity = (myTransform.position - myTransform.Find("Amarture/Core").position - oldCorePosition) / Time.deltaTime + Vector3.up * rigidbody.velocity.y;
            oldCorePosition = myTransform.position - myTransform.Find("Amarture/Core").position;
        }
        if (hasDie)
        {
            if (hitPause > 0f)
            {
                hitPause -= Time.deltaTime;
                if (hitPause <= 0f)
                {
                    animation[hitAnimation].speed = 1f;
                    hitPause = 0f;
                }
            }
            else if (animation.IsPlaying("die_blow"))
            {
                rigidbody.velocity = animation["die_blow"].normalizedTime < 0.55f
                    ? -myTransform.forward * 300f + Vector3.up * rigidbody.velocity.y
                    : (animation["die_blow"].normalizedTime < 0.83f
                        ? -myTransform.forward * 100f + Vector3.up * rigidbody.velocity.y : Vector3.up * rigidbody.velocity.y);
            }
        }
        else
        {
            if (nonAI && !IN_GAME_MAIN_CAMERA.isPausing && (state == TitanState.idle || state == TitanState.attack && attackAnimation == "jumper_1"))
            {
                var zero = Vector3.zero;
                if (controller.targetDirection != -874f)
                {
                    var flag = false;
                    if (stamina < 5f)
                        flag = true;
                    else if (stamina < 40f && !animation.IsPlaying("run_abnormal") && !animation.IsPlaying("crawler_run"))
                        flag = true;
                    zero = controller.isWALKDown || flag ? myTransform.forward * speed * Mathf.Sqrt(myLevel) * 0.2f : myTransform.forward * speed * Mathf.Sqrt(myLevel);
                    gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, Quaternion.Euler(0f, controller.targetDirection, 0f),
                        speed * 0.15f * Time.deltaTime);
                    if (state == TitanState.idle)
                    {
                        if (controller.isWALKDown || flag)
                        {
                            if (abnormalType == AbnormalType.TYPE_CRAWLER)
                            {
                                if (!animation.IsPlaying("crawler_run"))
                                    crossFade("crawler_run", 0.1f);
                            }
                            else if (!animation.IsPlaying("run_walk"))
                                crossFade("run_walk", 0.1f);
                        }
                        else if (abnormalType == AbnormalType.TYPE_CRAWLER)
                        {
                            if (!animation.IsPlaying("crawler_run"))
                                crossFade("crawler_run", 0.1f);
                            var obj2 = checkIfHitCrawlerMouth(head, 2.2f);
                            if (obj2 != null)
                            {
                                var position = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest").position;
                                if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                                    obj2.GetComponent<HERO>().die((obj2.transform.position - position) * 15f * myLevel, false);
                                else if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine && !obj2.GetComponent<HERO>().HasDied())
                                {
                                    obj2.GetComponent<HERO>().markDie();
                                    var parameters = new object[]
                                        { (obj2.transform.position - position) * 15f * myLevel, true, !nonAI ? -1 : photonView.viewID, name, true };
                                    obj2.GetComponent<HERO>().photonView.RPC("netDie", PhotonTargets.All, parameters);
                                }
                            }
                        }
                        else if (!animation.IsPlaying("run_abnormal"))
                            crossFade("run_abnormal", 0.1f);
                    }
                }
                else if (state == TitanState.idle)
                {
                    if (abnormalType == AbnormalType.TYPE_CRAWLER)
                    {
                        if (!animation.IsPlaying("crawler_idle"))
                            crossFade("crawler_idle", 0.1f);
                    }
                    else if (!animation.IsPlaying("idle"))
                        crossFade("idle", 0.1f);
                    zero = Vector3.zero;
                }
                if (state == TitanState.idle)
                {
                    var velocity = rigidbody.velocity;
                    var force = zero - velocity;
                    force.x = Mathf.Clamp(force.x, -maxVelocityChange, maxVelocityChange);
                    force.z = Mathf.Clamp(force.z, -maxVelocityChange, maxVelocityChange);
                    force.y = 0f;
                    rigidbody.AddForce(force, ForceMode.VelocityChange);
                }
                else if (state == TitanState.attack && attackAnimation == "jumper_0")
                {
                    var vector7 = rigidbody.velocity;
                    var vector8 = zero * 0.8f - vector7;
                    vector8.x = Mathf.Clamp(vector8.x, -maxVelocityChange, maxVelocityChange);
                    vector8.z = Mathf.Clamp(vector8.z, -maxVelocityChange, maxVelocityChange);
                    vector8.y = 0f;
                    rigidbody.AddForce(vector8, ForceMode.VelocityChange);
                }
            }
            if ((abnormalType == AbnormalType.TYPE_I || abnormalType == AbnormalType.TYPE_JUMPER) && !nonAI && state == TitanState.attack && attackAnimation == "jumper_0")
            {
                var vector9 = myTransform.forward * speed * myLevel * 0.5f;
                var vector10 = rigidbody.velocity;
                if (animation["attack_jumper_0"].normalizedTime <= 0.28f || animation["attack_jumper_0"].normalizedTime >= 0.8f)
                    vector9 = Vector3.zero;
                var vector11 = vector9 - vector10;
                vector11.x = Mathf.Clamp(vector11.x, -maxVelocityChange, maxVelocityChange);
                vector11.z = Mathf.Clamp(vector11.z, -maxVelocityChange, maxVelocityChange);
                vector11.y = 0f;
                rigidbody.AddForce(vector11, ForceMode.VelocityChange);
            }
            if (state != TitanState.chase && state != TitanState.wander && state != TitanState.to_check_point && state != TitanState.to_pvp_pt && state != TitanState.random_run)
                return;
            var vector12 = myTransform.forward * speed;
            var vector13 = rigidbody.velocity;
            var vector14 = vector12 - vector13;
            vector14.x = Mathf.Clamp(vector14.x, -maxVelocityChange, maxVelocityChange);
            vector14.z = Mathf.Clamp(vector14.z, -maxVelocityChange, maxVelocityChange);
            vector14.y = 0f;
            rigidbody.AddForce(vector14, ForceMode.VelocityChange);
            if (!stuck && abnormalType != AbnormalType.TYPE_CRAWLER && !nonAI)
            {
                if (animation.IsPlaying(runAnimation) && rigidbody.velocity.magnitude < speed * 0.5f)
                {
                    stuck = true;
                    stuckTime = 2f;
                    stuckTurnAngle = Random.Range(0, 2) * 140f - 70f;
                }
                if (state == TitanState.chase && myHero != null && myDistance > attackDistance && myDistance < 150f)
                {
                    var num = 0.05f;
                    if (myDifficulty > 1)
                        num += 0.05f;
                    if (abnormalType != AbnormalType.NORMAL)
                        num += 0.1f;
                    if (Random.Range(0f, 1f) < num)
                    {
                        stuck = true;
                        stuckTime = 1f;
                        var num2 = Random.Range(20f, 50f);
                        stuckTurnAngle = Random.Range(0, 2) * num2 * 2f - num2;
                    }
                }
            }
            var current = 0f;
            switch (state)
            {
                case TitanState.wander:
                    current = myTransform.rotation.eulerAngles.y - 90f;
                    break;
                case TitanState.to_check_point:
                case TitanState.to_pvp_pt:
                case TitanState.random_run:
                    var vector15 = targetCheckPt - myTransform.position;
                    current = -Mathf.Atan2(vector15.z, vector15.x) * 57.29578f;
                    break;
                default:
                    if (myHero == null)
                        return;
                    var vector16 = myHero.transform.position - myTransform.position;
                    current = -Mathf.Atan2(vector16.z, vector16.x) * 57.29578f;
                    break;
            }
            if (stuck)
            {
                stuckTime -= Time.deltaTime;
                if (stuckTime < 0f)
                    stuck = false;
                if (stuckTurnAngle > 0f)
                    stuckTurnAngle -= Time.deltaTime * 10f;
                else
                    stuckTurnAngle += Time.deltaTime * 10f;
                current += stuckTurnAngle;
            }
            var num4 = -Mathf.DeltaAngle(current, gameObject.transform.rotation.eulerAngles.y - 90f);
            if (abnormalType == AbnormalType.TYPE_CRAWLER)
                gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation,
                    Quaternion.Euler(0f, gameObject.transform.rotation.eulerAngles.y + num4, 0f), speed * 0.3f * Time.deltaTime / myLevel);
            else
                gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation,
                    Quaternion.Euler(0f, gameObject.transform.rotation.eulerAngles.y + num4, 0f), speed * 0.5f * Time.deltaTime / myLevel);
        }
    }

    private string[] GetAttackStrategy()
    {
        string[] output = null;
        var num = 0;
        if (!isAlarm && !(myHero.transform.position.y + 3f <= neck.position.y + 10f * myLevel))
            return output;
        if (myHero.transform.position.y > neck.position.y - 3f * myLevel)
        {
            if (myDistance < attackDistance * 0.5f)
            {
                if (Vector3.Distance(myHero.transform.position, myTransform.Find("chkOverHead").position) < 3.6f * myLevel)
                    output = between2 > 0f ? new[] { "grab_head_front_r" } : new[] { "grab_head_front_l" };
                else if (Mathf.Abs(between2) < 90f)
                {
                    if (Mathf.Abs(between2) < 30f)
                    {
                        if (Vector3.Distance(myHero.transform.position, myTransform.Find("chkFront").position) < 2.5f * myLevel)
                            output = new[] { "attack_bite", "attack_bite", "attack_slap_face" };
                    }
                    else if (between2 > 0f)
                    {
                        if (Vector3.Distance(myHero.transform.position, myTransform.Find("chkFrontRight").position) < 2.5f * myLevel)
                            output = new[] { "attack_bite_r" };
                    }
                    else if (Vector3.Distance(myHero.transform.position, myTransform.Find("chkFrontLeft").position) < 2.5f * myLevel)
                        output = new[] { "attack_bite_l" };
                }
                else if (between2 > 0f)
                {
                    if (Vector3.Distance(myHero.transform.position, myTransform.Find("chkBackRight").position) < 2.8f * myLevel)
                        output = new[] { "grab_head_back_r", "grab_head_back_r", "attack_slap_back" };
                }
                else if (Vector3.Distance(myHero.transform.position, myTransform.Find("chkBackLeft").position) < 2.8f * myLevel)
                    output = new[] { "grab_head_back_l", "grab_head_back_l", "attack_slap_back" };
            }
            if (output != null)
                return output;
            if (abnormalType == AbnormalType.NORMAL || abnormalType == AbnormalType.TYPE_PUNK)
            {
                if (myDifficulty <= 0 && Random.Range(0, 0x3e8) >= 3)
                    return output;
                return Mathf.Abs(between2) >= 60f ? output : new[] { "attack_combo" };
            }
            if (abnormalType != AbnormalType.TYPE_I && abnormalType != AbnormalType.TYPE_JUMPER)
                return output;
            if (myDifficulty <= 0 && Random.Range(0, 100) >= 50)
                return output;
            return new[] { "attack_abnormal_jump" };
        }
        if (Mathf.Abs(between2) < 90f)
            num = between2 > 0f ? 1 : 2;
        else
            num = between2 > 0f ? 4 : 3;
        switch (num)
        {
            case 1:
                if (myDistance >= attackDistance * 0.25f)
                {
                    if (myDistance < attackDistance * 0.5f)
                    {
                        if (abnormalType != AbnormalType.TYPE_PUNK && abnormalType == AbnormalType.NORMAL)
                            return new[] { "grab_ground_front_r", "grab_ground_front_r", "attack_stomp" };
                        return new[] { "grab_ground_front_r", "grab_ground_front_r", "attack_abnormal_jump" };
                    }
                    if (abnormalType == AbnormalType.TYPE_PUNK)
                        return new[] { "attack_combo", "attack_combo", "attack_abnormal_jump" };
                    if (abnormalType == AbnormalType.NORMAL)
                    {
                        return myDifficulty > 0
                            ? new[] { "attack_front_ground", "attack_combo", "attack_combo" }
                            : new[] { "attack_front_ground", "attack_front_ground", "attack_front_ground", "attack_front_ground", "attack_combo" };
                    }
                    return new[] { "attack_abnormal_jump" };
                }
                if (abnormalType != AbnormalType.TYPE_PUNK)
                    return abnormalType == AbnormalType.NORMAL ? new[] { "attack_front_ground", "attack_stomp" } : new[] { "attack_kick" };
                return new[] { "attack_kick", "attack_stomp" };
            case 2:
                if (myDistance >= attackDistance * 0.25f)
                {
                    if (myDistance < attackDistance * 0.5f)
                    {
                        if (abnormalType != AbnormalType.TYPE_PUNK && abnormalType == AbnormalType.NORMAL)
                            return new[] { "grab_ground_front_l", "grab_ground_front_l", "attack_stomp" };
                        return new[] { "grab_ground_front_l", "grab_ground_front_l", "attack_abnormal_jump" };
                    }
                    if (abnormalType == AbnormalType.TYPE_PUNK)
                        return new[] { "attack_combo", "attack_combo", "attack_abnormal_jump" };
                    if (abnormalType == AbnormalType.NORMAL)
                    {
                        return myDifficulty > 0
                            ? new[] { "attack_front_ground", "attack_combo", "attack_combo" }
                            : new[] { "attack_front_ground", "attack_front_ground", "attack_front_ground", "attack_front_ground", "attack_combo" };
                    }
                    return new[] { "attack_abnormal_jump" };
                }
                if (abnormalType != AbnormalType.TYPE_PUNK)
                    return abnormalType == AbnormalType.NORMAL ? new[] { "attack_front_ground", "attack_stomp" } : new[] { "attack_kick" };
                return new[] { "attack_kick", "attack_stomp" };
            case 3:
                return myDistance >= attackDistance * 0.5f ? output : new[] { "grab_ground_back_l" };
            case 4:
                return myDistance >= attackDistance * 0.5f ? output : new[] { "grab_ground_back_r" };
        }
        return output;
    }

    private void getDown()
    {
        state = TitanState.down;
        isAlarm = true;
        playAnimation("sit_hunt_down");
        getdownTime = Random.Range(3f, 5f);
    }

    private GameObject getNearestHero()
    {
        var objArray = GameObject.FindGameObjectsWithTag("Player");
        GameObject obj2 = null;
        var positiveInfinity = float.PositiveInfinity;
        var position = myTransform.position;
        foreach (var obj3 in objArray)
        {
            var vector2 = obj3.transform.position - position;
            var sqrMagnitude = vector2.sqrMagnitude;
            if (sqrMagnitude < positiveInfinity)
            {
                obj2 = obj3;
                positiveInfinity = sqrMagnitude;
            }
        }
        return obj2;
    }

    private int getPunkNumber()
    {
        var num = 0;
        foreach (var obj2 in GameObject.FindGameObjectsWithTag("titan"))
        {
            if (obj2.GetComponent<TITAN>() != null && obj2.GetComponent<TITAN>().name == "Punk")
                num++;
        }
        return num;
    }

    private void grab(string type)
    {
        state = TitanState.grab;
        attacked = false;
        isAlarm = true;
        attackAnimation = type;
        crossFade("grab_" + type, 0.1f);
        isGrabHandLeft = true;
        grabbedTarget = null;
        var key = type;
        if (key != null)
        {
            int num;
            if (grab_anims == null)
            {
                grab_anims = new Dictionary<string, int>(8)
                {
                    { "ground_back_l", 0 }, { "ground_back_r", 1 }, { "ground_front_l", 2 }, { "ground_front_r", 3 },
                    { "head_back_l", 4 }, { "head_back_r", 5 }, { "head_front_l", 6 }, { "head_front_r", 7 }
                };
            }
            if (grab_anims.TryGetValue(key, out num))
            {
                switch (num)
                {
                    case 0:
                        attackCheckTimeA = 0.34f;
                        attackCheckTimeB = 0.49f;
                        break;
                    case 1:
                        attackCheckTimeA = 0.34f;
                        attackCheckTimeB = 0.49f;
                        isGrabHandLeft = false;
                        break;
                    case 2:
                        attackCheckTimeA = 0.37f;
                        attackCheckTimeB = 0.6f;
                        break;
                    case 3:
                        attackCheckTimeA = 0.37f;
                        attackCheckTimeB = 0.6f;
                        isGrabHandLeft = false;
                        break;
                    case 4:
                        attackCheckTimeA = 0.45f;
                        attackCheckTimeB = 0.5f;
                        isGrabHandLeft = false;
                        break;
                    case 5:
                        attackCheckTimeA = 0.45f;
                        attackCheckTimeB = 0.5f;
                        break;
                    case 6:
                        attackCheckTimeA = 0.38f;
                        attackCheckTimeB = 0.55f;
                        break;
                    case 7:
                        attackCheckTimeA = 0.38f;
                        attackCheckTimeB = 0.55f;
                        isGrabHandLeft = false;
                        break;
                }
            }
        }
        currentGrabHand = myTransform.Find(isGrabHandLeft
            ? "Amarture/Core/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/hand_L_001"
            : "Amarture/Core/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/hand_R_001");
    }

    [RPC]
    public void grabbedTargetEscape()
    {
        grabbedTarget = null;
    }

    [RPC]
    public void grabToLeft()
    {
        var transform = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/hand_L_001");
        var col = transform.GetComponent<SphereCollider>();
        grabTF.transform.parent = transform;
        grabTF.transform.position = col.transform.position;
        grabTF.transform.rotation = col.transform.rotation;
        var transform1 = grabTF.transform;
        transform1.localPosition -= Vector3.right * col.radius * 0.3f;
        var transform2 = grabTF.transform;
        transform2.localPosition -= Vector3.up * col.radius * 0.51f;
        var transform3 = grabTF.transform;
        transform3.localPosition -= Vector3.forward * col.radius * 0.3f;
        grabTF.transform.localRotation = Quaternion.Euler(grabTF.transform.localRotation.eulerAngles.x, grabTF.transform.localRotation.eulerAngles.y + 180f,
            grabTF.transform.localRotation.eulerAngles.z + 180f);
    }

    [RPC]
    public void grabToRight()
    {
        var transform = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/hand_R_001");
        var col = transform.GetComponent<SphereCollider>();
        grabTF.transform.parent = transform;
        grabTF.transform.position = col.transform.position;
        grabTF.transform.rotation = col.transform.rotation;
        var transform1 = grabTF.transform;
        transform1.localPosition -= Vector3.right * col.radius * 0.3f;
        var transform2 = grabTF.transform;
        transform2.localPosition += Vector3.up * col.radius * 0.51f;
        var transform3 = grabTF.transform;
        transform3.localPosition -= Vector3.forward * col.radius * 0.3f;
        grabTF.transform.localRotation = Quaternion.Euler(grabTF.transform.localRotation.eulerAngles.x, grabTF.transform.localRotation.eulerAngles.y + 180f,
            grabTF.transform.localRotation.eulerAngles.z);
    }

    public void headMovement()
    {
        if (!hasDie)
        {
            if (!IN_GAME_MAIN_CAMERA.IsSingleplayer)
            {
                if (photonView.isMine)
                {
                    targetHeadRotation = head.rotation;
                    var looking = false;
                    if (abnormalType != AbnormalType.TYPE_CRAWLER && state != TitanState.attack && state != TitanState.down && state != TitanState.hit && state != TitanState.recover
                        && state != TitanState.eat && state != TitanState.hit_eye && !hasDie && myDistance < 100f && myHero != null)
                    {
                        var vector = myHero.transform.position - myTransform.position;
                        angle = -Mathf.Atan2(vector.z, vector.x) * 57.29578f;
                        var num = -Mathf.DeltaAngle(angle, myTransform.rotation.eulerAngles.y - 90f);
                        num = Mathf.Clamp(num, -40f, 40f);
                        var y = neck.position.y + myLevel * 2f - myHero.transform.position.y;
                        var num3 = Mathf.Atan2(y, myDistance) * 57.29578f;
                        num3 = Mathf.Clamp(num3, -40f, 30f);
                        targetHeadRotation = Quaternion.Euler(head.rotation.eulerAngles.x + num3, head.rotation.eulerAngles.y + num, head.rotation.eulerAngles.z);
                        if (!asClientLookTarget)
                        {
                            asClientLookTarget = true;
                            photonView.RPC("setIfLookTarget", PhotonTargets.Others, true);
                        }
                        looking = true;
                    }
                    if (!looking && asClientLookTarget)
                    {
                        asClientLookTarget = false;
                        photonView.RPC("setIfLookTarget", PhotonTargets.Others, false);
                    }
                    if (state == TitanState.attack || state == TitanState.hit || state == TitanState.hit_eye)
                        oldHeadRotation = Quaternion.Lerp(oldHeadRotation, targetHeadRotation, Time.deltaTime * 20f);
                    else
                        oldHeadRotation = Quaternion.Lerp(oldHeadRotation, targetHeadRotation, Time.deltaTime * 10f);
                }
                else
                {
                    targetHeadRotation = head.rotation;
                    if (asClientLookTarget && myHero != null)
                    {
                        var vector2 = myHero.transform.position - myTransform.position;
                        angle = -Mathf.Atan2(vector2.z, vector2.x) * 57.29578f;
                        var num4 = -Mathf.DeltaAngle(angle, myTransform.rotation.eulerAngles.y - 90f);
                        num4 = Mathf.Clamp(num4, -40f, 40f);
                        var num5 = neck.position.y + myLevel * 2f - myHero.transform.position.y;
                        var num6 = Mathf.Atan2(num5, myDistance) * 57.29578f;
                        num6 = Mathf.Clamp(num6, -40f, 30f);
                        targetHeadRotation = Quaternion.Euler(head.rotation.eulerAngles.x + num6, head.rotation.eulerAngles.y + num4,
                            head.rotation.eulerAngles.z);
                    }
                    if (!hasDie)
                        oldHeadRotation = Quaternion.Lerp(oldHeadRotation, targetHeadRotation, Time.deltaTime * 10f);
                }
            }
            else
            {
                targetHeadRotation = head.rotation;
                if (abnormalType != AbnormalType.TYPE_CRAWLER && state != TitanState.attack && state != TitanState.down && state != TitanState.hit
                    && state != TitanState.recover && state != TitanState.hit_eye && !hasDie && myDistance < 100f && myHero != null)
                {
                    var vector3 = myHero.transform.position - myTransform.position;
                    angle = -Mathf.Atan2(vector3.z, vector3.x) * 57.29578f;
                    var num7 = -Mathf.DeltaAngle(angle, myTransform.rotation.eulerAngles.y - 90f);
                    num7 = Mathf.Clamp(num7, -40f, 40f);
                    var num8 = neck.position.y + myLevel * 2f - myHero.transform.position.y;
                    var num9 = Mathf.Atan2(num8, myDistance) * 57.29578f;
                    num9 = Mathf.Clamp(num9, -40f, 30f);
                    targetHeadRotation = Quaternion.Euler(head.rotation.eulerAngles.x + num9, head.rotation.eulerAngles.y + num7, head.rotation.eulerAngles.z);
                }
                if (state == TitanState.attack || state == TitanState.hit || state == TitanState.hit_eye)
                    oldHeadRotation = Quaternion.Lerp(oldHeadRotation, targetHeadRotation, Time.deltaTime * 20f);
                else
                    oldHeadRotation = Quaternion.Lerp(oldHeadRotation, targetHeadRotation, Time.deltaTime * 10f);
            }
            head.rotation = oldHeadRotation;
        }
        if (!animation.IsPlaying("die_headOff"))
            head.localScale = headscale;
    }

    private void hit(string animationName, Vector3 attacker, float hitPauseTime)
    {
        state = TitanState.hit;
        hitAnimation = animationName;
        hitPause = hitPauseTime;
        playAnimation(hitAnimation);
        animation[hitAnimation].time = 0f;
        animation[hitAnimation].speed = 0f;
        myTransform.rotation = Quaternion.Euler(0f, Quaternion.LookRotation(attacker - myTransform.position).eulerAngles.y, 0f);
        needFreshCorePosition = true;
        if (photonView.isMine && grabbedTarget != null)
            grabbedTarget.GetPhotonView().RPC("netUngrabbed", PhotonTargets.All);
    }

    public void hitAnkle()
    {
        if (hasDie || state == TitanState.down)
            return;
        if (grabbedTarget != null)
            grabbedTarget.GetPhotonView().RPC("netUngrabbed", PhotonTargets.All);
        getDown();
    }

    [RPC]
    public void hitAnkleRPC(int viewID)
    {
        if (hasDie || state == TitanState.down)
            return;
        var v = PhotonView.Find(viewID);
        if (v == null)
            return;
        var vector = v.gameObject.transform.position - myTransform.position;
        if (!(vector.magnitude < 20f))
            return;
        if (photonView.isMine && grabbedTarget != null)
            grabbedTarget.GetPhotonView().RPC("netUngrabbed", PhotonTargets.All);
        getDown();
    }

    public void hitEye()
    {
        if (!hasDie)
            justHitEye();
    }

    [RPC]
    public void hitEyeRPC(int viewID)
    {
        if (hasDie)
            return;
        var vector = PhotonView.Find(viewID).gameObject.transform.position - neck.position;
        if (!(vector.magnitude < 20f))
            return;
        if (photonView.isMine && grabbedTarget != null)
            grabbedTarget.GetPhotonView().RPC("netUngrabbed", PhotonTargets.All);
        if (!hasDie)
            justHitEye();
    }

    public void hitL(Vector3 attacker, float hitPauseTime)
    {
        if (abnormalType == AbnormalType.TYPE_CRAWLER)
            return;
        if (!IN_GAME_MAIN_CAMERA.IsMultiplayer)
            hit("hit_eren_L", attacker, hitPauseTime);
        else
            photonView.RPC("hitLRPC", PhotonTargets.All, attacker, hitPauseTime);
    }

    [RPC]
    private void hitLRPC(Vector3 attacker, float hitPauseTime)
    {
        if (!photonView.isMine)
            return;
        var vector = attacker - myTransform.position;
        if (vector.magnitude < 80f)
            hit("hit_eren_L", attacker, hitPauseTime);
    }

    public void hitR(Vector3 attacker, float hitPauseTime)
    {
        if (abnormalType == AbnormalType.TYPE_CRAWLER)
            return;
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            hit("hit_eren_R", attacker, hitPauseTime);
        else
            photonView.RPC("hitRRPC", PhotonTargets.All, attacker, hitPauseTime);
    }

    [RPC]
    private void hitRRPC(Vector3 attacker, float hitPauseTime)
    {
        if (!photonView.isMine || hasDie)
            return;
        var vector = attacker - myTransform.position;
        if (vector.magnitude < 80f)
            hit("hit_eren_R", attacker, hitPauseTime);
    }

    private void idle(float sbtime = 0f)
    {
        stuck = false;
        this.sbtime = sbtime;
        if (myDifficulty == 2 && (abnormalType == AbnormalType.TYPE_JUMPER || abnormalType == AbnormalType.TYPE_I))
            this.sbtime = Random.Range(0f, 1.5f);
        else if (myDifficulty >= 1)
            this.sbtime = 0f;
        this.sbtime = Mathf.Max(0.5f, this.sbtime);
        if (abnormalType == AbnormalType.TYPE_PUNK)
        {
            this.sbtime = 0.1f;
            if (myDifficulty == 1)
                this.sbtime += 0.4f;
        }
        state = TitanState.idle;
        crossFade(abnormalType == AbnormalType.TYPE_CRAWLER ? "crawler_idle" : "idle", 0.2f);
    }

    private bool IsGrounded()
    {
        LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
        LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyAABB");
        LayerMask mask3 = mask2 | mask;
        return Physics.Raycast(gameObject.transform.position + Vector3.up * 0.1f, -Vector3.up, 0.3f, mask3.value);
    }

    private void justEatHero(GameObject target, Transform hand)
    {
        if (target == null)
            return;
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine)
        {
            if (target.GetComponent<HERO>().HasDied())
                return;
            target.GetComponent<HERO>().markDie();
            if (nonAI)
                target.GetComponent<HERO>().photonView.RPC("netDie2", PhotonTargets.All, photonView.viewID, name);
            else
                target.GetComponent<HERO>().photonView.RPC("netDie2", PhotonTargets.All, -1, name);
        }
        else if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            target.GetComponent<HERO>().die2(hand);
    }

    private void justHitEye()
    {
        if (state == TitanState.hit_eye)
            return;
        if (state == TitanState.down || state == TitanState.sit)
            playAnimation("sit_hit_eye");
        else
            playAnimation("hit_eye");
        state = TitanState.hit_eye;
    }

    public void lateUpdate()
    {
        updateCollider();
        if (IN_GAME_MAIN_CAMERA.isPausing && IN_GAME_MAIN_CAMERA.IsSingleplayer)
            return;
        var foot_AudioSource = myTransform.Find("snd_titan_foot").GetComponent<AudioSource>();
        if (animation.IsPlaying("run_walk"))
        {
            if (animation["run_walk"].normalizedTime % 1f > 0.1f && animation["run_walk"].normalizedTime % 1f < 0.6f && stepSoundPhase == 2)
            {
                stepSoundPhase = 1;
                foot_AudioSource.Stop();
                foot_AudioSource.Play();
            }
            if (animation["run_walk"].normalizedTime % 1f > 0.6f && stepSoundPhase == 1)
            {
                stepSoundPhase = 2;
                foot_AudioSource.Stop();
                foot_AudioSource.Play();
            }
        }
        if (animation.IsPlaying("crawler_run"))
        {
            if (animation["crawler_run"].normalizedTime % 1f > 0.1f && animation["crawler_run"].normalizedTime % 1f < 0.56f && stepSoundPhase == 2)
            {
                stepSoundPhase = 1;
                foot_AudioSource.Stop();
                foot_AudioSource.Play();
            }
            if (animation["crawler_run"].normalizedTime % 1f > 0.56f && stepSoundPhase == 1)
            {
                stepSoundPhase = 2;
                foot_AudioSource.Stop();
                foot_AudioSource.Play();
            }
        }
        if (animation.IsPlaying("run_abnormal"))
        {
            if (animation["run_abnormal"].normalizedTime % 1f > 0.47f && animation["run_abnormal"].normalizedTime % 1f < 0.95f && stepSoundPhase == 2)
            {
                stepSoundPhase = 1;
                foot_AudioSource.Stop();
                foot_AudioSource.Play();
            }
            if ((animation["run_abnormal"].normalizedTime % 1f > 0.95f || animation["run_abnormal"].normalizedTime % 1f < 0.47f) && stepSoundPhase == 1)
            {
                stepSoundPhase = 2;
                foot_AudioSource.Stop();
                foot_AudioSource.Play();
            }
        }
        headMovement();
        grounded = false;
    }

    [RPC]
    private void laugh(float sbtime = 0f)
    {
        if (state != TitanState.idle && state != TitanState.turn && state != TitanState.chase)
            return;
        this.sbtime = sbtime;
        state = TitanState.laugh;
        crossFade("laugh", 0.2f);
    }

    private bool longRangeAttackCheck()
    {
        if (abnormalType != AbnormalType.TYPE_PUNK || myHero == null || myHero.rigidbody == null)
            return false;
        var line = myHero.rigidbody.velocity * Time.deltaTime * 30f;
        if (line.sqrMagnitude > 10f)
        {
            if (simpleHitTestLineAndBall(line, myTransform.Find("chkAeLeft").position - myHero.transform.position, 5f * myLevel))
            {
                attack("anti_AE_l");
                return true;
            }
            if (simpleHitTestLineAndBall(line, myTransform.Find("chkAeLLeft").position - myHero.transform.position, 5f * myLevel))
            {
                attack("anti_AE_low_l");
                return true;
            }
            if (simpleHitTestLineAndBall(line, myTransform.Find("chkAeRight").position - myHero.transform.position, 5f * myLevel))
            {
                attack("anti_AE_r");
                return true;
            }
            if (simpleHitTestLineAndBall(line, myTransform.Find("chkAeLRight").position - myHero.transform.position, 5f * myLevel))
            {
                attack("anti_AE_low_r");
                return true;
            }
        }
        var vector2 = myHero.transform.position - myTransform.position;
        var current = -Mathf.Atan2(vector2.z, vector2.x) * 57.29578f;
        var f = -Mathf.DeltaAngle(current, gameObject.transform.rotation.eulerAngles.y - 90f);
        if (rockInterval > 0f)
            rockInterval -= Time.deltaTime;
        else if (Mathf.Abs(f) < 5f)
        {
            var vector3 = myHero.transform.position + line;
            var vector5 = vector3 - myTransform.position;
            var sqrMagnitude = vector5.sqrMagnitude;
            if (!(sqrMagnitude > 8000f) || !(sqrMagnitude < 90000f))
                return false;
            attack("throw");
            rockInterval = 2f;
            return true;
        }
        return false;
    }

    [RPC]
    private void netCrossFade(string aniName, float time)
    {
        animation.CrossFade(aniName, time);
    }

    [RPC]
    private void netDie()
    {
        asClientLookTarget = false;
        if (hasDie)
            return;
        hasDie = true;
        if (nonAI)
        {
            var c = currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>();
            c.setMainObject(null);
            c.setSpectorMode(true);
            c.gameOver = true;
            var propertiesToSet = new Hashtable
            {
                { PhotonPlayerProperty.dead, true }, { PhotonPlayerProperty.deaths, (int)PhotonNetwork.player.customProperties[PhotonPlayerProperty.deaths] + 1 }
            };
            PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        }
        dieAnimation();
    }

    [RPC]
    private void netPlayAnimation(string aniName)
    {
        animation.Play(aniName);
    }

    [RPC]
    private void netPlayAnimationAt(string aniName, float normalizedTime)
    {
        animation.Play(aniName);
        animation[aniName].normalizedTime = normalizedTime;
    }

    [RPC]
    private void netSetAbnormalType(int type, PhotonMessageInfo info)
    {
        switch (type)
        {
            case 0:
                abnormalType = AbnormalType.NORMAL;
                name = "Titan";
                runAnimation = "run_walk";
                GetComponent<TITAN_SETUP>().setHair();
                break;
            case 1:
                abnormalType = AbnormalType.TYPE_I;
                name = "Aberrant";
                runAnimation = "run_abnormal";
                GetComponent<TITAN_SETUP>().setHair();
                break;
            case 2:
                abnormalType = AbnormalType.TYPE_JUMPER;
                name = "Jumper";
                runAnimation = "run_abnormal";
                GetComponent<TITAN_SETUP>().setHair();
                break;
            case 3:
                abnormalType = AbnormalType.TYPE_CRAWLER;
                name = "Crawler";
                runAnimation = "crawler_run";
                GetComponent<TITAN_SETUP>().setHair();
                break;
            case 4:
                abnormalType = AbnormalType.TYPE_PUNK;
                name = "Punk";
                runAnimation = "run_abnormal_1";
                GetComponent<TITAN_SETUP>().setPunkHair();
                break;
        }
        if (abnormalType == AbnormalType.TYPE_I || abnormalType == AbnormalType.TYPE_JUMPER || abnormalType == AbnormalType.TYPE_PUNK)
        {
            speed = 18f;
            if (myLevel > 1f)
                speed *= Mathf.Sqrt(myLevel);
            if (myDifficulty == 1)
                speed *= 1.4f;
            if (myDifficulty == 2)
                speed *= 1.6f;
            animation["turnaround1"].speed = 2f;
            animation["turnaround2"].speed = 2f;
        }
        if (abnormalType == AbnormalType.TYPE_CRAWLER)
        {
            chaseDistance += 50f;
            speed = 25f;
            if (myLevel > 1f)
                speed *= Mathf.Sqrt(myLevel);
            if (myDifficulty == 1)
                speed *= 2f;
            if (myDifficulty == 2)
                speed *= 2.2f;
            var col = myTransform.Find("AABB").gameObject.GetComponent<CapsuleCollider>();
            col.height = 10f;
            col.radius = 5f;
            col.center = new Vector3(0f, 5.05f, 0f);
        }
        if (nonAI)
        {
            speed = Mathf.Min(abnormalType == AbnormalType.TYPE_CRAWLER ? 70f : 60f, speed);
            animation["attack_jumper_0"].speed = 7f;
            animation["attack_crawler_jump_0"].speed = 4f;
        }
        animation["attack_combo_1"].speed = animation["attack_combo_2"].speed = animation["attack_combo_3"].speed =
            animation["attack_quick_turn_l"].speed = animation["attack_quick_turn_r"].speed = 1f;
        animation["attack_anti_AE_l"].speed = animation["attack_anti_AE_low_l"].speed = animation["attack_anti_AE_r"].speed
            = animation["attack_anti_AE_low_r"].speed = 1.1f;
        idle();
    }

    [RPC]
    private void netSetLevel(float level, int AI, int skinColor)
    {
        setLevel(level, AI, skinColor);
    }

    private void OnCollisionStay()
    {
        grounded = true;
    }

    private void OnDestroy()
    {
        if (CacheGameObject.Find("MultiplayerManager") != null)
            FengGameManagerMKII.MKII.removeTitan(this);
    }

    private void playAnimation(string aniName)
    {
        animation.Play(aniName);
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine)
            photonView.RPC("netPlayAnimation", PhotonTargets.Others, aniName);
    }

    private void playAnimationAt(string aniName, float normalizedTime)
    {
        animation.Play(aniName);
        animation[aniName].normalizedTime = normalizedTime;
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine)
            photonView.RPC("netPlayAnimationAt", PhotonTargets.Others, aniName, normalizedTime);
    }

    private void playSound(string sndname)
    {
        playsoundRPC(sndname);
        if (photonView.isMine)
            photonView.RPC("playsoundRPC", PhotonTargets.Others, sndname);
    }

    [RPC]
    private void playsoundRPC(string sndname)
    {
        myTransform.Find(sndname).GetComponent<AudioSource>().Play();
    }

    public void randomRun(Vector3 targetPt, float r)
    {
        state = TitanState.random_run;
        targetCheckPt = targetPt;
        targetR = r;
        random_run_time = Random.Range(1f, 2f);
        crossFade(runAnimation, 0.5f);
    }

    private void recover()
    {
        state = TitanState.recover;
        playAnimation("idle_recovery");
        getdownTime = Random.Range(2f, 5f);
    }

    private void remainSitdown()
    {
        state = TitanState.sit;
        playAnimation("sit_idle");
        getdownTime = Random.Range(10f, 30f);
    }

    public void setAbnormalType(AbnormalType type, bool forceCrawler = false)
    {
        var finalType = 0; //0 - Normal, 1 - Aberrant, 2 - Jumper, 3 - Crawler, 4 - Punk
        var probability = 0.02f * (IN_GAME_MAIN_CAMERA.difficulty + 1);
        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_AHSS)
            probability = 100f;
        switch (type)
        {
            case AbnormalType.NORMAL:
                finalType = Random.Range(0f, 1f) < probability ? 4 : 0;
                break;
            case AbnormalType.TYPE_I:
                finalType = Random.Range(0f, 1f) < probability ? 4 : 1;
                break;
            case AbnormalType.TYPE_JUMPER:
                finalType = Random.Range(0f, 1f) < probability ? 4 : 2;
                break;
            case AbnormalType.TYPE_CRAWLER:
                //finalType = CacheGameObject.Find("Crawler") != null && Random.Range(0, 1000) > 5 ? 2 : 3;
                finalType = Random.Range(0f, 1f) < probability ? 4 : 3;
                break;
            case AbnormalType.TYPE_PUNK:
                finalType = 4;
                break;
        }
        if (forceCrawler)
            finalType = 3;
        
        if (finalType == 4)
        {
            if (!LevelInfo.getInfo(FengGameManagerMKII.level).punk)
                finalType = 1;
            else if (IN_GAME_MAIN_CAMERA.IsSingleplayer && getPunkNumber() >= 3)
                finalType = 1;
            if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
            {
                var wave = FengGameManagerMKII.MKII.wave;
                if (wave != 5 && wave != 10 && wave != 15 && wave != 20)
                    finalType = 1;
            }
        }
        if (!IN_GAME_MAIN_CAMERA.IsSingleplayer && photonView.isMine)
            photonView.RPC("netSetAbnormalType", PhotonTargets.AllBuffered, finalType);
        else if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            netSetAbnormalType(finalType, null);
    }

    [RPC]
    private void setIfLookTarget(bool bo)
    {
        asClientLookTarget = bo;
    }

    private void setLevel(float level, int AI, int skinColor)
    {
        myLevel = Mathf.Clamp(level, 0.1f, 3f);

        attackWait += Random.Range(0f, 2f);
        chaseDistance += myLevel * 10f;

        myTransform.localScale = new Vector3(myLevel, myLevel, myLevel);
        head = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head");
        var x = Mathf.Min(Mathf.Pow(2f / myLevel, 0.35f), 1.25f);
        head.localScale = new Vector3(x, x, x);

        if (skinColor != 0)
            mainMaterial.GetComponent<SkinnedMeshRenderer>().material.color = skinColor != 1 ? (skinColor != 2 ? FengColor.titanSkin3 : FengColor.titanSkin2) : FengColor.titanSkin1;
        var currentSpeed = 1.4f - (myLevel - 0.7f) * 0.15f;
        currentSpeed = Mathf.Clamp(currentSpeed, 0.9f, 1.5f);

        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                current.speed = currentSpeed;
            }
        } finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
        rigidbody.mass *= myLevel;
        rigidbody.rotation = Quaternion.Euler(0f, Random.Range(0, 360), 0f);
        if (myLevel > 1f)
            speed *= Mathf.Sqrt(myLevel);
        myDifficulty = AI;
        if (myDifficulty == 1 || myDifficulty == 2)
        {
            var enumerator2 = animation.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                {
                    var state2 = (AnimationState)enumerator2.Current;
                    state2.speed = currentSpeed * 1.05f;
                }
            } finally
            {
                var disposable2 = enumerator2 as IDisposable;
                if (disposable2 != null)
                    disposable2.Dispose();
            }
            speed *= nonAI ? 1.1f : 1.4f;
            chaseDistance *= 1.15f;
        }
        if (myDifficulty == 2)
        {
            var enumerator3 = animation.GetEnumerator();
            try
            {
                while (enumerator3.MoveNext())
                {
                    var state3 = (AnimationState)enumerator3.Current;
                    state3.speed = currentSpeed * 1.05f;
                }
            } finally
            {
                var disposable3 = enumerator3 as IDisposable;
                if (disposable3 != null)
                    disposable3.Dispose();
            }
            speed *= nonAI ? 1.1f : 1.5f;
            chaseDistance *= 1.3f;
        }
        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.ENDLESS_TITAN || IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
            chaseDistance = 999999f;
        if (nonAI)
            speed = Mathf.Min(abnormalType == AbnormalType.TYPE_CRAWLER ? 70f : 60f, speed);
        attackDistance = Vector3.Distance(myTransform.position, myTransform.Find("ap_front_ground").position) * 1.65f;
    }

    public void setmyLevel(float level = 0f)
    {
        if (level != 0f)
            myLevel = level;
        animation.cullingType = AnimationCullingType.BasedOnRenderers;
        if (!IN_GAME_MAIN_CAMERA.IsSingleplayer && photonView.isMine)
        {
            photonView.RPC("netSetLevel", PhotonTargets.AllBuffered, myLevel, FengGameManagerMKII.MKII.difficulty, Random.Range(0, 4));
            animation.cullingType = AnimationCullingType.AlwaysAnimate;
        }
        else if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            setLevel(myLevel, IN_GAME_MAIN_CAMERA.difficulty, Random.Range(0, 4));
    }

    [RPC]
    private void setMyTarget(int ID)
    {
        if (ID == -1)
            myHero = null;
        var v = PhotonView.Find(ID);
        if (v != null)
            myHero = v.gameObject;
    }

    public void setRoute(GameObject route)
    {
        checkPoints = new ArrayList();
        for (var i = 1; i <= 10; i++)
            checkPoints.Add(route.transform.Find("r" + i).position);
        checkPoints.Add("end");
    }

    private bool simpleHitTestLineAndBall(Vector3 line, Vector3 ball, float R)
    {
        var rhs = Vector3.Project(ball, line);
        var vector2 = ball - rhs;
        if (vector2.magnitude > R)
            return false;
        if (Vector3.Dot(line, rhs) < 0f)
            return false;
        if (rhs.sqrMagnitude > line.sqrMagnitude)
            return false;
        return true;
    }

    private void sitdown()
    {
        state = TitanState.sit;
        playAnimation("sit_down");
        getdownTime = Random.Range(10f, 30f);
    }

    private void Start()
    {
        FengGameManagerMKII.MKII.addTitan(this, photonView.ownerId);
        if (photonView.isMine)
            GlobalItems.myTitan = this;
        currentCamera = CacheGameObject.Find("MainCamera");

        runAnimation = "run_walk";
        grabTF = new GameObject { name = "titansTmpGrabTF" };
        head = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head");
        neck = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
        oldHeadRotation = head.rotation;
        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.MULTIPLAYER || photonView.isMine)
        {
            myLevel = Random.Range(0.5f, 3.5f);
            spawnPt = myTransform.position;
            setmyLevel();
            setAbnormalType(abnormalType);
            if (myHero == null)
                findNearestHero();
            controller = gameObject.GetComponent<TITAN_CONTROLLER>();
        }

        baseColliders = new List<Collider>();
        foreach (var childCollider in GetComponentsInChildren<Collider>())
        {
            if (childCollider.name != "AABB")
                baseColliders.Add(childCollider);
        }
        var obj = new GameObject { name = "PlayerDetectorRC" };
        var triggerCollider = obj.AddComponent<CapsuleCollider>();
        var referenceCollider = myTransform.Find("AABB").GetComponent<CapsuleCollider>();
        triggerCollider.center = referenceCollider.center;
        triggerCollider.radius = Math.Abs(myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head").position.y - myTransform.position.y);
        triggerCollider.height = referenceCollider.height * 1.2f;
        triggerCollider.material = referenceCollider.material;
        triggerCollider.isTrigger = true;
        triggerCollider.name = "PlayerDetectorRC";
        myTitanTrigger = obj.AddComponent<TitanTrigger>();
        myTitanTrigger.isCollide = false;
        obj.layer = 16;
        obj.transform.parent = myTransform.Find("AABB");
        obj.transform.localPosition = new Vector3(0f, 0f, 0f);
        colliderEnabled = true;
        isHooked = false;
        isLook = false;
    }

    public void suicide()
    {
        netDie();
        if (nonAI)
            FengGameManagerMKII.MKII.sendKillInfo(false, string.Empty, true, (string)PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]);
        FengGameManagerMKII.MKII.needChooseSide = true;
        FengGameManagerMKII.MKII.justSuicide = true;
    }

    [RPC]
    public void titanGetHit(int viewID, int speed)
    {
        var view = PhotonView.Find(viewID);
        if (view == null)
            return;
        var vector = view.gameObject.transform.position - neck.position;
        if (!(vector.magnitude < 30f) || hasDie)
            return;
        photonView.RPC("netDie", PhotonTargets.OthersBuffered);
        if (grabbedTarget != null)
            grabbedTarget.GetPhotonView().RPC("netUngrabbed", PhotonTargets.All);
        netDie();
        if (nonAI)
            FengGameManagerMKII.MKII.titanGetKill(view.owner, speed, (string)PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]);
        else
            FengGameManagerMKII.MKII.titanGetKill(view.owner, speed, name);
    }

    public void toCheckPoint(Vector3 targetPt, float r)
    {
        state = TitanState.to_check_point;
        targetCheckPt = targetPt;
        targetR = r;
        crossFade(runAnimation, 0.5f);
    }

    private void toPVPCheckPoint(Vector3 targetPt, float r)
    {
        state = TitanState.to_pvp_pt;
        targetCheckPt = targetPt;
        targetR = r;
        crossFade(runAnimation, 0.5f);
    }

    private void turn(float d)
    {
        if (abnormalType == AbnormalType.TYPE_CRAWLER)
            turnAnimation = d > 0f ? "crawler_turnaround_R" : "crawler_turnaround_L";
        else
            turnAnimation = d > 0f ? "turnaround2" : "turnaround1";
        playAnimation(turnAnimation);
        animation[turnAnimation].time = 0f;
        d = Mathf.Clamp(d, -120f, 120f);
        turnDeg = d;
        desDeg = gameObject.transform.rotation.eulerAngles.y + turnDeg;
        state = TitanState.turn;
    }

    private void updateCollider()
    {
        if (colliderEnabled)
        {
            if (isHooked || myTitanTrigger.isCollide || isLook)
                return;
            foreach (var col in baseColliders)
                col.enabled = false;
            colliderEnabled = false;
        }
        else if (isHooked || myTitanTrigger.isCollide || isLook)
        {
            foreach (var col in baseColliders)
                col.enabled = true;
            colliderEnabled = true;
        }
    }

    public void update()
    {
        if (IN_GAME_MAIN_CAMERA.isPausing && IN_GAME_MAIN_CAMERA.IsSingleplayer || myDifficulty < 0
            || !IN_GAME_MAIN_CAMERA.IsSingleplayer && !photonView.isMine)
            return;
        if (!nonAI)
        {
            if (activeRad < int.MaxValue && (state == TitanState.idle || state == TitanState.wander || state == TitanState.chase))
            {
                if (checkPoints.Count > 1)
                {
                    if (Vector3.Distance((Vector3)checkPoints[0], myTransform.position) > activeRad)
                        toCheckPoint((Vector3)checkPoints[0], 10f);
                }
                else if (Vector3.Distance(spawnPt, myTransform.position) > activeRad)
                    toCheckPoint(spawnPt, 10f);
            }
            if (whoHasTauntMe != null)
            {
                tauntTime -= Time.deltaTime;
                if (tauntTime <= 0f)
                    whoHasTauntMe = null;
                myHero = whoHasTauntMe;
                if (IN_GAME_MAIN_CAMERA.IsMultiplayer && PhotonNetwork.isMasterClient)
                    photonView.RPC("setMyTarget", PhotonTargets.Others, myHero.GetPhotonView().viewID);
            }
        }
        if (hasDie)
        {
            dieTime += Time.deltaTime;
            if (dieTime > 2f && !hasDieSteam)
            {
                hasDieSteam = true;
                if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                {
                    var obj2 = (GameObject)Instantiate(CacheResources.Load("FX/FXtitanDie1"));
                    obj2.transform.position = myTransform.Find("Amarture/Core/Controller_Body/hip").position;
                    obj2.transform.localScale = myTransform.localScale;
                }
                else if (photonView.isMine)
                    PhotonNetwork.Instantiate("FX/FXtitanDie1", myTransform.Find("Amarture/Core/Controller_Body/hip").position, Quaternion.Euler(-90f, 0f, 0f), 0)
                        .transform.localScale = myTransform.localScale;
            }
            if (!(dieTime > 5f))
                return;
            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            {
                var obj4 = (GameObject)Instantiate(CacheResources.Load("FX/FXtitanDie"));
                obj4.transform.position = myTransform.Find("Amarture/Core/Controller_Body/hip").position;
                obj4.transform.localScale = myTransform.localScale;
                Destroy(gameObject);
            }
            else if (photonView.isMine)
            {
                PhotonNetwork.Instantiate("FX/FXtitanDie", myTransform.Find("Amarture/Core/Controller_Body/hip").position, Quaternion.Euler(-90f, 0f, 0f), 0)
                    .transform.localScale = myTransform.localScale;
                PhotonNetwork.Destroy(gameObject);
                myDifficulty = -1;
            }
            return;
        }
        if (state == TitanState.hit)
        {
            if (hitPause > 0f)
            {
                hitPause -= Time.deltaTime;
                if (hitPause <= 0f)
                {
                    animation[hitAnimation].speed = 1f;
                    hitPause = 0f;
                }
            }
            if (animation[hitAnimation].normalizedTime >= 1f)
                idle();
        }
        if (!nonAI)
        {
            if (myHero == null)
                findNearestHero();
            if ((state == TitanState.idle || state == TitanState.chase || state == TitanState.wander) && whoHasTauntMe == null && Random.Range(0, 100) < 10)
                findNearestFacingHero();
            myDistance = myHero == null
                ? float.MaxValue
                : Mathf.Sqrt((myHero.transform.position.x - myTransform.position.x) * (myHero.transform.position.x - myTransform.position.x)
                    + (myHero.transform.position.z - myTransform.position.z) * (myHero.transform.position.z - myTransform.position.z));
        }
        else
        {
            if (stamina < maxStamina)
            {
                if (animation.IsPlaying("idle"))
                    stamina += Time.deltaTime * 30f;
                if (animation.IsPlaying("crawler_idle"))
                    stamina += Time.deltaTime * 35f;
                if (animation.IsPlaying("run_walk"))
                    stamina += Time.deltaTime * 10f;
            }
            if (animation.IsPlaying("run_abnormal_1"))
                stamina -= Time.deltaTime * 5f;
            if (animation.IsPlaying("crawler_run"))
                stamina -= Time.deltaTime * 15f;
            if (stamina < 0f)
                stamina = 0f;
            if (!IN_GAME_MAIN_CAMERA.isPausing)
                GameObject.Find("stamina_titan").transform.localScale = new Vector3(stamina, 16f);
        }
        switch (state)
        {
            case TitanState.laugh:
                if (animation["laugh"].normalizedTime >= 1f)
                    idle(2f);
                break;
            case TitanState.idle:
                if (nonAI)
                {
                    if (IN_GAME_MAIN_CAMERA.isPausing)
                        return;
                    if (abnormalType != AbnormalType.TYPE_CRAWLER)
                    {
                        if (controller.isAttackDown && stamina > 25f)
                        {
                            stamina -= 25f;
                            attack("combo_1");
                        }
                        else if (controller.isAttackIIDown && stamina > 50f)
                        {
                            stamina -= 50f;
                            attack("abnormal_jump");
                        }
                        else if (controller.isJumpDown && stamina > 15f)
                        {
                            stamina -= 15f;
                            attack("jumper_0");
                        }
                    }
                    else if (controller.isAttackDown && stamina > 40f)
                    {
                        stamina -= 40f;
                        attack("crawler_jump_0");
                    }
                    if (controller.isSuicide)
                        suicide();
                }
                else if (sbtime > 0f)
                    sbtime -= Time.deltaTime;
                else
                {
                    if (!isAlarm)
                    {
                        if (abnormalType != AbnormalType.TYPE_PUNK && abnormalType != AbnormalType.TYPE_CRAWLER && Random.Range(0f, 1f) < 0.005f)
                        {
                            sitdown();
                            return;
                        }
                        if (Random.Range(0f, 1f) < 0.02f)
                        {
                            wander();
                            return;
                        }
                        if (Random.Range(0f, 1f) < 0.01f)
                        {
                            turn(Random.Range(30, 120));
                            return;
                        }
                        if (Random.Range(0f, 1f) < 0.01f)
                        {
                            turn(Random.Range(-30, -120));
                            return;
                        }
                    }
                    angle = 0f;
                    between2 = 0f;
                    if (myDistance < chaseDistance || whoHasTauntMe != null)
                    {
                        var vector9 = myHero.transform.position - myTransform.position;
                        angle = -Mathf.Atan2(vector9.z, vector9.x) * 57.29578f;
                        between2 = -Mathf.DeltaAngle(angle, gameObject.transform.rotation.eulerAngles.y - 90f);
                        if (myDistance >= attackDistance)
                        {
                            if (isAlarm || Mathf.Abs(between2) < 90f)
                            {
                                chase();
                                return;
                            }
                            if (!isAlarm && myDistance < chaseDistance * 0.1f)
                            {
                                chase();
                                return;
                            }
                        }
                    }
                    if (!longRangeAttackCheck())
                    {
                        if (myDistance < chaseDistance)
                        {
                            if (abnormalType == AbnormalType.TYPE_JUMPER
                                && (myDistance > attackDistance || myHero.transform.position.y > head.position.y + 4f * myLevel) && Mathf.Abs(between2) < 120f && Vector3.Distance(myTransform.position, myHero.transform.position) < 1.5f * myHero.transform.position.y)
                            {
                                attack("jumper_0");
                                return;
                            }
                            if (abnormalType == AbnormalType.TYPE_CRAWLER && myDistance < attackDistance * 3f && Mathf.Abs(between2) < 90f && myHero.transform.position.y < neck.position.y + 30f * myLevel
                                && myHero.transform.position.y > neck.position.y + 10f * myLevel)
                            {
                                attack("crawler_jump_0");
                                return;
                            }
                        }
                        if (abnormalType == AbnormalType.TYPE_PUNK && myDistance < 90f && Mathf.Abs(between2) > 90f)
                        {
                            if (Random.Range(0f, 1f) < 0.4f)
                                randomRun(myTransform.position + new Vector3(Random.Range(-50f, 50f), Random.Range(-50f, 50f), Random.Range(-50f, 50f)), 10f);
                            if (Random.Range(0f, 1f) < 0.2f)
                                recover();
                            else if (Random.Range(0, 2) == 0)
                                attack("quick_turn_l");
                            else
                                attack("quick_turn_r");
                        }
                        else
                        {
                            if (myDistance < attackDistance)
                            {
                                if (abnormalType == AbnormalType.TYPE_CRAWLER)
                                {
                                    if (myHero.transform.position.y + 3f <= neck.position.y + 20f * myLevel && Random.Range(0f, 1f) < 0.1f)
                                        chase();
                                    return;
                                }
                                var decidedAction = string.Empty;
                                var attackStrategy = GetAttackStrategy();
                                if (attackStrategy != null)
                                    decidedAction = attackStrategy[Random.Range(0, attackStrategy.Length)];
                                if ((abnormalType == AbnormalType.TYPE_JUMPER || abnormalType == AbnormalType.TYPE_I) && Mathf.Abs(between2) > 40f)
                                {
                                    if (decidedAction.Contains("grab") || decidedAction.Contains("kick") || decidedAction.Contains("slap") || decidedAction.Contains("bite"))
                                    {
                                        if (Random.Range(0, 100) < 30)
                                        {
                                            turn(between2);
                                            return;
                                        }
                                    }
                                    else if (Random.Range(0, 100) < 90)
                                    {
                                        turn(between2);
                                        return;
                                    }
                                }
                                if (executeAttack(decidedAction))
                                    return;
                                if (abnormalType == AbnormalType.NORMAL)
                                {
                                    if (Random.Range(0, 100) < 30 && Mathf.Abs(between2) > 45f)
                                    {
                                        turn(between2);
                                        return;
                                    }
                                }
                                else if (Mathf.Abs(between2) > 45f)
                                {
                                    turn(between2);
                                    return;
                                }
                            }
                            if (PVPfromCheckPt == null)
                                return;
                            if (PVPfromCheckPt.state == CheckPointState.Titan)
                            {
                                GameObject chkPtNext;
                                if (Random.Range(0, 100) > 0x30)
                                {
                                    chkPtNext = PVPfromCheckPt.chkPtNext;
                                    if (chkPtNext != null && (chkPtNext.GetComponent<PVPcheckPoint>().state != CheckPointState.Titan || Random.Range(0, 100) < 20))
                                    {
                                        toPVPCheckPoint(chkPtNext.transform.position, 5 + Random.Range(0, 10));
                                        PVPfromCheckPt = chkPtNext.GetComponent<PVPcheckPoint>();
                                    }
                                }
                                else
                                {
                                    chkPtNext = PVPfromCheckPt.chkPtPrevious;
                                    if (chkPtNext != null && (chkPtNext.GetComponent<PVPcheckPoint>().state != CheckPointState.Titan || Random.Range(0, 100) < 5))
                                    {
                                        toPVPCheckPoint(chkPtNext.transform.position, 5 + Random.Range(0, 10));
                                        PVPfromCheckPt = chkPtNext.GetComponent<PVPcheckPoint>();
                                    }
                                }
                            }
                            else
                                toPVPCheckPoint(PVPfromCheckPt.transform.position, 5 + Random.Range(0, 10));
                        }
                    }
                }
                break;
            case TitanState.attack:
                if (attackAnimation == "combo")
                {
                    if (nonAI)
                    {
                        if (controller.isAttackDown)
                            nonAIcombo = true;
                        if (!nonAIcombo && animation["attack_" + attackAnimation].normalizedTime >= 0.385f)
                        {
                            idle();
                            return;
                        }
                    }
                    if (animation["attack_" + attackAnimation].normalizedTime >= 0.11f && animation["attack_" + attackAnimation].normalizedTime <= 0.16f)
                    {
                        var hero = checkIfHitHand(myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/hand_R_001"));
                        if (hero != null)
                        {
                            var position = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest").position;
                            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                                hero.GetComponent<HERO>().die((hero.transform.position - position) * 15f * myLevel, false);
                            else if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine && !hero.GetComponent<HERO>().HasDied())
                            {
                                hero.GetComponent<HERO>().markDie();
                                object[] args = { (hero.transform.position - position) * 15f * myLevel, false, !nonAI ? -1 : photonView.viewID, name, true };
                                hero.GetComponent<HERO>().photonView.RPC("netDie", PhotonTargets.All, args);
                            }
                        }
                    }
                    if (animation["attack_" + attackAnimation].normalizedTime >= 0.27f && animation["attack_" + attackAnimation].normalizedTime <= 0.32f)
                    {
                        var hero = checkIfHitHand(myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/hand_L_001"));
                        if (hero != null)
                        {
                            var vector21 = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest").position;
                            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                                hero.GetComponent<HERO>().die((hero.transform.position - vector21) * 15f * myLevel, false);
                            else if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine && !hero.GetComponent<HERO>().HasDied())
                            {
                                hero.GetComponent<HERO>().markDie();
                                object[] args = { (hero.transform.position - vector21) * 15f * myLevel, false, !nonAI ? -1 : photonView.viewID, name, true };
                                hero.GetComponent<HERO>().photonView.RPC("netDie", PhotonTargets.All, args);
                            }
                        }
                    }
                }
                if (attackCheckTimeA != 0f && animation["attack_" + attackAnimation].normalizedTime >= attackCheckTimeA
                    && animation["attack_" + attackAnimation].normalizedTime <= attackCheckTimeB)
                {
                    if (leftHandAttack)
                    {
                        var hero = checkIfHitHand(myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/hand_L_001"));
                        if (hero != null)
                        {
                            var vector22 = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest").position;
                            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                                hero.GetComponent<HERO>().die((hero.transform.position - vector22) * 15f * myLevel, false);
                            else if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine && !hero.GetComponent<HERO>().HasDied())
                            {
                                hero.GetComponent<HERO>().markDie();
                                object[] args = { (hero.transform.position - vector22) * 15f * myLevel, false, !nonAI ? -1 : photonView.viewID, name, true };
                                hero.GetComponent<HERO>().photonView.RPC("netDie", PhotonTargets.All, args);
                            }
                        }
                    }
                    else
                    {
                        var hero = checkIfHitHand(myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/hand_R_001"));
                        if (hero != null)
                        {
                            var vector23 = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest").position;
                            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                                hero.GetComponent<HERO>().die((hero.transform.position - vector23) * 15f * myLevel, false);
                            else if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine && !hero.GetComponent<HERO>().HasDied())
                            {
                                hero.GetComponent<HERO>().markDie();
                                object[] args = { (hero.transform.position - vector23) * 15f * myLevel, false, !nonAI ? -1 : photonView.viewID, name, true };
                                hero.GetComponent<HERO>().photonView.RPC("netDie", PhotonTargets.All, args);
                            }
                        }
                    }
                }
                if (!attacked && attackCheckTime != 0f && animation["attack_" + attackAnimation].normalizedTime >= attackCheckTime)
                {
                    attacked = true;
                    fxPosition = myTransform.Find("ap_" + attackAnimation).position;
                    var obj11 = IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine
                        ? PhotonNetwork.Instantiate("FX/" + fxName, fxPosition, fxRotation, 0)
                        : (GameObject)Instantiate(CacheResources.Load("FX/" + fxName), fxPosition, fxRotation);
                    if (nonAI)
                    {
                        obj11.transform.localScale = myTransform.localScale * 1.5f;
                        if (obj11.GetComponent<EnemyfxIDcontainer>() != null)
                            obj11.GetComponent<EnemyfxIDcontainer>().myOwnerViewID = photonView.viewID;
                    }
                    else
                        obj11.transform.localScale = myTransform.localScale;
                    if (obj11.GetComponent<EnemyfxIDcontainer>() != null)
                        obj11.GetComponent<EnemyfxIDcontainer>().titanName = name;
                    var b = 1f - Vector3.Distance(currentCamera.transform.position, obj11.transform.position) * 0.05f;
                    b = Mathf.Min(1f, b);
                    currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().startShake(b, b);
                }
                if (attackAnimation == "throw")
                {
                    if (!attacked && animation["attack_" + attackAnimation].normalizedTime >= 0.11f)
                    {
                        attacked = true;
                        var transform1 = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/hand_R_001");
                        throwRock = IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine
                            ? PhotonNetwork.Instantiate("FX/rockThrow", transform1.position, transform1.rotation, 0)
                            : (GameObject)Instantiate(CacheResources.Load("FX/rockThrow"), transform1.position, transform1.rotation);
                        throwRock.transform.localScale = myTransform.localScale;
                        throwRock.transform.position -= throwRock.transform.forward * 2.5f * myLevel;
                        if (throwRock.GetComponent<EnemyfxIDcontainer>() != null)
                        {
                            if (nonAI)
                                throwRock.GetComponent<EnemyfxIDcontainer>().myOwnerViewID = photonView.viewID;
                            throwRock.GetComponent<EnemyfxIDcontainer>().titanName = name;
                        }
                        throwRock.transform.parent = transform1;
                        if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine)
                        {
                            object[] args = { photonView.viewID, transform.localScale, throwRock.transform.localPosition, myLevel };
                            throwRock.GetPhotonView().RPC("initRPC", PhotonTargets.Others, args);
                        }
                    }
                    if (animation["attack_" + attackAnimation].normalizedTime >= 0.11f)
                    {
                        var y = Mathf.Atan2(myHero.transform.position.x - myTransform.position.x, myHero.transform.position.z - myTransform.position.z) * 57.29578f;
                        gameObject.transform.rotation = Quaternion.Euler(0f, y, 0f);
                    }
                    if (throwRock != null && animation["attack_" + attackAnimation].normalizedTime >= 0.62f)
                    {
                        Vector3 vector28;
                        const float num3 = 1f;
                        const float num4 = -20f;
                        if (myHero != null)
                        {
                            vector28 = (myHero.transform.position - throwRock.transform.position) / num3 + myHero.rigidbody.velocity;
                            var num5 = myHero.transform.position.y + 2f * myLevel;
                            var num6 = num5 - throwRock.transform.position.y;
                            vector28 = new Vector3(vector28.x, num6 / num3 - 0.5f * num4 * num3, vector28.z);
                        }
                        else
                            vector28 = myTransform.forward * 60f + Vector3.up * 10f;
                        throwRock.GetComponent<RockThrow>().launch(vector28);
                        throwRock.transform.parent = null;
                        throwRock = null;
                    }
                }
                if (attackAnimation == "jumper_0" || attackAnimation == "crawler_jump_0")
                {
                    if (!attacked)
                    {
                        if (animation["attack_" + attackAnimation].normalizedTime >= 0.68f)
                        {
                            attacked = true;
                            if (myHero == null || nonAI)
                            {
                                var num7 = 120f;
                                var vector31 = myTransform.forward * speed + Vector3.up * num7;
                                if (nonAI && abnormalType == AbnormalType.TYPE_CRAWLER)
                                {
                                    num7 = 100f;
                                    var a = speed * 2.5f;
                                    a = Mathf.Min(a, 100f);
                                    vector31 = myTransform.forward * a + Vector3.up * num7;
                                }
                                rigidbody.velocity = vector31;
                            }
                            else
                            {
                                float num18;
                                const float num10 = -20f;
                                var size = neck.position.y;
                                var num13 = (num10 - gravity) * 0.5f;
                                var height = myHero.rigidbody.velocity.y;
                                var dist = myHero.transform.position.y - size;
                                var num16 = Mathf.Abs((Mathf.Sqrt(height * height - 4f * num13 * dist) - height) / (2f * num13));
                                var vector8 = myHero.transform.position + myHero.rigidbody.velocity * num16 + Vector3.up * 0.5f * num10 * num16 * num16;
                                var num17 = vector8.y;
                                if (dist < 0f || num17 - size < 0f)
                                {
                                    num18 = 60f;
                                    var num19 = speed * 2.5f;
                                    num19 = Mathf.Min(num19, 100f);
                                    var vector9 = myTransform.forward * num19 + Vector3.up * num18;
                                    rigidbody.velocity = vector9;
                                    return;
                                }
                                var num20 = num17 - size;
                                var num21 = Mathf.Sqrt(2f * num20 / this.gravity);
                                num18 = this.gravity * num21;
                                num18 = Mathf.Max(30f, num18);
                                var vector10 = (vector8 - myTransform.position) / num16;
                                abnorma_jump_bite_horizon_v = new Vector3(vector10.x, 0f, vector10.z);
                                var velocity = rigidbody.velocity;
                                var force = new Vector3(abnorma_jump_bite_horizon_v.x, velocity.y, abnorma_jump_bite_horizon_v.z) - velocity;
                                rigidbody.AddForce(force, ForceMode.VelocityChange);
                                rigidbody.AddForce(Vector3.up * num18, ForceMode.VelocityChange);
                                var rot = Vector2.Angle(new Vector2(myTransform.position.x, myTransform.position.z),
                                    new Vector2(myHero.transform.position.x, myHero.transform.position.z));
                                rot = Mathf.Atan2(myHero.transform.position.x - myTransform.position.x, myHero.transform.position.z - myTransform.position.z)
                                    * 57.29578f;
                                gameObject.transform.rotation = Quaternion.Euler(0f, rot, 0f);
                            }
                        }
                        else
                            rigidbody.velocity = Vector3.zero;
                    }
                    if (animation["attack_" + attackAnimation].normalizedTime >= 1f)
                    {
                        //debug
                        /*Debug.DrawLine(transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head").position + Vector3.up * 1.5f * myLevel,
                            transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head").position + Vector3.up * 1.5f * myLevel
                            + Vector3.up * 3f * myLevel, Color.green);
                        Debug.DrawLine(transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head").position + Vector3.up * 1.5f * myLevel,
                            transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head").position + Vector3.up * 1.5f * myLevel
                            + Vector3.forward * 3f * myLevel, Color.green);*/
                        var obj12 = checkIfHitHead(head, 3f);
                        if (obj12 != null)
                        {
                            var vector48 = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest").position;
                            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                                obj12.GetComponent<HERO>().die((obj12.transform.position - vector48) * 15f * myLevel, false);
                            else if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine && !obj12.GetComponent<HERO>().HasDied())
                            {
                                obj12.GetComponent<HERO>().markDie();
                                object[] objArray7 = { (obj12.transform.position - vector48) * 15f * myLevel, true, !nonAI ? -1 : photonView.viewID, name, true };
                                obj12.GetComponent<HERO>().photonView.RPC("netDie", PhotonTargets.All, objArray7);
                            }
                            attackAnimation = abnormalType == AbnormalType.TYPE_CRAWLER ? "crawler_jump_1" : "jumper_1";
                            playAnimation("attack_" + attackAnimation);
                        }
                        if (Mathf.Abs(rigidbody.velocity.y) < 0.5f || rigidbody.velocity.y < 0f || IsGrounded())
                        {
                            attackAnimation = abnormalType == AbnormalType.TYPE_CRAWLER ? "crawler_jump_1" : "jumper_1";
                            playAnimation("attack_" + attackAnimation);
                        }
                    }
                }
                else if (attackAnimation == "jumper_1" || attackAnimation == "crawler_jump_1")
                {
                    if (animation["attack_" + attackAnimation].normalizedTime >= 1f && grounded)
                    {
                        attackAnimation = abnormalType == AbnormalType.TYPE_CRAWLER ? "crawler_jump_2" : "jumper_2";
                        crossFade("attack_" + attackAnimation, 0.1f);
                        fxPosition = myTransform.position;
                        var obj13 = IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine
                            ? PhotonNetwork.Instantiate("FX/boom2", fxPosition, fxRotation, 0)
                            : (GameObject)Instantiate(CacheResources.Load("FX/boom2"), fxPosition, fxRotation);
                        obj13.transform.localScale = myTransform.localScale * 1.6f;
                        var num23 = 1f - Vector3.Distance(currentCamera.transform.position, obj13.transform.position) * 0.05f;
                        num23 = Mathf.Min(1f, num23);
                        currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().startShake(num23, num23);
                    }
                }
                else if (attackAnimation == "jumper_2" || attackAnimation == "crawler_jump_2")
                {
                    if (animation["attack_" + attackAnimation].normalizedTime >= 1f)
                        idle();
                }
                else if (animation.IsPlaying("tired"))
                {
                    if (animation["tired"].normalizedTime >= 1f + Mathf.Max(attackEndWait * 2f, 3f))
                        idle(Random.Range(attackWait - 1f, 3f));
                }
                else if (animation["attack_" + attackAnimation].normalizedTime >= 1f + attackEndWait)
                {
                    if (nextAttackAnimation != null)
                        attack(nextAttackAnimation);
                    else if (attackAnimation == "quick_turn_l" || attackAnimation == "quick_turn_r")
                    {
                        myTransform.rotation = Quaternion.Euler(myTransform.rotation.eulerAngles.x, myTransform.rotation.eulerAngles.y + 180f,
                            myTransform.rotation.eulerAngles.z);
                        idle(Random.Range(0.5f, 1f));
                        playAnimation("idle");
                    }
                    else if (abnormalType == AbnormalType.TYPE_I || abnormalType == AbnormalType.TYPE_JUMPER)
                    {
                        attackCount++;
                        if (attackCount > 3 && attackAnimation == "abnormal_getup")
                        {
                            attackCount = 0;
                            crossFade("tired", 0.5f);
                        }
                        else
                            idle(Random.Range(attackWait - 1f, 3f));
                    }
                    else
                        idle(Random.Range(attackWait - 1f, 3f));
                }
                break;
            case TitanState.grab:
                if (animation["grab_" + attackAnimation].normalizedTime >= attackCheckTimeA
                    && animation["grab_" + attackAnimation].normalizedTime <= attackCheckTimeB && grabbedTarget == null)
                {
                    var grabTarget = checkIfHitHand(currentGrabHand);
                    if (grabTarget != null)
                    {
                        if (isGrabHandLeft)
                        {
                            eatSetL(grabTarget);
                            grabbedTarget = grabTarget;
                        }
                        else
                        {
                            eatSet(grabTarget);
                            grabbedTarget = grabTarget;
                        }
                    }
                }
                if (animation["grab_" + attackAnimation].normalizedTime >= 1f)
                {
                    if (grabbedTarget != null)
                        eat();
                    else
                        idle(Random.Range(attackWait - 1f, 2f));
                }
                break;
            case TitanState.eat:
                if (!attacked && animation[attackAnimation].normalizedTime >= 0.48f)
                {
                    attacked = true;
                    justEatHero(grabbedTarget, currentGrabHand);
                }
                if (animation[attackAnimation].normalizedTime >= 1f)
                    idle();
                break;
            case TitanState.chase:
                if (myHero == null)
                    idle();
                else if (!longRangeAttackCheck())
                {
                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE && PVPfromCheckPt != null && myDistance > chaseDistance)
                        idle();
                    else if (abnormalType == AbnormalType.TYPE_CRAWLER)
                    {
                        var vector54 = myHero.transform.position - myTransform.position;
                        var current = -Mathf.Atan2(vector54.z, vector54.x) * 57.29578f;
                        var f = -Mathf.DeltaAngle(current, gameObject.transform.rotation.eulerAngles.y - 90f);
                        if (myDistance < attackDistance * 3f && Random.Range(0f, 1f) < 0.1f && Mathf.Abs(f) < 90f && myHero.transform.position.y < neck.position.y + 30f * myLevel
                            && myHero.transform.position.y > neck.position.y + 10f * myLevel)
                            attack("crawler_jump_0");
                        else
                        {
                            var obj15 = checkIfHitCrawlerMouth(head, 2.2f);
                            if (obj15 != null)
                            {
                                var vector60 = myTransform.Find("Amarture/Core/Controller_Body/hip/spine/chest").position;
                                if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                                    obj15.GetComponent<HERO>().die((obj15.transform.position - vector60) * 15f * myLevel, false);
                                else if (IN_GAME_MAIN_CAMERA.IsMultiplayer && photonView.isMine)
                                {
                                    if (obj15.GetComponent<TITAN_EREN>() != null)
                                        obj15.GetComponent<TITAN_EREN>().hitByTitan();
                                    else if (!obj15.GetComponent<HERO>().HasDied())
                                    {
                                        obj15.GetComponent<HERO>().markDie();
                                        object[] args = { (obj15.transform.position - vector60) * 15f * myLevel, true, !nonAI ? -1 : photonView.viewID, name, true };
                                        obj15.GetComponent<HERO>().photonView.RPC("netDie", PhotonTargets.All, args);
                                    }
                                }
                            }
                            if (myDistance < attackDistance && Random.Range(0f, 1f) < 0.02f)
                                idle(Random.Range(0.05f, 0.2f));
                        }
                    }
                    else if (abnormalType == AbnormalType.TYPE_JUMPER
                             && (myDistance > attackDistance && myHero.transform.position.y > head.position.y + 4f * myLevel
                                 || myHero.transform.position.y > head.position.y + 4f * myLevel)
                        && Vector3.Distance(myTransform.position, myHero.transform.position) < 1.5f * myHero.transform.position.y)
                        attack("jumper_0");
                    else if (myDistance < attackDistance)
                        idle(Random.Range(0.05f, 0.2f));
                }
                break;
            case TitanState.wander:
                if (myDistance < chaseDistance || whoHasTauntMe != null)
                {
                    var vector66 = myHero.transform.position - myTransform.position;
                    var num26 = -Mathf.Atan2(vector66.z, vector66.x) * 57.29578f;
                    var num27 = -Mathf.DeltaAngle(num26, gameObject.transform.rotation.eulerAngles.y - 90f);
                    if (isAlarm || Mathf.Abs(num27) < 90f)
                    {
                        chase();
                        return;
                    }
                    if (!isAlarm && myDistance < chaseDistance * 0.1f)
                    {
                        chase();
                        return;
                    }
                }
                if (Random.Range(0f, 1f) < 0.01f)
                    idle();
                break;
            case TitanState.turn:
                gameObject.transform.rotation = Quaternion.Lerp(gameObject.transform.rotation, Quaternion.Euler(0f, desDeg, 0f),
                    Time.deltaTime * Mathf.Abs(turnDeg) * 0.015f);
                if (animation[turnAnimation].normalizedTime >= 1f)
                    idle();
                break;
            case TitanState.hit_eye:
                if (animation.IsPlaying("sit_hit_eye") && animation["sit_hit_eye"].normalizedTime >= 1f)
                    remainSitdown();
                else if (animation.IsPlaying("hit_eye") && animation["hit_eye"].normalizedTime >= 1f)
                {
                    if (nonAI)
                        idle();
                    else
                        attack("combo_1");
                }
                break;
            case TitanState.to_check_point:
                if (checkPoints.Count <= 0 && myDistance < attackDistance)
                {
                    var str2 = string.Empty;
                    var strArray2 = GetAttackStrategy();
                    if (strArray2 != null)
                        str2 = strArray2[Random.Range(0, strArray2.Length)];
                    if (executeAttack(str2))
                        return;
                }
                if (Vector3.Distance(myTransform.position, targetCheckPt) < targetR)
                {
                    if (checkPoints.Count > 0)
                    {
                        if (checkPoints.Count == 1)
                        {
                            if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.BOSS_FIGHT_CT)
                            {
                                GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().gameLose();
                                checkPoints = new ArrayList();
                                idle();
                            }
                        }
                        else
                        {
                            if (checkPoints.Count == 4)
                            {
                                GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().sendChatContentInfo(
                                    "<color=#A8FF24>*WARNING!* An abnormal titan is approaching the north gate!</color>");
                            }
                            var vector68 = (Vector3)checkPoints[0];
                            targetCheckPt = vector68;
                            checkPoints.RemoveAt(0);
                        }
                    }
                    else
                        idle();
                }
                break;
            case TitanState.to_pvp_pt:
                if (myDistance < chaseDistance * 0.7f)
                    chase();
                if (Vector3.Distance(myTransform.position, targetCheckPt) < targetR)
                    idle();
                break;
            case TitanState.random_run:
                random_run_time -= Time.deltaTime;
                if (Vector3.Distance(myTransform.position, targetCheckPt) < targetR || random_run_time <= 0f)
                    idle();
                break;
            case TitanState.down:
                getdownTime -= Time.deltaTime;
                if (animation.IsPlaying("sit_hunt_down") && animation["sit_hunt_down"].normalizedTime >= 1f)
                    playAnimation("sit_idle");
                if (getdownTime <= 0f)
                    crossFade("sit_getup", 0.1f);
                if (animation.IsPlaying("sit_getup") && animation["sit_getup"].normalizedTime >= 1f)
                    idle();
                break;
            case TitanState.sit:
                getdownTime -= Time.deltaTime;
                angle = 0f;
                between2 = 0f;
                if (myDistance < chaseDistance || whoHasTauntMe != null)
                {
                    if (myDistance < 50f)
                        isAlarm = true;
                    else
                    {
                        var vector69 = myHero.transform.position - myTransform.position;
                        angle = -Mathf.Atan2(vector69.z, vector69.x) * 57.29578f;
                        between2 = -Mathf.DeltaAngle(angle, gameObject.transform.rotation.eulerAngles.y - 90f);
                        if (Mathf.Abs(between2) < 100f)
                            isAlarm = true;
                    }
                }
                if (animation.IsPlaying("sit_down") && animation["sit_down"].normalizedTime >= 1f)
                    playAnimation("sit_idle");
                if ((getdownTime <= 0f || isAlarm) && animation.IsPlaying("sit_idle"))
                    crossFade("sit_getup", 0.1f);
                if (animation.IsPlaying("sit_getup") && animation["sit_getup"].normalizedTime >= 1f)
                    idle();
                break;
            case TitanState.recover:
                getdownTime -= Time.deltaTime;
                if (getdownTime <= 0f)
                    idle();
                if (animation.IsPlaying("idle_recovery") && animation["idle_recovery"].normalizedTime >= 1f)
                    idle();
                break;
        }
    }

    private void wander(float sbtime = 0f)
    {
        state = TitanState.wander;
        crossFade(runAnimation, 0.5f);
    }
}