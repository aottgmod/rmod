﻿using UnityEngine;

class Loader : MonoBehaviour
{
    public static GameObject modules;

    void Start()
    {
        modules = new GameObject();
        modules.AddComponent<Modules.Load>();
        DontDestroyOnLoad(modules);
    }
}