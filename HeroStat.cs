using System.Collections.Generic;

public class HeroStat
{
    private static bool init;

    private string name;
    public string skillId = "petra";

    public int ACL;
    public int BLA;
    public int GAS;
    public int SPD;

    private static HeroStat ARMIN;
    private static HeroStat EREN;
    private static HeroStat JEAN;
    private static HeroStat LEVI;
    private static HeroStat MARCO;
    private static HeroStat MIKASA;
    private static HeroStat PETRA;
    private static HeroStat SASHA;

    public static HeroStat[] heroStats;
    public static Dictionary<string, HeroStat> stats;

    public static HeroStat getInfo(string name)
    {
        initDATA();
        return stats[name];
    }

    public static int shouldHave(HeroStat hero)
    {
        return hero.SPD + hero.ACL + hero.BLA + hero.GAS;
    }

    public static int shouldHave(string str)
    {
        if (str.StartsWith("SET"))
            return 400;
        var hero = stats[str];
        return hero.SPD + hero.ACL + hero.BLA + hero.GAS;
    }

    private static void initDATA()
    {
        if (!init)
            init = true;
        else
            return;
        MIKASA = new HeroStat
        {
            name = "MIKASA",
            skillId = "mikasa",
            SPD = 125,
            GAS = 75,
            BLA = 75,
            ACL = 135
        };
        LEVI = new HeroStat
        {
            name = "LEVI",
            skillId = "levi",
            SPD = 0x5f,
            GAS = 100,
            BLA = 100,
            ACL = 150
        };
        ARMIN = new HeroStat
        {
            name = "ARMIN",
            skillId = "armin",
            SPD = 0x4b,
            GAS = 150,
            BLA = 0x7d,
            ACL = 0x55
        };
        MARCO = new HeroStat
        {
            name = "MARCO",
            skillId = "marco",
            SPD = 110,
            GAS = 100,
            BLA = 0x73,
            ACL = 0x5f
        };
        JEAN = new HeroStat
        {
            name = "JEAN",
            skillId = "jean",
            SPD = 100,
            GAS = 150,
            BLA = 80,
            ACL = 100
        };
        EREN = new HeroStat
        {
            name = "EREN",
            skillId = "eren",
            SPD = 100,
            GAS = 90,
            BLA = 90,
            ACL = 100
        };
        PETRA = new HeroStat
        {
            name = "PETRA",
            skillId = "petra",
            SPD = 80,
            GAS = 110,
            BLA = 100,
            ACL = 140
        };
        SASHA = new HeroStat
        {
            name = "SASHA",
            skillId = "sasha",
            SPD = 140,
            GAS = 100,
            BLA = 100,
            ACL = 0x73
        };
        
        var stat = new HeroStat
        {
            skillId = "petra",
            SPD = 100,
            GAS = 100,
            BLA = 100,
            ACL = 100
        };
        var stat2 = new HeroStat();
        SASHA.name = "AHSS";
        stat2.skillId = "sasha";
        stat2.SPD = 100;
        stat2.GAS = 100;
        stat2.BLA = 100;
        stat2.ACL = 100;
        stats = new Dictionary<string, HeroStat>
        {
            { "MIKASA", MIKASA },
            { "LEVI", LEVI },
            { "ARMIN", ARMIN },
            { "MARCO", MARCO },
            { "JEAN", JEAN },
            { "EREN", EREN },
            { "PETRA", PETRA },
            { "SASHA", SASHA },
            { "CUSTOM_DEFAULT", stat },
            { "AHSS", stat2 }
        };
    }
}