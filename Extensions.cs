﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using ExitGames.Client.Photon;
using mItems;
using Photon;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Object = UnityEngine.Object;

public static class Extensions
{
    public static bool GetActive(this GameObject target)
    {
        return target != null && target.activeInHierarchy;
    }

    public static IEnumerable<int> AllIndexesOf(this string str, string value)
    {
        if (value.IsNullOrEmpty())
            throw new ArgumentException("The string to find may not be empty", "value");
        var indexes = new List<int>();
        for (var i = 0; ; i += value.Length)
        {
            i = str.IndexOf(value, i);
            if (i == -1)
                return indexes;
            indexes.Add(i);
        }
    }

    public static bool AlmostEquals(this float target, float second, float floatDiff)
    {
        return Mathf.Abs(target - second) < floatDiff;
    }

    public static bool AlmostEquals(this Quaternion target, Quaternion second, float maxAngle)
    {
        return Quaternion.Angle(target, second) < maxAngle;
    }

    public static bool AlmostEquals(this Vector2 target, Vector2 second, float sqrMagnitudePrecision)
    {
        var vector = target - second;
        return vector.sqrMagnitude < sqrMagnitudePrecision;
    }

    public static bool AlmostEquals(this Vector3 target, Vector3 second, float sqrMagnitudePrecision)
    {
        var vector = target - second;
        return vector.sqrMagnitude < sqrMagnitudePrecision;
    }

    public static bool Contains<T>(this T[] array, T value)
    {
        return Array.IndexOf(array, value) > -1;
    }

    public static bool Contains(this string text, bool and, params string[] array)
    {
        if (array.Length == 0)
            return text.Contains(and.ToString());
        if (and)
        {
            for (var i = 0; i < array.Length; i++)
            {
                if (!text.Contains(array[i]))
                    return false;
            }
            return true;
        }
        for (var i = 0; i < array.Length; i++)
        {
            if (text.Contains(array[i]))
                return true;
        }
        return false;
    }

    public static int Count<TSource>(this IEnumerable<TSource> source)
    {
        if (source == null)
            throw new ArgumentNullException("source");
        var is2 = source as ICollection<TSource>;
        if (is2 != null)
            return is2.Count;
        var num = 0;
        using (var enumerator = source.GetEnumerator())
        {
            while (enumerator.MoveNext())
                num++;
        }
        return num;
    }

    public static IEnumerable<T> ForEach<T>(this IEnumerable<T> source, Action<T> action)
    {
        var forEach = source as T[] ?? source.ToArray();
        foreach (var val in forEach)
            action(val);
        return forEach;
    }

    public static PhotonView GetPhotonView(this GameObject go)
    {
        return go.GetComponent<PhotonView>();
    }

    public static PhotonView[] GetPhotonViewsInChildren(this GameObject go)
    {
        return go.GetComponentsInChildren<PhotonView>(true);
    }

    public static void HashNullFix(this IDictionary original)
    {
        int num = 0, i;
        var objArray = new object[original.Count];
        foreach (var obj in original.Keys)
            objArray[num++] = obj;
        for (i = 0; i < objArray.Length; i++)
        {
            object key;
            if (original[(key = objArray[i])] == null)
            {
                if (key != null)
                {
                    switch ((string)key)
                    {
                        case PhotonPlayerProperty.kills:
                        case PhotonPlayerProperty.deaths:
                        case PhotonPlayerProperty.max_dmg:
                        case PhotonPlayerProperty.total_dmg:
                        case PhotonPlayerProperty.isTitan:
                        case PhotonPlayerProperty.team:
                        case PhotonPlayerProperty.sex:
                        case PhotonPlayerProperty.heroCostumeId:
                        case PhotonPlayerProperty.cape:
                        case PhotonPlayerProperty.hairInfo:
                        case PhotonPlayerProperty.eye_texture_id:
                        case PhotonPlayerProperty.beard_texture_id:
                        case PhotonPlayerProperty.glass_texture_id:
                        case PhotonPlayerProperty.skin_color:
                        case PhotonPlayerProperty.division:
                            {
                                original[key] = 0;
                                continue;
                            }
                        case PhotonPlayerProperty.statACL:
                        case PhotonPlayerProperty.statBLA:
                        case PhotonPlayerProperty.statGAS:
                        case PhotonPlayerProperty.statSPD:
                            {
                                original[key] = 100;
                                continue;
                            }
                        case PhotonPlayerProperty.hair_color1:
                        case PhotonPlayerProperty.hair_color2:
                        case PhotonPlayerProperty.hair_color3:
                            {
                                original[key] = 0f;
                                continue;
                            }
                        case PhotonPlayerProperty.guildName:
                            {
                                original[key] = String.Empty;
                                continue;
                            }
                        case PhotonPlayerProperty.dead:
                            {
                                original[key] = true;
                                continue;
                            }
                        default:
                            break;
                    }
                }
                original.Remove(key);
            }
        }
    }

    public static string Join(this string[] value, string separator, int startIndex = 0, int? count = null)
    {
        return String.Join(separator, value, startIndex, count ?? value.Length - startIndex);
    }

    public static void Merge(this IDictionary target, IDictionary addHash)
    {
        if ((addHash != null) && !target.Equals(addHash))
        {
            var enumerator = addHash.Keys.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var current = enumerator.Current;
                    if (current != null)
                        target[current] = addHash[current];
                }
            }
            finally
            {
                var disposable = enumerator as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
        }
    }

    public static void MergeStringKeys(this IDictionary target, IDictionary addHash)
    {
        if ((addHash != null) && !target.Equals(addHash))
        {
            var enumerator = addHash.Keys.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var current = enumerator.Current;
                    if (current is string)
                        target[current] = addHash[current];
                }
            }
            finally
            {
                var disposable = enumerator as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
        }
    }

    public static T[] RemoveAt<T>(this T[] source, int index)
    {
        var dest = new T[source.Length - 1];
        if (index > 0)
            Array.Copy(source, 0, dest, 0, index);
        if (index < source.Length - 1)
            Array.Copy(source, index + 1, dest, index, source.Length - index - 1);
        return dest;
    }

    public static string RemoveCount(this string input, string str, int count)
    {
        for (var i = 0; i < count; i++)
            input = input.Remove(input.LastIndexOf(str), str.Length);
        return input;
    }

    public static string RemoveNonAlphanumericFast(this string input)
    {
        for (var i = 0; i < input.Length; i++)
        {
            switch (input[i])
            {
                #region cases
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                case 'g':
                case 'h':
                case 'i':
                case 'j':
                case 'k':
                case 'l':
                case 'm':
                case 'n':
                case 'o':
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 't':
                case 'u':
                case 'v':
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                case 'G':
                case 'H':
                case 'I':
                case 'J':
                case 'K':
                case 'L':
                case 'M':
                case 'N':
                case 'O':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                case 'T':
                case 'U':
                case 'V':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case ' ':
                    #endregion
                    continue;
                default:
                    input = input.Replace(input[i], '\0');
                    break;
            }
        }
        return input;
    }

    public static string Replace(this string str, string oldval, string newval)
    {
        var sb = new StringBuilder(str);
        sb.Replace(oldval, newval);
        return sb.ToString();
    }

    public static string Replace(this string str, char oldchar, char newchar)
    {
        var sb = new StringBuilder(str);
        sb.Replace(oldchar, newchar);
        return sb.ToString();
    }

    public static void StripKeysWithNullValues(this IDictionary original)
    {
        var objArray = new object[original.Count];
        var num = 0;
        var enumerator = original.Keys.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = enumerator.Current;
                objArray[num++] = current;
            }
        }
        finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
        for (var i = 0; i < objArray.Length; i++)
        {
            var key = objArray[i];
            if (original[key] == null)
                original.Remove(key);
        }
    }

    public static Hashtable StripToStringKeys(this IDictionary original)
    {
        var hashtable = new Hashtable();
        var enumerator = original.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                if (enumerator.Current != null)
                {
                    var current = (DictionaryEntry)enumerator.Current;
                    if (current.Key is string)
                        hashtable[current.Key] = current.Value;
                }
            }
        }
        finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
        return hashtable;
    }

    public static T To<T>(this string str)
    {
        return (T)Convert.ChangeType(str, typeof(T));
    }

    public static T To<T>(this object obj)
    {
        return (T)Convert.ChangeType(obj, typeof(T));
    }

    public static string ToHex(this Color clr, bool includeSyntax = true)
    {
        var r = Mathf.FloorToInt(clr.r).ToString("X2");
        var g = Mathf.FloorToInt(clr.g).ToString("X2");
        var b = Mathf.FloorToInt(clr.b).ToString("X2");
        if (includeSyntax)
            return String.Format("[{0}{1}{2}]", r, g, b);
        return r + g + b;
    }

    public static Color HexToColor(this string hex)
    {
        if (hex.Length == 8 && hex.StartsWith("[") && hex.EndsWith("]"))
            hex = hex.Substring(1, 6);
        if (hex.Length != 6)
            return Color.clear;
        var first = new int[3];
        first[0] = Int32.Parse(hex.Substring(0, 2), NumberStyles.AllowHexSpecifier);
        first[1] = Int32.Parse(hex.Substring(2, 2), NumberStyles.AllowHexSpecifier);
        first[2] = Int32.Parse(hex.Substring(4, 2), NumberStyles.AllowHexSpecifier);
        return new Color(first[0], first[1], first[2], 1f);
    }

    public static bool IsNullOrEmpty(this string str)
    {
        return String.IsNullOrEmpty(str);
    }

    public static int LevenshteinDistance(this string source, string target)
    {
        if ((source == null) || (target == null))
            return 0;
        if ((source.Length == 0) || (target.Length == 0))
            return 0;
        if (source == target)
            return source.Length;

        var sourceWordCount = source.Length;
        var targetWordCount = target.Length;

        if (sourceWordCount == 0)
            return targetWordCount;
        if (targetWordCount == 0)
            return sourceWordCount;

        var distance = new int[sourceWordCount + 1, targetWordCount + 1];

        for (var i = 0; i <= sourceWordCount; distance[i, 0] = i++)
        { }
        for (var j = 0; j <= targetWordCount; distance[0, j] = j++)
        { }

        for (var i = 1; i <= sourceWordCount; i++)
        {
            for (var j = 1; j <= targetWordCount; j++)
            {
                var cost = target[j - 1] == source[i - 1] ? 0 : 1;
                distance[i, j] = Math.Min(Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1), distance[i - 1, j - 1] + cost);
            }
        }
        return distance[sourceWordCount, targetWordCount];
    }

    public static double SimilarityBetween(this string source, string target)
    {
        if ((source == null) || (target == null))
            return 0.0;
        if ((source.Length == 0) || (target.Length == 0))
            return 0.0;
        if (source == target)
            return 1.0;

        var stepsToSame = LevenshteinDistance(source, target);
        return 1.0 - stepsToSame / (double)Math.Max(source.Length, target.Length);
    }

    public static void ToClipboard(this string str)
    {
        var te = new TextEditor { content = new GUIContent(str) };
        te.SelectAll();
        te.Copy();
    }

    public static string[] ToStringArray<TSource>(this IEnumerable<TSource> source)
    {
        return source.Where(x => x != null).Select(x => x.ToString()).ToArray();
        /*if (source == null)
            throw new ArgumentNullException("source");
        return new List<string>((IEnumerable<string>)source).ToArray();*/
    }

    public static string ToStringFull(this IDictionary origin)
    {
        return SupportClass.DictionaryToString(origin, false);
    }

    public static bool Has<T>(this Enum type, T value)
    {
        try
        {
            return ((int)(object)type & (int)(object)value) == (int)(object)value;
        }
        catch
        {
            return false;
        }
    }

    public static int Count(this Enum type)
    {
        try
        {
            return type.ToString().Split(',').Length;
            ////////////only for enums with 32 or less options////////////
            /*var v = (uint)(object)type;
            v = v - ((v >> 1) & 0x55555555);
            v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
            return (int)(((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24);*/
        }
        catch
        {
            return -1;
        }
    }

    public static bool Is<T>(this Enum type, T value)
    {
        try
        {
            return (int)(object)type == (int)(object)value;
        }
        catch
        {
            return false;
        }
    }


    public static T Add<T>(this Enum type, T value)
    {
        try
        {
            return (T)(object)((int)(object)type | (int)(object)value);
        }
        catch (Exception ex)
        {
            throw new ArgumentException(String.Format("Could not append value from enumerated type '{0}'.", typeof(T).Name), ex);
        }
    }

    public static T Remove<T>(this Enum type, T value)
    {
        try
        {
            return (T)(object)((int)(object)type & ~(int)(object)value);
        }
        catch (Exception ex)
        {
            throw new ArgumentException(String.Format("Could not remove value from enumerated type '{0}'.", typeof(T).Name), ex);
        }
    }

    public static void EditIndex<T1, T2>(this IEnumerable<Tuple<T1, T2>> list, T1 index, T2 val)
    {
        foreach (var tuple in list)
        {
            if (!tuple.First.Equals(index))
                continue;
            tuple.Second = val;
            break;
        }
    }

    public static void EditIndex<T1, T2>(this IEnumerable<Pair<T1, T2>> list, T1 index, T2 val)
    {
        foreach (var pair in list)
        {
            if (!pair.First.Equals(index))
                continue;
            pair.Second = val;
            break;
        }
    }

    public static void Remove<TKey, TValue>(this List<KeyValuePair<TKey, TValue>> list, TKey key, TValue value)
    {
        list.Remove(new KeyValuePair<TKey, TValue>(key, value));
    }

    public static void AddMonoObjectToAllGameObjects(this AssetBundle assetbundle)
    {
        foreach (Object obj in assetbundle.LoadAll())
        {
            if (obj == null || !(obj is GameObject))
                continue;
            ((GameObject)obj).AddMonoObject();
        }
    }

    public static MonoObject AddMonoObject(this GameObject GO)
    {
        if (GO == null)
            return null;
        MonoObject mono = null;
        if (GO.GetComponent<MonoObject>() == null)
            mono = GO.AddComponent<MonoObject>();
        GameObject GO2;
        Transform transform;
        var tarray = GO.GetComponentsInChildren<Transform>();
        for (var i = 0; i < tarray.Length; i++)
        {
            if ((transform = tarray[i]) == null)
                continue;
            if ((GO2 = transform.gameObject) == null)
                continue;
            if (GO2.GetComponent<MonoObject>() == null)
                GO2.AddComponent<MonoObject>();
            if (i == (tarray.Length - 1))
                tarray = GO2.GetComponentsInChildren<Transform>();
        }
        tarray = GO.GetComponentsInParent<Transform>();
        for (var i = 0; i < tarray.Length; i++)
        {
            if ((transform = tarray[i]) == null)
                continue;
            if ((GO2 = transform.gameObject) == null)
                continue;
            if (GO2.GetComponent<MonoObject>() == null)
                GO2.AddComponent<MonoObject>();
            if (i == (tarray.Length - 1))
                tarray = GO2.GetComponentsInParent<Transform>();
        }
        /*if (GO.activeInHierarchy)
        {
            return Photon.MonoObject.monocache[mono.GetInstanceIDofGameObject()];
        }*/
        return mono;
    }

    public static Component GetComponent(this GameObject obj, string TypeName)
    {
        if (obj == null)
            return null;
        MonoObject mono;
        if (!MonoObject.monocache.TryGetValue(obj.GetInstanceID(), out mono))
            throw new NullReferenceException("'monocache' doesn't contain this GameObject");
        if (mono == null)
            throw new NullReferenceException("this GameObject is null");
        return mono.GetComponent(TypeName);
    }

    public static T GetComponent<T>(this GameObject obj) where T : Component
    {
        if (obj == null)
            return null;
        MonoObject mono;
        if (!MonoObject.monocache.TryGetValue(obj.GetInstanceID(), out mono))
            throw new NullReferenceException("'monocache' doesn't contain this GameObject");
        if (mono == null)
            throw new NullReferenceException("this GameObject is null");
        return mono.GetComponent<T>();
    }

    public static Component GetComponent(this Component c, string TypeName)
    {
        if (c == null)
            return null;
        MonoObject mono;
        if (!MonoObject.monocache.TryGetValue(c.GetInstanceID(), out mono))
            throw new NullReferenceException("'monocache' doesn't contain this GameObject");
        if (mono == null)
            throw new NullReferenceException("this GameObject is null");
        return mono.GetComponent(TypeName);
    }

    public static T GetComponent<T>(this Component c) where T : Component
    {
        if (c == null)
            return null;
        MonoObject mono;
        if (!MonoObject.monocache.TryGetValue(c.GetInstanceID(), out mono))
            throw new NullReferenceException("'monocache' doesn't contain this GameObject");
        if (mono == null)
            throw new NullReferenceException("this GameObject is null");
        return mono.GetComponent<T>();
    }
}

namespace CompressString
{
    internal static class StringCompressor
    {
        public static string CompressString(string text)
        {
            var bytes = Encoding.UTF8.GetBytes(text);
            var compressedStream = new MemoryStream();
            using (var stream2 = new GZipStream(compressedStream, CompressionMode.Compress, true))
            {
                stream2.Write(bytes, 0, bytes.Length);
            }
            compressedStream.Position = 0L;
            var buffer = new byte[compressedStream.Length];
            compressedStream.Read(buffer, 0, buffer.Length);
            var dst = new byte[buffer.Length + 4];
            Buffer.BlockCopy(buffer, 0, dst, 4, buffer.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(bytes.Length), 0, dst, 0, 4);
            return Convert.ToBase64String(dst);
        }

        public static string DecompressString(string compressedText)
        {
            var buffer = Convert.FromBase64String(compressedText);
            using (var stream = new MemoryStream())
            {
                var num = BitConverter.ToInt32(buffer, 0);
                stream.Write(buffer, 4, buffer.Length - 4);
                var dest = new byte[num];
                stream.Position = 0L;
                using (var stream2 = new GZipStream(stream, CompressionMode.Decompress))
                {
                    stream2.Read(dest, 0, dest.Length);
                }
                return Encoding.UTF8.GetString(dest);
            }
        }
    }
}