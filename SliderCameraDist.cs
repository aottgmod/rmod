using UnityEngine;

public class SliderCameraDist : MonoBehaviour
{
    private bool init;

    private void OnSliderChange(float value)
    {
        if (!init)
        {
            init = true;
            if (PlayerPrefs.HasKey("cameraDistance"))
            {
                var val = PlayerPrefs.GetFloat("cameraDistance");
                gameObject.GetComponent<UISlider>().sliderValue = val;
                value = val;
            }
            else
                PlayerPrefs.SetFloat("cameraDistance", gameObject.GetComponent<UISlider>().sliderValue);
        }
        else
            PlayerPrefs.SetFloat("cameraDistance", gameObject.GetComponent<UISlider>().sliderValue);
        IN_GAME_MAIN_CAMERA.cameraDistance = 0.3f + value;
    }
}