﻿using System;
using System.Collections.Generic;
using mItems;
using UnityEngine;
using MonoBehaviour = Photon.MonoBehaviour;

public class rMicMod : MonoBehaviour
{
    AudioSource aud = CacheGameObject.Find<AudioSource>("MultiplayerManager");
    float t1 = Time.time;
    bool mon;
    bool waitmon;
    string device = "";
    public static int id = 0;
    //WWW speakerimgWWW = new WWW("http://i.imgur.com/RnGotrS.png");
    WWW speakerimgWWW = new WWW("http://i.imgur.com/w1oRhXj.png");
    WWW errorimgWWW = new WWW("http://ahgmd.com");
    Texture errorimg = new Texture();
    Texture speakerimg = new Texture();
    bool gotspeakerimg;
    List<int> speaker = new List<int>();
    List<float> speakingtime = new List<float>();

    public void Start()
    {
        //aud.clip = Microphone.Start("Built-in Microphone", true, 3, 44100);
        errorimg = errorimgWWW.texture;
    }

    public void Update()
    {
        if (!gotspeakerimg)
        {
            if (speakerimgWWW.progress == 1)
            {
                if (speakerimgWWW.texture != errorimg)
                {
                    speakerimg = speakerimgWWW.texture;
                    gotspeakerimg = true;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            aud.clip = Microphone.Start("", true, 1, 44100);
            mon = true;
            t1 = Time.time;
        }
        if (mon)
        {
            if (Time.time - t1 >= .99f)
            {
                if (waitmon)
                {
                    Microphone.End(device);
                    mon = false;
                    waitmon = false;
                    if (id == 0)
                        base.photonView.RPC("PlaySound", PhotonTargets.All, ConvertToBytes(aud.clip));
                    else
                        base.photonView.RPC("PlaySound", PhotonPlayer.Find(id), ConvertToBytes(aud.clip));
                }
                else
                {
                    if (id == 0)
                        base.photonView.RPC("PlaySound", PhotonTargets.All, ConvertToBytes(aud.clip));
                    else
                        base.photonView.RPC("PlaySound", PhotonPlayer.Find(id), ConvertToBytes(aud.clip));
                    t1 = Time.time;
                }
            }
        }
        if (Input.GetKeyUp(KeyCode.U))
        {
            waitmon = true;
        }
        if (Input.GetKeyDown(KeyCode.Y))
        {
            aud.Play();
        }
    }


    //-------------------GUIs------------------------


    void OnGUI()
    {
        GUILayout.Window(1, new Rect(250, 10, Screen.width / 3, Screen.height / 4), Window, "", GUIStyle.none);
        if (gotspeakerimg)
        {
            /*GUI.Label(new Rect(Screen.width / 14, Screen.height / 2.85f, Screen.width / 15, Screen.height / 12), "<size=15>" + Convert.ToString(0) + ":</size>");
            GUI.DrawTexture(new Rect(Screen.width / 10, Screen.height / 3, Screen.width / 15, Screen.height / 12), speakerimg);
            GUI.Label(new Rect(Screen.width / 14, Screen.height / 2.85f + (Screen.height * 1 / 20), Screen.width / 15, Screen.height / 12), "<size=15>" + Convert.ToString(0) + ":</size>");
            GUI.DrawTexture(new Rect(Screen.width / 10, Screen.height / 3 + (Screen.height * 1 / 20), Screen.width / 15, Screen.height / 12), speakerimg);*/
            for (var z = 0; z < speakingtime.Count; z++)
            {
                if (Time.time - speakingtime[z] <= .99f)
                {
                    GUI.Label(new Rect(Screen.width / 14, Screen.height / 2.85f + (Screen.height * z / 20), Screen.width / 15, Screen.height / 12), "<size=15>" + Convert.ToString(speaker[z]) + ":</size>");
                    GUI.DrawTexture(new Rect(Screen.width / 10, Screen.height / 3 + (Screen.height * z / 20), Screen.width / 15, Screen.height / 12), speakerimg);
                }
                else
                {
                    GUI.Label(new Rect(Screen.width / 14, Screen.height / 2.85f + (Screen.height * z / 20), Screen.width / 15, Screen.height / 12), "<size=15>" + Convert.ToString(speaker[z]) + ":</size>");
                    GUI.DrawTexture(new Rect(Screen.width / 10, Screen.height / 3 + (Screen.height * z / 20), Screen.width / 15, Screen.height / 12), speakerimg);
                    speakingtime.Remove(speakingtime[z]);
                    speaker.Remove(speaker[z]);
                }
            }
        }
    }

    void Window(int id)
    {
        GUI.Box(new Rect(0, 0, Screen.width / 3, Screen.height / 3), "");

        if (mon)
        {
            GUILayout.Label(device);

            if (GUILayout.Button("Stop Recording"))
            {
                Microphone.End(device);
            }
        }
        else
        {
            GUILayout.Label("Select microphone to start recording");

            foreach (var z in Microphone.devices)
            {
                if (GUILayout.Button(z))
                {
                    device = z;
                }
            }
        }

        if (device != "")
        {
            /*GUILayout.Label("Push-to-talk key: " + VoiceChatRecorder.Instance.PushToTalkKey);
            GUILayout.Label("Toggle-to-talk key: " + VoiceChatRecorder.Instance.ToggleToTalkKey);
            GUILayout.Label("Auto detect speech: " + (VoiceChatRecorder.Instance.AutoDetectSpeech ? "On" : "Off"));
                        
            if (GUILayout.Button("Toggle Auto Detect"))
            {
                VoiceChatRecorder.Instance.AutoDetectSpeech = !VoiceChatRecorder.Instance.AutoDetectSpeech;
            }*/

            GUI.color = mon ? Color.green : Color.red;
            GUILayout.Label(mon ? "Transmitting" : "Silent");
        }
    }


    //------------------------Conversions-----------------------


    public byte[] ConvertToBytes(AudioClip clip)
    {
        var samples = new float[clip.samples];

        clip.GetData(samples, 0);

        var intData = new Int16[samples.Length];
        //converting in 2 float[] steps to Int16[], //then Int16[] to Byte[]

        var bytesData = new byte[samples.Length * 2];
        //bytesData array is twice the size of
        //dataSource array because a float converted in Int16 is 2 bytes.

        var rescaleFactor = 32767; //to convert float to Int16

        for (var i = 0; i < samples.Length; i++)
        {
            intData[i] = (short)(samples[i] * rescaleFactor);
            var byteArr = new byte[2];
            byteArr = BitConverter.GetBytes(intData[i]);
            byteArr.CopyTo(bytesData, i * 2);
        }

        return bytesData;
    }

    private static float[] ConvertByteToFloat(byte[] array)
    {
        var floatArr = new float[array.Length / 2];

        for (var i = 0; i < floatArr.Length; i++)
        {
            floatArr[i] = (float)(((float)BitConverter.ToInt16(array, i * 2)) / 32768.0);
        }

        return floatArr;
    }


    //--------------------------RPCs----------------------------

    [RPC]
    public void PlaySound(byte[] a, PhotonMessageInfo b)
    {
        var t = CacheGameObject.Find<AudioSource>("MultiplayerManager");
        var f = ConvertByteToFloat(a);
        var ac = AudioClip.Create("test", f.Length, 1, 44100, false, false);
        ac.SetData(f, 0);
        t.PlayOneShot(ac);
        speakingtime.Add(Time.time);
        speaker.Add(b.sender.ID);
    }
}