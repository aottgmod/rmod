﻿using System;
using System.Collections.Generic;
using mItems;
using UnityEngine;
using Console = Modules.Console;
using MonoBehaviour = Photon.MonoBehaviour;

internal class rGameHall : MonoBehaviour
{
    public static List<string> r = new List<string>();
    private static int status;
    public static bool fakej;
    public static float status_timer = Time.time;

    public void Update()
    {
        if (GlobalItems.myHero != null && GlobalItems.myHero.transform.position.y > 950)
            fakej = true;
        if (Input.GetKeyDown(KeyCode.J) || fakej)
        {
            //ResetLevel();
            fakej = false;
            PhotonNetwork.LeaveRoom();
            status = 1;
        }
        if (status == 1)
        {
            //Debug.Log("Status: 1");
            if (PhotonNetwork.connectionStateDetailed.ToString().Contains("Authenticated"))
                status = 2;
            PhotonNetwork.JoinRandomRoom();
        }
        else if (status == 2)
        {
            //Debug.Log("Status: 2");
            if (PhotonNetwork.connectionStateDetailed.ToString().Contains("Joined"))
            {
                status = 3;
                status_timer = Time.time;
            }
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            ResetLevel();
            var tree = (GameObject)Instantiate(LoadAsset.Tree.Load("tree"));
            tree.transform.localScale = new Vector3(200, 300, 150);
            tree.transform.position = new Vector3(0, 0, 0);
            tree.gameObject.layer = LayerMask.NameToLayer("Ground");
            var cloud = (GameObject)Instantiate(LoadAsset.Cloud.Load("cloud2"));
            cloud.transform.position = new Vector3(0, 1000, 0);
        }
    }

    public void OnGUI()
    {
        if (status != 1 && status != 2 && status != 3)
            return;
        NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").panelMultiROOM, false);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), LoadAsset.loading);
        GUI.DrawTexture(new Rect(Screen.width / 30, 0, Screen.width, Screen.height), LoadAsset.rModLogo);
        if (status == 3 && Time.time - status_timer > 2)
            status = 0;
        //Debug.Log("Status: " + Convert.ToString(status));
    }

    public void ResetLevel()
    {
        var count = 0;
        foreach (GameObject temp in FindObjectsOfType<GameObject>())
        {
            if (!temp.tag.ToLower().Contains("titan") && !temp.tag.ToLower().Contains("player") && !temp.name.ToLower().Contains("cube_002") && !temp.name.ToLower().Contains("cube_005") && !temp.name.ToLower().Contains("cube_003") && !temp.name.ToLower().Contains("cube_006") && !temp.name.ToLower().Contains("cube_020"))
                Console.addChat(temp.name);
            if (temp.name.ToLower().Contains("cube_002") || temp.name.ToLower().Contains("cube_005") || temp.name.ToLower().Contains("cube_003") || temp.name.ToLower().Contains("cube_006") || temp.name.ToLower().Contains("tree"))
            {
                if (count % 3 != 1)
                    Destroy(temp);
                count++;
            }
        }
    }

    public void Start()
    {
    }

    public static void Restart()
    {
    }
}