using mItems;
using UnityEngine;

public class SupportLogger : MonoBehaviour //todo useful
{
    public bool LogTrafficStats = true;

    public void Start()
    {
        if (CacheGameObject.Find("PunSupportLogger") != null)
            return;
        var target = new GameObject("PunSupportLogger");
        DontDestroyOnLoad(target);
        target.AddComponent<SupportLogging>().LogTrafficStats = LogTrafficStats;
    }
}