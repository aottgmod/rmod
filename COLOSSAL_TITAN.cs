using System;
using System.Collections;
using dItems;
using mItems;
using UnityEngine;
using MonoBehaviour = Photon.MonoBehaviour;
using Random = UnityEngine.Random;

public class COLOSSAL_TITAN : MonoBehaviour
{
    private string actionName;
    private string attackAnimation;
    private float attackCheckTime;
    private float attackCheckTimeA;
    private float attackCheckTimeB;
    private bool attackChkOnce;
    private int attackCount;
    private int attackPattern = -1;
    public GameObject bottomObject;
    private Transform checkHitCapsuleEnd;
    private Vector3 checkHitCapsuleEndOld;
    private float checkHitCapsuleR;
    private Transform checkHitCapsuleStart;
    public GameObject door_broken;
    public GameObject door_closed;
    public bool hasDie;
    private bool isSteamNeed;
    public static float minusDistance = 99999f;
    public static GameObject minusDistanceEnemy;
    public float myDistance;
    public GameObject myHero;
    public int NapeArmor = 0x2710;
    public int NapeArmorTotal = 0x2710;
    public GameObject neckSteamObject;
    private string state = "idle";
    public GameObject sweepSmokeObject;
    private float waitTime = 2f;
    
    /*private COLOSSAL_TITAN()
        : base(SPECIES.COLOSSAL_TITAN)
    {
    }*/

    private void attack_sweep(string type = "")
    {
        callTitanHAHA();
        state = "attack_sweep";
        attackAnimation = "sweep" + type;
        attackCheckTimeA = 0.4f;
        attackCheckTimeB = 0.57f;
        checkHitCapsuleStart = transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R");
        checkHitCapsuleEnd = transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/hand_R_001");
        checkHitCapsuleR = 20f;
        crossFade("attack_" + attackAnimation, 0.1f);
        attackChkOnce = false;
        sweepSmokeObject.GetComponent<ParticleSystem>().enableEmission = true;
        sweepSmokeObject.GetComponent<ParticleSystem>().Play();
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
        {
            if (FengGameManagerMKII.LAN)
            {
                if (Network.peerType != NetworkPeerType.Server)
                {
                }
            }
            else
            {
                if (PhotonNetwork.isMasterClient)
                    photonView.RPC("startSweepSmoke", PhotonTargets.Others);
            }
        }
    }

    private void Awake()
    {
        rigidbody.freezeRotation = true;
        rigidbody.useGravity = false;
        rigidbody.isKinematic = true;
        //CacheTransforms();
    }

    public void beTauntedBy(GameObject target, float tauntTime)
    {
    }

    public void blowPlayer(GameObject player, Transform neck)
    {
        var vector = new Vector3() -(neck.position + transform.forward * 50f - player.transform.position);
        var num = 20f;
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            player.GetComponent<HERO>().blowAway(vector.normalized * num + Vector3.up * 1f);
        else
        {
            if ((IN_GAME_MAIN_CAMERA.IsMultiplayer) && PhotonNetwork.isMasterClient)
            {
                var parameters = new object[] { vector.normalized * num + Vector3.up * 1f };
                player.GetComponent<HERO>().photonView.RPC("blowAway", PhotonTargets.All, parameters);
            }
        }
    }

    private void callTitan(bool special = false)
    {
        if (special || (GameObject.FindGameObjectsWithTag("titan").Length <= 6))
        {
            GameObject titan;
            var spawnPoints = GameObject.FindGameObjectsWithTag("titanRespawn");
            var points = new ArrayList();
            foreach (var spawnPoint in spawnPoints)
            {
                if (spawnPoint.transform.parent.name == "titanRespawnCT")
                    points.Add(spawnPoint);
            }
            var point = (GameObject)points[Random.Range(0, points.Count)];
            var titanModels = new[] { "TITAN_VER3.1" };
            if (FengGameManagerMKII.LAN)
                titan = (GameObject)Network.Instantiate(CacheResources.Load(titanModels[Random.Range(0, titanModels.Length)]), point.transform.position, point.transform.rotation, 0);
            else
                titan = PhotonNetwork.Instantiate(titanModels[Random.Range(0, titanModels.Length)], point.transform.position, point.transform.rotation, 0);
            if (special)
            {
                var objArray3 = GameObject.FindGameObjectsWithTag("route");
                var route = objArray3[Random.Range(0, objArray3.Length)];
                while (route.name != "routeCT")
                    route = objArray3[Random.Range(0, objArray3.Length)];
                titan.GetComponent<TITAN>().setRoute(route);
                titan.GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_I);
                titan.GetComponent<TITAN>().activeRad = 0;
                titan.GetComponent<TITAN>().toCheckPoint((Vector3) titan.GetComponent<TITAN>().checkPoints[0], 10f);
            }
            else
            {
                var num2 = 0.7f;
                var num3 = 0.7f;
                if (IN_GAME_MAIN_CAMERA.difficulty != 0)
                {
                    if (IN_GAME_MAIN_CAMERA.difficulty == 1)
                    {
                        num2 = 0.4f;
                        num3 = 0.7f;
                    }
                    else if (IN_GAME_MAIN_CAMERA.difficulty == 2)
                    {
                        num2 = -1f;
                        num3 = 0.7f;
                    }
                }
                if (GameObject.FindGameObjectsWithTag("titan").Length == 5)
                    titan.GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_JUMPER);
                else if (Random.Range(0f, 1f) >= num2)
                    titan.GetComponent<TITAN>().setAbnormalType(Random.Range(0f, 1f) < num3 ? AbnormalType.TYPE_JUMPER : AbnormalType.TYPE_CRAWLER);
                titan.GetComponent<TITAN>().activeRad = 200;
            }
            if (FengGameManagerMKII.LAN)
            {
                var spawnFX = (GameObject)Network.Instantiate(CacheResources.Load("FX/FXtitanSpawn"), titan.transform.position, Quaternion.Euler(-90f, 0f, 0f), 0);
                spawnFX.transform.localScale = titan.transform.localScale;
            }
            else
                PhotonNetwork.Instantiate("FX/FXtitanSpawn", titan.transform.position, Quaternion.Euler(-90f, 0f, 0f), 0).transform.localScale = titan.transform.localScale;
        }
    }

    private void callTitanHAHA()
    {
        attackCount++;
        var num = 4;
        var num2 = 7;
        if (IN_GAME_MAIN_CAMERA.difficulty != 0)
        {
            if (IN_GAME_MAIN_CAMERA.difficulty == 1)
            {
                num = 4;
                num2 = 6;
            }
            else if (IN_GAME_MAIN_CAMERA.difficulty == 2)
            {
                num = 3;
                num2 = 5;
            }
        }
        if (attackCount % num == 0)
            callTitan(false);
        if (NapeArmor < NapeArmorTotal * 0.3)
        {
            if (attackCount % (int) (num2 * 0.5f) == 0)
                callTitan(true);
        }
        else if (attackCount % num2 == 0)
            callTitan(true);
    }

    [RPC]
    private void changeDoor()
    {
        door_broken.SetActive(true);
        door_closed.SetActive(false);
    }

    private RaycastHit[] checkHitCapsule(Vector3 start, Vector3 end, float r)
    {
        return Physics.SphereCastAll(start, r, end - start, Vector3.Distance(start, end));
    }

    private GameObject checkIfHitHand(Transform hand)
    {
        var num = 30f;
        foreach (var collider in Physics.OverlapSphere(hand.GetComponent<SphereCollider>().transform.position, num + 1f))
        {
            if (collider.transform.root.CompareTag("Player"))
            {
                var go = collider.transform.root.gameObject;
                if (go.GetComponent<TITAN_EREN>() != null)
                {
                    if (!go.GetComponent<TITAN_EREN>().isHit)
                        go.GetComponent<TITAN_EREN>().hitByTitan();
                    return go;
                }
                if ((go.GetComponent<HERO>() != null) && !go.GetComponent<HERO>().isInvincible())
                    return go;
            }
        }
        return null;
    }

    private void crossFade(string aniName, float time)
    {
        animation.CrossFade(aniName, time);
        if (!FengGameManagerMKII.LAN && (IN_GAME_MAIN_CAMERA.IsMultiplayer) && PhotonNetwork.isMasterClient)
        {
            var parameters = new object[] { aniName, time };
            photonView.RPC("netCrossFade", PhotonTargets.Others, parameters);
        }
    }

    private void findNearestHero()
    {
        myHero = getNearestHero();
    }

    private GameObject getNearestHero()
    {
        var objArray = GameObject.FindGameObjectsWithTag("Player");
        GameObject obj2 = null;
        var positiveInfinity = float.PositiveInfinity;
        foreach (var obj3 in objArray)
        {
            if (((obj3.GetComponent<HERO>() == null) || !obj3.GetComponent<HERO>().HasDied()) && ((obj3.GetComponent<TITAN_EREN>() == null) || !obj3.GetComponent<TITAN_EREN>().hasDied))
            {
                var num3 = Mathf.Sqrt((obj3.transform.position.x - transform.position.x) * (obj3.transform.position.x - transform.position.x) + (obj3.transform.position.z - transform.position.z) * (obj3.transform.position.z - transform.position.z));
                if ((obj3.transform.position.y - transform.position.y < 450f) && (num3 < positiveInfinity))
                {
                    obj2 = obj3;
                    positiveInfinity = num3;
                }
            }
        }
        return obj2;
    }

    private void idle()
    {
        state = "idle";
        crossFade("idle", 0.2f);
    }

    private void kick()
    {
        state = "kick";
        actionName = "attack_kick_wall";
        attackCheckTime = 0.64f;
        attackChkOnce = false;
        crossFade(actionName, 0.1f);
    }

    private void killPlayer(GameObject hitHero)
    {
        if (hitHero != null)
        {
            var position = transform.Find("Amarture/Core/Controller_Body/hip/spine/chest").position;
            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            {
                if (!hitHero.GetComponent<HERO>().HasDied())
                    hitHero.GetComponent<HERO>().die((hitHero.transform.position - position) * 15f * 4f, false);
            }
            else
            {
                if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                {
                    if (FengGameManagerMKII.LAN)
                    {
                        if (!hitHero.GetComponent<HERO>().HasDied())
                            hitHero.GetComponent<HERO>().markDie();
                    }
                    else
                    {
                        if (!hitHero.GetComponent<HERO>().HasDied())
                        {
                            hitHero.GetComponent<HERO>().markDie();
                            var parameters = new object[] { (hitHero.transform.position - position) * 15f * 4f, false, -1, "Colossal Titan", true };
                            hitHero.GetComponent<HERO>().photonView.RPC("netDie", PhotonTargets.All, parameters);
                        }
                    }
                }
            }
        }
    }

    private void neckSteam()
    {
        neckSteamObject.GetComponent<ParticleSystem>().Stop();
        neckSteamObject.GetComponent<ParticleSystem>().Play();
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
        {
            if (FengGameManagerMKII.LAN)
            {
                if (Network.peerType != NetworkPeerType.Server)
                {
                }
            }
            else
            {
                if (PhotonNetwork.isMasterClient)
                    photonView.RPC("startNeckSteam", PhotonTargets.Others);
            }
        }
        isSteamNeed = true;
        var neck = transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
        var radius = 30f;
        foreach (var collider in Physics.OverlapSphere(neck.transform.position - transform.forward * 10f, radius))
        {
            if (collider.transform.root.tag == "Player")
            {
                var gameObject = collider.transform.root.gameObject;
                if ((gameObject.GetComponent<TITAN_EREN>() == null) && (gameObject.GetComponent<HERO>() != null))
                    blowPlayer(gameObject, neck);
            }
        }
    }

    [RPC]
    private void netCrossFade(string aniName, float time)
    {
        animation.CrossFade(aniName, time);
    }

    [RPC]
    public void netDie()
    {
        if (!hasDie)
            hasDie = true;
    }

    [RPC]
    private void netPlayAnimation(string aniName)
    {
        animation.Play(aniName);
    }

    [RPC]
    private void netPlayAnimationAt(string aniName, float normalizedTime)
    {
        animation.Play(aniName);
        animation[aniName].normalizedTime = normalizedTime;
    }

    private void OnDestroy()
    {
        if (CacheGameObject.Find("MultiplayerManager") != null)
            FengGameManagerMKII.MKII.removeCT(this);
    }

    private void playAnimation(string aniName)
    {
        animation.Play(aniName);
        if (!FengGameManagerMKII.LAN && (IN_GAME_MAIN_CAMERA.IsMultiplayer) && PhotonNetwork.isMasterClient)
        {
            var parameters = new object[] { aniName };
            photonView.RPC("netPlayAnimation", PhotonTargets.Others, parameters);
        }
    }

    private void playAnimationAt(string aniName, float normalizedTime)
    {
        animation.Play(aniName);
        animation[aniName].normalizedTime = normalizedTime;
        if (!FengGameManagerMKII.LAN && (IN_GAME_MAIN_CAMERA.IsMultiplayer) && PhotonNetwork.isMasterClient)
        {
            var parameters = new object[] { aniName, normalizedTime };
            photonView.RPC("netPlayAnimationAt", PhotonTargets.Others, parameters);
        }
    }

    private void playSound(string sndname)
    {
        playsoundRPC(sndname);
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
        {
            if (FengGameManagerMKII.LAN)
            {
                if (Network.peerType != NetworkPeerType.Server)
                {
                }
            }
            else
            {
                if (PhotonNetwork.isMasterClient)
                {
                    var parameters = new object[] { sndname };
                    photonView.RPC("playsoundRPC", PhotonTargets.Others, parameters);
                }
            }
        }
    }

    [RPC]
    private void playsoundRPC(string sndname)
    {
        transform.Find(sndname).GetComponent<AudioSource>().Play();
    }

    [RPC]
    private void removeMe()
    {
        Destroy(gameObject);
    }

    private void slap(string type)
    {
        callTitanHAHA();
        state = "slap";
        attackAnimation = type;
        if ((type == "r1") || (type == "r2"))
            checkHitCapsuleStart = transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/hand_R_001");
        if ((type == "l1") || (type == "l2"))
            checkHitCapsuleStart = transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/hand_L_001");
        attackCheckTime = 0.57f;
        attackChkOnce = false;
        crossFade("attack_slap_" + attackAnimation, 0.1f);
    }

    private void Start()
    {
        FengGameManagerMKII.MKII.addCT(this);
        if (myHero == null)
            findNearestHero();
        name = "COLOSSAL_TITAN";
        //RecheckTransforms();
        NapeArmor = 0x3e8;
        var flag = LevelInfo.getInfo(FengGameManagerMKII.level).respawnMode == RespawnMode.NEVER;
        if (IN_GAME_MAIN_CAMERA.difficulty == 0)
            NapeArmor = !flag ? 0x1388 : 0x7d0;
        else
        {
            if (IN_GAME_MAIN_CAMERA.difficulty == 1)
            {
                NapeArmor = !flag ? 0x1f40 : 0xdac;
                var enumerator = animation.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        var current = (AnimationState) enumerator.Current;
                        current.speed = 1.02f;
                    }
                }
                finally
                {
                    var disposable = enumerator as IDisposable;
                    if (disposable != null)
                        disposable.Dispose();
                }
            }
            else
            {
                if (IN_GAME_MAIN_CAMERA.difficulty == 2)
                {
                    NapeArmor = !flag ? 0x2ee0 : 0x1388;
                    var enumerator2 = animation.GetEnumerator();
                    try
                    {
                        while (enumerator2.MoveNext())
                        {
                            var state2 = (AnimationState) enumerator2.Current;
                            state2.speed = 1.05f;
                        }
                    }
                    finally
                    {
                        var disposable2 = enumerator2 as IDisposable;
                        if (disposable2 != null)
                            disposable2.Dispose();
                    }
                }
            }
        }
        NapeArmorTotal = NapeArmor;
        state = "wait";
        var transform = this.transform;
        transform.position += -Vector3.up * 10000f;
        if (FengGameManagerMKII.LAN)
            GetComponent<PhotonView>().enabled = false;
        else
            GetComponent<NetworkView>().enabled = false;
        door_broken = CacheGameObject.Find("door_broke");
        door_closed = CacheGameObject.Find("door_fine");
        door_broken.SetActive(false);
        door_closed.SetActive(true);
    }

    [RPC]
    private void startNeckSteam()
    {
        neckSteamObject.GetComponent<ParticleSystem>().Stop();
        neckSteamObject.GetComponent<ParticleSystem>().Play();
    }

    [RPC]
    private void startSweepSmoke()
    {
        sweepSmokeObject.GetComponent<ParticleSystem>().enableEmission = true;
        sweepSmokeObject.GetComponent<ParticleSystem>().Play();
    }

    private void steam()
    {
        callTitanHAHA();
        state = "steam";
        actionName = "attack_steam";
        attackCheckTime = 0.45f;
        crossFade(actionName, 0.1f);
        attackChkOnce = false;
    }

    [RPC]
    private void stopSweepSmoke()
    {
        sweepSmokeObject.GetComponent<ParticleSystem>().enableEmission = false;
        sweepSmokeObject.GetComponent<ParticleSystem>().Stop();
    }

    [RPC]
    public void titanGetHit(int viewID, int speed)
    {
        if (FengGameManagerMKII.LAN)
        {
        }
        var transform = this.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
        var view = PhotonView.Find(viewID);
        if (view != null)
        {
            var vector = view.gameObject.transform.position - transform.transform.position;
            if (vector.magnitude < 40f)
            {
                NapeArmor -= speed;
                neckSteam();
                if (NapeArmor <= 0)
                {
                    NapeArmor = 0;
                    if (!hasDie)
                    {
                        if (FengGameManagerMKII.LAN)
                            netDie();
                        else
                        {
                            photonView.RPC("netDie", PhotonTargets.OthersBuffered);
                            netDie();
                            FengGameManagerMKII.MKII.titanGetKill(view.owner, speed, name);
                        }
                    }
                }
                else
                {
                    FengGameManagerMKII.MKII.sendKillInfo(false, (string) view.owner.customProperties[PhotonPlayerProperty.name], true, "Colossal Titan's neck", speed);
                    var parameters = new object[] { speed };
                    FengGameManagerMKII.MKII.photonView.RPC("netShowDamage", view.owner, parameters);
                }
            }
        }
    }

    public void update()
    {
        if (state != "null")
        {
            if (state == "wait")
            {
                waitTime -= Time.deltaTime;
                if (waitTime <= 0f)
                {
                    transform.position = new Vector3(30f, 0f, 784f);
                    Instantiate(CacheResources.Load("FX/ThunderCT"), transform.position + Vector3.up * 350f, Quaternion.Euler(270f, 0f, 0f));
                    IN_GAME_MAIN_CAMERA.maincamera.flashBlind();
                    if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                        idle();
                    else
                    {
                        if (!FengGameManagerMKII.LAN ? photonView.isMine : networkView.isMine)
                            idle();
                        else
                            state = "null";
                    }
                }
            }
            else
            {
                if (!(state == "idle"))
                {
                    if (state == "attack_sweep")
                    {
                        if ((attackCheckTimeA != 0f) && (((animation["attack_" + attackAnimation].normalizedTime >= attackCheckTimeA) && (animation["attack_" + attackAnimation].normalizedTime <= attackCheckTimeB)) || (!attackChkOnce && (animation["attack_" + attackAnimation].normalizedTime >= attackCheckTimeA))))
                        {
                            if (!attackChkOnce)
                                attackChkOnce = true;
                            foreach (var hit in checkHitCapsule(checkHitCapsuleStart.position, checkHitCapsuleEnd.position, checkHitCapsuleR))
                            {
                                var gameObject = hit.collider.gameObject;
                                if (gameObject.tag == "Player")
                                    killPlayer(gameObject);
                                if ((gameObject.tag == "erenHitbox") && (attackAnimation == "combo_3") && (IN_GAME_MAIN_CAMERA.IsMultiplayer) && (!FengGameManagerMKII.LAN ? PhotonNetwork.isMasterClient : Network.isServer))
                                    gameObject.transform.root.gameObject.GetComponent<TITAN_EREN>().hitByFTByServer(3);
                            }
                            foreach (var hit2 in checkHitCapsule(checkHitCapsuleEndOld, checkHitCapsuleEnd.position, checkHitCapsuleR))
                            {
                                var hitHero = hit2.collider.gameObject;
                                if (hitHero.tag == "Player")
                                    killPlayer(hitHero);
                            }
                            checkHitCapsuleEndOld = checkHitCapsuleEnd.position;
                        }
                        if (animation["attack_" + attackAnimation].normalizedTime >= 1f)
                        {
                            sweepSmokeObject.GetComponent<ParticleSystem>().enableEmission = false;
                            sweepSmokeObject.GetComponent<ParticleSystem>().Stop();
                            if ((IN_GAME_MAIN_CAMERA.IsMultiplayer) && !FengGameManagerMKII.LAN)
                                photonView.RPC("stopSweepSmoke", PhotonTargets.Others);
                            findNearestHero();
                            idle();
                            playAnimation("idle");
                        }
                    }
                    else
                    {
                        if (state == "kick")
                        {
                            if (!attackChkOnce && (animation[actionName].normalizedTime >= attackCheckTime))
                            {
                                attackChkOnce = true;
                                door_broken.SetActive(true);
                                door_closed.SetActive(false);
                                if ((IN_GAME_MAIN_CAMERA.IsMultiplayer) && !FengGameManagerMKII.LAN)
                                    photonView.RPC("changeDoor", PhotonTargets.OthersBuffered);
                                if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                                {
                                    if (FengGameManagerMKII.LAN)
                                    {
                                        Network.Instantiate(CacheResources.Load("FX/boom1_CT_KICK"), transform.position + transform.forward * 120f + transform.right * 30f, Quaternion.Euler(270f, 0f, 0f), 0);
                                        Network.Instantiate(CacheResources.Load("rock"), transform.position + transform.forward * 120f + transform.right * 30f, Quaternion.Euler(0f, 0f, 0f), 0);
                                    }
                                    else
                                    {
                                        PhotonNetwork.Instantiate("FX/boom1_CT_KICK", transform.position + transform.forward * 120f + transform.right * 30f, Quaternion.Euler(270f, 0f, 0f), 0);
                                        PhotonNetwork.Instantiate("rock", transform.position + transform.forward * 120f + transform.right * 30f, Quaternion.Euler(0f, 0f, 0f), 0);
                                    }
                                }
                                else
                                {
                                    Instantiate(CacheResources.Load("FX/boom1_CT_KICK"), transform.position + transform.forward * 120f + transform.right * 30f, Quaternion.Euler(270f, 0f, 0f));
                                    Instantiate(CacheResources.Load("rock"), transform.position + transform.forward * 120f + transform.right * 30f, Quaternion.Euler(0f, 0f, 0f));
                                }
                            }
                            if (animation[actionName].normalizedTime >= 1f)
                            {
                                findNearestHero();
                                idle();
                                playAnimation("idle");
                            }
                        }
                        else
                        {
                            if (state == "slap")
                            {
                                if (!attackChkOnce && (animation["attack_slap_" + attackAnimation].normalizedTime >= attackCheckTime))
                                {
                                    GameObject obj4;
                                    attackChkOnce = true;
                                    if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                                    {
                                        if (FengGameManagerMKII.LAN)
                                            obj4 = (GameObject) Network.Instantiate(CacheResources.Load("FX/boom1"), checkHitCapsuleStart.position, Quaternion.Euler(270f, 0f, 0f), 0);
                                        else
                                            obj4 = PhotonNetwork.Instantiate("FX/boom1", checkHitCapsuleStart.position, Quaternion.Euler(270f, 0f, 0f), 0);
                                        if (obj4.GetComponent<EnemyfxIDcontainer>() != null)
                                            obj4.GetComponent<EnemyfxIDcontainer>().titanName = name;
                                    }
                                    else
                                        obj4 = (GameObject) Instantiate(CacheResources.Load("FX/boom1"), checkHitCapsuleStart.position, Quaternion.Euler(270f, 0f, 0f));
                                    obj4.transform.localScale = new Vector3(5f, 5f, 5f);
                                }
                                if (animation["attack_slap_" + attackAnimation].normalizedTime >= 1f)
                                {
                                    findNearestHero();
                                    idle();
                                    playAnimation("idle");
                                }
                            }
                            else
                            {
                                if (state == "steam")
                                {
                                    if (!attackChkOnce && (animation[actionName].normalizedTime >= attackCheckTime))
                                    {
                                        attackChkOnce = true;
                                        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                                        {
                                            if (FengGameManagerMKII.LAN)
                                            {
                                                Network.Instantiate(CacheResources.Load("FX/colossal_steam"), transform.position + transform.up * 185f, Quaternion.Euler(270f, 0f, 0f), 0);
                                                Network.Instantiate(CacheResources.Load("FX/colossal_steam"), transform.position + transform.up * 303f, Quaternion.Euler(270f, 0f, 0f), 0);
                                                Network.Instantiate(CacheResources.Load("FX/colossal_steam"), transform.position + transform.up * 50f, Quaternion.Euler(270f, 0f, 0f), 0);
                                            }
                                            else
                                            {
                                                PhotonNetwork.Instantiate("FX/colossal_steam", transform.position + transform.up * 185f, Quaternion.Euler(270f, 0f, 0f), 0);
                                                PhotonNetwork.Instantiate("FX/colossal_steam", transform.position + transform.up * 303f, Quaternion.Euler(270f, 0f, 0f), 0);
                                                PhotonNetwork.Instantiate("FX/colossal_steam", transform.position + transform.up * 50f, Quaternion.Euler(270f, 0f, 0f), 0);
                                            }
                                        }
                                        else
                                        {
                                            Instantiate(CacheResources.Load("FX/colossal_steam"), transform.position + transform.forward * 185f, Quaternion.Euler(270f, 0f, 0f));
                                            Instantiate(CacheResources.Load("FX/colossal_steam"), transform.position + transform.forward * 303f, Quaternion.Euler(270f, 0f, 0f));
                                            Instantiate(CacheResources.Load("FX/colossal_steam"), transform.position + transform.forward * 50f, Quaternion.Euler(270f, 0f, 0f));
                                        }
                                    }
                                    if (animation[actionName].normalizedTime >= 1f)
                                    {
                                        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                                        {
                                            if (FengGameManagerMKII.LAN)
                                            {
                                                Network.Instantiate(CacheResources.Load("FX/colossal_steam_dmg"), transform.position + transform.up * 185f, Quaternion.Euler(270f, 0f, 0f), 0);
                                                Network.Instantiate(CacheResources.Load("FX/colossal_steam_dmg"), transform.position + transform.up * 303f, Quaternion.Euler(270f, 0f, 0f), 0);
                                                Network.Instantiate(CacheResources.Load("FX/colossal_steam_dmg"), transform.position + transform.up * 50f, Quaternion.Euler(270f, 0f, 0f), 0);
                                            }
                                            else
                                            {
                                                var obj5 = PhotonNetwork.Instantiate("FX/colossal_steam_dmg", transform.position + transform.up * 185f, Quaternion.Euler(270f, 0f, 0f), 0);
                                                if (obj5.GetComponent<EnemyfxIDcontainer>() != null)
                                                    obj5.GetComponent<EnemyfxIDcontainer>().titanName = name;
                                                obj5 = PhotonNetwork.Instantiate("FX/colossal_steam_dmg", transform.position + transform.up * 303f, Quaternion.Euler(270f, 0f, 0f), 0);
                                                if (obj5.GetComponent<EnemyfxIDcontainer>() != null)
                                                    obj5.GetComponent<EnemyfxIDcontainer>().titanName = name;
                                                obj5 = PhotonNetwork.Instantiate("FX/colossal_steam_dmg", transform.position + transform.up * 50f, Quaternion.Euler(270f, 0f, 0f), 0);
                                                if (obj5.GetComponent<EnemyfxIDcontainer>() != null)
                                                    obj5.GetComponent<EnemyfxIDcontainer>().titanName = name;
                                            }
                                        }
                                        else
                                        {
                                            Instantiate(CacheResources.Load("FX/colossal_steam_dmg"), transform.position + transform.forward * 185f, Quaternion.Euler(270f, 0f, 0f));
                                            Instantiate(CacheResources.Load("FX/colossal_steam_dmg"), transform.position + transform.forward * 303f, Quaternion.Euler(270f, 0f, 0f));
                                            Instantiate(CacheResources.Load("FX/colossal_steam_dmg"), transform.position + transform.forward * 50f, Quaternion.Euler(270f, 0f, 0f));
                                        }
                                        if (hasDie)
                                        {
                                            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                                                Destroy(gameObject);
                                            else
                                            {
                                                if (FengGameManagerMKII.LAN)
                                                {
                                                    if (!networkView.isMine)
                                                    {
                                                    }
                                                }
                                                else
                                                {
                                                    if (PhotonNetwork.isMasterClient)
                                                        PhotonNetwork.Destroy(photonView);
                                                }
                                            }
                                            FengGameManagerMKII.MKII.gameWin();
                                        }
                                        findNearestHero();
                                        idle();
                                        playAnimation("idle");
                                    }
                                }
                                else
                                {
                                    if (state == string.Empty)
                                    {
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (attackPattern == -1)
                    {
                        slap("r1");
                        attackPattern++;
                    }
                    else
                    {
                        if (attackPattern == 0)
                        {
                            attack_sweep(string.Empty);
                            attackPattern++;
                        }
                        else
                        {
                            if (attackPattern == 1)
                            {
                                steam();
                                attackPattern++;
                            }
                            else
                            {
                                if (attackPattern == 2)
                                {
                                    kick();
                                    attackPattern++;
                                }
                                else
                                {
                                    if (isSteamNeed || hasDie)
                                    {
                                        steam();
                                        isSteamNeed = false;
                                    }
                                    else
                                    {
                                        if (myHero == null)
                                            findNearestHero();
                                        else
                                        {
                                            var vector = myHero.transform.position - transform.position;
                                            var current = -Mathf.Atan2(vector.z, vector.x) * 57.29578f;
                                            var f = -Mathf.DeltaAngle(current, gameObject.transform.rotation.eulerAngles.y - 90f);
                                            myDistance = Mathf.Sqrt((myHero.transform.position.x - transform.position.x) * (myHero.transform.position.x - transform.position.x) + (myHero.transform.position.z - transform.position.z) * (myHero.transform.position.z - transform.position.z));
                                            var num3 = myHero.transform.position.y - transform.position.y;
                                            if ((myDistance < 85f) && (Random.Range(0, 100) < 5))
                                                steam();
                                            else
                                            {
                                                if ((num3 > 310f) && (num3 < 350f))
                                                {
                                                    if (Vector3.Distance(myHero.transform.position, transform.Find("APL1").position) < 40f)
                                                    {
                                                        slap("l1");
                                                        return;
                                                    }
                                                    if (Vector3.Distance(myHero.transform.position, transform.Find("APL2").position) < 40f)
                                                    {
                                                        slap("l2");
                                                        return;
                                                    }
                                                    if (Vector3.Distance(myHero.transform.position, transform.Find("APR1").position) < 40f)
                                                    {
                                                        slap("r1");
                                                        return;
                                                    }
                                                    if (Vector3.Distance(myHero.transform.position, transform.Find("APR2").position) < 40f)
                                                    {
                                                        slap("r2");
                                                        return;
                                                    }
                                                    if ((myDistance < 150f) && (Mathf.Abs(f) < 80f))
                                                    {
                                                        attack_sweep(string.Empty);
                                                        return;
                                                    }
                                                }
                                                if ((num3 < 300f) && (Mathf.Abs(f) < 80f) && (myDistance < 85f))
                                                    attack_sweep("_vertical");
                                                else
                                                {
                                                    switch (Random.Range(0, 7))
                                                    {
                                                        case 0:
                                                            slap("l1");
                                                            break;

                                                        case 1:
                                                            slap("l2");
                                                            break;

                                                        case 2:
                                                            slap("r1");
                                                            break;

                                                        case 3:
                                                            slap("r2");
                                                            break;

                                                        case 4:
                                                            attack_sweep(string.Empty);
                                                            break;

                                                        case 5:
                                                            attack_sweep("_vertical");
                                                            break;

                                                        case 6:
                                                            steam();
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}