﻿using System.Collections.Generic;
using mItems;
using UnityEngine;

class HoverCraft : MonoBehaviour
{
    public static bool IsActive;

    //input
    private string xAxis = "Horizontal";
    private string zAxis = "Vertical";
    private string yaw = "Horizontal2";
    private Vector3 input;

    //thrust & torque
    public float HoverThrust = 11000f;       //2500
    public float ForwardThrust = 4000f;      //200
    public float RotationTorque = 1300f;      //75

    private float appliedForwardThrust;
    private float appliedHoverThrust;

    //advanced
    public float StrafeForceRatio = .66f;   //.66f
    public float SlopeCompensation = 1.5f;  //1.5f

    private readonly List<Transform> rayCasters = new List<Transform>();
    private Transform CenterOfMass;
    private float currentDistance;
    public float HoverDistance = 2f;        //2f
    public float GroundDistance = 2.5f;     //2.5f
    private LayerMask GroundLayer;

    //extra
    private bool ClampTilt = true;
    private float MaxtTilt = 30f;           //45f
    private bool AirControl = false;
    private bool IdleBraking = false;
    public float IdleBrakeFactor = 1.5f;    //1.5f
    private bool boost; //todo

    private GameObject pilot;
    private Rigidbody craftBody;

    private bool grounded;

    private static bool debug = true;

    void Start()
    {
        IsActive = true;
        GroundLayer = dItems.Layer.Ground;
        
        pilot = transform.FindChild("AOTTG_HERO 1(Clone)").gameObject;
        foreach (var child in GetComponentsInChildren<Transform>())
            if (child.name.StartsWith("HoverCraft"))
                rayCasters.Add(child);

        craftBody = gameObject.AddComponent<Rigidbody>();
        craftBody.mass = 100;        //25
        craftBody.drag = .25f;      //0.15f
        craftBody.angularDrag = 2f; //2f - drag applied to rotation
        craftBody.useGravity = true;
        

        CenterOfMass = craftBody.transform;
    }

    void Update()
    {
        //pilot.transform.localScale = pilot.transform.localScale;
        //pilot.transform.localScale = new Vector3(1f, 2f, 0.5f);
        pilot.transform.rotation = transform.rotation;
    }

    void OnDestroy()
    {
        IsActive = false;
    }

    void OnGUI()
    {
        if (!debug)
            return;
        GUI.Label(new Rect(500, 100, 200, 100), "<color=red><b>Craft X:</b></color> " + input.x);
        GUI.Label(new Rect(500, 120, 200, 100), "<color=red><b>Craft Y:</b></color> " + input.y);
        GUI.Label(new Rect(500, 140, 200, 100), "<color=red><b>Craft Z:</b></color> " + input.z);

        GUI.Label(new Rect(500, 170, 400, 100), "<color=red><b>CenterOfMass:</b></color> " + CenterOfMass.position);
        GUI.Label(new Rect(500, 190, 200, 100), "<color=red><b>Grounded:</b></color> " + grounded);
        GUI.Label(new Rect(500, 210, 200, 100), "<color=red><b>Distance:</b></color> " + currentDistance);
        GUI.Label(new Rect(500, 235, 200, 100), "<color=red><b>AppliedHoverThrust:</b></color> " + appliedHoverThrust);
        GUI.Label(new Rect(500, 255, 200, 100), "<color=red><b>AppliedForwardThrust:</b></color> " + appliedForwardThrust);
    }

    private void FixedUpdate()
    {
        //input = new Vector3(Input.GetAxis(xAxis), Input.GetAxis(yaw), Input.GetAxis(zAxis));
        myInput();
        GroundCheck();
        HoverNormal();
        ProcessInput(input);
        ClampRotation();
        IdleDamp(input);
        
        CenterOfMass.rigidbody.AddForce(Vector3.down * 15f, ForceMode.Acceleration);
    }

    void myInput()
    {
        var x = 0f;
        var y = 0f;
        var z = 0f;
        if (!IN_GAME_MAIN_CAMERA.isTyping)
        {
            if (Input.GetKey(KeyCode.W))
                z = 1f;
            else if (Input.GetKey(KeyCode.S))
                z = -1f;
            if (Input.GetKey(KeyCode.Q))
                x = -1f;
            else if (Input.GetKey(KeyCode.E))
                x = 1f;
            if (Input.GetKey(KeyCode.D))
                y = 1f;
            else if (Input.GetKey(KeyCode.A))
                y = -1f;
            boost = Input.GetKey(KeyCode.Space);
        }
        input = new Vector3(x, y, z);
    }

    private void GroundCheck()
    {
        var ray = new Ray(CenterOfMass.position, -CenterOfMass.up);
        RaycastHit hitInfo;

        grounded = Physics.Raycast(ray, out hitInfo, GroundDistance, GroundLayer);
    }

    private void HoverNormal()
    {
        var ray = new Ray(CenterOfMass.position, -CenterOfMass.up);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo, HoverDistance, GroundLayer))
        {
            currentDistance = Vector3.Distance(CenterOfMass.position, hitInfo.point);
            Vector3 upVector;
            //check if the surface is sloped or flat and compensate
            if (Vector3.Magnitude(Quaternion.FromToRotation(Vector3.up, hitInfo.normal).eulerAngles) > 0)
            {
                appliedForwardThrust = ForwardThrust * SlopeCompensation;
                appliedHoverThrust = HoverThrust * (SlopeCompensation / 2f);
                upVector = transform.up;
            }
            else
            {
                appliedForwardThrust = ForwardThrust;
                appliedHoverThrust = HoverThrust;
                upVector = Vector3.up;
            }

            if (currentDistance < HoverDistance)
            {
                craftBody.AddForce(upVector * appliedHoverThrust * (1f - currentDistance / HoverDistance));
                craftBody.rotation = Quaternion.Slerp(craftBody.rotation, Quaternion.FromToRotation(transform.up, GetAverageNormal(rayCasters, GroundDistance)) * craftBody.rotation, Time.fixedDeltaTime * 3.75f);
                //craftBody.rotation = Quaternion.Slerp(craftBody.rotation, Quaternion.FromToRotation(transform.up, hitInfo.normal) * craftBody.rotation, Time.fixedDeltaTime * 3.75f);
            }
        }
    }

    public void ProcessInput(Vector3 input)
    {
        if (grounded || AirControl)
        {
            craftBody.AddRelativeForce(Vector3.forward * input.z * appliedForwardThrust, ForceMode.Force);
            craftBody.AddRelativeForce(Vector3.right * input.x * (ForwardThrust * StrafeForceRatio), ForceMode.Force);
            craftBody.AddRelativeTorque(Vector3.up * RotationTorque * input.y, ForceMode.Force);
        }
    }

    //for when the craft has multiple parts
    private Vector3 GetAverageNormal(IEnumerable<Transform> casters, float distance)
    {
        var normals = new List<Vector3>();
        var sumNormals = Vector3.zero;

        foreach (var caster in casters)
        {
            var ray = new Ray(caster.position, -caster.up);
            RaycastHit hitInfo;

            if (Physics.Raycast(ray, out hitInfo, distance, GroundLayer))
                normals.Add(hitInfo.normal);
        }

        foreach (var normal in normals)
            sumNormals += normal;

        return sumNormals / normals.Count;
    }

    private void ClampRotation()
    {
        if (ClampTilt)
        {
            var eulerAngles = craftBody.rotation.eulerAngles;
            eulerAngles = FixAngles(eulerAngles);
            eulerAngles = new Vector3(Mathf.Clamp(eulerAngles.x, -MaxtTilt, MaxtTilt), eulerAngles.y, Mathf.Clamp(eulerAngles.z, -MaxtTilt, MaxtTilt));
            craftBody.rotation = Quaternion.Euler(eulerAngles);
        }
    }

    private void IdleDamp(Vector3 input)
    {
        if (input.x + input.z == 0 && IdleBraking)
            craftBody.velocity = Vector3.Lerp(craftBody.velocity, Vector3.zero, Time.deltaTime * IdleBrakeFactor);
    }

    //fuck this tbh
    private Vector3 FixAngles(Vector3 angles)
    {
        if (angles.x > 180)
            angles.x -= 360;
        if (angles.x < -180)
            angles.x += 360;
        if (angles.y > 180)
            angles.y -= 360;
        if (angles.y < 180)
            angles.y += 360;
        if (angles.z > 180)
            angles.z -= 360;
        if (angles.z < -180)
            angles.z += 360;
        return angles;
    }
}