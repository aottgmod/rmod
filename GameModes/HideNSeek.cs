﻿using System;
using System.Collections.Generic;
using mItems;
using UnityEngine;

internal class HideNSeek : Photon.MonoBehaviour
{
    public static int game_starting;
    public static float game_countdown;
    public static float game_time = 15;
    public static List<int> player_ids = new List<int>();
    public static int seeker;
    public static bool is_ahss;
    public static bool is_caught;
    public static GameObject box;
    public static int down;
    public static List<int> caught = new List<int>();
    public static PhotonView rphotonview;

    public void First()
    {
        if (Input.GetKeyDown(KeyCode.J)) //adds id to list of players
        {
            LoadAsset.myHero = CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").main_object;
            photonView.RPC("JoinList", PhotonTargets.All, PhotonNetwork.player.ID);
        }
        if ((player_ids.Count > 1) && PhotonNetwork.isMasterClient && Input.GetKeyDown(KeyCode.H)) //mc starts the match
        {
            seeker = player_ids[UnityEngine.Random.Range(0, player_ids.Count)];
            photonView.RPC("SetGame", PhotonTargets.All, seeker, 1);
        }
    }

    public void Second()
    {
        if (is_ahss && (LoadAsset.myHero != null))
        {
            LoadAsset.myHero.transform.position = GameObject.FindGameObjectsWithTag("playerRespawn")[0].transform.position + new Vector3(0, 150, 0);
            box.transform.position = LoadAsset.myHero.transform.position + new Vector3(0, -5, 0);
        }
        if ((game_starting < 3) && (game_time - (Time.time - game_countdown) < 10.5f) && (game_time - (Time.time - game_countdown) > 5.5f))
        {
            if (is_ahss)
            {
                foreach (HERO temp in FengGameManagerMKII.heroes)
                    temp.myNetWorkName.GetComponent<UILabel>().text = "";
            }
            game_starting = 2;
        }
        else if ((game_starting < 3) && (game_time - (Time.time - game_countdown) < 5.5f))
        {
            game_starting = 3;
        }
        else if (game_starting >= 3)
        {
            if (!(Time.time - game_countdown > 1))
                return;
            game_countdown = Time.time;
            if (PhotonNetwork.isMasterClient)
                photonView.RPC("SetGame", PhotonTargets.All, 0, game_starting + 1);
        }
    }

    public void Third()
    {
        if (box == null)
            return;
        if (is_ahss)
        {
            PhotonNetwork.Instantiate("FX/Thunder", LoadAsset.myHero.transform.position, LoadAsset.myHero.transform.rotation, 0);
            LoadAsset.myHero.transform.position = GameObject.FindGameObjectsWithTag("playerRespawn")[0].transform.position;
            PhotonNetwork.Instantiate("FX/Thunder", LoadAsset.myHero.transform.position, LoadAsset.myHero.transform.rotation, 0);
        }
        Destroy(box);
    }

    public void Update()
    {
        if (game_starting == 0)
            First();
        else if (game_starting < 8)
            Second();
        else
            Third();
    }

    public void OnGUI()
    {
        if (game_starting == 1)
        {
            if (PhotonNetwork.player.ID != seeker)
                GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, Screen.width / 2, Screen.height / 2), "Go Hide!\n Id " + Convert.ToString(seeker) + " is the seeker");
            else
                GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, Screen.width / 2, Screen.height / 2), "You're the seeker!");
        }
        else
        {
            if (game_starting == 2)
            {
                GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, Screen.width / 2, Screen.height / 2), "Game Starting in 10 Seconds");
            }
            else
            {
                if ((game_starting > 3) && (game_starting < 8))
                    GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, Screen.width / 2, Screen.height / 2), Convert.ToString(9 - game_starting));
            }
        }
    }

    public void Start()
    {
        game_starting = 0;
        game_time = 15;
        player_ids = new List<int>();
        caught = new List<int>();
        is_ahss = false;
        is_caught = false;
        rphotonview = photonView;
        down = 0;
        if (box != null)
            Destroy(box);
    }

    public static void Restart()
    {
        game_starting = 0;
        game_time = 15;
        player_ids = new List<int>();
        caught = new List<int>();
        is_ahss = false;
        is_caught = false;
        down = 0;
        if (box != null)
            Destroy(box);
        Modules.Console.addChat("Reset");
    }

    public GameObject CreateBox()
    {
        var temp = GameObject.CreatePrimitive(PrimitiveType.Cube);
        temp.transform.localScale = new Vector3(20, 1, 20);
        var temp1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        temp1.transform.localScale = new Vector3(20, 20, 1);
        var temp2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        temp2.transform.localScale = new Vector3(20, 20, 1);
        var temp3 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        temp3.transform.localScale = new Vector3(1, 20, 20);
        var temp4 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        temp4.transform.localScale = new Vector3(1, 20, 20);

        temp.transform.position += new Vector3(0, -20, 0);
        temp1.transform.position += new Vector3(0, -10, -10);
        temp1.transform.parent = temp.transform;
        temp2.transform.position += new Vector3(0, -10, 10);
        temp2.transform.parent = temp.transform;
        temp3.transform.position += new Vector3(-10, -10, 0);
        temp3.transform.parent = temp.transform;
        temp4.transform.position += new Vector3(10, -10, 0);
        temp4.transform.parent = temp.transform;
        return temp;

    }

    [RPC]
    public void SetGame(int a, int b)
    {
        if (a != 0)
        {
            seeker = a;
            if (b == 1)
            {
                if (a == PhotonNetwork.player.ID)
                {
                    is_ahss = true;
                    FengGameManagerMKII.spawnahss = true;
                }
                else
                {
                    FengGameManagerMKII.spawnblade = true;
                    //rImports.myHero.transform.position = GameObject.FindGameObjectsWithTag("playerRespawn")[0].transform.position;
                }
                //CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").gameObject.transform.position = GameObject.FindGameObjectsWithTag("playerRespawn")[0].transform.position;
                box = CreateBox();
                box.transform.position = GameObject.FindGameObjectsWithTag("playerRespawn")[0].transform.position + new Vector3(0, 150, 0);
                game_countdown = Time.time;
            }
        }
        game_starting = b;
    }

    [RPC]
    public void Catch(int a)
    {
        caught.Add(a);
        if (PhotonNetwork.player.ID == a)
            is_caught = true;
        Modules.Console.addChat("ID " + Convert.ToString(a) + " has been caught");
    }

    [RPC]
    public void JoinList(int a)
    {
        player_ids.Add(a);
        if (PhotonNetwork.player.ID == a)
            Modules.Console.addChat("Joined match");
        else
            Modules.Console.addChat("ID " + Convert.ToString(a) + " Joined");   
    }
}