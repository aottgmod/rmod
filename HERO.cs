using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using dItems;
using mItems;
using Modules;
using Season2;
using UnityEngine;
using Xft;
using Console = Modules.Console;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using MonoBehaviour = Photon.MonoBehaviour;
using Random = UnityEngine.Random;
using Sprite = UnityEngine.Sprite;

public class HERO : MonoBehaviour
{
    private HERO_STATE _state;
    private bool almostSingleHook;
    private string attackAnimation;
    public List<TITAN> myTitans;
    private int attackLoop;
    private bool attackMove;
    private bool attackReleased;
    public AudioSource audio_ally;
    public AudioSource audio_hitwall;
    private GameObject badGuy;
    private float buffTime;
    public GameObject bulletLeft;
    private int bulletMAX = 7;
    public GameObject bulletRight;
    private bool buttonAttackRelease;
    public bool canJump = true;
    public GameObject checkBoxLeft;
    public GameObject checkBoxRight;
    public string currentAnimation;
    private int currentBladeNum = 5;
    private float currentBladeSta = 100f;
    private BUFF currentBuff;
    public Camera currentCamera;
    private float currentGas = 100f;
    public float currentSpeed;
    private bool dashD;
    private Vector3 dashDirection;
    private bool dashL;
    private bool dashR;
    private float dashTime;
    private bool dashU;
    private Vector3 dashV;
    private float dTapTime = -1f;
    private bool EHold;
    private GameObject eren_titan;
    private int escapeTimes = 1;
    private float facingDirection;
    private float flare1CD;
    private float flare2CD;
    private float flare3CD;
    private float flareTotalCD = 30f;
    private Transform forearmL;
    private Transform forearmR;
    private float gravity = 20f;
    private bool grounded;
    private GameObject gunDummy;
    private Vector3 gunTarget;
    private Transform handL;
    private Transform handR;
    public bool hasDied;
    private bool hookBySomeOne = true;
    public GameObject hookRefL1;
    public GameObject hookRefL2;
    public GameObject hookRefR1;
    public GameObject hookRefR2;
    private bool hookSomeOne;
    private GameObject hookTarget;
    public FengCustomInputs inputManager;
    private float invincible = 3f;
    private bool isLaunchLeft;
    private bool isLaunchRight;
    private bool isLeftHandHooked;
    public bool isMounted;
    private bool isRightHandHooked;
    public float jumpHeight = 2f;
    private bool justGrounded;
    public Transform lastHook;
    private float launchElapsedTimeL;
    private float launchElapsedTimeR;
    private Vector3 launchForce;
    private Vector3 launchPointLeft;
    private Vector3 launchPointRight;
    private bool leanLeft;
    private bool leftArmAim;
    public XWeaponTrail leftbladetrail;
    public XWeaponTrail leftbladetrail2;
    private int leftBulletLeft = 7;
    private bool leftGunHasBullet = true;
    private float lTapTime = -1f;
    public float maxVelocityChange = 10f;
    public AudioSource meatDie;
    public GROUP myGroup;
    private GameObject myHorse;
    public GameObject myNetWorkName;
    public float myScale = 1f;
    public int myTeam = 1;
    private bool needLean;
    private Quaternion oldHeadRotation;
    private float originVM;
    private bool QHold;
    private string reloadAnimation = string.Empty;
    private bool rightArmAim;
    public XWeaponTrail rightbladetrail;
    public XWeaponTrail rightbladetrail2;
    private int rightBulletLeft = 7;
    private bool rightGunHasBullet = true;
    public AudioSource rope;
    private float rTapTime = -1f;
    public HERO_SETUP setup;
    private GameObject skillCD;
    public float skillCDDuration;
    public float skillCDLast;
    private string skillId;
    public AudioSource slash;
    public AudioSource slashHit;
    public ParticleSystem smoke_3dmg;
    private ParticleSystem sparks;
    public float speed = 10f;
    public GameObject speedFX;
    public GameObject speedFX1;
    private ParticleSystem speedFXPS;
    private string standAnimation = "stand";
    private Quaternion targetHeadRotation;
    private Quaternion targetRotation;
    private bool throwedBlades;
    public bool titanForm;
    private GameObject titanWhoGrabMe;
    private int titanWhoGrabMeID;
    private int totalBladeNum = 5;
    public float totalBladeSta = 100f;
    public float totalGas = 100f;
    private Transform upperarmL;
    private Transform upperarmR;
    private float useGasSpeed = 0.2f;
    public bool useGun;
    private float uTapTime = -1f;
    private bool wallJump;
    private float wallRunTime;

    private Transform myTransform;

    public static Vector3 focusPoint;

    public static Texture horse;

    private bool canLeftReel;
    private bool canRightReel;
    private float contValue;

    private float reelthreshold;

    private static Color bottom_color_default;

    private bool spinning { get; set; }

    private void applyForceToBody(GameObject GO, Vector3 v)
    {
        GO.rigidbody.AddForce(v);
        GO.rigidbody.AddTorque(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f));
    }

    public void attackAccordingToMouse()
    {
        attackAnimation = Input.mousePosition.x < Screen.width * 0.5 ? "attack2" : "attack1";
    }

    public void attackAccordingToTarget(Transform a)
    {
        var vector = a.position - myTransform.position;
        var current = -Mathf.Atan2(vector.z, vector.x) * 57.29578f;
        var f = -Mathf.DeltaAngle(current, myTransform.rotation.eulerAngles.y - 90f);
        if ((Mathf.Abs(f) < 90f) && (vector.magnitude < 6f) && (a.position.y <= myTransform.position.y + 2f) && (a.position.y >= myTransform.position.y - 5f))
            attackAnimation = "attack4";
        else
            attackAnimation = f > 0f ? "attack1" : "attack2";
    }

    private void Awake()
    {
        setup = gameObject.GetComponent<HERO_SETUP>();
        rigidbody.freezeRotation = true;
        rigidbody.useGravity = false;

        myTransform = transform;

        handL = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L");
        handR = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R");
        forearmL = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L");
        forearmR = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R");
        upperarmL = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L");
        upperarmR = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R");
    }

    public void backToHuman()
    {
        gameObject.GetComponent<SmoothSyncMovement>().disabled = false;
        rigidbody.velocity = Vector3.zero;
        titanForm = false;
        ungrabbed();
        falseAttack();
        skillCDDuration = skillCDLast;
        IN_GAME_MAIN_CAMERA.maincamera.setMainObject(gameObject);
        if (!IN_GAME_MAIN_CAMERA.IsSingleplayer)
            photonView.RPC("backToHumanRPC", PhotonTargets.Others);
    }

    [RPC]
    private void backToHumanRPC()
    {
        titanForm = false;
        eren_titan = null;
        gameObject.GetComponent<SmoothSyncMovement>().disabled = false;
    }

    [RPC]
    public void badGuyReleaseMe()
    {
        hookBySomeOne = false;
        badGuy = null;
    }

    [RPC]
    public void blowAway(Vector3 force)
    {
        if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine)
        {
            rigidbody.AddForce(force, ForceMode.Impulse);
            myTransform.LookAt(myTransform.position);
        }
    }

    private void bodyLean()
    {
        if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine)
        {
            var z = 0f;
            needLean = false;
            if (!useGun && (state == HERO_STATE.Attack) && (attackAnimation != "attack3_1") && (attackAnimation != "attack3_2"))
            {
                var y = rigidbody.velocity.y;
                var x = rigidbody.velocity.x;
                var num4 = rigidbody.velocity.z;
                var num5 = Mathf.Sqrt(x * x + num4 * num4);
                var num6 = Mathf.Atan2(y, num5) * 57.29578f;
                targetRotation = Quaternion.Euler(-num6 * (1f - Vector3.Angle(rigidbody.velocity, myTransform.forward) / 90f), facingDirection, 0f);
                if ((isLeftHandHooked && (bulletLeft != null)) || (isRightHandHooked && (bulletRight != null)))
                    myTransform.rotation = targetRotation;
            }
            else
            {
                if (isLeftHandHooked && (bulletLeft != null) && isRightHandHooked && (bulletRight != null))
                {
                    if (almostSingleHook)
                    {
                        needLean = true;
                        z = getLeanAngle(bulletRight.transform.position, true);
                    }
                }
                else
                {
                    if (isLeftHandHooked && (bulletLeft != null))
                    {
                        needLean = true;
                        z = getLeanAngle(bulletLeft.transform.position, true);
                    }
                    else
                    {
                        if (isRightHandHooked && (bulletRight != null))
                        {
                            needLean = true;
                            z = getLeanAngle(bulletRight.transform.position, false);
                        }
                    }
                }
                if (needLean)
                {
                    var a = 0f;
                    if (!useGun && (state != HERO_STATE.Attack))
                    {
                        a = currentSpeed * 0.1f;
                        a = Mathf.Min(a, 20f);
                    }
                    targetRotation = Quaternion.Euler(-a, facingDirection, z);
                }
                else
                {
                    if (state != HERO_STATE.Attack)
                        targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
                }
            }
        }
    }

    private void breakApart(Vector3 v, bool isBite)
    {
        GameObject obj6;
        GameObject obj7;
        GameObject obj8;
        GameObject obj9;
        GameObject obj10;
        var obj5 = (GameObject)Instantiate(CacheResources.Load("Character_parts/AOTTG_HERO_body"), myTransform.position, myTransform.rotation);
        obj5.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
        obj5.GetComponent<HERO_DEAD_BODY_SETUP>().init(currentAnimation, animation[currentAnimation].normalizedTime, BODY_PARTS.ARM_R);
        if (!isBite)
        {
            var gO = (GameObject)Instantiate(CacheResources.Load("Character_parts/AOTTG_HERO_body"), myTransform.position, myTransform.rotation);
            var obj3 = (GameObject)Instantiate(CacheResources.Load("Character_parts/AOTTG_HERO_body"), myTransform.position, myTransform.rotation);
            var obj4 = (GameObject)Instantiate(CacheResources.Load("Character_parts/AOTTG_HERO_body"), myTransform.position, myTransform.rotation);
            gO.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
            obj3.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
            obj4.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
            gO.GetComponent<HERO_DEAD_BODY_SETUP>().init(currentAnimation, animation[currentAnimation].normalizedTime, BODY_PARTS.UPPER);
            obj3.GetComponent<HERO_DEAD_BODY_SETUP>().init(currentAnimation, animation[currentAnimation].normalizedTime, BODY_PARTS.LOWER);
            obj4.GetComponent<HERO_DEAD_BODY_SETUP>().init(currentAnimation, animation[currentAnimation].normalizedTime, BODY_PARTS.ARM_L);
            applyForceToBody(gO, v);
            applyForceToBody(obj3, v);
            applyForceToBody(obj4, v);
            if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine)
                currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(gO, false);
        }
        else
        {
            if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine)
                currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(obj5, false);
        }
        applyForceToBody(obj5, v);
        var l_handTransform = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L").transform;
        var r_handTransform = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R").transform;
        if (useGun)
        {
            obj6 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_gun_l"), l_handTransform.position, l_handTransform.rotation);
            obj7 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_gun_r"), r_handTransform.position, r_handTransform.rotation);
            obj8 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_3dmg_2"), myTransform.position, myTransform.rotation);
            obj9 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_gun_mag_l"), myTransform.position, myTransform.rotation);
            obj10 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_gun_mag_r"), myTransform.position, myTransform.rotation);
        }
        else
        {
            obj6 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_blade_l"), l_handTransform.position, l_handTransform.rotation);
            obj7 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_blade_r"), r_handTransform.position, r_handTransform.rotation);
            obj8 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_3dmg"), myTransform.position, myTransform.rotation);
            obj9 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_3dmg_gas_l"), myTransform.position, myTransform.rotation);
            obj10 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_3dmg_gas_r"), myTransform.position, myTransform.rotation);
        }
        obj6.renderer.material = CharacterMaterials.materials[setup.myCostume._3dmg_texture];
        obj7.renderer.material = CharacterMaterials.materials[setup.myCostume._3dmg_texture];
        obj8.renderer.material = CharacterMaterials.materials[setup.myCostume._3dmg_texture];
        obj9.renderer.material = CharacterMaterials.materials[setup.myCostume._3dmg_texture];
        obj10.renderer.material = CharacterMaterials.materials[setup.myCostume._3dmg_texture];
        applyForceToBody(obj6, v);
        applyForceToBody(obj7, v);
        applyForceToBody(obj8, v);
        applyForceToBody(obj9, v);
        applyForceToBody(obj10, v);
    }

    private void bufferUpdate()
    {
        if (!(buffTime > 0f))
            return;
        buffTime -= Time.deltaTime;
        if (!(buffTime <= 0f))
            return;
        buffTime = 0f;
        if ((currentBuff == BUFF.SpeedUp) && animation.IsPlaying("run_sasha"))
            crossFade("run", 0.1f);
        currentBuff = BUFF.NoBuff;
    }

    private void calcFlareCD()
    {
        if (flare1CD > 0f)
        {
            flare1CD -= Time.deltaTime;
            if (flare1CD < 0f)
                flare1CD = 0f;
        }
        if (flare2CD > 0f)
        {
            flare2CD -= Time.deltaTime;
            if (flare2CD < 0f)
                flare2CD = 0f;
        }
        if (flare3CD > 0f)
        {
            flare3CD -= Time.deltaTime;
            if (flare3CD < 0f)
                flare3CD = 0f;
        }
    }

    private void calcSkillCD()
    {
        if (skillCDDuration > 0f)
        {
            skillCDDuration -= Time.deltaTime;
            if (skillCDDuration < 0f)
                skillCDDuration = 0f;
        }
    }

    private float CalculateJumpVerticalSpeed()
    {
        return Mathf.Sqrt(2f * jumpHeight * gravity);
    }

    private void changeBlade()
    {
        if (!useGun || grounded || (LevelInfo.getInfo(FengGameManagerMKII.level).type != GAMEMODE.PVP_AHSS))
        {
            state = HERO_STATE.ChangeBlade;
            throwedBlades = false;
            if (useGun)
            {
                if (!leftGunHasBullet && !rightGunHasBullet)
                {
                    if (grounded)
                        reloadAnimation = "AHSS_gun_reload_both";
                    else
                        reloadAnimation = "AHSS_gun_reload_both_air";
                }
                else
                {
                    if (!leftGunHasBullet)
                    {
                        if (grounded)
                            reloadAnimation = "AHSS_gun_reload_l";
                        else
                            reloadAnimation = "AHSS_gun_reload_l_air";
                    }
                    else
                    {
                        if (!rightGunHasBullet)
                        {
                            if (grounded)
                                reloadAnimation = "AHSS_gun_reload_r";
                            else
                                reloadAnimation = "AHSS_gun_reload_r_air";
                        }
                        else
                        {
                            if (grounded)
                                reloadAnimation = "AHSS_gun_reload_both";
                            else
                                reloadAnimation = "AHSS_gun_reload_both_air";
                            leftGunHasBullet = rightGunHasBullet = false;
                        }
                    }
                }
                crossFade(reloadAnimation, 0.05f);
            }
            else
            {
                if (!grounded)
                    reloadAnimation = "changeBlade_air";
                else
                    reloadAnimation = "changeBlade";
                crossFade(reloadAnimation, 0.1f);
            }
        }
    }

    public void checkTitan()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        LayerMask mask = 1 << LayerMask.NameToLayer("PlayerAttackBox");
        LayerMask mask2 = 1 << LayerMask.NameToLayer("Ground");
        LayerMask mask3 = 1 << LayerMask.NameToLayer("EnemyBox");
        LayerMask mask4 = mask | mask2 | mask3;
        var hits = Physics.RaycastAll(ray, 180f, mask4.value);
        var sortedHits = new List<RaycastHit>();
        var currentTitans = new List<TITAN>();
        foreach (var hit in hits)
            sortedHits.Add(hit);
        sortedHits.Sort((x, y) => x.distance.CompareTo(y.distance));
        var maxDistance = 180f;
        for (var i = 0; i < sortedHits.Count; i++)
        {
            var hitObject = sortedHits[i].collider.gameObject;
            if (hitObject.layer == 16)
            {
                if (hitObject.name.Contains("PlayerDetectorRC") && (sortedHits[i].distance < maxDistance))
                {
                    maxDistance -= 60f;
                    if (maxDistance <= 60f)
                        i = sortedHits.Count;
                    var titan = hitObject.transform.root.gameObject.GetComponent<TITAN>();
                    if (titan != null)
                        currentTitans.Add(titan);
                }
            }
            else
                i = sortedHits.Count;
        }
        foreach (var oldTitan in myTitans)
        {
            if (!currentTitans.Contains(oldTitan))
                oldTitan.isLook = false;
        }
        foreach (var newTitan in currentTitans)
            newTitan.isLook = true;
        myTitans = currentTitans;
    }

    private void checkDashDoubleTap()
    {
        if (uTapTime >= 0f)
        {
            uTapTime += Time.deltaTime;
            if (uTapTime > 0.2f)
                uTapTime = -1f;
        }
        if (dTapTime >= 0f)
        {
            dTapTime += Time.deltaTime;
            if (dTapTime > 0.2f)
                dTapTime = -1f;
        }
        if (lTapTime >= 0f)
        {
            lTapTime += Time.deltaTime;
            if (lTapTime > 0.2f)
                lTapTime = -1f;
        }
        if (rTapTime >= 0f)
        {
            rTapTime += Time.deltaTime;
            if (rTapTime > 0.2f)
                rTapTime = -1f;
        }
        if (inputManager.isInputDown[InputCode.up])
        {
            if (uTapTime == -1f)
                uTapTime = 0f;
            if (uTapTime != 0f)
                dashU = true;
        }
        if (inputManager.isInputDown[InputCode.down])
        {
            if (dTapTime == -1f)
                dTapTime = 0f;
            if (dTapTime != 0f)
                dashD = true;
        }
        if (inputManager.isInputDown[InputCode.left])
        {
            if (lTapTime == -1f)
                lTapTime = 0f;
            if (lTapTime != 0f)
                dashL = true;
        }
        if (inputManager.isInputDown[InputCode.right])
        {
            if (rTapTime == -1f)
                rTapTime = 0f;
            if (rTapTime != 0f)
                dashR = true;
        }
    }

    public void continueAnimation()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                if (current.speed == 1f)
                    return;
                current.speed = 1f;
            }
        }
        finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
        customAnimationSpeed();
        playAnimation(currentPlayingClipName());
        if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) && photonView.isMine)
            photonView.RPC("netContinueAnimation", PhotonTargets.Others);
    }

    public void crossFade(string aniName, float time)
    {
        currentAnimation = aniName;
        animation.CrossFade(aniName, time);
        if (PhotonNetwork.connected && photonView.isMine)
            photonView.RPC("netCrossFade", PhotonTargets.Others, aniName, time);
    }

    public string currentPlayingClipName()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                if (animation.IsPlaying(current.name))
                    return current.name;
            }
        }
        finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
        return string.Empty;
    }

    private void customAnimationSpeed()
    {
        animation["attack5"].speed = 1.85f;
        animation["changeBlade"].speed = 1.2f;
        animation["air_release"].speed = 0.6f;
        animation["changeBlade_air"].speed = 0.8f;
        animation["AHSS_gun_reload_both"].speed = 0.38f;
        animation["AHSS_gun_reload_both_air"].speed = 0.5f;
        animation["AHSS_gun_reload_l"].speed = 0.4f;
        animation["AHSS_gun_reload_l_air"].speed = 0.5f;
        animation["AHSS_gun_reload_r"].speed = 0.4f;
        animation["AHSS_gun_reload_r_air"].speed = 0.5f;
    }

    private void dash(float horizontal, float vertical)
    {
        print("HERO - dashTime: " + dashTime + " currentGas: " + currentGas);
        if ((dashTime <= 0f) && (currentGas > 0f) && !isMounted)
        {
            useGas(totalGas * 0.04f);
            facingDirection = getGlobalFacingDirection(horizontal, vertical);
            dashV = getGlobaleFacingVector3(facingDirection);
            originVM = currentSpeed;
            var quaternion = Quaternion.Euler(0f, facingDirection, 0f);
            rigidbody.rotation = quaternion;
            targetRotation = quaternion;
            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                Instantiate(CacheResources.Load("FX/boost_smoke"), myTransform.position, myTransform.rotation);
            else
                PhotonNetwork.Instantiate("FX/boost_smoke", myTransform.position, myTransform.rotation, 0);
            dashTime = 0.5f;
            crossFade("dash", 0.1f);
            animation["dash"].time = 0.1f;
            state = HERO_STATE.AirDodge;
            falseAttack();
            rigidbody.AddForce(dashV * 40f, ForceMode.VelocityChange);
        }
    }

    public void die(Vector3 v, bool isBite)
    {
        if (invincible <= 0f)
        {
            if (titanForm && (eren_titan != null))
                eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
            if (bulletLeft != null)
                bulletLeft.GetComponent<Bullet>().removeMe();
            if (bulletRight != null)
                bulletRight.GetComponent<Bullet>().removeMe();
            meatDie.Play();
            if (((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine) && !useGun)
            {
                leftbladetrail.Deactivate();
                rightbladetrail.Deactivate();
                leftbladetrail2.Deactivate();
                rightbladetrail2.Deactivate();
            }
            breakApart(v, isBite);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().gameOver = true;
            FengGameManagerMKII.MKII.gameLose();
            falseAttack();
            hasDied = true;
            var transform = myTransform.Find("audio_die");
            transform.parent = null;
            transform.GetComponent<AudioSource>().Play();
            if (PlayerPrefs.HasKey("EnableSS") && (PlayerPrefs.GetInt("EnableSS") == 1))
                IN_GAME_MAIN_CAMERA.maincamera.startSnapShot(myTransform.position, 0);
            Destroy(gameObject);
        }
    }

    public void die2(Transform tf)
    {
        if (invincible <= 0f)
        {
            if (titanForm && (eren_titan != null))
                eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
            if (bulletLeft != null)
                bulletLeft.GetComponent<Bullet>().removeMe();
            if (bulletRight != null)
                bulletRight.GetComponent<Bullet>().removeMe();
            var transform1 = myTransform.Find("audio_die");
            transform.parent = null;
            transform.GetComponent<AudioSource>().Play();
            meatDie.Play();
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(null);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().gameOver = true;
            FengGameManagerMKII.MKII.gameLose();
            falseAttack();
            hasDied = true;
            var obj2 = (GameObject)Instantiate(CacheResources.Load("hitMeat2"));
            obj2.transform.position = myTransform.position;
            Destroy(gameObject);
        }
    }

    private void dodge(bool offTheWall = false)
    {
        if ((myHorse != null) && !isMounted && (Vector3.Distance(myHorse.transform.position, myTransform.position) < 15f))
            getOnHorse();
        else
        {
            state = HERO_STATE.GroundDodge;
            if (!offTheWall)
            {
                float num;
                float num2;
                if (inputManager.isInput[InputCode.up])
                    num2 = 1f;
                else
                {
                    if (inputManager.isInput[InputCode.down])
                        num2 = -1f;
                    else
                        num2 = 0f;
                }
                if (inputManager.isInput[InputCode.left])
                    num = -1f;
                else
                {
                    if (inputManager.isInput[InputCode.right])
                        num = 1f;
                    else
                        num = 0f;
                }
                var num3 = getGlobalFacingDirection(num, num2);
                if ((num != 0f) || (num2 != 0f))
                {
                    facingDirection = num3 + 180f;
                    targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
                }
                crossFade("dodge", 0.1f);
            }
            else
            {
                playAnimation("dodge");
                playAnimationAt("dodge", 0.2f);
            }
            sparks.enableEmission = false;
        }
    }

    private void erenTransform()
    {
        skillCDDuration = skillCDLast;
        if (bulletLeft != null)
            bulletLeft.GetComponent<Bullet>().removeMe();
        if (bulletRight != null)
            bulletRight.GetComponent<Bullet>().removeMe();
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            eren_titan = (GameObject)Instantiate(CacheResources.Load("TITAN_EREN"), myTransform.position, myTransform.rotation);
        else
            eren_titan = PhotonNetwork.Instantiate("TITAN_EREN", myTransform.position, myTransform.rotation, 0);
        eren_titan.GetComponent<TITAN_EREN>().realBody = gameObject;
        IN_GAME_MAIN_CAMERA.maincamera.flashBlind();
        IN_GAME_MAIN_CAMERA.maincamera.setMainObject(eren_titan);
        eren_titan.GetComponent<TITAN_EREN>().born();
        eren_titan.rigidbody.velocity = rigidbody.velocity;
        rigidbody.velocity = Vector3.zero;
        myTransform.position = eren_titan.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck").position;
        titanForm = true;
        if (!IN_GAME_MAIN_CAMERA.IsSingleplayer)
            photonView.RPC("whoIsMyErenTitan", PhotonTargets.Others, eren_titan.GetPhotonView().viewID);
        if (smoke_3dmg.enableEmission && (!IN_GAME_MAIN_CAMERA.IsSingleplayer) && photonView.isMine)
            photonView.RPC("net3DMGSMOKE", PhotonTargets.Others, false);
        smoke_3dmg.enableEmission = false;
    }

    private void escapeFromGrab()
    {
    }

    public void falseAttack()
    {
        attackMove = false;
        if (useGun)
        {
            if (!attackReleased)
            {
                continueAnimation();
                attackReleased = true;
            }
        }
        else
        {
            if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine)
            {
                checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = false;
                checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = false;
                checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                leftbladetrail.StopSmoothly(2f);  //0.2 - trail duration
                rightbladetrail.StopSmoothly(2f);
                leftbladetrail2.StopSmoothly(2f);
                rightbladetrail2.StopSmoothly(2f);
            }
            attackLoop = 0;
            if (!attackReleased)
            {
                continueAnimation();
                attackReleased = true;
            }
        }
    }

    public void fillGas()
    {
        currentGas = totalGas;
    }

    private GameObject findNearestTitan()
    {
        var objArray = GameObject.FindGameObjectsWithTag("titan");
        GameObject obj2 = null;
        var positiveInfinity = float.PositiveInfinity;
        var position = myTransform.position;
        foreach (var obj3 in objArray)
        {
            var vector2 = obj3.transform.position - position;
            var sqrMagnitude = vector2.sqrMagnitude;
            if (sqrMagnitude < positiveInfinity)
            {
                obj2 = obj3;
                positiveInfinity = sqrMagnitude;
            }
        }
        return obj2;
    }

    private void OnGUI()
    {
        if (photonView.isMine && false)
        {
            GUI.Label(new Rect(100, 100, 500, 500), getDebugInfo());
            if (Input.GetKeyDown(KeyCode.Keypad7))
            {
                foreach (var obj2 in (GameObject[])FindObjectsOfType(typeof(GameObject)))
                {
                    var n = obj2.name;
                    if (n.Contains("TREE") || n.Contains("HOUSE") || n == "Cube" || n == "Cube_005" || n == "Cube_003" || n == "Cube_002" || n.Contains("EpicCube") && obj2.transform.position.z != 0f && obj2.transform.position.x != 0f)
                    {
                        foreach (var renderer32 in obj2.GetComponentsInChildren<Renderer>())
                            renderer32.enabled = false;
                        Destroy(obj2);
                    }
                }
                var lastCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                lastCube.name = "EpicCube1";
                lastCube.transform.localScale = new Vector3(8f, 200f, 8f);
                lastCube.transform.position = new Vector3(0f, 0f, 0f);
                lastCube.gameObject.layer = LayerMask.NameToLayer("Ground");
                myTransform.position = lastCube.transform.position + Vector3.up * 100f;
            }
            var cap = Math.Round(mSmoothMouseWheelTranslation.Inf[0], 3);
            if (cap < 0)
                cap *= 2f;
            if (FengGameManagerMKII.heroes.Count > 0)
            {
                foreach (var h in FengGameManagerMKII.heroes)
                {
                    var hero = h as HERO;
                    if (hero.gameObject.GetPhotonView().photonView.isMine)
                    {
                        GUI.Label(new Rect(500, 660, 500, 20), "speed: " + hero.gameObject.rigidbody.velocity.magnitude);
                        GUI.Label(new Rect(700, 660, 500, 20), "contradiction force: " + contValue);

                        var MUST_SEE = new GUIStyle
                        {
                            fontSize = 20
                        };
                        MUST_SEE.normal.textColor = Color.red;
                        GUI.Label(new Rect(Screen.width / 2 - 300, Screen.height / 2 + 50, 500, 20), "scroll speed: <color=white>" + cap + "</color>", MUST_SEE);
                        if (bulletLeft != null)
                            GUI.Label(new Rect(Screen.width / 2 - 300, Screen.height / 2 + 100, 500, 20), "left rope length: <color=white>" + Vector3.Distance(bulletLeft.transform.position, myTransform.position) + "</color>", MUST_SEE);
                        if (bulletRight != null)
                            GUI.Label(new Rect(Screen.width / 2 + 100, Screen.height / 2 + 100, 500, 20), "right rope length: <color=white>" + Vector3.Distance(bulletRight.transform.position, myTransform.position) + "</color>", MUST_SEE);
                        break;
                    }
                }
            }
            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            {
                GUI.Label(new Rect(500, 660, 500, 20), "speed: " + gameObject.rigidbody.velocity.magnitude);
                GUI.Label(new Rect(700, 660, 500, 20), "contradiction force: " + contValue);

                var MUST_SEE = new GUIStyle
                {
                    fontSize = 20
                };
                MUST_SEE.normal.textColor = Color.red;
                GUI.Label(new Rect(Screen.width / 2 - 300, Screen.height / 2 + 50, 500, 20), "scroll speed: <color=white>" + cap + "</color>", MUST_SEE);
                if (bulletLeft != null)
                    GUI.Label(new Rect(Screen.width / 2 - 300, Screen.height / 2 + 100, 500, 20), "left rope length: <color=white>" + Vector3.Distance(bulletLeft.transform.position, myTransform.position) + "</color>", MUST_SEE);
                if (bulletRight != null)
                    GUI.Label(new Rect(Screen.width / 2 + 100, Screen.height / 2 + 100, 500, 20), "right rope length: <color=white>" + Vector3.Distance(bulletRight.transform.position, myTransform.position) + "</color>", MUST_SEE);
            }
        }
    }

    private void FixedUpdate()
    {
        if (titanForm || (IN_GAME_MAIN_CAMERA.isPausing && (IN_GAME_MAIN_CAMERA.IsSingleplayer)))
            return;
        currentSpeed = rigidbody.velocity.magnitude;
        if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) && !photonView.isMine)
            return;
        if (state == HERO_STATE.Grab)
            rigidbody.AddForce(-rigidbody.velocity, ForceMode.VelocityChange);
        else
        {
            //contValue = rigidbody.velocity.magnitude / 100 * 0.03f;
            if (IsGrounded())
            {
                if (!grounded)
                    justGrounded = true;
                grounded = true;
            }
            else
                grounded = false;
            if (hookSomeOne)
            {
                if (hookTarget != null)
                {
                    var vector = hookTarget.transform.position - myTransform.position;
                    var magnitude = vector.magnitude;
                    if (magnitude > 2f)
                        rigidbody.AddForce(vector.normalized * Mathf.Pow(magnitude, 0.15f) * 30f - rigidbody.velocity * 0.95f, ForceMode.VelocityChange);
                }
                else
                    hookSomeOne = false;
            }
            else if (hookBySomeOne && (badGuy != null))
            {
                if (badGuy != null)
                {
                    var vector2 = badGuy.transform.position - myTransform.position;
                    var f = vector2.magnitude;
                    if (f > 5f)
                        rigidbody.AddForce(vector2.normalized * Mathf.Pow(f, 0.15f) * 0.2f, ForceMode.Impulse);
                }
                else
                    hookBySomeOne = false;
            }
            var x = 0f;
            var z = 0f;
            if (!IN_GAME_MAIN_CAMERA.isTyping)
            {
                if (inputManager.isInput[InputCode.up])
                    z = 1f;
                else if (inputManager.isInput[InputCode.down])
                    z = -1f;
                else
                    z = 0f;

                if (inputManager.isInput[InputCode.left])
                    x = -1f;
                else if (inputManager.isInput[InputCode.right])
                    x = 1f;
                else
                    x = 0f;
            }
            var reeling = false;
            canLeftReel = false;
            canRightReel = false;
            isLeftHandHooked = false;
            isRightHandHooked = false;
            reelthreshold = 90f;
            if (isLaunchLeft)
            {
                if ((bulletLeft != null) && bulletLeft.GetComponent<Bullet>().isHooked())
                {
                    isLeftHandHooked = true;
                    var to = bulletLeft.transform.position - myTransform.position;
                    to.Normalize();
                    to *= 10f;
                    if (!isLaunchRight)
                        to *= 2f;
                    if ((Vector3.Angle(rigidbody.velocity, to) > reelthreshold) && inputManager.isInput[InputCode.jump])
                    {
                        canLeftReel = true;
                        reeling = true;
                    }
                    if (!canLeftReel)
                    {
                        rigidbody.AddForce(to);
                        if (Vector3.Angle(rigidbody.velocity, to) > 90f)
                            rigidbody.AddForce(-rigidbody.velocity * 2f, ForceMode.Acceleration);
                    }
                }
                launchElapsedTimeL += Time.deltaTime;
                if (QHold && (currentGas > 0f))
                    useGas(useGasSpeed * Time.deltaTime);
                else if (launchElapsedTimeL > 0.1f)  //when the hook can detach
                {
                    isLaunchLeft = false;
                    if (bulletLeft != null)
                    {
                        bulletLeft.GetComponent<Bullet>().disable();
                        releaseIfIHookSb();
                        bulletLeft = null;
                        canLeftReel = false;
                    }
                }
            }
            if (isLaunchRight)
            {
                if ((bulletRight != null) && bulletRight.GetComponent<Bullet>().isHooked())
                {
                    isRightHandHooked = true;
                    var to = bulletRight.transform.position - myTransform.position;
                    to.Normalize();
                    to *= 10f;
                    if (!isLaunchLeft)
                        to *= 2f;
                    //90� - angle where the rope body will keep you from going forward (when you start "orbting")
                    //>avoiding the use of "Centripetal force"
                    if ((Vector3.Angle(rigidbody.velocity, to) > reelthreshold) && inputManager.isInput[InputCode.jump])
                    {
                        canRightReel = true;
                        reeling = true;
                    }
                    if (!canRightReel)
                    {
                        rigidbody.AddForce(to);
                        //90� - when you reel you will be pulled in this angle (that 90� sharp change of direction)
                        if (Vector3.Angle(rigidbody.velocity, to) > 90)
                            rigidbody.AddForce(-rigidbody.velocity * 2f, ForceMode.Acceleration);
                    }
                }
                launchElapsedTimeR += Time.deltaTime;
                if (EHold && (currentGas > 0f))
                    useGas(useGasSpeed * Time.deltaTime);
                else
                {
                    if (launchElapsedTimeR > 0.1f)  //when the hook can detach
                    {
                        isLaunchRight = false;
                        if (bulletRight != null)
                        {
                            bulletRight.GetComponent<Bullet>().disable();
                            releaseIfIHookSb();
                            bulletRight = null;
                            canRightReel = false;
                        }
                    }
                }
            }
            if (grounded)
            {
                Vector3 vector26;
                var zero = Vector3.zero;
                if (state == HERO_STATE.Attack)
                {
                    if (attackAnimation == "attack5")
                    {
                        if ((animation[attackAnimation].normalizedTime > 0.4f) && (animation[attackAnimation].normalizedTime < 0.61f))
                            rigidbody.AddForce(gameObject.transform.forward * 200f);
                    }
                    else if (attackAnimation == "special_petra")
                    {
                        if ((animation[attackAnimation].normalizedTime > 0.35f) && (animation[attackAnimation].normalizedTime < 0.48f))
                            rigidbody.AddForce(gameObject.transform.forward * 200f);
                    }
                    else
                    {
                        if (animation.IsPlaying("attack3_2"))
                            zero = Vector3.zero;
                        else if (animation.IsPlaying("attack1") || animation.IsPlaying("attack2"))
                            rigidbody.AddForce(gameObject.transform.forward * 200f);
                    }
                    if (animation.IsPlaying("attack3_2"))
                        zero = Vector3.zero;
                }
                if (justGrounded)
                {
                    if ((state != HERO_STATE.Attack) || ((attackAnimation != "attack3_1") && (attackAnimation != "attack5") && (attackAnimation != "special_petra")))
                    {
                        if ((state != HERO_STATE.Attack) && (x == 0f) && (z == 0f) && (bulletLeft == null) && (bulletRight == null) && (state != HERO_STATE.FillGas))
                        {
                            state = HERO_STATE.Land;
                            crossFade("dash_land", 0.01f);
                        }
                        else
                        {
                            buttonAttackRelease = true;
                            if ((state != HERO_STATE.Attack) && (rigidbody.velocity.x * rigidbody.velocity.x + rigidbody.velocity.z * rigidbody.velocity.z > speed * speed * 1.5f) && (state != HERO_STATE.FillGas))
                            {
                                state = HERO_STATE.Slide;
                                crossFade("slide", 0.05f);
                                facingDirection = Mathf.Atan2(rigidbody.velocity.x, rigidbody.velocity.z) * 57.29578f;
                                targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
                                sparks.enableEmission = true;
                            }
                        }
                    }
                    justGrounded = false;
                    zero = rigidbody.velocity;
                }
                if ((state == HERO_STATE.Attack) && (attackAnimation == "attack3_1") && (animation[attackAnimation].normalizedTime >= 1f))
                {
                    playAnimation("attack3_2");
                    resetAnimationSpeed();
                    vector26 = Vector3.zero;
                    rigidbody.velocity = vector26;
                    zero = vector26;
                    currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().startShake(0.2f, 0.3f);
                }
                if (state == HERO_STATE.GroundDodge)
                {
                    if ((animation["dodge"].normalizedTime >= 0.2f) && (animation["dodge"].normalizedTime < 0.8f))
                        zero = -myTransform.forward * 2.4f * speed;
                    if (animation["dodge"].normalizedTime > 0.8f)
                        zero = rigidbody.velocity * 0.9f;
                }
                else if (state == HERO_STATE.Idle)
                {
                    var vector6 = new Vector3(x, 0f, z);
                    var resultAngle = getGlobalFacingDirection(x, z);
                    zero = getGlobaleFacingVector3(resultAngle);
                    var num6 = vector6.magnitude <= 0.95f ? (vector6.magnitude >= 0.25f ? vector6.magnitude : 0f) : 1f;
                    zero = zero * num6;
                    zero = zero * speed;
                    if ((buffTime > 0f) && (currentBuff == BUFF.SpeedUp))
                        zero = zero * 4f;
                    if ((x != 0f) || (z != 0f))
                    {
                        if (!animation.IsPlaying("run") && !animation.IsPlaying("jump") && !animation.IsPlaying("run_sasha") && (!animation.IsPlaying("horse_geton") || (animation["horse_geton"].normalizedTime >= 0.5f)))
                        {
                            if ((buffTime > 0f) && (currentBuff == BUFF.SpeedUp))
                                crossFade("run_sasha", 0.1f);
                            else
                                crossFade("run", 0.1f);
                        }
                    }
                    else
                    {
                        if (!animation.IsPlaying(standAnimation) && (state != HERO_STATE.Land) && !animation.IsPlaying("jump") && !animation.IsPlaying("horse_geton") && !animation.IsPlaying("grabbed"))
                        {
                            crossFade(standAnimation, 0.1f);
                            zero = zero * 0f;
                        }
                        resultAngle = -874f;
                    }
                    if (resultAngle != -874f)
                    {
                        facingDirection = resultAngle;
                        targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
                    }
                }
                else if (state == HERO_STATE.Land)
                    zero = rigidbody.velocity * 0.96f;
                else if (state == HERO_STATE.Slide)
                {
                    zero = rigidbody.velocity * 0.99f;
                    if (currentSpeed < speed * 1.2f)
                    {
                        idle();
                        sparks.enableEmission = false;
                    }
                }
                var velocity = rigidbody.velocity;
                var force = zero - velocity;
                force.x = Mathf.Clamp(force.x, -maxVelocityChange, maxVelocityChange);
                force.z = Mathf.Clamp(force.z, -maxVelocityChange, maxVelocityChange);
                force.y = 0f;
                if (animation.IsPlaying("jump") && (animation["jump"].normalizedTime > 0.18f))
                    force.y += 8f;
                if (animation.IsPlaying("horse_geton") && (animation["horse_geton"].normalizedTime > 0.18f) && (animation["horse_geton"].normalizedTime < 1f))
                {
                    const float num7 = 6f;
                    force = -rigidbody.velocity;
                    force.y = num7;
                    var num8 = Vector3.Distance(myHorse.transform.position, myTransform.position);
                    var num9 = 0.6f * gravity * num8 / (2f * num7);
                    vector26 = myHorse.transform.position - myTransform.position;
                    force += num9 * vector26.normalized;
                }
                if ((state != HERO_STATE.Attack) || !useGun)
                {
                    rigidbody.AddForce(force, ForceMode.VelocityChange);
                    rigidbody.rotation = Quaternion.Lerp(gameObject.transform.rotation, Quaternion.Euler(0f, facingDirection, 0f), Time.deltaTime * 10f);
                }
            }
            else
            {
                if (sparks.enableEmission)
                    sparks.enableEmission = false;
                if ((myHorse != null) && (animation.IsPlaying("horse_geton") || animation.IsPlaying("air_fall")) && (rigidbody.velocity.y < 0f) && (Vector3.Distance(myHorse.transform.position + Vector3.up * 1.65f, myTransform.position) < 0.5f))
                {
                    myTransform.position = myHorse.transform.position + Vector3.up * 1.65f;
                    myTransform.rotation = myHorse.transform.rotation;
                    isMounted = true;
                    crossFade("horse_idle", 0.1f);
                    myHorse.GetComponent<Horse>().mounted();
                }
                if (((state == HERO_STATE.Idle) && !animation.IsPlaying("dash") && !animation.IsPlaying("wallrun") && !animation.IsPlaying("toRoof") && !animation.IsPlaying("horse_geton") && !animation.IsPlaying("horse_getoff") && !animation.IsPlaying("air_release") && !isMounted && (!animation.IsPlaying("air_hook_l_just") || (animation["air_hook_l_just"].normalizedTime >= 1f)) && (!animation.IsPlaying("air_hook_r_just") || (animation["air_hook_r_just"].normalizedTime >= 1f))) || (animation["dash"].normalizedTime >= 0.99f))
                    //if (((state == HERO_STATE.Idle) && !animation.IsPlayingAND("dash", "wallrun", "toRoof", "horse_geton", "horse_getoff", "air_release") && !isMounted && (!animation.IsPlaying("air_hook_l_just") || (animation["air_hook_l_just"].normalizedTime >= 1f)) && (!animation.IsPlaying("air_hook_r_just") || (animation["air_hook_r_just"].normalizedTime >= 1f))) || (animation["dash"].normalizedTime >= 0.99f))
                {
                    if (!isLeftHandHooked && !isRightHandHooked && (animation.IsPlaying("air_hook_l") || animation.IsPlaying("air_hook_r") || animation.IsPlaying("air_hook")) && (rigidbody.velocity.y > 20f))
                        //if (!isLeftHandHooked && !isRightHandHooked && (animation.IsPlayingOR("air_hook_l", "air_hook_r", "air_hook")) && (rigidbody.velocity.y > 20f))
                        animation.CrossFade("air_release");
                    else
                    {
                        var flag4 = Mathf.Abs(rigidbody.velocity.x) + Mathf.Abs(rigidbody.velocity.z) > 25f;
                        var flag5 = rigidbody.velocity.y < 0f;
                        if (!flag4)
                        {
                            if (flag5)
                            {
                                if (!animation.IsPlaying("air_fall"))
                                    crossFade("air_fall", 0.2f);
                            }
                            else if (!animation.IsPlaying("air_rise"))
                                crossFade("air_rise", 0.2f);
                        }
                        else if (!isLeftHandHooked && !isRightHandHooked)
                        {
                            var current = -Mathf.Atan2(rigidbody.velocity.z, rigidbody.velocity.x) * 57.29578f;
                            var num11 = -Mathf.DeltaAngle(current, myTransform.rotation.eulerAngles.y - 90f);
                            if (Mathf.Abs(num11) < 45f)
                            {
                                if (!animation.IsPlaying("air2"))
                                    crossFade("air2", 0.2f);
                            }
                            else if ((num11 < 135f) && (num11 > 0f))
                            {
                                if (!animation.IsPlaying("air2_right"))
                                    crossFade("air2_right", 0.2f);
                            }
                            else if ((num11 > -135f) && (num11 < 0f))
                            {
                                if (!animation.IsPlaying("air2_left"))
                                    crossFade("air2_left", 0.2f);
                            }
                            else if (!animation.IsPlaying("air2_backward"))
                                crossFade("air2_backward", 0.2f);
                        }
                        else if (useGun)
                        {
                            if (!isRightHandHooked)
                            {
                                if (!animation.IsPlaying("AHSS_hook_forward_l"))
                                    crossFade("AHSS_hook_forward_l", 0.1f);
                            }
                            else if (!isLeftHandHooked)
                            {
                                if (!animation.IsPlaying("AHSS_hook_forward_r"))
                                    crossFade("AHSS_hook_forward_r", 0.1f);
                            }
                            else if (!animation.IsPlaying("AHSS_hook_forward_both"))
                                crossFade("AHSS_hook_forward_both", 0.1f);
                        }
                        else if (!isRightHandHooked)
                        {
                            if (!animation.IsPlaying("air_hook_l"))
                                crossFade("air_hook_l", 0.1f);
                        }
                        else if (!isLeftHandHooked)
                        {
                            if (!animation.IsPlaying("air_hook_r"))
                                crossFade("air_hook_r", 0.1f);
                        }
                        else if (!animation.IsPlaying("air_hook"))
                            crossFade("air_hook", 0.1f);
                    }
                }
                if ((state == HERO_STATE.Idle) && animation.IsPlaying("air_release") && (animation["air_release"].normalizedTime >= 1f))
                    crossFade("air_rise", 0.2f);
                if (animation.IsPlaying("horse_getoff") && (animation["horse_getoff"].normalizedTime >= 1f))
                    crossFade("air_rise", 0.2f);
                if (animation.IsPlaying("toRoof"))
                {
                    if (animation["toRoof"].normalizedTime < 0.22f)
                    {
                        rigidbody.velocity = Vector3.zero;
                        rigidbody.AddForce(new Vector3(0f, gravity * rigidbody.mass, 0f));
                    }
                    else
                    {
                        if (!wallJump)
                        {
                            wallJump = true;
                            rigidbody.AddForce(Vector3.up * 8f, ForceMode.Impulse);
                        }
                        rigidbody.AddForce(myTransform.forward * 0.05f, ForceMode.Impulse);
                    }
                    if (animation["toRoof"].normalizedTime >= 1f)
                        playAnimation("air_rise");
                }
                else if ((state == HERO_STATE.Idle) && isPressDirectionTowardsHero(x, z) && !inputManager.isInput[InputCode.jump] && !inputManager.isInput[InputCode.leftRope] && !inputManager.isInput[InputCode.rightRope] && !inputManager.isInput[InputCode.bothRope] && IsFrontGrounded() && !animation.IsPlaying("wallrun") && !animation.IsPlaying("dodge"))
                {
                    crossFade("wallrun", 0.1f);
                    wallRunTime = 0f;
                }
                else if (animation.IsPlaying("wallrun"))
                {
                    rigidbody.AddForce(Vector3.up * speed - rigidbody.velocity, ForceMode.VelocityChange);
                    wallRunTime += Time.deltaTime;
                    if ((wallRunTime > 1f) || ((z == 0f) && (x == 0f)))
                    {
                        rigidbody.AddForce(-myTransform.forward * speed * 0.75f, ForceMode.Impulse);
                        dodge(true);
                    }
                    else if (!IsUpFrontGrounded())
                    {
                        wallJump = false;
                        crossFade("toRoof", 0.1f);
                    }
                    else if (!IsFrontGrounded())
                        crossFade("air_fall", 0.1f);
                }
                else if (!animation.IsPlaying("attack5") && !animation.IsPlaying("special_petra") && !animation.IsPlaying("dash") && !animation.IsPlaying("jump"))
                {
                    var vector9 = new Vector3(x, 0f, z);
                    var num12 = getGlobalFacingDirection(x, z);
                    var vector10 = getGlobaleFacingVector3(num12);
                    var num13 = vector9.magnitude <= 0.95f ? (vector9.magnitude >= 0.25f ? vector9.magnitude : 0f) : 1f;
                    vector10 = vector10 * num13;
                    vector10 = vector10 * (setup.myCostume.stat.ACL / 10f * 2f);
                    if ((x == 0f) && (z == 0f))
                    {
                        if (state == HERO_STATE.Attack)
                            vector10 *= 0f;
                        num12 = -874f;
                    }
                    if (num12 != -874f)
                    {
                        facingDirection = num12;
                        targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
                    }
                    if (!canLeftReel && !canRightReel && !isMounted && inputManager.isInput[InputCode.jump] && (currentGas > 0f))
                    {
                        if ((x != 0f) || (z != 0f))
                            rigidbody.AddForce(vector10, ForceMode.Acceleration);
                        else
                            rigidbody.AddForce(myTransform.forward * vector10.magnitude, ForceMode.Acceleration);
                        reeling = true;
                    }
                }
                if (animation.IsPlaying("air_fall") && (currentSpeed < 0.2f) && IsFrontGrounded())
                    crossFade("onWall", 0.3f);
            }
            if ((state == HERO_STATE.Attack) && ((attackAnimation == "attack5") || (attackAnimation == "special_petra")) && (animation[attackAnimation].normalizedTime > 0.4f) && !attackMove)
            {
                attackMove = true;
                if (launchPointRight.magnitude > 0f)
                {
                    var vector17 = launchPointRight - myTransform.position;
                    vector17.Normalize();
                    vector17 = vector17 * 13f;
                    rigidbody.AddForce(vector17, ForceMode.Impulse);
                }
                if ((attackAnimation == "special_petra") && (launchPointLeft.magnitude > 0f))
                {
                    var vector18 = launchPointLeft - myTransform.position;
                    vector18.Normalize();
                    vector18 = vector18 * 13f;
                    rigidbody.AddForce(vector18, ForceMode.Impulse);
                    if (bulletRight != null)
                    {
                        bulletRight.GetComponent<Bullet>().disable();
                        releaseIfIHookSb();
                    }
                    if (bulletLeft != null)
                    {
                        bulletLeft.GetComponent<Bullet>().disable();
                        releaseIfIHookSb();
                    }
                }
                rigidbody.AddForce(Vector3.up * 2f, ForceMode.Impulse);
            }
            var flag6 = false;
            if ((bulletLeft != null) || (bulletRight != null))
            {
                if ((bulletLeft != null) && (bulletLeft.transform.position.y > gameObject.transform.position.y) && isLaunchLeft && bulletLeft.GetComponent<Bullet>().isHooked())
                    flag6 = true;
                if ((bulletRight != null) && (bulletRight.transform.position.y > gameObject.transform.position.y) && isLaunchRight && bulletRight.GetComponent<Bullet>().isHooked())
                    flag6 = true;
            }

            rigidbody.AddForce(flag6 ? new Vector3(0f, -10f * rigidbody.mass, 0f) : new Vector3(0f, -gravity * rigidbody.mass, 0f));
            currentCamera.GetComponent<Camera>().fieldOfView = Mathf.Lerp(currentCamera.GetComponent<Camera>().fieldOfView, currentSpeed > 10f ? Mathf.Min(100f, currentSpeed + 40f) : 50f, 0.1f);

            if (reeling)
            {
                useGas(useGasSpeed * Time.deltaTime);
                if (!smoke_3dmg.enableEmission && (!IN_GAME_MAIN_CAMERA.IsSingleplayer) && photonView.isMine)
                    photonView.RPC("net3DMGSMOKE", PhotonTargets.Others, true);
                smoke_3dmg.enableEmission = true;
            }
            else
            {
                if (smoke_3dmg.enableEmission && (!IN_GAME_MAIN_CAMERA.IsSingleplayer) && photonView.isMine)
                    photonView.RPC("net3DMGSMOKE", PhotonTargets.Others, false);
                smoke_3dmg.enableEmission = false;
            }
            if (currentSpeed > 100f)
            {
                if (!speedFXPS.enableEmission)
                    speedFXPS.enableEmission = true;
                speedFXPS.startSpeed = currentSpeed;
                speedFX.transform.LookAt(myTransform.position + rigidbody.velocity);
            }
            else if (speedFXPS.enableEmission)
                speedFXPS.enableEmission = false;
        }
    }

    public string getDebugInfo()  //mod me
    {
        var final = "\n";
        final = "Left: " + isLeftHandHooked + " ";
        if (isLeftHandHooked && bulletLeft != null)
        {
            var vector = bulletLeft.transform.position - myTransform.position;
            final = final + (int)(Mathf.Atan2(vector.x, vector.z) * 57.29578f);
        }
        final = string.Concat(final, "\nRight: ", isRightHandHooked, " ");
        if (isRightHandHooked && bulletRight != null)
        {
            var vector2 = bulletRight.transform.position - myTransform.position;
            final = final + (int)(Mathf.Atan2(vector2.x, vector2.z) * 57.29578f);
        }
        final += "\nfacingDirection: " + (int)facingDirection + "\nActual facingDirection: " + (int)myTransform.rotation.eulerAngles.y + "\nState: " + state + "\n\n";
        if (state == HERO_STATE.Attack)
            targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
        final += "\nSkillID: " + skillId;
        return final;
    }

    private Vector3 getGlobaleFacingVector3(float resultAngle)
    {
        var num = -resultAngle + 90f;
        var x = Mathf.Cos(num * 0.01745329f);
        return new Vector3(x, 0f, Mathf.Sin(num * 0.01745329f));
    }

    private Vector3 getGlobaleFacingVector3(float horizontal, float vertical)
    {
        var num = -getGlobalFacingDirection(horizontal, vertical) + 90f;
        var x = Mathf.Cos(num * 0.01745329f);
        return new Vector3(x, 0f, Mathf.Sin(num * 0.01745329f));
    }

    private float getGlobalFacingDirection(float horizontal, float vertical)
    {
        if ((vertical == 0f) && (horizontal == 0f))
            return myTransform.rotation.eulerAngles.y;
        var y = currentCamera.transform.rotation.eulerAngles.y;
        var num2 = Mathf.Atan2(vertical, horizontal) * 57.29578f;
        num2 = -num2 + 90f;
        return y + num2;
    }

    private float getLeanAngle(Vector3 p, bool left)
    {
        if (!useGun && (state == HERO_STATE.Attack))
            return 0f;

        var num = p.y - myTransform.position.y;
        var num2 = Vector3.Distance(p, myTransform.position);

        var tan = Mathf.Acos(num / num2) * 57.29578f;
        tan *= 0.1f;
        tan *= 1f + Mathf.Pow(rigidbody.velocity.magnitude, 0.2f);

        var vector = p - myTransform.position;
        var current = Mathf.Atan2(vector.x, vector.z) * 57.29578f;
        var target = Mathf.Atan2(rigidbody.velocity.x, rigidbody.velocity.z) * 57.29578f;
        var num6 = Mathf.DeltaAngle(current, target);

        tan += Mathf.Abs(num6 * 0.5f);
        if (state != HERO_STATE.Attack)
            tan = Mathf.Min(tan, 80f);

        leanLeft = num6 > 0f;

        if (useGun)
            return tan * (num6 >= 0f ? 1 : -1);
        var num7 = (left && (num6 < 0f)) || (!left && (num6 > 0f)) ? 0.1f : 0.5f;
        return tan * (num6 >= 0f ? num7 : -num7);
    }

    private void getOffHorse()
    {
        playAnimation("horse_getoff");
        rigidbody.AddForce(Vector3.up * 10f - myTransform.forward * 2f - myTransform.right * 1f, ForceMode.VelocityChange);
        unmounted();
        
        setStat();
    }

    private void getOnHorse()
    {
        playAnimation("horse_geton");
        facingDirection = myHorse.transform.rotation.eulerAngles.y;
        targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
        
        setStat(true);
    }

    public void getSupply()
    {
        if ((animation.IsPlaying(standAnimation) || animation.IsPlaying("run") || animation.IsPlaying("run_sasha")) && ((currentBladeSta != totalBladeSta) || (currentBladeNum != totalBladeNum) || (currentGas != totalGas) || (leftBulletLeft != bulletMAX) || (rightBulletLeft != bulletMAX)))
        {
            state = HERO_STATE.FillGas;
            crossFade("supply", 0.1f);
        }
    }

    public void grabbed(GameObject titan, bool leftHand)
    {
        photonView.owner.grabbed = true;
        ModControl.playerlist_speedup = true;

        if (isMounted)
            unmounted();
        state = HERO_STATE.Grab;
        GetComponent<CapsuleCollider>().isTrigger = true;
        falseAttack();
        titanWhoGrabMe = titan;
        if (titanForm && (eren_titan != null))
            eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
        if (!useGun && ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine))
        {
            leftbladetrail.Deactivate();
            rightbladetrail.Deactivate();
            leftbladetrail2.Deactivate();
            rightbladetrail2.Deactivate();
        }
        smoke_3dmg.enableEmission = false;
        sparks.enableEmission = false;
    }

    public bool HasDied()
    {
        return hasDied || isInvincible();
    }

    private void ahssHeadMovement()
    {
        var head = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/neck/head");
        var neck = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/neck");
        var x = Mathf.Sqrt((gunTarget.x - myTransform.position.x) * (gunTarget.x - myTransform.position.x) + (gunTarget.z - myTransform.position.z) * (gunTarget.z - myTransform.position.z));
        targetHeadRotation = head.rotation;
        var vector = gunTarget - myTransform.position;
        var current = -Mathf.Atan2(vector.z, vector.x) * 57.29578f;
        var num3 = -Mathf.DeltaAngle(current, myTransform.rotation.eulerAngles.y - 90f);
        num3 = Mathf.Clamp(num3, -40f, 40f);
        var y = neck.position.y - gunTarget.y;
        var num5 = Mathf.Atan2(y, x) * 57.29578f;
        num5 = Mathf.Clamp(num5, -40f, 30f);
        targetHeadRotation = Quaternion.Euler(head.rotation.eulerAngles.x + num5, head.rotation.eulerAngles.y + num3, head.rotation.eulerAngles.z);
        oldHeadRotation = Quaternion.Lerp(oldHeadRotation, targetHeadRotation, Time.deltaTime * 60f);
        head.rotation = oldHeadRotation;
    }

    private void headMovement()
    {
        var head = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/neck/head");
        var neck = myTransform.Find("Amarture/Controller_Body/hip/spine/chest/neck");
        var x = Mathf.Sqrt((focusPoint.x - myTransform.position.x) * (focusPoint.x - myTransform.position.x) + (focusPoint.z - myTransform.position.z) * (focusPoint.z - myTransform.position.z));
        targetHeadRotation = head.rotation;
        var target = focusPoint - myTransform.position;
        var current = -Mathf.Atan2(target.z, target.x) * 57.29578f;
        var lockpos = -Mathf.DeltaAngle(current, myTransform.rotation.eulerAngles.y - 90f);
        var y_rotation = Mathf.Clamp(lockpos, -40f, 40f);
        var y = neck.position.y - focusPoint.y;
        var x_rotation = Mathf.Atan2(y, x) * 57.29578f;
        x_rotation = Mathf.Clamp(x_rotation, -40f, 30f);
        
        if (lockpos <= -90f || lockpos >= 90f)
        {
            targetHeadRotation = Quaternion.Euler(head.rotation.eulerAngles.x, head.rotation.eulerAngles.y, head.rotation.eulerAngles.z);
            oldHeadRotation = Quaternion.Lerp(oldHeadRotation, targetHeadRotation, Time.deltaTime * 3f);
        }
        else
        {
            targetHeadRotation = Quaternion.Euler(head.rotation.eulerAngles.x + x_rotation, head.rotation.eulerAngles.y + y_rotation, head.rotation.eulerAngles.z);
            oldHeadRotation = Quaternion.Lerp(oldHeadRotation, targetHeadRotation, Time.deltaTime * 7f);
        }
        head.rotation = oldHeadRotation;

        IN_GAME_MAIN_CAMERA.headX = head.rotation.x;
        IN_GAME_MAIN_CAMERA.headY = lockpos;
    }

    public void hookedByHuman(int hooker, Vector3 hookPosition)
    {
        photonView.RPC("RPCHookedByHuman", photonView.owner, hooker, hookPosition);
    }

    [RPC]
    public void hookFail()
    {
        hookTarget = null;
        hookSomeOne = false;
    }

    public void hookToHuman(GameObject target, Vector3 hookPosition)
    {
        releaseIfIHookSb();
        hookTarget = target;
        hookSomeOne = true;
        if (target.GetComponent<HERO>() != null)
            target.GetComponent<HERO>().hookedByHuman(photonView.viewID, hookPosition);
        launchForce = hookPosition - myTransform.position;
        var num = Mathf.Pow(launchForce.magnitude, 0.1f);
        if (grounded)
            rigidbody.AddForce(Vector3.up * Mathf.Min(launchForce.magnitude * 0.2f, 10f), ForceMode.Impulse);
        rigidbody.AddForce(launchForce * num * 0.1f, ForceMode.Impulse);
    }

    private void idle()
    {
        if (state == HERO_STATE.Attack)
            falseAttack();
        state = HERO_STATE.Idle;
        crossFade(standAnimation, 0.1f);
    }

    private bool IsFrontGrounded()
    {
        LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
        LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
        LayerMask mask3 = mask2 | mask;
        return Physics.Raycast(gameObject.transform.position + gameObject.transform.up * 1f, gameObject.transform.forward, 1f, mask3.value);
    }

    public bool IsGrounded()
    {
        LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
        LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
        LayerMask mask3 = mask2 | mask;
        return Physics.Raycast(gameObject.transform.position + Vector3.up * 0.1f, -Vector3.up, 0.3f, mask3.value);
    }

    public bool isInvincible()
    {
        return invincible > 0f;
    }

    private bool isPressDirectionTowardsHero(float h, float v)
    {
        if ((h == 0f) && (v == 0f))
            return false;
        return Mathf.Abs(Mathf.DeltaAngle(getGlobalFacingDirection(h, v), myTransform.rotation.eulerAngles.y)) < 45f;
    }

    private bool IsUpFrontGrounded()
    {
        LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
        LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
        LayerMask mask3 = mask2 | mask;
        return Physics.Raycast(gameObject.transform.position + gameObject.transform.up * 3f, gameObject.transform.forward, 1.2f, mask3.value);
    }

    [RPC]
    private void killObject()
    {
        Destroy(gameObject);
    }

    public void lateUpdate()
    {
        if (state != HERO_STATE.Grab)
            handle3DMG();
        if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) && (myNetWorkName != null))
        {
            if (titanForm && (eren_titan != null))
                myNetWorkName.transform.localPosition = Vector3.up * Screen.height * 2f;
            var start = new Vector3(myTransform.position.x, myTransform.position.y + 2f, myTransform.position.z);
            var obj2 = CacheGameObject.Find("MainCamera");
            LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
            LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask3 = mask2 | mask;
            if ((Vector3.Angle(obj2.transform.forward, start - obj2.transform.position) > 90f) || Physics.Linecast(start, obj2.transform.position, mask3))
                myNetWorkName.transform.localPosition = Vector3.up * Screen.height * 2f;
            else
            {
                Vector2 vector2 = CacheGameObject.Find<Camera>("MainCamera").WorldToScreenPoint(start);
                myNetWorkName.transform.localPosition = new Vector3((int)(vector2.x - Screen.width * 0.5f), (int)(vector2.y - Screen.height * 0.5f), 0f);
            }
        }
        if (!titanForm)
        {
            if ((IN_GAME_MAIN_CAMERA.cameraTilt == 1) && ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine))
            {
                Quaternion quaternion;
                var zero = Vector3.zero;
                var position = Vector3.zero;
                if (isLaunchLeft && (bulletLeft != null) && bulletLeft.GetComponent<Bullet>().isHooked())
                    zero = bulletLeft.transform.position;
                if (isLaunchRight && (bulletRight != null) && bulletRight.GetComponent<Bullet>().isHooked())
                    position = bulletRight.transform.position;
                var vector5 = Vector3.zero;
                if ((zero.magnitude != 0f) && (position.magnitude == 0f))
                    vector5 = zero;
                else
                {
                    if ((zero.magnitude == 0f) && (position.magnitude != 0f))
                        vector5 = position;
                    else
                    {
                        if ((zero.magnitude != 0f) && (position.magnitude != 0f))
                            vector5 = (zero + position) * 0.5f;
                    }
                }
                //projection on the green axis (Y-axis)
                var from = Vector3.Project(vector5 - myTransform.position, CacheGameObject.Find("MainCamera").transform.up);
                //projection on the red axis (Z-axis)
                var vector7 = Vector3.Project(vector5 - myTransform.position, CacheGameObject.Find("MainCamera").transform.right);
                if (vector5.magnitude > 0f)
                {
                    var to = from + vector7;
                    var num = Vector3.Angle(vector5 - myTransform.position, rigidbody.velocity) * 0.005f;
                    var vector15 = CacheGameObject.Find("MainCamera").transform.right + vector7.normalized;
                    quaternion = Quaternion.Euler(CacheGameObject.Find("MainCamera").transform.rotation.eulerAngles.x, CacheGameObject.Find("MainCamera").transform.rotation.eulerAngles.y, vector15.magnitude >= 1f ? -Vector3.Angle(from, to) * num : Vector3.Angle(from, to) * num);
                }
                else
                    quaternion = Quaternion.Euler(CacheGameObject.Find("MainCamera").transform.rotation.eulerAngles.x, CacheGameObject.Find("MainCamera").transform.rotation.eulerAngles.y, 0f);
                CacheGameObject.Find("MainCamera").transform.rotation = Quaternion.Lerp(CacheGameObject.Find("MainCamera").transform.rotation, quaternion, Time.deltaTime * 2f);
            }
            if ((state == HERO_STATE.Grab) && (titanWhoGrabMe != null))
            {
                if (titanWhoGrabMe.GetComponent<TITAN>() != null)
                {
                    myTransform.position = titanWhoGrabMe.GetComponent<TITAN>().grabTF.transform.position;
                    myTransform.rotation = titanWhoGrabMe.GetComponent<TITAN>().grabTF.transform.rotation;
                }
                else
                {
                    if (titanWhoGrabMe.GetComponent<FEMALE_TITAN>() != null)
                    {
                        myTransform.position = titanWhoGrabMe.GetComponent<FEMALE_TITAN>().grabTF.transform.position;
                        myTransform.rotation = titanWhoGrabMe.GetComponent<FEMALE_TITAN>().grabTF.transform.rotation;
                    }
                }
            }
            if (useGun)
            {
                if (leftArmAim || rightArmAim)
                {
                    var vector9 = gunTarget - myTransform.position;
                    var current = -Mathf.Atan2(vector9.z, vector9.x) * 57.29578f;
                    var num3 = -Mathf.DeltaAngle(current, myTransform.rotation.eulerAngles.y - 90f);
                    ahssHeadMovement();
                    if (!isLeftHandHooked && leftArmAim && (num3 < 40f) && (num3 > -90f))
                        leftArmAimTo(gunTarget);
                    if (!isRightHandHooked && rightArmAim && (num3 > -40f) && (num3 < 90f))
                        rightArmAimTo(gunTarget);
                }
                else
                {
                    if (!grounded)
                    {
                        handL.localRotation = Quaternion.Euler(90f, 0f, 0f);
                        handR.localRotation = Quaternion.Euler(-90f, 0f, 0f);
                    }
                }
                if (isLeftHandHooked && (bulletLeft != null))
                    leftArmAimTo(bulletLeft.transform.position);
                if (isRightHandHooked && (bulletRight != null))
                    rightArmAimTo(bulletRight.transform.position);
            }
            else if (photonView.isMine)
                headMovement();
            setHookedPplDirection();
            bodyLean();
            if (!animation.IsPlaying("attack3_2") && !animation.IsPlaying("attack5") && !animation.IsPlaying("special_petra"))
                rigidbody.rotation = Quaternion.Lerp(gameObject.transform.rotation, targetRotation, Time.deltaTime * 6f);
        }
    }

    public void launch(Vector3 des, bool left = true, bool leviMode = false)
    {
        if (isMounted)
            unmounted();
        if (state != HERO_STATE.Attack)
            idle();
        var vector = des - myTransform.position;
        if (left)
            launchPointLeft = des;
        else
            launchPointRight = des;
        vector.Normalize();
        vector = vector * 20f;
        if ((bulletLeft != null) && (bulletRight != null) && bulletLeft.GetComponent<Bullet>().isHooked() && bulletRight.GetComponent<Bullet>().isHooked())
            vector = vector * 0.8f;
        if (animation.IsPlaying("attack5") || animation.IsPlaying("special_petra"))
            leviMode = true;
        else
            leviMode = false;
        if (!leviMode)
        {
            falseAttack();
            idle();
            if (useGun)
                crossFade("AHSS_hook_forward_both", 0.1f);
            else
            {
                if (left && !isRightHandHooked)
                    crossFade("air_hook_l_just", 0.1f);
                else
                {
                    if (!left && !isLeftHandHooked)
                        crossFade("air_hook_r_just", 0.1f);
                    else
                    {
                        crossFade("dash", 0.1f);
                        animation["dash"].time = 0f;
                    }
                }
            }
        }
        if (left)
            isLaunchLeft = true;
        if (!left)
            isLaunchRight = true;
        launchForce = vector;
        if (!leviMode)
        {
            if (vector.y < 30f)
                launchForce += Vector3.up * (30f - vector.y);
            if (des.y >= myTransform.position.y)
                launchForce += Vector3.up * (des.y - myTransform.position.y) * 10f;
            rigidbody.AddForce(launchForce);
        }
        facingDirection = Mathf.Atan2(launchForce.x, launchForce.z) * 57.29578f;
        var quaternion = Quaternion.Euler(0f, facingDirection, 0f);
        gameObject.transform.rotation = quaternion;
        rigidbody.rotation = quaternion;
        targetRotation = quaternion;
        if (left)
            launchElapsedTimeL = 0f;
        else
            launchElapsedTimeR = 0f;
        if (leviMode)
            launchElapsedTimeR = -100f;
        if (animation.IsPlaying("special_petra"))
        {
            launchElapsedTimeR = -100f;
            launchElapsedTimeL = -100f;
            if (bulletRight != null)
            {
                bulletRight.GetComponent<Bullet>().disable();
                releaseIfIHookSb();
            }
            if (bulletLeft != null)
            {
                bulletLeft.GetComponent<Bullet>().disable();
                releaseIfIHookSb();
            }
        }
        sparks.enableEmission = false;
    }

    private void launchLeftRope(RaycastHit hit, bool single, int mode = 0)
    {
        if (currentGas != 0f)
        {
            useGas();
            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                bulletLeft = (GameObject)Instantiate(CacheResources.Load("hook"));
            else
            {
                if (photonView.isMine)
                    bulletLeft = PhotonNetwork.Instantiate("hook", myTransform.position, myTransform.rotation, 0);
            }
            var obj2 = !useGun ? hookRefL1 : hookRefL2;
            var str = !useGun ? "hookRefL1" : "hookRefL2";
            bulletLeft.transform.position = obj2.transform.position;
            var component = bulletLeft.GetComponent<Bullet>();
            var num = !single ? (hit.distance <= 50f ? hit.distance * 0.05f : hit.distance * 0.3f) : 0f;
            var vector = hit.point - myTransform.right * num - bulletLeft.transform.position;
            vector.Normalize();
            if (mode == 1)
                component.launch(vector * 3f, rigidbody.velocity, str, true, gameObject, true);
            else
                component.launch(vector * 3f, rigidbody.velocity, str, true, gameObject);
            launchPointLeft = Vector3.zero;
        }
    }

    private void launchRightRope(RaycastHit hit, bool single, int mode = 0)
    {
        if (currentGas != 0f)
        {
            useGas();
            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                bulletRight = (GameObject)Instantiate(CacheResources.Load("hook"));
            else
            {
                if (photonView.isMine)
                    bulletRight = PhotonNetwork.Instantiate("hook", myTransform.position, myTransform.rotation, 0);
            }
            var obj2 = !useGun ? hookRefR1 : hookRefR2;
            var str = !useGun ? "hookRefR1" : "hookRefR2";
            bulletRight.transform.position = obj2.transform.position;
            var component = bulletRight.GetComponent<Bullet>();
            var num = !single ? (hit.distance <= 50f ? hit.distance * 0.05f : hit.distance * 0.3f) : 0f;
            var vector = hit.point + myTransform.right * num - bulletRight.transform.position;
            vector.Normalize();
            if (mode == 1)
                component.launch(vector * 5f, rigidbody.velocity, str, false, gameObject, true);
            else
                component.launch(vector * 3f, rigidbody.velocity, str, false, gameObject);
            launchPointRight = Vector3.zero;
        }
    }

    private void leftArmAimTo(Vector3 target)
    {
        var y = target.x - upperarmL.transform.position.x;
        var num2 = target.y - upperarmL.transform.position.y;
        var x = target.z - upperarmL.transform.position.z;
        var num4 = Mathf.Sqrt(y * y + x * x);
        handL.localRotation = Quaternion.Euler(90f, 0f, 0f);
        forearmL.localRotation = Quaternion.Euler(-90f, 0f, 0f);
        upperarmL.rotation = Quaternion.Euler(0f, 90f + Mathf.Atan2(y, x) * 57.29578f, -Mathf.Atan2(num2, num4) * 57.29578f);
    }

    public void markDie()
    {
        hasDied = true;
        state = HERO_STATE.Die;
    }

    [RPC]
    private void net3DMGSMOKE(bool ifON)
    {
        if (smoke_3dmg != null)
            smoke_3dmg.enableEmission = ifON;
    }

    [RPC]
    private void netContinueAnimation()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                if (current.speed == 1f)
                    return;
                current.speed = 1f;
            }
        }
        finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
        playAnimation(currentPlayingClipName());
    }

    [RPC]
    private void netCrossFade(string aniName, float time)
    {
        currentAnimation = aniName;
        if (animation != null)
            animation.CrossFade(aniName, time);
    }

    [RPC]
    public void netDie(Vector3 v, bool isBite, int viewID = -1, string titanName = "", bool killByTitan = true)
    {
        if (photonView.isMine && titanForm && (eren_titan != null))
            eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
        if (bulletLeft != null)
            bulletLeft.GetComponent<Bullet>().removeMe();
        if (bulletRight != null)
            bulletRight.GetComponent<Bullet>().removeMe();
        meatDie.Play();
        if (!useGun && ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine))
        {
            leftbladetrail.Deactivate();
            rightbladetrail.Deactivate();
            leftbladetrail2.Deactivate();
            rightbladetrail2.Deactivate();
        }
        falseAttack();
        breakApart(v, isBite);
        if (photonView.isMine)
        {
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setSpectorMode(false);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().gameOver = true;
            FengGameManagerMKII.MKII.myRespawnTime = 0f;
        }
        hasDied = true;
        var transform = myTransform.Find("audio_die");
        transform.parent = null;
        transform.GetComponent<AudioSource>().Play();
        gameObject.GetComponent<SmoothSyncMovement>().disabled = true;
        if (photonView.isMine)
        {
            PhotonNetwork.RemoveRPCs(photonView);
            var propertiesToSet = new Hashtable
            {
                { PhotonPlayerProperty.dead, true },
                { PhotonPlayerProperty.deaths, (int)PhotonNetwork.player.customProperties[PhotonPlayerProperty.deaths] + 1 }
            };
            PhotonNetwork.player.SetCustomProperties(propertiesToSet);
            FengGameManagerMKII.MKII.photonView.RPC("someOneIsDead", PhotonTargets.MasterClient, titanName != string.Empty ? 1 : 0);
            if (viewID != -1)
            {
                var view = PhotonView.Find(viewID);
                if (view != null)
                {
                    FengGameManagerMKII.MKII.sendKillInfo(killByTitan, (string)view.owner.customProperties[PhotonPlayerProperty.name], false, (string)PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]);
                    propertiesToSet = new Hashtable { { PhotonPlayerProperty.kills, (int)view.owner.customProperties[PhotonPlayerProperty.kills] + 1 } };
                    view.owner.SetCustomProperties(propertiesToSet);
                }
            }
            else
                FengGameManagerMKII.MKII.sendKillInfo(titanName != string.Empty, titanName, false, (string)PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]);
        }
        if (photonView.isMine)
            PhotonNetwork.Destroy(photonView);
    }

    [RPC]
    private void netDie2(int viewID = -1, string titanName = "")
    {
        GameObject obj2;
        if (photonView.isMine)
        {
            PhotonNetwork.RemoveRPCs(photonView);
            if (titanForm && (eren_titan != null))
                eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
        }
        meatDie.Play();
        if (bulletLeft != null)
            bulletLeft.GetComponent<Bullet>().removeMe();
        if (bulletRight != null)
            bulletRight.GetComponent<Bullet>().removeMe();
        var transform = myTransform.Find("audio_die");
        transform.parent = null;
        transform.GetComponent<AudioSource>().Play();
        if (photonView.isMine)
        {
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(null);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setSpectorMode(true);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().gameOver = true;
            FengGameManagerMKII.MKII.myRespawnTime = 0f;
        }
        falseAttack();
        hasDied = true;
        gameObject.GetComponent<SmoothSyncMovement>().disabled = true;
        if ((IN_GAME_MAIN_CAMERA.IsMultiplayer) && photonView.isMine)
        {
            PhotonNetwork.RemoveRPCs(photonView);
            var propertiesToSet = new Hashtable
            {
                { PhotonPlayerProperty.dead, true },
                { PhotonPlayerProperty.deaths, (int)PhotonNetwork.player.customProperties[PhotonPlayerProperty.deaths] + 1 }
            };
            PhotonNetwork.player.SetCustomProperties(propertiesToSet);
            if (viewID != -1)
            {
                var view = PhotonView.Find(viewID);
                if (view != null)
                {
                    FengGameManagerMKII.MKII.sendKillInfo(true, (string)view.owner.customProperties[PhotonPlayerProperty.name], false, (string)PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]);
                    propertiesToSet = new Hashtable { { PhotonPlayerProperty.kills, (int)view.owner.customProperties[PhotonPlayerProperty.kills] + 1 } };
                    view.owner.SetCustomProperties(propertiesToSet);
                }
            }
            else
                FengGameManagerMKII.MKII.sendKillInfo(true, titanName, false, (string)PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]);
            FengGameManagerMKII.MKII.photonView.RPC("someOneIsDead", PhotonTargets.MasterClient, titanName != string.Empty ? 1 : 0);
        }
        if (!IN_GAME_MAIN_CAMERA.IsSingleplayer)
            obj2 = PhotonNetwork.Instantiate("hitMeat2", myTransform.position, Quaternion.Euler(270f, 0f, 0f), 0);
        else
            obj2 = (GameObject)Instantiate(CacheResources.Load("hitMeat2"));
        obj2.transform.position = myTransform.position;
        if (photonView.isMine)
            PhotonNetwork.Destroy(photonView);
    }

    [RPC]
    private void netGrabbed(int id, bool leftHand)
    {
        photonView.owner.grabbed = true;
        ModControl.playerlist_speedup = true;

        titanWhoGrabMeID = id;
        grabbed(PhotonView.Find(id).gameObject, leftHand);
    }

    [RPC]
    private void netlaughAttack()
    {
        foreach (var obj2 in GameObject.FindGameObjectsWithTag("titan"))
        {
            if ((Vector3.Distance(obj2.transform.position, myTransform.position) < 50f) && (Vector3.Angle(obj2.transform.forward, myTransform.position - obj2.transform.position) < 90f) && (obj2.GetComponent<TITAN>() != null))
                obj2.GetComponent<TITAN>().beLaughAttacked();
        }
    }

    [RPC]
    private void netPauseAnimation()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                current.speed = 0f;
            }
        }
        finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
    }

    [RPC]
    private void netPlayAnimation(string aniName)
    {
        currentAnimation = aniName;
        if (animation != null)
            animation.Play(aniName);
    }

    [RPC]
    private void netPlayAnimationAt(string aniName, float normalizedTime)
    {
        currentAnimation = aniName;
        if (animation != null)
        {
            animation.Play(aniName);
            animation[aniName].normalizedTime = normalizedTime;
        }
    }

    [RPC]
    private void netSetIsGrabbedFalse()
    {
        photonView.owner.grabbed = false;
        ModControl.playerlist_speedup = false;
        state = HERO_STATE.Idle;
    }

    [RPC]
    private void netTauntAttack(float tauntTime, float distance = 100f)
    {
        foreach (var obj2 in GameObject.FindGameObjectsWithTag("titan"))
        {
            if ((Vector3.Distance(obj2.transform.position, myTransform.position) < distance) && (obj2.GetComponent<TITAN>() != null))
                obj2.GetComponent<TITAN>().beTauntedBy(gameObject, tauntTime);
        }
    }

    [RPC]
    private void netUngrabbed()
    {
        photonView.owner.grabbed = false;
        ModControl.playerlist_speedup = false;

        ungrabbed();
        netPlayAnimation(standAnimation);
        falseAttack();
    }

    private void OnDestroy()
    {
        if (myNetWorkName != null)
            Destroy(myNetWorkName);
        if (gunDummy != null)
            Destroy(gunDummy);
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
            releaseIfIHookSb();
        if (CacheGameObject.Find("MultiplayerManager") != null)
            FengGameManagerMKII.MKII.removeHero(this);
    }

    public void pauseAnimation()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                current.speed = 0f;
            }
        }
        finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
        if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) && photonView.isMine)
            photonView.RPC("netPauseAnimation", PhotonTargets.Others);
    }

    public void playAnimation(string aniName)
    {
        currentAnimation = aniName;
        animation.Play(aniName);
        if (PhotonNetwork.connected && photonView.isMine)
            photonView.RPC("netPlayAnimation", PhotonTargets.Others, aniName);
    }

    private void playAnimationAt(string aniName, float normalizedTime)
    {
        currentAnimation = aniName;
        animation.Play(aniName);
        animation[aniName].normalizedTime = normalizedTime;
        if (PhotonNetwork.connected && photonView.isMine)
            photonView.RPC("netPlayAnimationAt", PhotonTargets.Others, aniName, normalizedTime);
    }

    private void releaseIfIHookSb()
    {
        if (hookSomeOne && (hookTarget != null))
        {
            hookTarget.GetPhotonView().RPC("badGuyReleaseMe", hookTarget.GetPhotonView().owner);
            hookTarget = null;
            hookSomeOne = false;
        }
    }

    public void resetAnimationSpeed()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                current.speed = 1f;
            }
        }
        finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
        customAnimationSpeed();
    }

    private void rightArmAimTo(Vector3 target)
    {
        var y = target.x - upperarmR.transform.position.x;
        var num2 = target.y - upperarmR.transform.position.y;
        var x = target.z - upperarmR.transform.position.z;
        var num4 = Mathf.Sqrt(y * y + x * x);
        handR.localRotation = Quaternion.Euler(-90f, 0f, 0f);
        forearmR.localRotation = Quaternion.Euler(90f, 0f, 0f);
        upperarmR.rotation = Quaternion.Euler(180f, 90f + Mathf.Atan2(y, x) * 57.29578f, Mathf.Atan2(num2, num4) * 57.29578f);
    }

    [RPC]
    private void RPCHookedByHuman(int hooker, Vector3 hookPosition)
    {
        hookBySomeOne = true;
        badGuy = PhotonView.Find(hooker).gameObject;
        if (Vector3.Distance(hookPosition, myTransform.position) < 15f)
        {
            launchForce = PhotonView.Find(hooker).gameObject.transform.position - myTransform.position;
            rigidbody.AddForce(-rigidbody.velocity * 0.9f, ForceMode.VelocityChange);
            var num = Mathf.Pow(launchForce.magnitude, 0.1f);
            if (grounded)
                rigidbody.AddForce(Vector3.up * Mathf.Min(launchForce.magnitude * 0.2f, 10f), ForceMode.Impulse);
            rigidbody.AddForce(launchForce * num * 0.1f, ForceMode.Impulse);
            if (state != HERO_STATE.Grab)
            {
                dashTime = 1f;
                crossFade("dash", 0.05f);
                animation["dash"].time = 0.1f;
                state = HERO_STATE.AirDodge;
                falseAttack();
                facingDirection = Mathf.Atan2(launchForce.x, launchForce.z) * 57.29578f;
                var quaternion = Quaternion.Euler(0f, facingDirection, 0f);
                gameObject.transform.rotation = quaternion;
                rigidbody.rotation = quaternion;
                targetRotation = quaternion;
            }
        }
        else
        {
            hookBySomeOne = false;
            badGuy = null;
            PhotonView.Find(hooker).RPC("hookFail", PhotonView.Find(hooker).owner);
        }
    }

    private void salute()
    {
        state = HERO_STATE.Salute;
        crossFade("salute", 0.1f);
    }

    private void setHookedPplDirection()
    {
        almostSingleHook = false;
        if (isRightHandHooked && isLeftHandHooked)
        {
            if ((bulletLeft != null) && (bulletRight != null))
            {
                var normal = bulletLeft.transform.position - bulletRight.transform.position;
                if (normal.sqrMagnitude < 4f)
                {
                    var vector2 = (bulletLeft.transform.position + bulletRight.transform.position) * 0.5f - myTransform.position;
                    facingDirection = Mathf.Atan2(vector2.x, vector2.z) * 57.29578f;
                    if (useGun && (state != HERO_STATE.Attack))
                    {
                        var current = -Mathf.Atan2(rigidbody.velocity.z, rigidbody.velocity.x) * 57.29578f;
                        var target = -Mathf.Atan2(vector2.z, vector2.x) * 57.29578f;
                        var num3 = -Mathf.DeltaAngle(current, target);
                        facingDirection += num3;
                    }
                    almostSingleHook = true;
                }
                else
                {
                    var to = myTransform.position - bulletLeft.transform.position;
                    var vector4 = myTransform.position - bulletRight.transform.position;
                    var vector5 = (bulletLeft.transform.position + bulletRight.transform.position) * 0.5f;
                    var from = myTransform.position - vector5;
                    if ((Vector3.Angle(from, to) < 30f) && (Vector3.Angle(from, vector4) < 30f))
                    {
                        almostSingleHook = true;
                        var vector7 = vector5 - myTransform.position;
                        facingDirection = Mathf.Atan2(vector7.x, vector7.z) * 57.29578f;
                    }
                    else
                    {
                        almostSingleHook = false;
                        var forward = myTransform.forward;
                        Vector3.OrthoNormalize(ref normal, ref forward);
                        facingDirection = Mathf.Atan2(forward.x, forward.z) * 57.29578f;
                        var num4 = Mathf.Atan2(to.x, to.z) * 57.29578f;
                        if (Mathf.DeltaAngle(num4, facingDirection) > 0f)
                            facingDirection += 180f;
                    }
                }
            }
        }
        else
        {
            almostSingleHook = true;
            var zero = Vector3.zero;
            if (isRightHandHooked && (bulletRight != null))
                zero = bulletRight.transform.position - myTransform.position;
            else
            {
                if (isLeftHandHooked && (bulletLeft != null))
                    zero = bulletLeft.transform.position - myTransform.position;
                else
                    return;
            }
            facingDirection = Mathf.Atan2(zero.x, zero.z) * 57.29578f;
            if (state != HERO_STATE.Attack)
            {
                var num6 = -Mathf.Atan2(rigidbody.velocity.z, rigidbody.velocity.x) * 57.29578f;
                var num7 = -Mathf.Atan2(zero.z, zero.x) * 57.29578f;
                var num8 = -Mathf.DeltaAngle(num6, num7);
                if (useGun)
                    facingDirection += num8;
                else
                {
                    var num9 = 0f;
                    if ((isLeftHandHooked && (num8 < 0f)) || (isRightHandHooked && (num8 > 0f)))
                        num9 = -0.1f;
                    else
                        num9 = 0.1f;
                    facingDirection += num8 * num9;
                }
            }
        }
    }

    [RPC]
    private void setMyTeam(int val)
    {
        print("HERO.setMyTeam() - set team " + val);
        myTeam = val;
        checkBoxLeft.GetComponent<TriggerColliderWeapon>().myTeam = val;
        checkBoxRight.GetComponent<TriggerColliderWeapon>().myTeam = val;
    }

    public void setSkillHUDPosition()
    {
        skillCD = CacheGameObject.Find("skill_cd_" + skillId);
        if (skillCD != null)
            skillCD.transform.localPosition = CacheGameObject.Find("skill_cd_bottom").transform.localPosition;
        if (useGun)
            skillCD.transform.localPosition = Vector3.up * 5000f;
    }

    public void setStat(bool onHorse = false)
    {
        skillCD = null;
        skillCDLast = 1.5f;
        skillId = !onHorse ? setup.myCostume.stat.skillId : "horse";
        if (skillId == "levi")
            skillCDLast = 3.5f;
        customAnimationSpeed();
        if (skillId == "armin")
            skillCDLast = 5f;
        if (skillId == "marco")
            skillCDLast = 10f;
        if (skillId == "jean")
            skillCDLast = 0.001f;
        if (skillId == "eren")
        {
            skillCDLast = 120f;
            if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
            {
                if (LevelInfo.getInfo(FengGameManagerMKII.level).teamTitan || (LevelInfo.getInfo(FengGameManagerMKII.level).type == GAMEMODE.RACING) || (LevelInfo.getInfo(FengGameManagerMKII.level).type == GAMEMODE.PVP_CAPTURE) || (LevelInfo.getInfo(FengGameManagerMKII.level).type == GAMEMODE.TROST))
                {
                    skillId = "petra";
                    skillCDLast = 1f;
                }
                else
                {
                    var num = 0;
                    foreach (var player in PhotonNetwork.playerList)
                    {
                        if (player.info.isHuman && player.stats.CHARACTER == "EREN")
                            num++;
                    }
                    if (num > 1)
                    {
                        skillId = "petra";
                        skillCDLast = 1f;
                    }
                }
            }
        }
        if (skillId == "sasha")
            skillCDLast = 20f;
        if (skillId == "petra")
            skillCDLast = 3.5f;
        if (skillId == "horse")
        {
            InitUIElements();
            skillId = "horse";
            skillCDLast = 5f;
        }
        skillCDDuration = skillCDLast;
        speed = setup.myCostume.stat.SPD / 10f;
        totalGas = currentGas = setup.myCostume.stat.GAS;
        totalBladeSta = currentBladeSta = setup.myCostume.stat.BLA;
        rigidbody.mass = 0.5f - (setup.myCostume.stat.ACL - 100) * 0.001f;
        CacheGameObject.Find("skill_cd_bottom").transform.localPosition = new Vector3(0f, -Screen.height * 0.5f + 5f, 0f);
        skillCD = CacheGameObject.Find("skill_cd_" + skillId);
        skillCD.transform.localPosition = CacheGameObject.Find("skill_cd_bottom").transform.localPosition;
        CacheGameObject.Find("GasUI").transform.localPosition = CacheGameObject.Find("skill_cd_bottom").transform.localPosition;
        if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine)
        {
            CacheGameObject.Find<UISprite>("bulletL").enabled = false;
            CacheGameObject.Find<UISprite>("bulletR").enabled = false;
            CacheGameObject.Find<UISprite>("bulletL1").enabled = false;
            CacheGameObject.Find<UISprite>("bulletR1").enabled = false;
            CacheGameObject.Find<UISprite>("bulletL2").enabled = false;
            CacheGameObject.Find<UISprite>("bulletR2").enabled = false;
            CacheGameObject.Find<UISprite>("bulletL3").enabled = false;
            CacheGameObject.Find<UISprite>("bulletR3").enabled = false;
            CacheGameObject.Find<UISprite>("bulletL4").enabled = false;
            CacheGameObject.Find<UISprite>("bulletR4").enabled = false;
            CacheGameObject.Find<UISprite>("bulletL5").enabled = false;
            CacheGameObject.Find<UISprite>("bulletR5").enabled = false;
            CacheGameObject.Find<UISprite>("bulletL6").enabled = false;
            CacheGameObject.Find<UISprite>("bulletR6").enabled = false;
            CacheGameObject.Find<UISprite>("bulletL7").enabled = false;
            CacheGameObject.Find<UISprite>("bulletR7").enabled = false;
        }
        if (setup.myCostume.uniform_type == UNIFORM_TYPE.CasualAHSS)
        {
            standAnimation = "AHSS_stand_gun";
            useGun = true;
            gunDummy = new GameObject { name = "gunDummy" };
            gunDummy.transform.position = myTransform.position;
            gunDummy.transform.rotation = myTransform.rotation;
            myGroup = GROUP.A;
            setTeam(2);
            if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine)
            {
                CacheGameObject.Find<UISprite>("bladeCL").enabled = false;
                CacheGameObject.Find<UISprite>("bladeCR").enabled = false;
                CacheGameObject.Find<UISprite>("bladel1").enabled = false;
                CacheGameObject.Find<UISprite>("blader1").enabled = false;
                CacheGameObject.Find<UISprite>("bladel2").enabled = false;
                CacheGameObject.Find<UISprite>("blader2").enabled = false;
                CacheGameObject.Find<UISprite>("bladel3").enabled = false;
                CacheGameObject.Find<UISprite>("blader3").enabled = false;
                CacheGameObject.Find<UISprite>("bladel4").enabled = false;
                CacheGameObject.Find<UISprite>("blader4").enabled = false;
                CacheGameObject.Find<UISprite>("bladel5").enabled = false;
                CacheGameObject.Find<UISprite>("blader5").enabled = false;
                CacheGameObject.Find<UISprite>("bulletL").enabled = true;
                CacheGameObject.Find<UISprite>("bulletR").enabled = true;
                CacheGameObject.Find<UISprite>("bulletL1").enabled = true;
                CacheGameObject.Find<UISprite>("bulletR1").enabled = true;
                CacheGameObject.Find<UISprite>("bulletL2").enabled = true;
                CacheGameObject.Find<UISprite>("bulletR2").enabled = true;
                CacheGameObject.Find<UISprite>("bulletL3").enabled = true;
                CacheGameObject.Find<UISprite>("bulletR3").enabled = true;
                CacheGameObject.Find<UISprite>("bulletL4").enabled = true;
                CacheGameObject.Find<UISprite>("bulletR4").enabled = true;
                CacheGameObject.Find<UISprite>("bulletL5").enabled = true;
                CacheGameObject.Find<UISprite>("bulletR5").enabled = true;
                CacheGameObject.Find<UISprite>("bulletL6").enabled = true;
                CacheGameObject.Find<UISprite>("bulletR6").enabled = true;
                CacheGameObject.Find<UISprite>("bulletL7").enabled = true;
                CacheGameObject.Find<UISprite>("bulletR7").enabled = true;
                skillCD.transform.localPosition = Vector3.up * 5000f;
            }
        }
        else
        {
            if (setup.myCostume.sex == SEX.FEMALE)
            {
                standAnimation = "stand";
                setTeam(1);
            }
            else
            {
                standAnimation = "stand_levi";
                setTeam(1);
            }
        }
    }

    public static void InitUIElements()
    {
        if (GameObject.Find("skill_cd_horse") == null)
        {
            bottom_color_default = GameObject.Find("skill_cd_bottom").GetComponent<UISprite>().color;
            var gameObject = LoadGameObjects.Instantiate(LoadGameObjects.Fetch("skill_cd_horse"));
            gameObject.hideFlags = HideFlags.DontSave;
            gameObject.transform.parent = GameObject.Find("skill_cd_bottom").transform.parent;
            gameObject.layer = gameObject.transform.parent.gameObject.layer;
            gameObject.transform.localScale = new Vector3(32f, 32f, 1f);
        }
    }

    public void setTeam(int team)
    {
        setMyTeam(team);
        if ((IN_GAME_MAIN_CAMERA.IsMultiplayer) && photonView.isMine)
        {
            photonView.RPC("setMyTeam", PhotonTargets.OthersBuffered, team);
            var propertiesToSet = new Hashtable { { PhotonPlayerProperty.team, team } };
            PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        }
    }

    public void shootFlare(int type)
    {
        var flag = false;
        if ((type == 1) && (flare1CD == 0f))
        {
            flare1CD = flareTotalCD;
            flag = true;
        }
        if ((type == 2) && (flare2CD == 0f))
        {
            flare2CD = flareTotalCD;
            flag = true;
        }
        if ((type == 3) && (flare3CD == 0f))
        {
            flare3CD = flareTotalCD;
            flag = true;
        }
        if (flag)
        {
            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            {
                var obj2 = (GameObject)Instantiate(CacheResources.Load("FX/flareBullet" + type), myTransform.position, myTransform.rotation);
                obj2.GetComponent<FlareMovement>().dontShowHint();
                Destroy(obj2, 25f);
            }
            else
                PhotonNetwork.Instantiate("FX/flareBullet" + type, myTransform.position, myTransform.rotation, 0).GetComponent<FlareMovement>().dontShowHint();
        }
    }

    private void showAimUI()
    {
        Vector3 vector5;
        if (Screen.showCursor)
        {
            var cross1 = CacheGameObject.Find("cross1");
            var cross2 = CacheGameObject.Find("cross2");
            var crossl1 = CacheGameObject.Find("crossL1");
            var crossl2 = CacheGameObject.Find("crossL2");
            var crossr1 = CacheGameObject.Find("crossR1");
            var crossr2 = CacheGameObject.Find("crossR2");
            var dist = CacheGameObject.Find("LabelDistance");
            vector5 = Vector3.up * 10000f;
            crossr1.transform.localPosition = crossr2.transform.localPosition = crossl1.transform.localPosition = crossl2.transform.localPosition = vector5;
            cross2.transform.localPosition = cross1.transform.localPosition = dist.transform.localPosition = vector5;
        }
        else
        {
            checkTitan();
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
            LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask3 = mask2 | mask;
            if (Physics.Raycast(ray, out hit, 1E+07f, mask3.value))
            {
                RaycastHit hit2;
                var obj9 = CacheGameObject.Find("cross1");
                var obj10 = CacheGameObject.Find("cross2");
                obj9.transform.localPosition = Input.mousePosition;
                var transform = obj9.transform;
                transform.localPosition -= new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);
                obj10.transform.localPosition = obj9.transform.localPosition;
                vector5 = hit.point - myTransform.position;
                focusPoint = hit.point;
                var magnitude = vector5.magnitude;
                var obj11 = CacheGameObject.Find("LabelDistance");
                var str = magnitude <= 10000f ? ((int)magnitude).ToString() : "???";
                obj11.GetComponent<UILabel>().text = str;
                if (magnitude > 120f)
                {
                    var transform2 = obj9.transform;
                    transform2.localPosition += Vector3.up * 10000f;
                    obj11.transform.localPosition = obj10.transform.localPosition;
                }
                else
                {
                    var transform3 = obj10.transform;
                    transform3.localPosition += Vector3.up * 10000f;
                    obj11.transform.localPosition = obj9.transform.localPosition;
                }
                var transform4 = obj11.transform;
                transform4.localPosition -= new Vector3(0f, 15f, 0f);
                var vector = new Vector3(0f, 0.4f, 0f);
                vector -= myTransform.right * 0.3f;
                var vector2 = new Vector3(0f, 0.4f, 0f);
                vector2 += myTransform.right * 0.3f;
                var num2 = hit.distance <= 50f ? hit.distance * 0.05f : hit.distance * 0.3f;
                var vector3 = hit.point - myTransform.right * num2 - (myTransform.position + vector);
                var vector4 = hit.point + myTransform.right * num2 - (myTransform.position + vector2);
                vector3.Normalize();
                vector4.Normalize();
                vector3 = vector3 * 1000000f;
                vector4 = vector4 * 1000000f;
                if (Physics.Linecast(myTransform.position + vector, myTransform.position + vector + vector3, out hit2, mask3.value))
                {
                    var crossl1 = CacheGameObject.Find("crossL1");
                    crossl1.transform.localPosition = currentCamera.WorldToScreenPoint(hit2.point);
                    var transform5 = crossl1.transform;
                    transform5.localPosition -= new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);
                    crossl1.transform.localRotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(crossl1.transform.localPosition.y - (Input.mousePosition.y - Screen.height * 0.5f), crossl1.transform.localPosition.x - (Input.mousePosition.x - Screen.width * 0.5f)) * 57.29578f + 180f);
                    var crossl2 = CacheGameObject.Find("crossL2");
                    crossl2.transform.localPosition = crossl1.transform.localPosition;
                    crossl2.transform.localRotation = crossl1.transform.localRotation;
                    if (hit2.distance > 120f)
                        crossl1.transform.localPosition += Vector3.up * 10000f;
                    else
                        crossl2.transform.localPosition += Vector3.up * 10000f;

                    if (photonView.isMine && bulletLeft != null && bulletLeft.GetComponent<Bullet>().isHooked()) //toton hook check
                    {
                        crossl1.GetComponent<UISprite>().color = Color.green;
                        crossl2.GetComponent<UISprite>().color = Color.clear;
                    }
                    else
                    {
                        crossl1.GetComponent<UISprite>().color = Color.white;
                        crossl2.GetComponent<UISprite>().color = Color.red;
                    }
                }
                if (Physics.Linecast(myTransform.position + vector2, myTransform.position + vector2 + vector4, out hit2, mask3.value))
                {
                    var crossr1 = CacheGameObject.Find("crossR1");
                    crossr1.transform.localPosition = currentCamera.WorldToScreenPoint(hit2.point);
                    var transform8 = crossr1.transform;
                    transform8.localPosition -= new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);
                    crossr1.transform.localRotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(crossr1.transform.localPosition.y - (Input.mousePosition.y - Screen.height * 0.5f), crossr1.transform.localPosition.x - (Input.mousePosition.x - Screen.width * 0.5f)) * 57.29578f);
                    var crossr2 = CacheGameObject.Find("crossR2");
                    crossr2.transform.localPosition = crossr1.transform.localPosition;
                    crossr2.transform.localRotation = crossr1.transform.localRotation;
                    if (hit2.distance > 120f)
                        crossr1.transform.localPosition += Vector3.up * 10000f;
                    else
                        crossr2.transform.localPosition += Vector3.up * 10000f;

                    if (photonView.isMine && bulletRight != null && bulletRight.GetComponent<Bullet>().isHooked()) //toton hook check
                    {
                        crossr1.GetComponent<UISprite>().color = Color.green;
                        crossr2.GetComponent<UISprite>().color = Color.clear;
                    }
                    else
                    {
                        crossr1.GetComponent<UISprite>().color = Color.white;
                        crossr2.GetComponent<UISprite>().color = Color.red;
                    }
                }
            }
        }
    }

    private void showFlareCD()
    {
        if (CacheGameObject.Find("UIflare1") != null)
        {
            CacheGameObject.Find<UISprite>("UIflare1").fillAmount = (flareTotalCD - flare1CD) / flareTotalCD;
            CacheGameObject.Find<UISprite>("UIflare2").fillAmount = (flareTotalCD - flare2CD) / flareTotalCD;
            CacheGameObject.Find<UISprite>("UIflare3").fillAmount = (flareTotalCD - flare3CD) / flareTotalCD;
        }
    }

    private void showGas()
    {
        var num = currentGas / totalGas;
        var num2 = currentBladeSta / totalBladeSta;
        CacheGameObject.Find<UISprite>("gasL1").fillAmount = CacheGameObject.Find<UISprite>("gasR1").fillAmount = num;
        if (!useGun)
        {
            CacheGameObject.Find<UISprite>("bladeCL").fillAmount = CacheGameObject.Find<UISprite>("bladeCR").fillAmount = num2;

            //mau: im doing this because going from green straight to red looks weird, the color would turn brownish as it neared middle tank/blade
            var col1 = Color.red;
            var col2 = Color.yellow;
            var col3 = Color.green;
            if (num < .5)
                CacheGameObject.Find<UISprite>("gasL").color = CacheGameObject.Find<UISprite>("gasR").color = Color.Lerp(col1, col2, num * 2);
            else
                CacheGameObject.Find<UISprite>("gasL").color = CacheGameObject.Find<UISprite>("gasR").color = Color.Lerp(col2, col3, (num - .5f) * 2);
            if (num2 < .5)
                CacheGameObject.Find<UISprite>("bladel1").color = CacheGameObject.Find<UISprite>("blader1").color = Color.Lerp(col1, col2, num2 * 2);
            else
                CacheGameObject.Find<UISprite>("bladel1").color = CacheGameObject.Find<UISprite>("blader1").color = Color.Lerp(col2, col3, (num2 - .5f) * 2);

            if (currentBladeNum <= 4)
                CacheGameObject.Find<UISprite>("bladel5").enabled = CacheGameObject.Find<UISprite>("blader5").enabled = false;
            else
                CacheGameObject.Find<UISprite>("bladel5").enabled = CacheGameObject.Find<UISprite>("blader5").enabled = true;
            if (currentBladeNum <= 3)
                CacheGameObject.Find<UISprite>("bladel4").enabled = CacheGameObject.Find<UISprite>("blader4").enabled = false;
            else
                CacheGameObject.Find<UISprite>("bladel4").enabled = CacheGameObject.Find<UISprite>("blader4").enabled = true;
            if (currentBladeNum <= 2)
                CacheGameObject.Find<UISprite>("bladel3").enabled = CacheGameObject.Find<UISprite>("blader3").enabled = false;
            else
                CacheGameObject.Find<UISprite>("bladel3").enabled = CacheGameObject.Find<UISprite>("blader3").enabled = true;
            if (currentBladeNum <= 1)
                CacheGameObject.Find<UISprite>("bladel2").enabled = CacheGameObject.Find<UISprite>("blader2").enabled = false;
            else
                CacheGameObject.Find<UISprite>("bladel2").enabled = CacheGameObject.Find<UISprite>("blader2").enabled = true;
            if (currentBladeNum <= 0)
                CacheGameObject.Find<UISprite>("bladel1").enabled = CacheGameObject.Find<UISprite>("blader1").enabled = false;
            else
                CacheGameObject.Find<UISprite>("bladel1").enabled = CacheGameObject.Find<UISprite>("blader1").enabled = true;
        }
        else
        {
            CacheGameObject.Find<UISprite>("bulletL").enabled = leftGunHasBullet;
            CacheGameObject.Find<UISprite>("bulletR").enabled = rightGunHasBullet;
        }
    }

    [RPC]
    private void showHitDamage()
    {
        var target = CacheGameObject.Find("LabelScore");
        if (target != null)
        {
            speed = Mathf.Max(10f, speed);
            target.GetComponent<UILabel>().text = speed.ToString();
            target.transform.localScale = Vector3.zero;
            speed = (int)(speed * 0.1f);
            speed = Mathf.Clamp(speed, 40f, 150f);
            iTween.Stop(target);
            iTween.ScaleTo(target, iTween.Hash("x", speed, "y", speed, "z", speed, "easetype", iTween.EaseType.easeOutElastic, "time", 1f));
            iTween.ScaleTo(target, iTween.Hash("x", 0, "y", 0, "z", 0, "easetype", iTween.EaseType.easeInBounce, "time", 0.5f, "delay", 2f));
        }
    }

    private void showSkillCD()  //mod me
    {
        if (skillCD != null)
            skillCD.GetComponent<UISprite>().fillAmount = (skillCDLast - skillCDDuration) / skillCDLast;
    }

    private void Start()
    {
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
            photonView.owner.hero = this;
        FengGameManagerMKII.MKII.addHero(this, photonView.ownerId);
        if (photonView.isMine)
            GlobalItems.myHero = this;
        //gameObject.rigidbody.isKinematic = true;
        gameObject.AddComponent<mSmoothMouseWheelTranslation>();
        if (LevelInfo.getInfo(FengGameManagerMKII.level).horse && (IN_GAME_MAIN_CAMERA.IsMultiplayer) && photonView.isMine)
        {
            myHorse = PhotonNetwork.Instantiate("horse", myTransform.position + Vector3.up * 5f, myTransform.rotation, 0);
            myHorse.GetComponent<Horse>().myHero = gameObject;
        }
        var flag = false;
        if ((Application.platform == RuntimePlatform.WindowsWebPlayer) || (Application.platform == RuntimePlatform.OSXWebPlayer))
        {
            if (Application.srcValue != "aog1.unity3d")
                flag = true;
            if (string.Compare(Application.absoluteURL, "http://fenglee.com/game/aog/aog1.unity3d", true) != 0)
                flag = true;
            if (flag)
            {
                Application.ExternalEval("top.location.href=\"http://fenglee.com/\";");
                Destroy(gameObject);
            }
            Application.ExternalEval("if(window != window.top) {document.location='http://fenglee.com/game/aog/playhere.html'}");
        }
        sparks = myTransform.Find("slideSparks").GetComponent<ParticleSystem>();
        smoke_3dmg = myTransform.Find("3dmg_smoke").GetComponent<ParticleSystem>();
        myTransform.localScale = new Vector3(myScale, myScale, myScale);
        facingDirection = myTransform.rotation.eulerAngles.y;
        targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
        smoke_3dmg.enableEmission = false;
        sparks.enableEmission = false;
        speedFXPS = speedFX1.GetComponent<ParticleSystem>();
        speedFXPS.enableEmission = false;
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
        {
            var obj2 = CacheGameObject.Find("UI_IN_GAME");
            myNetWorkName = (GameObject)Instantiate(CacheResources.Load("UI/LabelNameOverHead"));
            myNetWorkName.name = "LabelNameOverHead";
            myNetWorkName.transform.parent = obj2.GetComponent<UIReferArray>().panels[0].transform;
            myNetWorkName.transform.localScale = new Vector3(14f, 14f, 14f);
            myNetWorkName.GetComponent<UILabel>().text = string.Empty;
            if ((int)photonView.owner.customProperties[PhotonPlayerProperty.team] == 2)
                myNetWorkName.GetComponent<UILabel>().text = "[FF0000]AHSS\n[FFFFFF]";
            var str = (string)photonView.owner.customProperties[PhotonPlayerProperty.guildName];
            if (str != string.Empty)
            {
                var component = myNetWorkName.GetComponent<UILabel>();
                var text = component.text;
                var textArray1 = new[] { text, "[FFFF00]", str, "\n[FFFFFF]", (string)photonView.owner.customProperties[PhotonPlayerProperty.name] };
                component.text = string.Concat(textArray1);
            }
            else
            {
                var local2 = myNetWorkName.GetComponent<UILabel>();
                local2.text = local2.text + (string)photonView.owner.customProperties[PhotonPlayerProperty.name];
            }
        }
        if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) && !photonView.isMine)
        {
            gameObject.layer = LayerMask.NameToLayer("NetworkObject");
            if (IN_GAME_MAIN_CAMERA.dayLight == DayLight.Night)
            {
                var obj3 = (GameObject)Instantiate(CacheResources.Load("flashlight"));
                obj3.transform.parent = myTransform;
                obj3.transform.position = myTransform.position + Vector3.up;
                obj3.transform.rotation = Quaternion.Euler(353f, 0f, 0f);
            }
            setup.init();
            setup.myCostume = new HeroCostume();
            setup.myCostume = CostumeConverter.PhotonDataToHeroCostume(photonView.owner);
            setup.setCharacterComponent();
            Destroy(checkBoxLeft);
            Destroy(checkBoxRight);
            Destroy(leftbladetrail);
            Destroy(rightbladetrail);
            Destroy(leftbladetrail2);
            Destroy(rightbladetrail2);
        }
        else
        {
            currentCamera = CacheGameObject.Find<Camera>("MainCamera");
            inputManager = CacheGameObject.Find<FengCustomInputs>("InputManagerController");
        }
        //StartCoroutine(loadhorse());
    }

    IEnumerator loadhorse()
    {
        var errorTextureWWW = new WWW("http://i.imgur.com/caaiiw66m.png");
        yield return errorTextureWWW;
        var errorTexture = errorTextureWWW.texture;
        var www = new WWW("http://i.imgur.com/HfIvpI2.png");
        yield return www;
        horse = www.texture;
        if (errorTexture == horse)
            Console.addLog("Horse sprite failed to load", Console.LogType.Error);
        else
            Console.addLog("Horse sprite loaded succefully", Console.LogType.Info);
    }

    private void suicide()
    {
        if (!IN_GAME_MAIN_CAMERA.IsSingleplayer)
        {
            netDie(rigidbody.velocity * 50f, false, -1, string.Empty);
            FengGameManagerMKII.MKII.needChooseSide = true;
            FengGameManagerMKII.MKII.justSuicide = true;
        }
    }

    private void throwBlades()
    {
        var transform = setup.part_blade_l.transform;
        var transform2 = setup.part_blade_r.transform;
        var obj2 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_blade_l"), transform.position, transform.rotation);
        var obj3 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_blade_r"), transform2.position, transform2.rotation);
        obj2.renderer.material = CharacterMaterials.materials[setup.myCostume._3dmg_texture];
        obj3.renderer.material = CharacterMaterials.materials[setup.myCostume._3dmg_texture];
        var force = myTransform.forward + myTransform.up * 2f - myTransform.right;
        obj2.rigidbody.AddForce(force, ForceMode.Impulse);
        var vector2 = myTransform.forward + myTransform.up * 2f + myTransform.right;
        obj3.rigidbody.AddForce(vector2, ForceMode.Impulse);
        var torque = new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100));
        torque.Normalize();
        obj2.rigidbody.AddTorque(torque);
        torque = new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100));
        torque.Normalize();
        obj3.rigidbody.AddTorque(torque);
        setup.part_blade_l.SetActive(false);
        setup.part_blade_r.SetActive(false);
        currentBladeNum--;
        if (currentBladeNum == 0)
            currentBladeSta = 0f;
        if (state == HERO_STATE.Attack)
            falseAttack();
    }

    public void ungrabbed()
    {
        photonView.owner.grabbed = false;
        ModControl.playerlist_speedup = false;

        facingDirection = 0f;
        targetRotation = Quaternion.Euler(0f, 0f, 0f);
        myTransform.parent = null;
        GetComponent<CapsuleCollider>().isTrigger = false;
        state = HERO_STATE.Idle;
    }

    private void unmounted()
    {
        myHorse.GetComponent<Horse>().unmounted();
        isMounted = false;
    }

    private void uGrabbed()
    {
        if (skillId == "jean")
        {
            if ((state != HERO_STATE.Attack) && (inputManager.isInputDown[InputCode.attack0] || inputManager.isInputDown[InputCode.attack1])
                && (escapeTimes > 0) && !animation.IsPlaying("grabbed_jean"))
            {
                playAnimation("grabbed_jean");
                animation["grabbed_jean"].time = 0f;
                escapeTimes--;
            }
            if (!animation.IsPlaying("grabbed_jean") || (!(animation["grabbed_jean"].normalizedTime > 0.64f))
                || (titanWhoGrabMe.GetComponent<TITAN>() == null))
                return;
            ungrabbed();
            rigidbody.velocity = Vector3.up * 30f;
            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
            else
            {
                photonView.RPC("netSetIsGrabbedFalse", PhotonTargets.All);
                if (PhotonNetwork.isMasterClient)
                    titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                else
                    PhotonView.Find(titanWhoGrabMeID).RPC("grabbedTargetEscape", PhotonTargets.MasterClient);
            }
        }
        else if (skillId == "eren")
        {
            showSkillCD();
            if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) || ((IN_GAME_MAIN_CAMERA.IsSingleplayer) && !IN_GAME_MAIN_CAMERA.isPausing))
            {
                calcSkillCD();
                calcFlareCD();
            }
            if (!inputManager.isInputDown[InputCode.attack1])
                return;
            var flag = false;
            if ((skillCDDuration > 0f) || flag)
                flag = true;
            else
            {
                skillCDDuration = skillCDLast;
                if ((skillId != "eren") || (titanWhoGrabMe.GetComponent<TITAN>() == null))
                    return;
                ungrabbed();
                if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                    titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                else
                {
                    photonView.RPC("netSetIsGrabbedFalse", PhotonTargets.All);
                    if (PhotonNetwork.isMasterClient)
                        titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                    else
                        PhotonView.Find(titanWhoGrabMeID).photonView.RPC("grabbedTargetEscape", PhotonTargets.MasterClient);
                }
                erenTransform();
            }
        }
    }

    public void uNotTitanDash()
    {
        if (!grounded && (state != HERO_STATE.AirDodge))
        {
            checkDashDoubleTap();
            if (dashD)
            {
                dashD = false;
                dash(0f, -1f);
                return;
            }
            if (dashU)
            {
                dashU = false;
                dash(0f, 1f);
                return;
            }
            if (dashL)
            {
                dashL = false;
                dash(-1f, 0f);
                return;
            }
            if (dashR)
            {
                dashR = false;
                dash(1f, 0f);
                return;
            }
        }
    }

    public void uNotTitanIdleAttack()
    {
            var flag2 = false;
            if (inputManager.isInputDown[InputCode.attack1])
            {
                if ((skillCDDuration > 0f) || flag2)
                    flag2 = true;
                else
                {
                    skillCDDuration = skillCDLast;
                    switch (skillId)
                    {
                        case "eren":
                            erenTransform();
                            return;
                        case "marco":
                            if (IsGrounded())
                            {
                                attackAnimation = Random.Range(0, 2) != 0 ? "special_marco_1" : "special_marco_0";
                                playAnimation(attackAnimation);
                            }
                            else
                            {
                                flag2 = true;
                                skillCDDuration = 0f;
                            }
                            break;
                        case "armin":
                            if (IsGrounded())
                            {
                                attackAnimation = "special_armin";
                                playAnimation("special_armin");
                            }
                            else
                            {
                                flag2 = true;
                                skillCDDuration = 0f;
                            }
                            break;
                        case "sasha":
                            if (IsGrounded())
                            {
                                attackAnimation = "special_sasha";
                                playAnimation("special_sasha");
                                currentBuff = BUFF.SpeedUp;
                                buffTime = 10f;
                            }
                            else
                            {
                                flag2 = true;
                                skillCDDuration = 0f;
                            }
                            break;
                        case "mikasa":
                            attackAnimation = "attack3_1";
                            playAnimation("attack3_1");
                            ((Component)this).rigidbody.velocity = Vector3.up * 10f;
                            break;
                        case "levi":
                            RaycastHit hit;
                            attackAnimation = "attack5";
                            playAnimation("attack5");
                            var rigidbody = this.rigidbody;
                            rigidbody.velocity += Vector3.up * 5f;
                            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                            LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
                            LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
                            LayerMask mask3 = mask2 | mask;
                            if (Physics.Raycast(ray, out hit, 1E+07f, mask3.value))
                            {
                                if (bulletRight != null)
                                {
                                    bulletRight.GetComponent<Bullet>().disable();
                                    releaseIfIHookSb();
                                }
                                dashDirection = hit.point - myTransform.position;
                                launchRightRope(hit, true, 1);
                                rope.Play();
                            }
                            facingDirection = Mathf.Atan2(dashDirection.x, dashDirection.z) * 57.29578f;
                            targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
                            attackLoop = 3;
                            break;
                        case "petra":
                            RaycastHit hit2;
                            attackAnimation = "special_petra";
                            playAnimation("special_petra");
                            var rigidbody2 = ((Component)this).rigidbody;
                            rigidbody2.velocity += Vector3.up * 5f;
                            var ray2 = Camera.main.ScreenPointToRay(Input.mousePosition);
                            LayerMask mask4 = 1 << LayerMask.NameToLayer("Ground");
                            LayerMask mask5 = 1 << LayerMask.NameToLayer("EnemyBox");
                            LayerMask mask6 = mask5 | mask4;
                            if (Physics.Raycast(ray2, out hit2, 1E+07f, mask6.value))
                            {
                                if (bulletRight != null)
                                {
                                    bulletRight.GetComponent<Bullet>().disable();
                                    releaseIfIHookSb();
                                }
                                if (bulletLeft != null)
                                {
                                    bulletLeft.GetComponent<Bullet>().disable();
                                    releaseIfIHookSb();
                                }
                                dashDirection = hit2.point - myTransform.position;
                                launchLeftRope(hit2, true);
                                launchRightRope(hit2, true);
                                rope.Play();
                            }
                            facingDirection = Mathf.Atan2(dashDirection.x, dashDirection.z) * 57.29578f;
                            targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
                            attackLoop = 3;
                            break;
                        default:
                            if (needLean)
                            {
                                if (leanLeft)
                                    attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_l1" : "attack1_hook_l2";
                                else
                                    attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_r1" : "attack1_hook_r2";
                            }
                            else
                                attackAnimation = "attack1";
                            break;
                    }
                    playAnimation(attackAnimation);
                }
            }
            else if (inputManager.isInputDown[InputCode.attack0])
            {
                if (needLean)
                {
                    if (inputManager.isInput[InputCode.left])
                        attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_l1" : "attack1_hook_l2";
                    else if (inputManager.isInput[InputCode.right])
                        attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_r1" : "attack1_hook_r2";
                    else
                    {
                        if (leanLeft)
                            attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_l1" : "attack1_hook_l2";
                        else
                            attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_r1" : "attack1_hook_r2";
                    }
                }
                else if (inputManager.isInput[InputCode.left])
                    attackAnimation = "attack2";
                else if (inputManager.isInput[InputCode.right])
                    attackAnimation = "attack1";
                else
                {
                    if (lastHook != null)
                    {
                        if (lastHook.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck") != null)
                            attackAccordingToTarget(lastHook.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck"));
                        else
                            flag2 = true;
                    }
                    else if ((bulletLeft != null) && (bulletLeft.transform.parent != null))
                    {
                        var a = bulletLeft.transform.parent.transform.root.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
                        if (a != null)
                            attackAccordingToTarget(a);
                        else
                            attackAccordingToMouse();
                    }
                    else if ((bulletRight != null) && (bulletRight.transform.parent != null))
                    {
                        var transform2 = bulletRight.transform.parent.transform.root.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
                        if (transform2 != null)
                            attackAccordingToTarget(transform2);
                        else
                            attackAccordingToMouse();
                    }
                    else
                    {
                        var obj2 = findNearestTitan();
                        if (obj2 != null)
                        {
                            var transform3 = obj2.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
                            if (transform3 != null)
                                attackAccordingToTarget(transform3);
                            else
                                attackAccordingToMouse();
                        }
                        else
                            attackAccordingToMouse();
                    }
                }
            }

            if (!flag2)
            {
                checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                if (grounded)
                    rigidbody.AddForce(gameObject.transform.forward * 200f);
                playAnimation(attackAnimation);
                animation[attackAnimation].time = 0f;
                buttonAttackRelease = false;
                state = HERO_STATE.Attack;
                if (grounded || (attackAnimation == "attack3_1") || (attackAnimation == "attack5") || (attackAnimation == "special_petra"))
                {
                    attackReleased = true;
                    buttonAttackRelease = true;
                }
                else
                    attackReleased = false;
                sparks.enableEmission = false;
            }
        
    }

    private void uNotTitanIdleUseGun()
    {
        if (inputManager.isInput[InputCode.attack1])
        {
            leftArmAim = true;
            rightArmAim = true;
        }
        else if (inputManager.isInput[InputCode.attack0])
        {
            if (leftGunHasBullet)
            {
                leftArmAim = true;
                rightArmAim = false;
            }
            else
            {
                leftArmAim = false;
                rightArmAim = rightGunHasBullet;
            }
        }
        else
        {
            leftArmAim = false;
            rightArmAim = false;
        }

        if (leftArmAim || rightArmAim)
        {
            RaycastHit hit3;
            var ray3 = Camera.main.ScreenPointToRay(Input.mousePosition);
            LayerMask mask7 = 1 << LayerMask.NameToLayer("Ground");
            LayerMask mask8 = 1 << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask9 = mask8 | mask7;
            if (Physics.Raycast(ray3, out hit3, 1E+07f, mask9.value))
                gunTarget = hit3.point;
        }
        var flag3 = false;
        var flag4 = false;
        var flag5 = false;
        if (inputManager.isInputUp[InputCode.attack1])
        {
            if (leftGunHasBullet && rightGunHasBullet)
            {
                attackAnimation = grounded ? "AHSS_shoot_both" : "AHSS_shoot_both_air";
                flag3 = true;
            }
            else if (!leftGunHasBullet && !rightGunHasBullet)
                flag4 = true;
            else
                flag5 = true;
        }
        if (flag5 || inputManager.isInputUp[InputCode.attack0])
        {
            if (grounded)
            {
                if (leftGunHasBullet && rightGunHasBullet)
                {
                    attackAnimation = isLeftHandHooked ? "AHSS_shoot_r" : "AHSS_shoot_l";
                }
                else if (leftGunHasBullet)
                    attackAnimation = "AHSS_shoot_l";
                else if (rightGunHasBullet)
                    attackAnimation = "AHSS_shoot_r";
            }
            else if (leftGunHasBullet && rightGunHasBullet)
            {
                attackAnimation = isLeftHandHooked ? "AHSS_shoot_r_air" : "AHSS_shoot_l_air";
            }
            else if (leftGunHasBullet)
                attackAnimation = "AHSS_shoot_l_air";
            else if (rightGunHasBullet)
                attackAnimation = "AHSS_shoot_r_air";


            if (leftGunHasBullet || rightGunHasBullet)
                flag3 = true;
            else
                flag4 = true;
        }
        if (flag3)
        {
            state = HERO_STATE.Attack;
            crossFade(attackAnimation, 0.05f);
            gunDummy.transform.position = myTransform.position;
            gunDummy.transform.rotation = myTransform.rotation;
            gunDummy.transform.LookAt(gunTarget);
            attackReleased = false;
            facingDirection = gunDummy.transform.rotation.eulerAngles.y;
            targetRotation = Quaternion.Euler(0f, facingDirection, 0f);
        }
        else if (flag4 && (grounded || (LevelInfo.getInfo(FengGameManagerMKII.level).type != GAMEMODE.PVP_AHSS)))
            changeBlade();
    }

    private void uNotTitanAttackUseBlade()
    {
        if (!inputManager.isInput[InputCode.attack0])
            buttonAttackRelease = true;
        if (!attackReleased)
        {
            if (buttonAttackRelease)
            {
                continueAnimation();
                attackReleased = true;
            }
            else
            {
                if (animation[attackAnimation].normalizedTime >= 0.32f)
                    pauseAnimation();
            }
        }
        if ((attackAnimation == "attack3_1") && (currentBladeSta > 0f))
        {
            if (animation[attackAnimation].normalizedTime >= 0.8f)
            {
                if (!checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
                {
                    checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = true;
                    leftbladetrail2.Activate();
                    rightbladetrail2.Activate();
                    //if (QualitySettings.GetQualityLevel() >= 2)
                    {
                        leftbladetrail.Activate();
                        rightbladetrail.Activate();
                    }
                    rigidbody.velocity = -Vector3.up * 30f;
                }
                if (!checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me)
                {
                    checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = true;
                    slash.Play();
                }
            }
            else if (checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
            {
                checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = false;
                checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = false;
                checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                leftbladetrail.StopSmoothly(0.1f);
                rightbladetrail.StopSmoothly(0.1f);
                leftbladetrail2.StopSmoothly(0.1f);
                rightbladetrail2.StopSmoothly(0.1f);
            }
        }
        else
        {
            float num;
            float num2;
            if (currentBladeSta == 0f)
                num = num2 = -1f;
            else
                switch (attackAnimation)
                {
                    case "attack5":
                        num = 0.35f;
                        num2 = 0.5f;
                        break;
                    case "special_petra":
                        num = 0.35f;
                        num2 = 0.48f;
                        break;
                    case "special_armin":
                        num = 0.25f;
                        num2 = 0.35f;
                        break;
                    case "attack4":
                        num = 0.6f;
                        num2 = 0.9f;
                        break;
                    case "special_sasha":
                        num = num2 = -1f;
                        break;
                    default:
                        num = 0.5f;
                        num2 = 0.85f;
                        break;
                }
            if ((animation[attackAnimation].normalizedTime > num) && (animation[attackAnimation].normalizedTime < num2))
            {
                if (!checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
                {
                    checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = true;
                    slash.Play();
                    leftbladetrail2.Activate();
                    rightbladetrail2.Activate();
                    //if (QualitySettings.GetQualityLevel() >= 2)
                    {
                        leftbladetrail.Activate();
                        rightbladetrail.Activate();
                    }
                }
                if (!checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me)
                    checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = true;
            }
            else if (checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
            {
                checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = false;
                checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = false;
                checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                leftbladetrail2.StopSmoothly(0.1f);
                rightbladetrail2.StopSmoothly(0.1f);
                //if (QualitySettings.GetQualityLevel() >= 2)
                {
                    leftbladetrail.StopSmoothly(0.1f);
                    rightbladetrail.StopSmoothly(0.1f);
                }
            }
            if ((attackLoop > 0) && (animation[attackAnimation].normalizedTime > num2))
            {
                attackLoop--;
                playAnimationAt(attackAnimation, num);
            }
        }
        if (animation[attackAnimation].normalizedTime >= 1f)
        {
            if ((attackAnimation == "special_marco_0") || (attackAnimation == "special_marco_1"))
            {
                if (!IN_GAME_MAIN_CAMERA.IsSingleplayer)
                {
                    if (!PhotonNetwork.isMasterClient)
                        photonView.RPC("netTauntAttack", PhotonTargets.MasterClient, 5f, 100f);
                    else
                        netTauntAttack(5f);
                }
                else
                    netTauntAttack(5f);
                falseAttack();
                idle();
            }
            else if (attackAnimation == "special_armin")
            {
                if (!IN_GAME_MAIN_CAMERA.IsSingleplayer)
                {
                    if (!PhotonNetwork.isMasterClient)
                        photonView.RPC("netlaughAttack", PhotonTargets.MasterClient);
                    else
                        netlaughAttack();
                }
                else
                {
                    foreach (var obj3 in GameObject.FindGameObjectsWithTag("titan"))
                    {
                        if ((Vector3.Distance(obj3.transform.position, myTransform.position) < 50f)
                            && (Vector3.Angle(obj3.transform.forward, myTransform.position - obj3.transform.position) < 90f)
                            && (obj3.GetComponent<TITAN>() != null))
                            obj3.GetComponent<TITAN>().beLaughAttacked();
                    }
                }
                falseAttack();
                idle();
            }
            else if (attackAnimation == "attack3_1")
                rigidbody.velocity -= Vector3.up * Time.deltaTime * 30f;
            else
            {
                falseAttack();
                idle();
            }
        }
        if (animation.IsPlaying("attack3_2") && (animation["attack3_2"].normalizedTime >= 1f))
        {
            falseAttack();
            idle();
        }
    }

    private void uNotTitanAttackUseGun()
    {
        myTransform.rotation = Quaternion.Lerp(myTransform.rotation, gunDummy.transform.rotation, Time.deltaTime * 30f);
        print("HERO - attackAnimation: " + attackAnimation);
        if (!attackReleased && (animation[attackAnimation].normalizedTime > 0.167f))
        {
            GameObject obj4;
            attackReleased = true;
            var flag6 = false;
            if ((attackAnimation == "AHSS_shoot_both") || (attackAnimation == "AHSS_shoot_both_air"))
            {
                flag6 = true;
                leftGunHasBullet = false;
                rightGunHasBullet = false;
                rigidbody.AddForce(-myTransform.forward * 1000f, ForceMode.Acceleration);
            }
            else
            {
                if ((attackAnimation == "AHSS_shoot_l") || (attackAnimation == "AHSS_shoot_l_air"))
                    leftGunHasBullet = false;
                else
                    rightGunHasBullet = false;
                rigidbody.AddForce(-myTransform.forward * 600f, ForceMode.Acceleration);
            }
            rigidbody.AddForce(Vector3.up * 200f, ForceMode.Acceleration);
            var prefabName = "FX/shotGun";
            if (flag6)
                prefabName = "FX/shotGun 1";
            if ((IN_GAME_MAIN_CAMERA.IsMultiplayer) && photonView.isMine)
            {
                obj4 = PhotonNetwork.Instantiate(prefabName, myTransform.position + myTransform.up * 0.8f - myTransform.right * 0.1f, myTransform.rotation,
                    0);
                if (obj4.GetComponent<EnemyfxIDcontainer>() != null)
                    obj4.GetComponent<EnemyfxIDcontainer>().myOwnerViewID = photonView.viewID;
            }
            else
                obj4 =
                    (GameObject)
                    Instantiate(Resources.Load(prefabName), myTransform.position + myTransform.up * 0.8f - myTransform.right * 0.1f, myTransform.rotation);
        }
        if (animation[attackAnimation].normalizedTime >= 1f)
        {
            falseAttack();
            idle();
        }
        if (!animation.IsPlaying(attackAnimation))
        {
            falseAttack();
            idle();
        }
    }

    private void uNotTitanReloadGun()
    {
        if (animation[reloadAnimation].normalizedTime > 0.22f)
        {
            if (!leftGunHasBullet && setup.part_blade_l.activeSelf)
            {
                setup.part_blade_l.SetActive(false);
                var transform = setup.part_blade_l.transform;
                var obj5 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_gun_l"), transform.position, transform.rotation);
                obj5.renderer.material = CharacterMaterials.materials[setup.myCostume._3dmg_texture];
                var force = -myTransform.forward * 10f + myTransform.up * 5f - myTransform.right;
                obj5.rigidbody.AddForce(force, ForceMode.Impulse);
                var torque = new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100));
                obj5.rigidbody.AddTorque(torque, ForceMode.Acceleration);
            }
            if (!rightGunHasBullet && setup.part_blade_r.activeSelf)
            {
                setup.part_blade_r.SetActive(false);
                var transform5 = setup.part_blade_r.transform;
                var obj6 = (GameObject)Instantiate(CacheResources.Load("Character_parts/character_gun_r"), transform5.position, transform5.rotation);
                obj6.renderer.material = CharacterMaterials.materials[setup.myCostume._3dmg_texture];
                var vector3 = -myTransform.forward * 10f + myTransform.up * 5f + myTransform.right;
                obj6.rigidbody.AddForce(vector3, ForceMode.Impulse);
                var vector4 = new Vector3(Random.Range(-300, 300), Random.Range(-300, 300), Random.Range(-300, 300));
                obj6.rigidbody.AddTorque(vector4, ForceMode.Acceleration);
            }
        }
        if ((animation[reloadAnimation].normalizedTime > 0.62f) && !throwedBlades)
        {
            throwedBlades = true;
            if ((leftBulletLeft > 0) && !leftGunHasBullet)
            {
                leftBulletLeft--;
                setup.part_blade_l.SetActive(true);
                leftGunHasBullet = true;
            }
            if ((rightBulletLeft > 0) && !rightGunHasBullet)
            {
                setup.part_blade_r.SetActive(true);
                rightBulletLeft--;
                rightGunHasBullet = true;
            }
            updateRightMagUI();
            updateLeftMagUI();
        }
        if (animation[reloadAnimation].normalizedTime > 1f)
            idle();
    }

    private void uNotTitanReloadBlade()
    {
        if (!grounded)
        {
            if ((animation[reloadAnimation].normalizedTime >= 0.2f) && !throwedBlades)
            {
                throwedBlades = true;
                if (setup.part_blade_l.activeSelf)
                    throwBlades();
            }
            if ((animation[reloadAnimation].normalizedTime >= 0.56f) && (currentBladeNum > 0))
            {
                setup.part_blade_l.SetActive(true);
                setup.part_blade_r.SetActive(true);
                currentBladeSta = totalBladeSta;
            }
        }
        else if ((animation[reloadAnimation].normalizedTime >= 0.13f) && !throwedBlades)
        {
            throwedBlades = true;
            if (setup.part_blade_l.activeSelf)
                throwBlades();
        }
        leftbladetrail.cleanBlood();
        rightbladetrail.cleanBlood();
        leftbladetrail2.cleanBlood();
        rightbladetrail2.cleanBlood();
    }

    private void uNotTitanLaunchRopes()
    {
        if (inputManager.isInput[InputCode.leftRope] && !animation.IsPlaying("attack3_1") && !animation.IsPlaying("attack5")
                && !animation.IsPlaying("special_petra") && (state != HERO_STATE.ChangeBlade) && (state != HERO_STATE.Grab))
        {
            if (bulletLeft != null)
                QHold = true;
            else
            {
                RaycastHit hit4;
                var ray4 = Camera.main.ScreenPointToRay(Input.mousePosition);
                LayerMask mask10 = 1 << LayerMask.NameToLayer("Ground");
                LayerMask mask11 = 1 << LayerMask.NameToLayer("EnemyBox");
                LayerMask mask12 = mask11 | mask10;
                if (Physics.Raycast(ray4, out hit4, 10000f, mask12.value))
                {
                    launchLeftRope(hit4, true);
                    rope.Play();
                }
            }
        }
        else
            QHold = false;

        if (inputManager.isInput[InputCode.rightRope] && ((!animation.IsPlaying("attack3_1") && !animation.IsPlaying("attack5")
            && !animation.IsPlaying("special_petra")) || (state == HERO_STATE.Idle)))
        {
            if (bulletRight != null)
                EHold = true;
            else
            {
                RaycastHit hit5;
                var ray5 = Camera.main.ScreenPointToRay(Input.mousePosition);
                LayerMask mask13 = 1 << LayerMask.NameToLayer("Ground");
                LayerMask mask14 = 1 << LayerMask.NameToLayer("EnemyBox");
                LayerMask mask15 = mask14 | mask13;
                if (Physics.Raycast(ray5, out hit5, 10000f, mask15.value))
                {
                    launchRightRope(hit5, true);
                    rope.Play();
                }
            }
        }
        else
            EHold = false;

        if (inputManager.isInput[InputCode.bothRope] && ((!animation.IsPlaying("attack3_1") && !animation.IsPlaying("attack5")
            && !animation.IsPlaying("special_petra")) || (state == HERO_STATE.Idle)))
        {
            QHold = true;
            EHold = true;
            if ((bulletLeft == null) && (bulletRight == null))
            {
                RaycastHit hit6;
                var ray6 = Camera.main.ScreenPointToRay(Input.mousePosition);
                LayerMask mask16 = 1 << LayerMask.NameToLayer("Ground");
                LayerMask mask17 = 1 << LayerMask.NameToLayer("EnemyBox");
                LayerMask mask18 = mask17 | mask16;
                if (Physics.Raycast(ray6, out hit6, 1000000f, mask18.value))
                {
                    launchLeftRope(hit6, false);
                    launchRightRope(hit6, false);
                    rope.Play();
                }
            }
        }
    }

    private void uNotTitanUpdateTrails()
    {
        if (leftbladetrail.gameObject.GetActive())
        {
            leftbladetrail.update();
            rightbladetrail.update();
        }
        if (leftbladetrail2.gameObject.GetActive())
        {
            leftbladetrail2.update();
            rightbladetrail2.update();
        }
        if (leftbladetrail.gameObject.GetActive())
        {
            leftbladetrail.lateUpdate();
            rightbladetrail.lateUpdate();
        }
        if (leftbladetrail2.gameObject.GetActive())
        {
            leftbladetrail2.lateUpdate();
            rightbladetrail2.lateUpdate();
        }
    }

    public void update()
    {
        if (IN_GAME_MAIN_CAMERA.isPausing)
            return;
        if (invincible > 0f)
            invincible -= Time.deltaTime;
        if (hasDied)
            return;
        /*if (state != HERO_STATE.Grab)
            handle3DMG();*/
        if (titanForm && (eren_titan != null))
        {
            myTransform.position = eren_titan.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck").position;
            gameObject.GetComponent<SmoothSyncMovement>().disabled = true;
        }
        if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) && !photonView.isMine)
            return;
        if ((state == HERO_STATE.Grab) && !useGun)
            uGrabbed();
        else if (!titanForm)
        {
            bufferUpdate();
            uNotTitanDash();
            if (grounded && ((state == HERO_STATE.Idle) || (state == HERO_STATE.Slide)))
            {
                if (inputManager.isInputDown[InputCode.jump] && !animation.IsPlaying("jump") && !animation.IsPlaying("horse_geton"))
                {
                    idle();
                    crossFade("jump", 0.1f);
                    sparks.enableEmission = false;
                }
                if (inputManager.isInputDown[InputCode.dodge] && !animation.IsPlaying("jump") && !animation.IsPlaying("horse_geton"))
                {
                    dodge();
                    return;
                }
            }
            switch (state)
            {
                case HERO_STATE.Idle:
                    if (inputManager.isInputDown[InputCode.flare1])
                        shootFlare(1);
                    if (inputManager.isInputDown[InputCode.flare2])
                        shootFlare(2);
                    if (inputManager.isInputDown[InputCode.flare3])
                        shootFlare(3);
                    if (inputManager.isInputDown[InputCode.restart])
                        suicide();
                    if ((myHorse != null) && isMounted && inputManager.isInputDown[InputCode.dodge])
                        getOffHorse();
                    if ((animation.IsPlaying(standAnimation) || !grounded) && inputManager.isInputDown[InputCode.reload])
                    {
                        changeBlade();
                        return;
                    }
                    if (animation.IsPlaying(standAnimation) && inputManager.isInputDown[InputCode.salute])
                    {
                        salute();
                        return;
                    }
                    if (!isMounted && (inputManager.isInputDown[InputCode.attack0] || inputManager.isInputDown[InputCode.attack1]) && !useGun)
                        uNotTitanIdleAttack();
                    if (useGun)
                        uNotTitanIdleUseGun();
                    break;
                case HERO_STATE.Attack:
                    if (useGun)
                        uNotTitanAttackUseGun();
                    else
                        uNotTitanAttackUseBlade();
                    break;
                case HERO_STATE.ChangeBlade:
                    if (useGun)
                        uNotTitanReloadGun();
                    else 
                        uNotTitanReloadBlade();
                    if ((animation[reloadAnimation].normalizedTime >= 0.37f) && (currentBladeNum > 0))
                    {
                        setup.part_blade_l.SetActive(true);
                        setup.part_blade_r.SetActive(true);
                        currentBladeSta = totalBladeSta;
                    }
                    if (animation[reloadAnimation].normalizedTime >= 1f)
                        idle();
                    break;
                case HERO_STATE.Salute:
                    if (animation["salute"].normalizedTime >= 1f)
                        idle();
                    break;
                case HERO_STATE.GroundDodge:
                    if (animation.IsPlaying("dodge"))
                    {
                        if (!grounded && (animation["dodge"].normalizedTime > 0.6f))
                            idle();
                        if (animation["dodge"].normalizedTime >= 1f)
                            idle();
                    }
                    break;
                case HERO_STATE.Land:
                    if (animation.IsPlaying("dash_land") && (animation["dash_land"].normalizedTime >= 1f))
                        idle();
                    break;
                case HERO_STATE.FillGas:
                    if (animation.IsPlaying("supply") && (animation["supply"].normalizedTime >= 1f))
                    {
                        currentBladeSta = totalBladeSta;
                        currentBladeNum = totalBladeNum;
                        currentGas = totalGas;
                        if (!useGun)
                        {
                            setup.part_blade_l.SetActive(true);
                            setup.part_blade_r.SetActive(true);
                        }
                        else
                        {
                            leftBulletLeft = rightBulletLeft = bulletMAX;
                            leftGunHasBullet = rightGunHasBullet = true;
                            setup.part_blade_l.SetActive(true);
                            setup.part_blade_r.SetActive(true);
                            updateRightMagUI();
                            updateLeftMagUI();
                        }
                        idle();
                    }
                    break;
                case HERO_STATE.Slide:
                    if (!grounded)
                        idle();
                    break;
                case HERO_STATE.AirDodge:
                    if (dashTime > 0f)
                    {
                        dashTime -= Time.deltaTime;
                        if (currentSpeed > originVM)
                            rigidbody.AddForce(-rigidbody.velocity * Time.deltaTime * 1.7f, ForceMode.VelocityChange);
                    }
                    else
                    {
                        dashTime = 0f;
                        idle();
                    }
                    break;
            }
            uNotTitanLaunchRopes();
            if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) || ((IN_GAME_MAIN_CAMERA.IsSingleplayer) && !IN_GAME_MAIN_CAMERA.isPausing))
            {
                calcSkillCD();
                calcFlareCD();
            }
            if (!useGun)
                uNotTitanUpdateTrails();
            if (!IN_GAME_MAIN_CAMERA.isPausing)
            {
                showSkillCD();
                showFlareCD();
                showGas();
                showAimUI();
            }
        }
    }

    private void updateLeftMagUI()
    {
        for (var i = 1; i <= bulletMAX; i++)
            CacheGameObject.Find<UISprite>("bulletL" + i).enabled = false;
        for (var j = 1; j <= leftBulletLeft; j++)
            CacheGameObject.Find<UISprite>("bulletL" + j).enabled = true;
    }

    private void updateRightMagUI()
    {
        for (var i = 1; i <= bulletMAX; i++)
            CacheGameObject.Find<UISprite>("bulletR" + i).enabled = false;
        for (var j = 1; j <= rightBulletLeft; j++)
            CacheGameObject.Find<UISprite>("bulletR" + j).enabled = true;
    }

    public void useBlade(int amount = 0)
    {
        if (amount == 0)
            amount = 1;
        amount *= 2;
        if (currentBladeSta > 0f)
        {
            currentBladeSta -= amount;
            if (currentBladeSta <= 0f)
            {
                if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || photonView.isMine)
                {
                    leftbladetrail.Deactivate();
                    rightbladetrail.Deactivate();
                    leftbladetrail2.Deactivate();
                    rightbladetrail2.Deactivate();
                    checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = false;
                    checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = false;
                }
                currentBladeSta = 0f;
                throwBlades();
            }
        }
    }

    private void useGas(float amount = 0)
    {
        if (amount == 0f)
            amount = useGasSpeed;
        if (currentGas > 0f)
        {
            currentGas -= amount;
            if (currentGas < 0f)
                currentGas = 0f;
        }
    }

    [RPC]
    private void whoIsMyErenTitan(int id)
    {
        eren_titan = PhotonView.Find(id).gameObject;
        titanForm = true;
    }

    public bool isGrabbed
    {
        get
        {
            return state == HERO_STATE.Grab;
        }
    }

    public HERO_STATE state
    {
        get
        {
            return _state;
        }
        set
        {
            if ((_state == HERO_STATE.AirDodge) || (_state == HERO_STATE.GroundDodge))
                dashTime = 0f;
            //print("state = " + value + " ");
            _state = value;
        }
    }

    private void handle3DMG()
    {
        spinning = false;
        var leftReelStrength = (float)Math.Round(mSmoothMouseWheelTranslation.Inf[0], 3);
        var rigthReelStrength = leftReelStrength;

        if (canLeftReel && canRightReel)
        {
            var accelMagnitude = currentSpeed + 0.1f;
            rigidbody.AddForce(-rigidbody.velocity, ForceMode.VelocityChange);
            var distanceFromHooks = (bulletRight.transform.position + bulletLeft.transform.position) * 0.5f - myTransform.position;
            var bothReelStrength = Input.GetAxis("Mouse ScrollWheel") * 5555f;
            bothReelStrength = Mathf.Clamp(bothReelStrength, -0.8f, 0.8f);
            contValue = rigidbody.velocity.magnitude / 100 * 0.057f;
            var reelDelta = 1f + bothReelStrength - contValue;
            var rotate = Vector3.RotateTowards(distanceFromHooks, rigidbody.velocity, 1.53938f * reelDelta, 1.53938f * reelDelta);
            rotate.Normalize();
            spinning = true;
            rigidbody.velocity = rotate * accelMagnitude;
        }
        else
        {
            if (canLeftReel)
            {
                var accelMagnitude = currentSpeed + 0.085f;  //add speed to rotation 0.1f
                rigidbody.AddForce(-rigidbody.velocity, ForceMode.VelocityChange);
                var distFromLeftHook = bulletLeft.transform.position - myTransform.position;
                //var leftReelStrength = strL = Input.GetAxis("Mouse ScrollWheel") * 5555f;
                //leftReelStrength = Mathf.Clamp(scrollPower, -0.8f, 100.8f);

                /*if (leftReelStrength < 0)
                    leftReelStrength *= 2f;*/
                leftReelStrength = Mathf.Clamp(leftReelStrength, -0.8f, 1.6f);

                //leftReelStrength = Mathf.Clamp(leftReelStrength, -0.8f, 0.8f);
                //contValue = rigidbody.velocity.magnitude / 100 * 0.045f;
                var reelDelta = 1f + leftReelStrength/* - contValue*/;  //reel in < 1 < reel out
                var rotate = Vector3.RotateTowards(distFromLeftHook, rigidbody.velocity, 1.53938f * reelDelta, 1.53938f * reelDelta);
                rotate.Normalize();
                spinning = true;
                rigidbody.velocity = rotate * accelMagnitude;
            }
            else if (canRightReel)
            {
                var accelMagnitude = currentSpeed + 0.085f;
                rigidbody.AddForce(-rigidbody.velocity, ForceMode.VelocityChange);
                var distFromRightHook = bulletRight.transform.position - myTransform.position;
                //var rigthReelStrength = strR = Input.GetAxis("Mouse ScrollWheel") * 5555f;
                //rigthReelStrength = Mathf.Clamp(rigthReelStrength, -0.8f, 0.8f);

                /*if (rigthReelStrength < 0)
                    rigthReelStrength *= 2f;*/
                rigthReelStrength = Mathf.Clamp(rigthReelStrength, -0.8f, 1.6f);

                //contValue = rigidbody.velocity.magnitude / 100 * 0.00873f;
                //contValue = rigidbody.velocity.magnitude / 100 * 0.045f;
                var reelDelta = 1f + rigthReelStrength/* - contValue*/;
                var rotate = Vector3.RotateTowards(distFromRightHook, rigidbody.velocity, 1.53938f * reelDelta, 1.53938f * reelDelta);
                rotate.Normalize();
                spinning = true;
                rigidbody.velocity = rotate * accelMagnitude;
            }
        }
    }

    public bool IsNearingGround()
    {
        return ((new Queue<GameObject>(from core in Physics.OverlapSphere(myTransform.position, 7f, Layer.GroundEnemyAABBHit.value)
                                       where core != null
                                       let coreT = core.transform
                                       where coreT != null
                                       let coreG = coreT.root.gameObject
                                       where coreG != null
                                       let coreN = coreG.name
                                       where coreN != null && !coreN.Contains("character_blade")
                                       select coreG).Count > 0)
            || Physics.Raycast(myTransform.position + ((Vector3.up * 0.1f)), -Vector3.up, 0.5f, Layer.GroundEnemyAABBHit.value));
    }
    
    [RPC]
    private void loadskinRPC(int horse, string url, PhotonMessageInfo info)
    {/*
        if (info.sender == this.basePV.owner && (string)FengGameManagerMKII.settingsRC[0] == "1")
        {
            base.StartCoroutine(this.loadskinE(horse, url.Trim()));
        }*/
        var t = url.Trim().Split(',');
        GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().photonView.RPC("SkinRPC", PhotonTargets.All, info.sender.ID, t );
    }
}