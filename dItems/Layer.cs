﻿using UnityEngine;

namespace dItems
{
    public static class Layer
    {
        public const int DefaultN = 0, TransparentFXN = 1, Ignore_RaycastN = 2, WaterN = 4, UIN = 5, PlayersN = 8, GroundN = 9, EnemyN = 10, AABBN = 11, FXN = 12, NetworkObjectN = 13, InnerContactN = 14, noPhysicsN = 15, AttackN = 16, PlayerHitBoxN = 17, EnemyAttackBoxN = 18, EnemyHitBoxN = 19;
        public static readonly LayerMask Default, TransparentFX, Ignore_Raycast, Water, UI, FX, InnerContact, noPhysics, PlayerHitBox, EnemyAttackBox, Ground, GroundHit, GroundAABB, GroundEnemy, GroundPlayers, GroundNetworkObject, GroundAttack, GroundEnemyHit, GroundAABBHit, GroundPlayersHit, GroundEnemyAABB, GroundEnemyPlayers, GroundNetworkObjectHit, GroundEnemyAttack, GroundEnemyAABBHit, GroundAttackHit, GroundEnemyPlayersHit, GroundEnemyAttackHit, GroundEnemyNetworkObject, GroundEnemyNetworkObjectHit, GroundEnemyPlayersNetworkObject, GroundPlayersAttack, GroundPlayersAABB, GroundPlayersNetworkObject, GroundPlayersNetworkObjectHit, GroundPlayersAttackHit, GroundEnemyPlayersNetworkObjectHit, GroundPlayersAABBHit, GroundEnemyPlayersAttackHit, GroundEnemyPlayersAABBHit, GroundPlayersNetworkObjectAttack, GroundPlayersNetworkObjectAABB, GroundPlayersAttackAABB, GroundPlayersNetworkObjectAttackHit, GroundPlayersNetworkObjectAABBHit, GroundPlayersAttackAABBHit, GroundEnemyPlayersNetworkObjectAttack, GroundEnemyPlayersNetworkObjectAABB, GroundEnemyPlayersNetworkObjectAttackHit, GroundEnemyPlayersNetworkObjectAABBHit, GroundEnemyPlayersAttackAABB, GroundEnemyPlayersAttackAABBHit, GroundEnemyPlayersNetworkObjectAttackAABB, GroundEnemyPlayersNetworkObjectAttackAABBHit,
            Enemy, EnemyHit, EnemyAABB, EnemyPlayers, EnemyNetworkObject, EnemyAttack, EnemyAttackHit, EnemyAttackAABB, EnemyPlayersAABB, EnemyPlayersHit, EnemyPlayersNetworkObject, EnemyAABBHit, EnemyPlayersAttack, EnemyNetworkObjectAttack, EnemyPlayersNetworkObjectAttack, EnemyPlayersAABBHit, EnemyNetworkObjectHit, EnemyPlayersAttackHit, EnemyPlayersNetworkObjectAABB, EnemyPlayersNetworkObjectAttackHit, EnemyPlayersNetworkObjectAABBHit, EnemyPlayersNetworkObjectAttackAABB, EnemyPlayersNetworkObjectAttackAABBHit, 
            Players, PlayersHit, PlayersAABB, PlayersNetworkObject, PlayersAttack, PlayersAttackHit, PlayersNetworkObjectAttack, PlayersAttackAABB, PlayersAttackAABBHit, PlayersAABBHit, PlayersNetworkObjectHit, PlayersNetworkObjectAABB, PlayersNetworkObjectAttackAABB, PlayersNetworkObjectAABBHit, PlayersNetworkObjectAttackHit, PlayersNetworkObjectAttackAABBHit,
            NetworkObject, NetworkObjectHit, NetworkObjectAABB, NetworkObjectAttack, NetworkObjectAttackAABB, NetworkObjectAABBHit, NetworkObjectAttackHit, NetworkObjectAttackAABBHit,
            Attack, AttackAABB, AttackHit, AttackAABBHit,
            AABB, AABBHit,
            EnemyHitBox;

        static Layer()
        {
            /*GroundN = LayerMask.NameToLayer("Ground");
            EnemyN = LayerMask.NameToLayer("EnemyBox");
            PlayersN = LayerMask.NameToLayer("Players");
            NetworkObjectN = LayerMask.NameToLayer("NetworkObject");
            AttackN = LayerMask.NameToLayer("PlayerAttackBox");
            AABBN = LayerMask.NameToLayer("EnemyAABB");*/
            Default = 1 << DefaultN;
            TransparentFX = 1 << TransparentFXN;
            Ignore_Raycast = 1 << Ignore_RaycastN;
            Water = 1 << WaterN;
            UI = 1 << UIN;
            FX = 1 << FXN;
            InnerContact = 1 << InnerContactN;
            noPhysics = 1 << noPhysicsN;
            PlayerHitBox = 1 << PlayerHitBoxN;
            EnemyAttackBox = 1 << EnemyAttackBoxN;
            Ground = 1 << GroundN;// LayerMask.NameToLayer("Ground");
            Enemy = 1 << EnemyN;// LayerMask.NameToLayer("EnemyBox");
            Players = 1 << PlayersN;// LayerMask.NameToLayer("Players");
            NetworkObject = 1 << NetworkObjectN;// LayerMask.NameToLayer("NetworkObject");
            Attack = 1 << AttackN;// LayerMask.NameToLayer("PlayerAttackBox");
            AABB = 1 << AABBN;// LayerMask.NameToLayer("EnemyAABB");
            EnemyHitBox = 1 << EnemyHitBoxN;
            GroundHit = Ground | EnemyHitBox;
            GroundAABBHit = Ground | AABB | EnemyHitBox;
            GroundEnemyHit = Ground | Enemy | EnemyHitBox;
            GroundPlayersHit = Ground | Players | EnemyHitBox;
            GroundNetworkObjectHit = Ground | NetworkObject | EnemyHitBox;
            GroundAttackHit = Ground | Attack | EnemyHitBox;
            GroundEnemyPlayersHit = Ground | Enemy | Players | EnemyHitBox;
            GroundEnemyAABBHit = Ground | Enemy | AABB | EnemyHitBox;
            GroundEnemyAttackHit = Ground | Enemy | Attack | EnemyHitBox;
            GroundEnemyNetworkObjectHit = Ground | Enemy | NetworkObject | EnemyHitBox;
            GroundPlayersNetworkObject = Ground | Players | NetworkObject;
            GroundPlayersNetworkObjectHit = Ground | Players | NetworkObject | EnemyHitBox;
            GroundEnemyPlayersNetworkObjectHit = Ground | Enemy | Players | NetworkObject | EnemyHitBox;
            GroundEnemyPlayersAttackHit = Ground | Enemy | Players | Attack | EnemyHitBox;
            GroundEnemyPlayersAABBHit = Ground | Enemy | Players | AABB | EnemyHitBox;
            GroundPlayersAttackHit = Ground | Players | Attack | EnemyHitBox;
            GroundPlayersAABBHit = Ground | Players | AABB | EnemyHitBox;
            GroundPlayersNetworkObjectAttackHit = Ground | NetworkObject | Players | Attack | EnemyHitBox;
            GroundPlayersNetworkObjectAABBHit = Ground | Players | Attack | AABB | EnemyHitBox;
            GroundEnemyPlayersAttackAABB = Ground | Enemy | Players | Attack | AABB;
            GroundPlayersAttackAABBHit = Ground | Players | Attack | AABB | EnemyHitBox;
            GroundEnemyPlayersAttackAABBHit = Ground | Enemy | Players | Attack | AABB | EnemyHitBox;
            GroundEnemyPlayersNetworkObjectAttackHit = Ground | Enemy | Players | NetworkObject | Attack | EnemyHitBox;
            GroundEnemyPlayersNetworkObjectAABBHit = Ground | Enemy | Players | NetworkObject | AABB | EnemyHitBox;
            GroundEnemyPlayersNetworkObjectAttackAABBHit = Ground | Enemy | Players | NetworkObject | Attack | AABB | EnemyHitBox;
            EnemyHit = Enemy | EnemyHitBox;
            EnemyAttackHit = Enemy | Attack | EnemyHitBox;
            EnemyPlayersHit = Enemy | Players | EnemyHitBox;
            EnemyAABBHit = Enemy | AABB | EnemyHitBox;
            EnemyNetworkObjectHit = Enemy | NetworkObject | EnemyHitBox;
            EnemyPlayersAABBHit = Enemy | Players | AABB | EnemyHitBox;
            EnemyPlayersAttackHit = Enemy | Players | Attack | EnemyHitBox;
            EnemyPlayersNetworkObjectAttackHit = Enemy | Players | NetworkObject | Attack | EnemyHitBox;
            EnemyPlayersNetworkObjectAABBHit = Enemy | Players | NetworkObject | AABB | EnemyHitBox;
            EnemyPlayersNetworkObjectAttackAABBHit = Enemy | Players | NetworkObject | Attack | AABB | EnemyHitBox;
            PlayersHit = Players | EnemyHitBox;
            PlayersAttackHit = Players | Attack | EnemyHitBox;
            PlayersAABBHit = Players | AABB | EnemyHitBox;
            PlayersAttackAABBHit = Players | Attack | AABB | EnemyHitBox;
            PlayersNetworkObjectHit = Players | NetworkObject | EnemyHitBox;
            PlayersNetworkObjectAABBHit = Players | NetworkObject | AABB | EnemyHitBox;
            PlayersNetworkObjectAttackHit = Players | NetworkObject | Attack | EnemyHitBox;
            PlayersNetworkObjectAttackAABBHit = Players | NetworkObject | Attack | AABB | EnemyHitBox;
            NetworkObjectHit = NetworkObject | EnemyHitBox;
            NetworkObjectAABBHit = NetworkObject | AABB | EnemyHitBox;
            NetworkObjectAttackHit = NetworkObject | Attack | EnemyHitBox;
            NetworkObjectAttackAABBHit = NetworkObject | Attack | AABB | EnemyHitBox;
            AttackHit = Attack | EnemyHitBox;
            AABBHit = AABB | EnemyHitBox;
            AttackAABBHit = Attack | AABB | EnemyHitBox;
            GroundEnemy = Ground | Enemy;
            GroundPlayers = Ground | Players;
            GroundAttack = Ground | Attack;
            GroundAABB = Ground | AABB;
            GroundPlayersNetworkObjectAABB = Ground | Players | NetworkObject | AABB;
            GroundEnemyPlayersNetworkObjectAABB = Ground | Enemy | Players | NetworkObject | AABB;
            GroundPlayersAttackAABB = Ground | Players | Attack | AABB;
            GroundPlayersAABB = Ground | Players | AABB;
            GroundEnemyAABB = Ground | Enemy | AABB;
            GroundPlayersAttack = Ground | Players | Attack;
            GroundEnemyAttack = Ground | Enemy | Attack;
            GroundNetworkObject = Ground | NetworkObject;
            GroundPlayersNetworkObjectAttack = Ground | Players | NetworkObject;
            GroundEnemyPlayersNetworkObjectAttackAABB = Ground | Enemy | Players | NetworkObject | Attack | AABB;
            GroundEnemyNetworkObject = Ground | Enemy | NetworkObject;
            GroundEnemyPlayers = Ground | Enemy | Players;
            GroundEnemyPlayersNetworkObject = Ground | Enemy | Players | NetworkObject;
            GroundEnemyPlayersNetworkObjectAttack = Ground | Enemy | Players | NetworkObject | Attack;
            EnemyNetworkObject = Enemy | NetworkObject;
            EnemyPlayers = Enemy | Players;
            EnemyAttack = Enemy | Attack;
            EnemyAABB = Enemy | AABB;
            EnemyAttackAABB = Enemy | Attack | AABB;
            EnemyPlayersAttack = Enemy | Players | Attack;
            EnemyPlayersNetworkObject = Enemy | Players | NetworkObject;
            EnemyPlayersAABB = Enemy | Players | AABB;
            EnemyNetworkObjectAttack = Enemy | NetworkObject | Attack;
            EnemyPlayersNetworkObjectAttack = Enemy | Players | NetworkObject | Attack;
            EnemyPlayersNetworkObjectAABB = Enemy | Players | NetworkObject | AABB;
            EnemyPlayersNetworkObjectAttackAABB = Enemy | Players | NetworkObject | Attack | AABB;
            PlayersAttack = Players | Attack;
            PlayersAABB = Players | AABB;
            PlayersAttackAABB = Players | Attack | AABB;
            PlayersNetworkObject = Players | NetworkObject;
            PlayersNetworkObjectAttack = Players | NetworkObject | Attack;
            PlayersNetworkObjectAABB = Players | NetworkObject | AABB;
            PlayersNetworkObjectAttackAABB = Players | NetworkObject | Attack | AABB;
            NetworkObjectAABB = NetworkObject | AABB;
            NetworkObjectAttack = NetworkObject | Attack;
            NetworkObjectAttackAABB = NetworkObject | Attack | AABB;
            AttackAABB = Attack | AABB;
        }
    }
}