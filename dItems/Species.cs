﻿using System;

namespace dItems
{
    [Flags]
    public enum SPECIES : byte
    {
        NONE = 0,
        HERO = 1,
        TITAN = 2,
        FEMALE_TITAN = 4,
        COLOSSAL_TITAN = 8,
        TITAN_EREN = 16,
        HORSE = 32,
        ALL = (HERO | TITAN | FEMALE_TITAN | COLOSSAL_TITAN | TITAN_EREN | HORSE)
    }
}