using mItems;
using UnityEngine;

public class TITAN_CONTROLLER : MonoBehaviour //mod me make it better
{
    public Camera currentCamera;
    public FengCustomInputs inputManager;
    public bool isAttackDown;
    public bool isAttackIIDown;
    public bool isJumpDown;
    public bool isSuicide;
    public bool isWALKDown;
    public float targetDirection;

    private void Start()
    {
        inputManager = CacheGameObject.Find<FengCustomInputs>("InputManagerController");
        currentCamera = CacheGameObject.Find<Camera>("MainCamera");
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            enabled = false;
    }

    private void Update()
    {
        var vertical = inputManager.isInput[InputCode.up] ? 1 : (inputManager.isInput[InputCode.down] ? -1 : 0);
        var horizontal = inputManager.isInput[InputCode.left] ? -1 : (inputManager.isInput[InputCode.right] ? 1 : 0);
        if ((horizontal != 0) || (vertical != 0))
        {
            var y = currentCamera.transform.rotation.eulerAngles.y;
            var num5 = Mathf.Atan2(vertical, horizontal) * 57.29578f;
            num5 = -num5 + 90f;
            var num3 = y + num5;
            targetDirection = num3;
        }
        else
            targetDirection = -874f;
        isAttackDown = false;
        isJumpDown = false;
        isAttackIIDown = false;
        isSuicide = false;
        if (inputManager.isInputDown[InputCode.attack0])
            isAttackDown = true;
        if (inputManager.isInputDown[InputCode.attack1])
            isAttackIIDown = true;
        if (inputManager.isInputDown[InputCode.bothRope])
            isJumpDown = true;
        if (inputManager.isInputDown[InputCode.restart])
            isSuicide = true;
        isWALKDown = inputManager.isInput[InputCode.jump];
    }
}