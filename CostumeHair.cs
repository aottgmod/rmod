public class CostumeHair
{
    public string hair = string.Empty;
    public string hair_1 = string.Empty;
    public static CostumeHair[] hairsF;
    public static CostumeHair[] hairsM;
    public bool hasCloth;
    public int id;
    public string texture = string.Empty;

    public static void init()
    {
        hairsM = new CostumeHair[11];
        hairsM[0] = new CostumeHair { hair = "hair_boy1", texture = "hair_boy1" };
        hairsM[1] = new CostumeHair { hair = "hair_boy2", texture = "hair_boy2" };
        hairsM[2] = new CostumeHair { hair = "hair_boy3", texture = "hair_boy3" };
        hairsM[3] = new CostumeHair { hair = "hair_boy4", texture = "hair_boy4" };
        hairsM[4] = new CostumeHair { hair = "hair_eren", texture = "hair_eren" };
        hairsM[5] = new CostumeHair { hair = "hair_armin", texture = "hair_armin" };
        hairsM[6] = new CostumeHair { hair = "hair_jean", texture = "hair_jean" };
        hairsM[7] = new CostumeHair { hair = "hair_levi", texture = "hair_levi" };
        hairsM[8] = new CostumeHair { hair = "hair_marco", texture = "hair_marco" };
        hairsM[9] = new CostumeHair { hair = "hair_mike", texture = "hair_mike" };
        hairsM[10] = new CostumeHair();

        hairsM[10].hair = hairsM[10].texture = string.Empty;
        for (var i = 0; i <= 10; i++)
            hairsM[i].id = i;

        hairsF = new CostumeHair[11];
        hairsF[0] = new CostumeHair { hair = "hair_girl1", texture = "hair_girl1" };
        hairsF[1] = new CostumeHair { hair = "hair_girl2", texture = "hair_girl2" };
        hairsF[2] = new CostumeHair { hair = "hair_girl3", texture = "hair_girl3" };
        hairsF[3] = new CostumeHair { hair = "hair_girl4", texture = "hair_girl4" };
        hairsF[4] = new CostumeHair { hair = "hair_girl5", texture = "hair_girl5", hasCloth = true, hair_1 = "hair_girl5_cloth" };
        hairsF[5] = new CostumeHair { hair = "hair_annie", texture = "hair_annie" };
        hairsF[6] = new CostumeHair { hair = "hair_hanji", texture = "hair_hanji", hasCloth = true, hair_1 = "hair_hanji_cloth" };
        hairsF[7] = new CostumeHair { hair = "hair_mikasa", texture = "hair_mikasa" };
        hairsF[8] = new CostumeHair { hair = "hair_petra", texture = "hair_petra" };
        hairsF[9] = new CostumeHair { hair = "hair_rico", texture = "hair_rico" };
        hairsF[10] = new CostumeHair { hair = "hair_sasha", texture = "hair_sasha", hasCloth = true, hair_1 = "hair_sasha_cloth" };

        for (var j = 0; j <= 10; j++)
            hairsF[j].id = j;
    }
}