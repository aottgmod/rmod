using System.Collections;
using System.Collections.Generic;
using dItems;
using mItems;
using UnityEngine;
using MonoBehaviour = Photon.MonoBehaviour;

public class Bullet : MonoBehaviour
{
    private Vector3 heightOffSet = Vector3.up * 0.48f;
    private bool isdestroying;
    private float killTime;
    private float killTime2;
    private Vector3 launchOffSet = Vector3.zero;
    private bool left = true;
    public bool leviMode;
    public float leviShootTime;
    private LineRenderer lineRenderer;
    public TITAN myTitan;
    private GameObject master;
    private GameObject myRef;
    private ArrayList _nodes = new ArrayList();
    private int phase;
    private GameObject rope;
    private int spiralcount;
    private ArrayList spiralNodes;
    private Vector3 velocity = Vector3.zero;
    private Vector3 velocity2 = Vector3.zero;
    
    /*private Queue<Photon.Mono> titans = new Queue<Photon.Mono>();
    private Renderer render;
    private float size;
    public bool isHooked1;*/

    public ArrayList nodes
    {
        get { return _nodes; }
    }

    public void disable()
    {
        phase = 2;
        killTime = 0f;
        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
            photonView.RPC("setPhase", PhotonTargets.Others, 2);
    }

    private void FixedUpdate()
    {
        if (((phase == 2) || (phase == 1)) && leviMode)
        {
            spiralcount++;
            if (spiralcount >= 60)
            {
                isdestroying = true;
                removeMe();
                return;
            }
        }
        if (!IN_GAME_MAIN_CAMERA.IsSingleplayer && !photonView.isMine)
        {
            if (phase != 0)
                return;
            var transform = gameObject.transform;
            transform.position += velocity * Time.deltaTime * 50f + velocity2 * Time.deltaTime;
            _nodes.Add(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z));
        }
        else if (phase == 0)
        {
            checkTitan();
            RaycastHit hit;
            var transform2 = gameObject.transform;
            transform2.position += velocity * Time.deltaTime * 50f + velocity2 * Time.deltaTime;
            LayerMask mask = 1 << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask2 = 1 << LayerMask.NameToLayer("Ground");
            LayerMask mask3 = 1 << LayerMask.NameToLayer("NetworkObject");
            LayerMask mask4 = mask | mask2 | mask3;
            var flag = false;
            var flag2 = false;
            flag2 = _nodes.Count > 1
                ? Physics.Linecast((Vector3)_nodes[_nodes.Count - 2], gameObject.transform.position, out hit, mask4.value)
                : Physics.Linecast((Vector3)_nodes[_nodes.Count - 1], gameObject.transform.position, out hit, mask4.value);
            if (flag2)
            {
                var flag3 = true;
                var t = hit.collider.transform;
                if (t.gameObject.layer == LayerMask.NameToLayer("EnemyBox"))
                {
                    if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                        photonView.RPC("tieMeToOBJ", PhotonTargets.Others, t.root.gameObject.GetPhotonView().viewID);
                    master.GetComponent<HERO>().lastHook = t.root;
                    transform.parent = t;
                }
                else if (t.gameObject.layer == LayerMask.NameToLayer("Ground"))
                    master.GetComponent<HERO>().lastHook = null;
                else if (t.gameObject.layer == LayerMask.NameToLayer("NetworkObject") && (t.gameObject.CompareTag("Player")) && !leviMode)
                {
                    if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                        photonView.RPC("tieMeToOBJ", PhotonTargets.Others, t.root.gameObject.GetPhotonView().viewID);
                    master.GetComponent<HERO>().hookToHuman(t.root.gameObject, transform.position);
                    transform.parent = t;
                    master.GetComponent<HERO>().lastHook = null;
                }
                else
                    flag3 = false;
                if (phase == 2)
                    flag3 = false;
                if (flag3)
                {
                    master.GetComponent<HERO>().launch(hit.point, left, leviMode);
                    transform.position = hit.point;
                    if (phase != 2)
                    {
                        phase = 1;
                        if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                        {
                            photonView.RPC("setPhase", PhotonTargets.Others, 1);
                            photonView.RPC("tieMeTo", PhotonTargets.Others, transform.position);
                        }
                        if (leviMode)
                            getSpiral(master.transform.position, master.transform.rotation.eulerAngles);
                        flag = true;
                    }
                }
            }
            _nodes.Add(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z));
            if (flag)
                return;
            killTime2 += Time.deltaTime;
            if (!(killTime2 > 0.8f))
                return;
            phase = 4;
            if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                photonView.RPC("setPhase", PhotonTargets.Others, 4);
        }
    }

    private void getSpiral(Vector3 masterposition, Vector3 masterrotation)
    {
        const float f1 = 30f;
        var num = 0.5f;
        var distance = 30f;
        var turns = 0.05f + spiralcount * 0.03f;
        if (spiralcount < 5)
            distance = Vector2.Distance(new Vector2(masterposition.x, masterposition.z), new Vector2(gameObject.transform.position.x, gameObject.transform.position.z));
        else
            distance = 1.2f + (60 - spiralcount) * 0.1f;
        num -= spiralcount * 0.06f;
        var num1 = distance / f1;
        var num2 = turns / f1;
        var num3 = num2 * 2f * 3.141593f;
        num *= 6.283185f;
        spiralNodes = new ArrayList();
        for (var i = 1; i <= f1; i++)
        {
            var num4 = i * num1 * (1f + 0.05f * i);
            var f = i * num3 + num + 1.256637f + masterrotation.y * 0.0173f;
            var x = Mathf.Cos(f) * num4;
            var z = -Mathf.Sin(f) * num4;
            spiralNodes.Add(new Vector3(x, 0f, z));
        }
    }

    public void checkTitan()
    {
        var main = Camera.main.GetComponent<IN_GAME_MAIN_CAMERA>().main_object;
        if (main == null || master == null || master != main)
            return;
        LayerMask mask = 1 << LayerMask.NameToLayer("PlayerAttackBox");
        RaycastHit hit;
        if (!Physics.Raycast(transform.position, velocity, out hit, 10f, mask.value))
            return;
        var hitCollider = hit.collider;
        if (!hitCollider.name.Contains("PlayerDetectorRC"))
            return;
        var titan = hitCollider.transform.root.gameObject.GetComponent<TITAN>();
        if (titan == null)
            return;
        if (myTitan == null)
        {
            myTitan = titan;
            myTitan.isHooked = true;
        }
        else if (myTitan != titan)
        {
            myTitan.isHooked = false;
            myTitan = titan;
            myTitan.isHooked = true;
        }
    }

    public bool isHooked()
    {
        return phase == 1;
    }

    [RPC]
    private void killObject()
    {
        Destroy(rope);
        Destroy(gameObject);
    }

    public void launch(Vector3 v, Vector3 v2, string launcher_ref, bool isLeft, GameObject hero, bool leviMode = false)
    {
        if (phase == 2)
            return;
        master = hero;
        velocity = v;
        var f = Mathf.Acos(Vector3.Dot(v.normalized, v2.normalized)) * 57.29578f;
        velocity2 = Mathf.Abs(f) > 90f ? Vector3.zero : Vector3.Project(v2, v);
        if (launcher_ref == "hookRefL1")
            myRef = hero.GetComponent<HERO>().hookRefL1;
        if (launcher_ref == "hookRefL2")
            myRef = hero.GetComponent<HERO>().hookRefL2;
        if (launcher_ref == "hookRefR1")
            myRef = hero.GetComponent<HERO>().hookRefR1;
        if (launcher_ref == "hookRefR2")
            myRef = hero.GetComponent<HERO>().hookRefR2;
        _nodes = new ArrayList { myRef.transform.position };
        phase = 0;
        this.leviMode = leviMode;
        left = isLeft;
        if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) && photonView.isMine)
        {
            photonView.RPC("myMasterIs", PhotonTargets.Others, hero.GetComponent<HERO>().photonView.viewID, launcher_ref);
            photonView.RPC("setVelocityAndLeft", PhotonTargets.Others, v, velocity2, left);
        }
        transform.position = myRef.transform.position;
        transform.rotation = Quaternion.LookRotation(v.normalized);
    }

    [RPC]
    private void myMasterIs(int id, string launcherRef)
    {
        master = PhotonView.Find(id).gameObject;
        if (launcherRef == "hookRefL1")
            myRef = master.GetComponent<HERO>().hookRefL1;
        if (launcherRef == "hookRefL2")
            myRef = master.GetComponent<HERO>().hookRefL2;
        if (launcherRef == "hookRefR1")
            myRef = master.GetComponent<HERO>().hookRefR1;
        if (launcherRef == "hookRefR2")
            myRef = master.GetComponent<HERO>().hookRefR2;
    }

    [RPC]
    private void netLaunch(Vector3 newPosition)
    {
        _nodes = new ArrayList { newPosition };
    }

    [RPC]
    private void netUpdateLeviSpiral(Vector3 newPosition, Vector3 masterPosition, Vector3 masterrotation)
    {
        phase = 2;
        leviMode = true;
        getSpiral(masterPosition, masterrotation);
        var vector = masterPosition - (Vector3)spiralNodes[0];
        lineRenderer.SetVertexCount(spiralNodes.Count - (int)(spiralcount * 0.5f));
        for (var i = 0; i <= spiralNodes.Count - 1 - spiralcount * 0.5f; i++)
        {
            if (spiralcount < 5)
            {
                var position = (Vector3)spiralNodes[i] + vector;
                var num2 = spiralNodes.Count - 1 - spiralcount * 0.5f;
                position = new Vector3(position.x, position.y * ((num2 - i) / num2) + newPosition.y * (i / num2), position.z);
                lineRenderer.SetPosition(i, position);
            }
            else
                lineRenderer.SetPosition(i, (Vector3)spiralNodes[i] + vector);
        }
    }

    [RPC]
    private void netUpdatePhase1(Vector3 newPosition, Vector3 masterPosition)
    {
        lineRenderer.SetVertexCount(2);
        lineRenderer.SetPosition(0, newPosition);
        lineRenderer.SetPosition(1, masterPosition);
        transform.position = newPosition;
    }

    private void OnDestroy()
    {
        if (CacheGameObject.Find("MultiplayerManager") != null)
            FengGameManagerMKII.MKII.removeHook(this);
        if (myTitan != null)
            myTitan.isHooked = false;
        Destroy(rope);
    }

    public void removeMe()
    {
        isdestroying = true;
        if (!IN_GAME_MAIN_CAMERA.IsSingleplayer && photonView.isMine)
        {
            PhotonNetwork.Destroy(photonView);
            PhotonNetwork.RemoveRPCs(photonView);
        }
        else if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
        {
            Destroy(rope);
            Destroy(gameObject);
        }
    }

    private void setLinePhase0()
    {
        if (master == null)
        {
            Destroy(rope);
            Destroy(gameObject);
        }
        else if (_nodes.Count > 0)
        {
            var vector = myRef.transform.position - (Vector3)_nodes[0];
            lineRenderer.SetVertexCount(_nodes.Count);
            for (var i = 0; i <= _nodes.Count - 1; i++)
                lineRenderer.SetPosition(i, (Vector3)_nodes[i] + vector * Mathf.Pow(0.75f, i));
            if (_nodes.Count > 1)
                lineRenderer.SetPosition(1, myRef.transform.position);
        }
    }

    [RPC]
    private void setPhase(int value)
    {
        phase = value;
    }

    [RPC]
    private void setVelocityAndLeft(Vector3 value, Vector3 v2, bool l)
    {
        velocity = value;
        velocity2 = v2;
        left = l;
        transform.rotation = Quaternion.LookRotation(value.normalized);
    }

    private void Start()
    {
        rope = (GameObject)Instantiate(CacheResources.Load("rope"));
        lineRenderer = rope.GetComponent<LineRenderer>();
        FengGameManagerMKII.MKII.addHook(this);
    }

    [RPC]
    private void tieMeTo(Vector3 p)
    {
        transform.position = p;
    }

    [RPC]
    private void tieMeToOBJ(int id)
    {
        transform.parent = PhotonView.Find(id).gameObject.transform;
    }

    public void update()
    {
        if (master == null)
            removeMe();
        else if (!isdestroying)
        {
            if (leviMode)
            {
                leviShootTime += Time.deltaTime;
                if (leviShootTime > 0.4f)
                {
                    phase = 2;
                    gameObject.GetComponent<MeshRenderer>().enabled = false;
                }
            }
            switch (phase)
            {
                case 0:
                    setLinePhase0();
                    break;
                case 1:
                    var distance = this.transform.position - myRef.transform.position;
                    var speed = master.rigidbody.velocity;
                    var magnitude = speed.magnitude;
                    var f = distance.magnitude;
                    var num3 = (int)((f + magnitude) / 5f);
                    num3 = Mathf.Clamp(num3, 2, 6);
                    lineRenderer.SetVertexCount(num3);
                    lineRenderer.SetPosition(0, myRef.transform.position);
                    var index = 1;
                    var num6 = Mathf.Pow(f, 0.3f);
                    while (index < num3)
                    {
                        var num7 = num3 / 2;
                        float num8 = Mathf.Abs(index - num7);
                        var num9 = (num7 - num8) / num7;
                        num9 = Mathf.Pow(num9, 0.5f);
                        var max = (num6 + magnitude) * 0.0015f * num9;
                        lineRenderer.SetPosition(index,
                            new Vector3(Random.Range(-max, max), Random.Range(-max, max), Random.Range(-max, max)) + myRef.transform.position
                            + distance * (index / (float)num3) - Vector3.up * num6 * 0.05f * num9 - speed * 0.001f * num9 * num6);
                        index++;
                    }
                    lineRenderer.SetPosition(num3 - 1, ((Component)this).transform.position);
                    break;
                case 2:
                    if (!leviMode)
                    {
                        lineRenderer.SetVertexCount(2);
                        lineRenderer.SetPosition(0, ((Component)this).transform.position);
                        lineRenderer.SetPosition(1, myRef.transform.position);
                        killTime += Time.deltaTime * 0.2f;
                        lineRenderer.SetWidth(0.1f - killTime, 0.1f - killTime);
                        if (killTime > 0.1f)
                            removeMe();
                    }
                    else
                    {
                        getSpiral(master.transform.position, master.transform.rotation.eulerAngles);
                        var vector4 = myRef.transform.position - (Vector3)spiralNodes[0];
                        lineRenderer.SetVertexCount(spiralNodes.Count - (int)(spiralcount * 0.5f));
                        for (var i = 0; i <= spiralNodes.Count - 1 - spiralcount * 0.5f; i++)
                        {
                            if (spiralcount < 5)
                            {
                                var position = (Vector3)spiralNodes[i] + vector4;
                                var num11 = spiralNodes.Count - 1 - spiralcount * 0.5f;
                                position = new Vector3(position.x, position.y * ((num11 - i) / num11) + gameObject.transform.position.y * (i / num11), position.z);
                                lineRenderer.SetPosition(i, position);
                            }
                            else
                                lineRenderer.SetPosition(i, (Vector3)spiralNodes[i] + vector4);
                        }
                    }
                    break;
                case 4:
                    var transform = gameObject.transform;
                    transform.position += this.velocity + velocity2 * Time.deltaTime;
                    _nodes.Add(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z));
                    var vector6 = myRef.transform.position - (Vector3)_nodes[0];
                    for (var j = 0; j <= _nodes.Count - 1; j++)
                    {
                        lineRenderer.SetVertexCount(_nodes.Count);
                        lineRenderer.SetPosition(j, (Vector3)_nodes[j] + vector6 * Mathf.Pow(0.5f, j));
                    }
                    killTime2 += Time.deltaTime;
                    if (killTime2 > 0.8f)
                    {
                        killTime += Time.deltaTime * 0.2f;
                        lineRenderer.SetWidth(0.1f - killTime, 0.1f - killTime);
                        if (killTime > 0.1f)
                            removeMe();
                    }
                    break;
            }
        }
    }

    /*public void checkTitan1()
    {
        Photon.Mono titan;
        var currentTitans = new Queue<Photon.Mono>();
        foreach (var core in Physics.OverlapSphere(render.bounds.center, size, Layer.Attack.value))
        {
            if (core != null)
            {
                if (core.name.Contains("PlayerDetectorRC"))
                {
                    titan = core.transform.root.GetComponent<Photon.Mono>();
                    if (titan.specie == SPECIES.TITAN)
                    {
                        currentTitans.Enqueue(titan as TITAN);
                    }
                    else
                    {
                        if (titan.specie == SPECIES.FEMALE_TITAN)
                            currentTitans.Enqueue(titan as FEMALE_TITAN);
                    }
                }
            }
        }
        foreach (var oldTitan in titans)
        {
            if (oldTitan == null)
                continue;
            if (!currentTitans.Contains(oldTitan))
            {
                if (oldTitan.specie == SPECIES.TITAN)
                {
                    ((TITAN)oldTitan).isHooked = false;
                }
                else
                {
                    if (oldTitan.specie == SPECIES.FEMALE_TITAN)
                        ((FEMALE_TITAN)oldTitan).isHooked = false;
                }
            }
        }
        foreach (var newTitan in currentTitans)
        {
            if (newTitan == null)
                continue;
            if (newTitan.specie == SPECIES.TITAN)
            {
                ((TITAN)newTitan).isHooked = true;
            }
            else
            {
                if (newTitan.specie == SPECIES.FEMALE_TITAN)
                    ((FEMALE_TITAN)newTitan).isHooked = true;
            }
        }
        titans = currentTitans;
    }*/
}