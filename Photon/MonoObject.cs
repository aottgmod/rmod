﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Photon
{
    public class MonoObject : UnityEngine.MonoBehaviour
    {
        public static readonly Dictionary<int, MonoObject> monocache = new Dictionary<int, MonoObject>();
        public Dictionary<string, Component> components = new Dictionary<string, Component>();
        public Dictionary<Type, Component> componentsType = new Dictionary<Type, Component>();

        private Transform baseT;
        private Animation baseA;
        private Rigidbody baseR;
        private PhotonView basePV;
        private GameObject baseG;
        private Renderer baseRend;
        private Collider baseC;

        public new Transform transform
        {
            get { return baseT; }
            set { baseT = value; }
        }

        public new Animation animation
        {
            get { return baseA; }
            set { baseA = value; }
        }

        public new Rigidbody rigidbody
        {
            get { return baseR; }
            set { baseR = value; }
        }

        public new PhotonView photonView
        {
            get { return basePV; }
            set { basePV = value; }
        }

        public new GameObject gameObject
        {
            get { return baseG; }
            set { baseG = value; }
        }

        public new Renderer renderer
        {
            get { return baseRend; }
            set { baseRend = value; }
        }

        public new Collider collider
        {
            get { return baseC; }
            set { baseC = value; }
        }

        public static implicit operator MonoObject(GameObject obj)
        {
            if (obj == null)
                return null;
            MonoObject mono;
            if (monocache.TryGetValue(obj.GetInstanceID(), out mono))
                return mono;
            return obj.AddMonoObject();
        }

        public static implicit operator MonoObject(Photon.Mono obj)
        {
            if (obj == null || obj.monoG == null)
                return null;
            return monocache[obj.monoG.GetInstanceID()];
        }

        public static implicit operator GameObject(MonoObject obj)
        {
            if (obj == null)
                return null;
            return obj.gameObject;
        }

        private void Awake()
        {
            monocache.Add((baseG = base.gameObject).GetInstanceID(), this);
            string name;
            foreach (Component c in this.GetComponents<Component>())
            {
                name = c.GetType().Name;
                switch (name)
                {
                    case "Transform":
                        baseT = c as Transform;
                        break;
                    case "Animation":
                        baseA = c as Animation;
                        break;
                    case "Rigidbody":
                        baseR = c as Rigidbody;
                        break;
                    case "PhotonView":
                        basePV = c as PhotonView;
                        break;
                    case "Renderer":
                        baseRend = c as Renderer;
                        break;
                    case "Collider":
                        baseC = c as Collider;
                        break;
                }
                this.components.Add(name, c);
                this.componentsType.Add(c.GetType(), c);
            }
        }

        private void Start() {}

        private void OnDestroy()
        {
            monocache.Remove(baseG.GetInstanceID());
        }

        public int GetInstanceIDofGameObject()
        {
            return baseG.GetInstanceID();
        }

        public new Component GetComponent(string TypeName)
        {
            switch (TypeName)
            {
                case "Transform":
                    return baseT;
                case "Animation":
                    return baseA;
                case "Rigidbody":
                    return baseR;
                case "PhotonView":
                    return basePV;
                case "Renderer":
                    return baseRend;
                case "Collider":
                    return baseC;
            }
            return this.components[TypeName];
        }

        public new T GetComponent<T>() where T : Component
        {
            return (T)this.componentsType[typeof(T)];
        }
    }
}