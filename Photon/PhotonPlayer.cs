using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using ExitGames.Client.Photon;
using UnityEngine;

public partial class PhotonPlayer
{
    private int actorID;
    public readonly bool isLocal;
    private string nameField;
    public object TagObject;

    private bool mGrabbed;
    private HERO mHero;

    private DateTime lastKill = DateTime.Now;
    private DateTime lastSpawn = DateTime.Now;

    protected internal PhotonPlayer(bool isLocal, int actorID, Hashtable properties)
    {
        this.actorID = -1;
        nameField = string.Empty;
        customProperties = new Hashtable();
        modProperties = new Hashtable();
        this.isLocal = isLocal;
        this.actorID = actorID;
        InternalCacheProperties(properties);

        stats = new Stats(this);
        info = new Info(this);
        mod = new Mod(this);
    }

    public PhotonPlayer(bool isLocal, int actorID, string name)
    {
        this.actorID = -1;
        nameField = string.Empty;
        customProperties = new Hashtable();
        modProperties = new Hashtable();
        this.isLocal = isLocal;
        this.actorID = actorID;
        nameField = name;

        stats = new Stats(this);
        info = new Info(this);
        mod = new Mod(this);
    }

    public override bool Equals(object p)
    {
        var player = p as PhotonPlayer;
        return player != null && GetHashCode() == player.GetHashCode();
    }

    public static PhotonPlayer Find(int ID)
    {
        foreach (var p1 in PhotonNetwork.playerList)
        {
            if (p1 != null && p1 == ID)
                return p1;
        }
        return null;
    }

    public PhotonPlayer Get(int id)
    {
        return Find(id);
    }

    public override int GetHashCode()
    {
        return ID;
    }

    public PhotonPlayer GetNext()
    {
        return GetNextFor(ID);
    }

    public PhotonPlayer GetNextFor(PhotonPlayer currentPlayer)
    {
        if (currentPlayer == null)
            return null;
        return GetNextFor(currentPlayer.ID);
    }

    public PhotonPlayer GetNextFor(int currentPlayerId)
    {
        if (PhotonNetwork.networkingPeer == null || PhotonNetwork.networkingPeer.mActors == null || PhotonNetwork.networkingPeer.mActors.Count < 2)
            return null;
        var mActors = PhotonNetwork.networkingPeer.mActors;
        var num = int.MaxValue; //0x7fffffff;
        var num2 = currentPlayerId;
        foreach (var num3 in mActors.Keys)
        {
            if (num3 < num2)
                num2 = num3;
            else
            {
                if (num3 > currentPlayerId && num3 < num)
                    num = num3;
            }
        }
        return num == int.MaxValue ? mActors[num2] : mActors[num];
    }

    internal void InternalCacheProperties(Hashtable properties)
    {
        if (properties != null && properties.Count != 0 && !customProperties.Equals(properties))
        {
            if (properties.ContainsKey((byte)255))
                nameField = (string)properties[(byte)255];
            if (properties[PhotonPlayerProperty.dead] != null && info.isDead != (bool)properties[PhotonPlayerProperty.dead])
            {
                lastSpawn = DateTime.Now;
                lastKill = DateTime.Now;
            }
            else
            {
                if (choseSide && properties[PhotonPlayerProperty.kills] != null && kills < (int)properties[PhotonPlayerProperty.kills])
                    lastKill = DateTime.Now;
            }
            customProperties.MergeStringKeys(properties);
            customProperties.StripKeysWithNullValues();
            
        }
    }

    internal void InternalChangeLocalID(int newID)
    {
        if (!isLocal)
            Debug.LogError("ERROR You should never change PhotonPlayer IDs!");
        else
            actorID = newID;
    }

    internal void SetCustomProperties(Hashtable propertiesToSet)
    {
        if (propertiesToSet != null)
        {
            if (propertiesToSet[PhotonPlayerProperty.dead] != null && info.isDead != (bool)propertiesToSet[PhotonPlayerProperty.dead])
            {
                lastSpawn = DateTime.Now;
                lastKill = DateTime.Now;
            }
            else
            {
                if (choseSide && propertiesToSet[PhotonPlayerProperty.kills] != null && kills < (int)propertiesToSet[PhotonPlayerProperty.kills])
                    lastKill = DateTime.Now;
            }
            customProperties.MergeStringKeys(propertiesToSet);
            customProperties.HashNullFix();
            //this.customProperties.StripKeysWithNullValues();
            if (actorID > 0 && !PhotonNetwork.offlineMode)
                PhotonNetwork.networkingPeer.OpSetCustomPropertiesOfActor(actorID, propertiesToSet, true, 0);
            NetworkingPeer.SendMonoMessage(PhotonNetworkingMessage.OnPhotonPlayerPropertiesChanged, this, propertiesToSet);
        }
    }

    public override string ToString()
    {
        if (name.IsNullOrEmpty())
            return string.Format("#{0:00}{1}", ID, !isMasterClient ? string.Empty : "(mc)");
        return string.Format("'{0}'{1}", name, !isMasterClient ? string.Empty : "(mc)");
    }

    public string ToStringFull()
    {
        return string.Format("#{0:00} '{1}' {2}", ID, name, customProperties.ToStringFull());
    }

    public Hashtable allProperties
    {
        get
        {
            var target = new Hashtable();
            target.Merge(customProperties);
            target[(byte)0xff] = name;
            return target;
        }
    }

    public Hashtable customProperties { get; private set; }

    public int ID
    {
        get { return actorID; }
    }

    public bool isMasterClient
    {
        get { return PhotonNetwork.networkingPeer.mMasterClient == this; }
    }

    public string name
    {
        get { return nameField; }
        set
        {
            if (!isLocal)
                Debug.LogError("Error: Cannot change the name of a remote player!");
            else
                nameField = value;
        }
    }

    public static bool Find(int ID, out PhotonPlayer player)
    {
        foreach (var p1 in PhotonNetwork.playerList)
        {
            if (p1 != null && p1 == ID)
            {
                player = p1;
                return true;
            }
        }
        player = null;
        return false;
    }

    public static bool operator ==(PhotonPlayer p1, PhotonPlayer p2)
    {
        var oneNull = ReferenceEquals(p1, null);
        var twoNull = ReferenceEquals(p2, null);
        if (oneNull != twoNull)
            return false;
        if (oneNull)
            return true;
        return p1.actorID == p2.actorID;
    }

    public static bool operator !=(PhotonPlayer p1, PhotonPlayer p2)
    {
        var oneNull = ReferenceEquals(p1, null);
        var twoNull = ReferenceEquals(p2, null);
        if (oneNull != twoNull)
            return true;
        if (oneNull)
            return false;
        return p1.actorID != p2.actorID;
    }

    public static bool operator ==(PhotonPlayer p1, int ID)
    {
        return p1 != null && p1.actorID == ID;
    }

    public static bool operator !=(PhotonPlayer p1, int ID)
    {
        return p1 != null && p1.actorID != ID;
    }

    public static bool operator ==(int ID, PhotonPlayer p1)
    {
        return p1 != null && p1.actorID == ID;
    }

    public static bool operator !=(int ID, PhotonPlayer p1)
    {
        return p1 != null && p1.actorID != ID;
    }

    public static bool operator ==(PhotonPlayer p1, short ID)
    {
        return p1 != null && p1.actorID == ID;
    }

    public static bool operator !=(PhotonPlayer p1, short ID)
    {
        return p1 != null && p1.actorID != ID;
    }

    public static bool operator ==(short ID, PhotonPlayer p1)
    {
        return p1 != null && p1.actorID == ID;
    }

    public static bool operator !=(short ID, PhotonPlayer p1)
    {
        return p1 != null && p1.actorID != ID;
    }

    public static bool IsInPlayerList(int ID)
    {
        foreach (var p1 in PhotonNetwork.playerList)
        {
            if (p1 != null && p1 == ID)
                return true;
        }
        return false;
    }

    public bool IsInPlayerList()
    {
        foreach (var p1 in PhotonNetwork.playerList)
        {
            if (p1 != null && this == p1)
                return true;
        }
        return false;
    }

    

    public bool choseSide
    {
        get
        {
            var obj = customProperties[PhotonPlayerProperty.isTitan];
            if (obj == null)
                return false;
            switch ((int)obj)
            {
                case 1:
                case 2:
                    return true;
                default:
                    return false;
            }
        }
    }

    public int kills
    {
        get
        {
            var obj = customProperties[PhotonPlayerProperty.kills];
            if (obj is int)
                return (int)obj;
            return 0;
        }
        private set
        {
            SetCustomProperties(new Hashtable
            {
                { PhotonPlayerProperty.kills, value }
            });
        }
    }

    public int max_dmg
    {
        get
        {
            var obj = customProperties[PhotonPlayerProperty.max_dmg];
            if (obj is int)
                return (int)obj;
            return 0;
        }
        private set
        {
            SetCustomProperties(new Hashtable
            {
                { PhotonPlayerProperty.max_dmg, value }
            });
        }
    }

    public int total_dmg
    {
        get
        {
            var obj = customProperties[PhotonPlayerProperty.total_dmg];
            if (obj is int)
                return (int)obj;
            return 0;
        }
        private set
        {
            SetCustomProperties(new Hashtable
            {
                { PhotonPlayerProperty.total_dmg, value }
            });
        }
    }

    public int deaths
    {
        get
        {
            var obj = customProperties[PhotonPlayerProperty.deaths];
            if (obj is int)
                return (int)obj;
            return 0;
        }
        private set
        {
            SetCustomProperties(new Hashtable
            {
                { PhotonPlayerProperty.deaths, value }
            });
        }
    }

    public Dictionary<string, string> allProps
    {
        get
        {
            var targetProps = new Dictionary<string, string>();
            foreach (var prop in customProperties)
                targetProps.Add((string)prop.Key, (string)prop.Value);

            return targetProps;
        }
    }

    public HERO hero
    {
        get { return mHero/* ?? GlobalItems.getHero(ID)*/; }
        set { mHero = value; }

    }

    public bool grabbed
    {
        get { return hero != null && hero.isGrabbed; }
        set { mGrabbed = value; }
    }

    public Info info;
    public struct Info
    {
        private readonly PhotonPlayer p;


        public Info(PhotonPlayer player)
        {
            p = player;
        }

        public string name
        {
            get
            {
                var obj = p.customProperties[PhotonPlayerProperty.name];
                if (obj is string)
                    return (string)obj;
                return string.Empty;
            }
            private set
            {
                p.SetCustomProperties(new Hashtable
                {
                    { PhotonPlayerProperty.name, value }
                });
            }
        }

        public string guild
        {
            get
            {
                var obj = p.customProperties[PhotonPlayerProperty.guildName];
                if (obj is string)
                    return (string)obj;
                return string.Empty;
            }
            private set
            {
                p.SetCustomProperties(new Hashtable
                {
                    { PhotonPlayerProperty.guildName, value }
                });
            }
        }

        public bool isDead
        {
            get
            {
                var obj = p.customProperties[PhotonPlayerProperty.dead];
                if (obj is bool)
                    return (bool)obj;
                return true;
            }
            private set
            {
                p.SetCustomProperties(new Hashtable
                {
                    { PhotonPlayerProperty.dead, value }
                });
            }
        }

        public bool isTitan
        {
            get { return p.customProperties[PhotonPlayerProperty.isTitan] != null && p.customProperties[PhotonPlayerProperty.isTitan].Equals(2); }
        }

        public bool isHuman
        {
            get { return p.customProperties[PhotonPlayerProperty.isTitan] != null && p.customProperties[PhotonPlayerProperty.isTitan].Equals(1); }
        }

        public bool isAHSS
        {
            get { return team == 2; }
        }

        public int team
        {
            get
            {
                if (p.customProperties[PhotonPlayerProperty.team] == null)
                    return 0;
                return (int)p.customProperties[PhotonPlayerProperty.team];
            }
            private set
            {
                p.SetCustomProperties(new Hashtable
                {
                    { PhotonPlayerProperty.team, value }
                });
            }
        }
    }

    public Mod mod;
    public struct Mod
    {
        private readonly PhotonPlayer p;

        public Mod(PhotonPlayer player)
        {
            p = player;
        }

        public bool RC
        {
            get
            {
                if (p.customProperties[PhotonPlayerProperty.RCteam] == null && p.customProperties[PhotonPlayerProperty.currentLevel] == null)
                    return false;
                if (p.customProperties[PhotonPlayerProperty.version] != null || p.customProperties[PhotonPlayerProperty.chatname] != null)
                    return false;
                return true;
            }
        }
    }

    public Stats stats;
    public struct Stats
    {
        private readonly PhotonPlayer p;

        public Stats(PhotonPlayer player)
        {
            p = player;
        }

        public string CHARACTER
        {
            get { return p.customProperties[PhotonPlayerProperty.character] != null ? p.customProperties[PhotonPlayerProperty.character].ToString().ToUpper() : "null"; }
        }

        public string skill
        {
            get { return (string)p.customProperties[PhotonPlayerProperty.statSKILL] ?? "null"; }
        }

        public int SPD
        {
            get { return (int?)p.customProperties[PhotonPlayerProperty.statSPD] ?? HeroStat.stats[CHARACTER].SPD; }
        }

        public int ACL
        {
            get { return (int?)p.customProperties[PhotonPlayerProperty.statACL] ?? HeroStat.stats[CHARACTER].ACL; }
        }

        public int GAS
        {
            get { return (int?)p.customProperties[PhotonPlayerProperty.statGAS] ?? HeroStat.stats[CHARACTER].GAS; }
        }

        public int BLA
        {
            get { return (int?)p.customProperties[PhotonPlayerProperty.statBLA] ?? HeroStat.stats[CHARACTER].BLA; }
        }

        public int total
        {
            get { return SPD + ACL + GAS + BLA; }
        }

        public bool hasBoost
        {
            get { return total > HeroStat.shouldHave(CHARACTER); }
        }
    }
}