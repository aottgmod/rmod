using System;
using System.IO;
using UnityEngine;

public class PhotonStatsGui : MonoBehaviour
{
    public bool buttonsOn = true;
    public bool healthStatsVisible = true;
    public bool statsOn;
    public Rect statsRect = new Rect(0f, 100f, 200f, 50f);
    public bool statsWindowOn;
    public bool trafficStatsOn = true;
    public int WindowId = 100;

    private Texture2D _ActiveButtontexture;
    private Texture2D _HoverButtontexture;
    private Texture2D _NormalButtonTexture;
    private Texture2D _NormalWindowTexture;

    public void OnGUI()
    {
        if (PhotonNetwork.networkingPeer.TrafficStatsEnabled != statsOn)
            PhotonNetwork.networkingPeer.TrafficStatsEnabled = statsOn;
        if (statsWindowOn)
            statsRect = GUILayout.Window(WindowId, statsRect, TrafficStatsWindow, "Messages (LeftAlt)");
        
        if (statsRect.x < 0f)
            statsRect.x = 0f;
        else if (statsRect.x > Screen.width - statsRect.width)
            statsRect.x = Screen.width - statsRect.width;
        if (statsRect.y < 0f)
            statsRect.y = 0f;
        else if (statsRect.y > Screen.height - statsRect.height)
            statsRect.y = Screen.height - statsRect.height;
    }

    public void Start()
    {
        statsRect.x = Screen.width - statsRect.width;

        _HoverButtontexture = new Texture2D(1, 1);
        _HoverButtontexture.SetPixel(1, 1, new Color(0.33f, 0.33f, 0.33f, 1f));
        _HoverButtontexture.Apply();
        _NormalButtonTexture = new Texture2D(1, 1);
        _NormalButtonTexture.SetPixel(1, 1, new Color(0.037f, 0.037f, 0.037f, 1f));
        _NormalButtonTexture.Apply();
        _ActiveButtontexture = new Texture2D(1, 1);
        _ActiveButtontexture.SetPixel(1, 1, new Color(0.026f, 0.026f, 0.026f, 1f));
        _ActiveButtontexture.Apply();
        _NormalWindowTexture = new Texture2D(1, 1);
        _NormalWindowTexture.SetPixel(1, 1, new Color(0.145f, 0.145f, 0.145f, 0.98f));
        _NormalWindowTexture.Apply();
    }

    public void TrafficStatsWindow(int windowID)
    {
        GUI.skin.window.normal.background =  GUI.skin.window.active.background = GUI.skin.window.onActive.background = GUI.skin.window.onNormal.background = _NormalWindowTexture;
        GUI.skin.button.active.background = GUI.skin.button.normal.background = _NormalButtonTexture;
        GUI.skin.box.active.background = GUI.skin.box.onActive.background = _NormalWindowTexture;
        GUI.skin.button.hover.background = _HoverButtontexture;

        var flag = false;
        var trafficStatsGameLevel = PhotonNetwork.networkingPeer.TrafficStatsGameLevel;
        var num = PhotonNetwork.networkingPeer.TrafficStatsElapsedMs / 0x3e8L;
        if (num == 0)
            num = 1L;
        GUILayout.BeginHorizontal();
        buttonsOn = GUILayout.Toggle(buttonsOn, "buttons");
        healthStatsVisible = GUILayout.Toggle(healthStatsVisible, "health");
        trafficStatsOn = GUILayout.Toggle(trafficStatsOn, "traffic");
        GUILayout.EndHorizontal();
        var text = string.Format("<color=#00CC00>Out</color>({4})|<color=#CC0000>In</color>({3})|<color=#006699>Sum</color>:\n<color=#00CC00>{0,4}</color>|<color=#CC0000>{1,4}</color>|<color=#006699>{2,4}</color>", trafficStatsGameLevel.TotalOutgoingMessageCount, trafficStatsGameLevel.TotalIncomingMessageCount, trafficStatsGameLevel.TotalMessageCount, trafficStatsGameLevel.TotalIncomingByteCount, trafficStatsGameLevel.TotalOutgoingByteCount);
        var str2 = string.Format("{0}sec average:", num);
        var str3 = string.Format("<color=#00CC00>Out</color>|<color=#CC0000>In</color>|<color=#006699>Sum</color>:\n<color=#00CC00>{0,4}</color>|<color=#CC0000>{1,4}</color>|<color=#006699>{2,4}</color>", trafficStatsGameLevel.TotalOutgoingMessageCount / num, trafficStatsGameLevel.TotalIncomingMessageCount / num, trafficStatsGameLevel.TotalMessageCount / num);
        GUILayout.Label(text);
        GUILayout.Label(str2);
        GUILayout.Label(str3);
        if (buttonsOn)
        {
            GUILayout.BeginHorizontal();
            statsOn = GUILayout.Toggle(statsOn, "stats on");
            if (GUILayout.Button("Reset"))
            {
                PhotonNetwork.networkingPeer.TrafficStatsReset();
                PhotonNetwork.networkingPeer.TrafficStatsEnabled = true;
            }
            flag = GUILayout.Button("To Log");
            GUILayout.EndHorizontal();
        }
        var str4 = string.Empty;
        var str5 = string.Empty;
        if (trafficStatsOn)
        {
            str4 = "<color=#CC0000>Incoming:</color> " + PhotonNetwork.networkingPeer.TrafficStatsIncoming;
            str5 = "<color=#00CC00>Outgoing:</color> " + PhotonNetwork.networkingPeer.TrafficStatsOutgoing;
            GUILayout.Label(str4);
            GUILayout.Label(str5);
        }
        var str6 = string.Empty;
        if (healthStatsVisible)
        {
            var args = new object[] { trafficStatsGameLevel.LongestDeltaBetweenSending, trafficStatsGameLevel.LongestDeltaBetweenDispatching, trafficStatsGameLevel.LongestEventCallback, trafficStatsGameLevel.LongestEventCallbackCode, trafficStatsGameLevel.LongestOpResponseCallback, trafficStatsGameLevel.LongestOpResponseCallbackOpCode, PhotonNetwork.networkingPeer.RoundTripTime, PhotonNetwork.networkingPeer.RoundTripTimeVariance };
            str6 = string.Format("ping: <color=#33FF00>{6}</color>[+/-{7}]ms\nlongest delta between\nsend: <color=#99FF00>{0,4}ms</color> disp: <color=#99FF00>{1,4}ms</color>\nlongest time for:\nev({3}):{2,3}ms op({5}):{4,3}ms", args);
            GUILayout.Label(str6);
        }
        if (flag)
        {
            var objArray2 = new object[] { text, str2, str3, str4, str5, str6 };
            Debug.Log(string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n{5}", objArray2));
            File.AppendAllText("PhotonStatsLog.txt", string.Format("{0}\n{1}\n{2}\n{3}\n{4}\n{5}", objArray2) + Environment.NewLine + Environment.NewLine);
        }
        if (GUI.changed)
            statsRect.height = 100f;
        GUI.DragWindow();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            statsWindowOn = !statsWindowOn;
            statsOn = true;
        }
    }
}