using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using ExitGames.Client.Photon;
using UnityEngine;
using Debug = UnityEngine.Debug;
using MonoBehaviour = Photon.MonoBehaviour;

internal class PhotonHandler : MonoBehaviour, IPhotonPeerListener
{
    public static bool AppQuits;
    internal static CloudRegionCode BestRegionCodeCurrently = CloudRegionCode.none;
    private int nextSendTickCount;
    private int nextSendTickCountOnSerialize;
    public static Type PingImplementation;
    private const string PlayerPrefsKey = "PUNCloudBestRegion";
    private static bool sendThreadShouldRun;
    public static PhotonHandler SP;
    public int updateInterval;
    public int updateIntervalOnSerialize;

    protected void Awake()
    {
        if ((SP != null) && (SP != this) && (SP.gameObject != null))
        {
            DestroyImmediate(SP.gameObject);
        }
        SP = this;
        DontDestroyOnLoad(gameObject);
        updateInterval = 0x3e8 / PhotonNetwork.sendRate;
        updateIntervalOnSerialize = 0x3e8 / PhotonNetwork.sendRateOnSerialize;
        StartFallbackSendAckThread();
    }

    public void DebugReturn(DebugLevel level, string message)
    {
        if (level == DebugLevel.ERROR)
            Debug.LogError(message);
        else
        {
            if (level == DebugLevel.WARNING)
                Debug.LogWarning(message);
            else
            {
                if ((level == DebugLevel.INFO) && (PhotonNetwork.logLevel >= PhotonLogLevel.Informational))
                    Debug.Log(message);
                else
                {
                    if ((level == DebugLevel.ALL) && (PhotonNetwork.logLevel == PhotonLogLevel.Full))
                        Debug.Log(message);
                }
            }
        }
    }

    public static bool FallbackSendAckThread()
    {
        if (sendThreadShouldRun && (PhotonNetwork.networkingPeer != null))
            PhotonNetwork.networkingPeer.SendAcksOnly();
        return sendThreadShouldRun;
    }

    protected void OnApplicationQuit()
    {
        AppQuits = true;
        StopFallbackSendAckThread();
        PhotonNetwork.Disconnect();
    }

    protected void OnCreatedRoom()
    {
        PhotonNetwork.networkingPeer.SetLevelInPropsIfSynced(Application.loadedLevelName);
    }

    public void OnEvent(EventData photonEvent)
    {
    }

    protected void OnJoinedRoom()
    {
        PhotonNetwork.networkingPeer.LoadLevelIfSynced();
    }

    protected void OnLevelWasLoaded(int level)
    {
        PhotonNetwork.networkingPeer.NewSceneLoaded();
        PhotonNetwork.networkingPeer.SetLevelInPropsIfSynced(Application.loadedLevelName);
    }

    public void OnOperationResponse(OperationResponse operationResponse)
    {
    }

    public void OnStatusChanged(StatusCode statusCode)
    {
    }

    protected internal static void PingAvailableRegionsAndConnectToBest()
    {
        SP.StartCoroutine(SP.PingAvailableRegionsCoroutine(true));
    }

    [DebuggerHidden]
    internal IEnumerator PingAvailableRegionsCoroutine(bool connectToBest)
    {
        return new c__IteratorA { connectToBest = connectToBest};
    }

    public static void StartFallbackSendAckThread()
    {
        if (!sendThreadShouldRun)
        {
            sendThreadShouldRun = true;
            SupportClass.CallInBackground(FallbackSendAckThread);
        }
    }

    public static void StopFallbackSendAckThread()
    {
        sendThreadShouldRun = false;
    }

    protected void Update()
    {
        if (PhotonNetwork.networkingPeer == null)
            Debug.LogError("NetworkPeer broke!");
        else
        {
            if ((PhotonNetwork.connectionStateDetailed != PeerState.PeerCreated) && (PhotonNetwork.connectionStateDetailed != PeerState.Disconnected) && !PhotonNetwork.offlineMode && PhotonNetwork.isMessageQueueRunning)
            {
                for (var flag = true; PhotonNetwork.isMessageQueueRunning && flag; flag = PhotonNetwork.networkingPeer.DispatchIncomingCommands())
                {
                }
                var num = (int) (Time.realtimeSinceStartup * 1000f);
                if (PhotonNetwork.isMessageQueueRunning && (num > nextSendTickCountOnSerialize))
                {
                    PhotonNetwork.networkingPeer.RunViewUpdate();
                    nextSendTickCountOnSerialize = num + updateIntervalOnSerialize;
                    nextSendTickCount = 0;
                }
                num = (int) (Time.realtimeSinceStartup * 1000f);
                if (num > nextSendTickCount)
                {
                    for (var flag2 = true; PhotonNetwork.isMessageQueueRunning && flag2; flag2 = PhotonNetwork.networkingPeer.SendOutgoingCommands())
                    {
                    }
                    nextSendTickCount = num + updateInterval;
                }
            }
        }
    }

    internal static CloudRegionCode BestRegionCodeInPreferences
    {
        get
        {
            var str = PlayerPrefs.GetString("PUNCloudBestRegion", string.Empty);
            if (!string.IsNullOrEmpty(str))
                return Region.Parse(str);
            return CloudRegionCode.none;
        }
        set
        {
            if (value == CloudRegionCode.none)
                PlayerPrefs.DeleteKey("PUNCloudBestRegion");
            else
                PlayerPrefs.SetString("PUNCloudBestRegion", value.ToString());
        }
    }

    [CompilerGenerated]
    private sealed class c__IteratorA : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object current;
        internal int PC;
//        internal bool connectToBest; //RD:That line was detected as a duplicated variable declaration
        internal List<Region>.Enumerator s_89__1;
        internal Region best__3;
        internal PhotonPingManager pingManager__0;
        internal Region region__2;
        internal bool connectToBest;

        [DebuggerHidden]
        public void Dispose()
        {
            PC = -1;
        }

        public bool MoveNext()
        {
            var num = (uint) PC;
            PC = -1;
            switch (num)
            {
                case 0:
                    BestRegionCodeCurrently = CloudRegionCode.none;
                    break;

                case 1:
                    break;

                case 2:
                    goto Label_01A7;

                default:
                    goto Label_026C;
            }
            if (PhotonNetwork.networkingPeer.AvailableRegions == null)
            {
                if ((PhotonNetwork.connectionStateDetailed != PeerState.ConnectingToNameServer) && (PhotonNetwork.connectionStateDetailed != PeerState.ConnectedToNameServer))
                {
                    Debug.LogError("Call ConnectToNameServer to ping available regions.");
                    goto Label_026C;
                }
                Debug.Log(string.Concat("Waiting for AvailableRegions. State: ", PhotonNetwork.connectionStateDetailed, " Server: ", PhotonNetwork.Server, " PhotonNetwork.networkingPeer.AvailableRegions ", PhotonNetwork.networkingPeer.AvailableRegions != null));
                current = new WaitForSeconds(0.25f);
                PC = 1;
                goto Label_026E;
            }
            if ((PhotonNetwork.networkingPeer.AvailableRegions == null) || (PhotonNetwork.networkingPeer.AvailableRegions.Count == 0))
            {
                Debug.LogError("No regions available. Are you sure your appid is valid and setup?");
                goto Label_026C;
            }
            pingManager__0 = new PhotonPingManager();
            s_89__1 = PhotonNetwork.networkingPeer.AvailableRegions.GetEnumerator();
            try
            {
                while (s_89__1.MoveNext())
                {
                    region__2 = s_89__1.Current;
                    SP.StartCoroutine(pingManager__0.PingSocket(region__2));
                }
            }
            finally
            {
                s_89__1.Dispose();
            }
        Label_01A7:
            while (!pingManager__0.Done)
            {
                current = new WaitForSeconds(0.1f);
                PC = 2;
                goto Label_026E;
            }
            best__3 = pingManager__0.BestRegion;
            BestRegionCodeCurrently = best__3.Code;
            BestRegionCodeInPreferences = best__3.Code;
            Debug.Log(string.Concat("Found best region: ", best__3.Code, " ping: ", best__3.Ping, ". Calling ConnectToRegionMaster() is: ", connectToBest));
            if (connectToBest)
                PhotonNetwork.networkingPeer.ConnectToRegionMaster(best__3.Code);
            PC = -1;
        Label_026C:
            return false;
        Label_026E:
            return true;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }
    }
}