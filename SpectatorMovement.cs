using mItems;
using UnityEngine;

public class SpectatorMovement : MonoBehaviour //todo make it better
{
    public bool disable;
    public FengCustomInputs inputManager;
    private readonly float speed = 100f;

    private void Start()
    {
        inputManager = CacheGameObject.Find<FengCustomInputs>("InputManagerController");
    }

    private void Update()
    {
        if (!disable)
        {
            var vertical = inputManager.isInput[InputCode.up] ? 1f : (inputManager.isInput[InputCode.down] ? -1f : 0f);
            var horizontal = inputManager.isInput[InputCode.left] ? -1f : (inputManager.isInput[InputCode.right] ? 1f : 0f);

            if (vertical > 0f)
                transform.position += transform.forward * speed * Time.deltaTime;
            if (vertical < 0f)
                transform.position -= transform.forward * speed * Time.deltaTime;
            if (horizontal > 0f)
                transform.position += transform.right * speed * Time.deltaTime;
            if (horizontal < 0f)
                transform.position -= transform.right * speed * Time.deltaTime;
            if (inputManager.isInput[InputCode.leftRope])
                transform.position -= transform.up * speed * Time.deltaTime;
            if (inputManager.isInput[InputCode.rightRope])
                transform.position += transform.up * speed * Time.deltaTime;
        }
    }
}