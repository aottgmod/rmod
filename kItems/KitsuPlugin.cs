﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace kItems
{
    public class TypeBinder
    {
        public Type type;
        public object instance;
        public Assembly assembly;

        public TypeBinder(Type type)
        {
            this.type = type;
            assembly = Assembly.GetAssembly(type);
            instance = Activator.CreateInstance(this.type, null);
        }
    }

    public interface IPlugin
    {
        string Name { get; }

        void OnLoad();
    }

    public interface IBehaviour
    {
        void Start();

        void Update();

        void LateUpdate();

        void FixedUpdate();

        void OnGUI();
    }
}