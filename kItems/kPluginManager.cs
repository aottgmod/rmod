﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using kItems;
using UnityEngine;
using MonoBehaviour = Photon.MonoBehaviour;

public class kPluginManager : MonoBehaviour
{
    public static kPluginManager instance;

    public List<Assembly> plugins;
    private List<TypeBinder> mainInstances;
    private List<IBehaviour> behaviours;

    private Dictionary<string, Assembly> loaded_plugins;

    public void Start()
    {
        DontDestroyOnLoad(this);
        instance = this;

        mainInstances = new List<TypeBinder>();
        plugins = new List<Assembly>();
        behaviours = new List<IBehaviour>();
        loaded_plugins = new Dictionary<string, Assembly>();

        foreach (var file in Directory.GetFiles(string.Format("{0}/Assets/Plugins/", Application.dataPath)).Where(file => file.EndsWith(".dll")))
        {
            //what im trying to do is to load the plugin references into the same appDomain
            Debug.Log("=" + file);
            var ass = Assembly.Load(file);
            //var ass = AppDomain.CurrentDomain.Load(file);
            if (ass != null)
            {
                loaded_plugins.Add(ass.FullName, ass);//test
                plugins.Add(ass);
            }
        }
        loadPlugins();
    }

    public bool hasPlugin(string name)
    {
        return AppDomain.CurrentDomain.GetAssemblies().Any(x => x.FullName.ToLower().Contains(name.ToLower()));
    }

    public void loadPlugins()
    {
        foreach (var ass in plugins)
        {
            /*if (ass.GetType().GetInterfaces().Contains(typeof(IPlugin)))  //test
                loaded_plugins.Add(ass.GetType().GetProperty("Name").ToString(), ass);*/
            foreach (var t in ass.GetTypes())
            {
                if (t.Name.Equals("Main"))
                {
                    var tb = new TypeBinder(t);
                    mainInstances.Add(tb);
                }
            }
        }
        foreach (var tb in mainInstances)
        {
            var t = tb.type;
            var name_field = t.GetField("Name");
            if (name_field != null)
                Debug.Log(string.Format("Loaded Plugin {0}!", (string)name_field.GetValue(tb.instance)));
            else
                Debug.Log(string.Format("Unknown plugin name loaded from Assembly {0}!", tb.assembly.GetName().Name));
            var method = t.GetMethod("OnLoad");
            if (method != null)
                method.Invoke(tb.instance, null);
            var gm = typeof(IBehaviour);
            foreach (var game in tb.assembly.GetTypes())
            {
                if (game.BaseType == gm)
                {
                    var g = (IBehaviour)Activator.CreateInstance(game, null);

                    g.Start();
                    behaviours.Add(g);
                }
            }
        }
    }

    public void Update()
    {
        return;
        foreach (var behaviour in behaviours)
            behaviour.Update();
    }

    public void FixedUpdate()
    {
        return;
        foreach (var behaviour in behaviours)
            behaviour.FixedUpdate();
    }

    public void LateUpdate()
    {
        return;
        foreach (var behaviour in behaviours)
            behaviour.LateUpdate();
    }

    public void OnGUI()
    {
        return;
        foreach (var behaviour in behaviours)
            behaviour.OnGUI();
    }
}