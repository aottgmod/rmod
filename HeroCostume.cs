using UnityEngine;

public class HeroCostume
{
    public string _3dmg_texture = string.Empty;
    public string arm_l_mesh = string.Empty;
    public string arm_r_mesh = string.Empty;
    public string beard_mesh = string.Empty;
    public int beard_texture_id = -1;
    public static string[] body_casual_fa_texture;
    public static string[] body_casual_fb_texture;
    public static string[] body_casual_ma_texture;
    public static string[] body_casual_mb_texture;
    public string body_mesh = string.Empty;
    public string body_texture = string.Empty;
    public static string[] body_uniform_fa_texture;
    public static string[] body_uniform_fb_texture;
    public static string[] body_uniform_ma_texture;
    public static string[] body_uniform_mb_texture;
    public string brand_texture = string.Empty;
    public string brand1_mesh = string.Empty;
    public string brand2_mesh = string.Empty;
    public string brand3_mesh = string.Empty;
    public string brand4_mesh = string.Empty;
    public bool cape;
    public string cape_mesh = string.Empty;
    public string cape_texture = string.Empty;
    public static HeroCostume[] costume;
    public int costumeId;
    public static HeroCostume[] costumeOption;
    public DIVISION division;
    public string eye_mesh = string.Empty;
    public int eye_texture_id = -1;
    public string face_texture = string.Empty;
    public string glass_mesh = string.Empty;
    public int glass_texture_id = -1;
    public string hair_1_mesh = string.Empty;
    public Color hair_color = new Color(0.5f, 0.1f, 0f);
    public string hair_mesh = string.Empty;
    public CostumeHair hairInfo;
    public string hand_l_mesh = string.Empty;
    public string hand_r_mesh = string.Empty;
    public int id;
    private static bool inited;
    public string mesh_3dmg = string.Empty;
    public string mesh_3dmg_belt = string.Empty;
    public string mesh_3dmg_gas_l = string.Empty;
    public string mesh_3dmg_gas_r = string.Empty;
    public string name = string.Empty;
    public string part_chest_1_object_mesh = string.Empty;
    public string part_chest_1_object_texture = string.Empty;
    public string part_chest_object_mesh = string.Empty;
    public string part_chest_object_texture = string.Empty;
    public string part_chest_skinned_cloth_mesh = string.Empty;
    public string part_chest_skinned_cloth_texture = string.Empty;
    public SEX sex;
    public int skin_color = 1;
    public string skin_texture = string.Empty;
    public HeroStat stat;
    public UNIFORM_TYPE uniform_type = UNIFORM_TYPE.CasualA;
    public string weapon_l_mesh = string.Empty;
    public string weapon_r_mesh = string.Empty;

    public void checkstat()
    {
        var num = 0;
        num += stat.SPD;
        num += stat.GAS;
        num += stat.BLA;
        num += stat.ACL;
        if (num > 400)
            stat.SPD = stat.GAS = stat.BLA = stat.ACL = 100;
    }

    public static void init()
    {
        if (!inited)
        {
            inited = true;
            CostumeHair.init();
            body_uniform_ma_texture = new[] { "aottg_hero_uniform_ma_1", "aottg_hero_uniform_ma_2", "aottg_hero_uniform_ma_3" };
            body_uniform_fa_texture = new[] { "aottg_hero_uniform_fa_1", "aottg_hero_uniform_fa_2", "aottg_hero_uniform_fa_3" };
            body_uniform_mb_texture = new[] { "aottg_hero_uniform_mb_1", "aottg_hero_uniform_mb_2", "aottg_hero_uniform_mb_3", "aottg_hero_uniform_mb_4" };
            body_uniform_fb_texture = new[] { "aottg_hero_uniform_fb_1", "aottg_hero_uniform_fb_2" };
            body_casual_ma_texture = new[] { "aottg_hero_casual_ma_1", "aottg_hero_casual_ma_2", "aottg_hero_casual_ma_3" };
            body_casual_fa_texture = new[] { "aottg_hero_casual_fa_1", "aottg_hero_casual_fa_2", "aottg_hero_casual_fa_3" };
            body_casual_mb_texture = new[] { "aottg_hero_casual_mb_1", "aottg_hero_casual_mb_2", "aottg_hero_casual_mb_3", "aottg_hero_casual_mb_4" };
            body_casual_fb_texture = new[] { "aottg_hero_casual_fb_1", "aottg_hero_casual_fb_2" };
            costume = new HeroCostume[38];
            costume[0] = new HeroCostume
            {
                name = "annie",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.UniformB,
                part_chest_object_mesh = "character_cap_uniform",
                part_chest_object_texture = "aottg_hero_annie_cap_uniform",
                cape = true,
                body_texture = body_uniform_fb_texture[0],
                hairInfo = CostumeHair.hairsF[5],
                eye_texture_id = 0,
                beard_texture_id = 33,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(1f, 0.9f, 0.5f),
                division = DIVISION.TheMilitaryPolice,
                costumeId = 0
            };
            costume[1] = new HeroCostume
            {
                name = "annie",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.UniformB,
                part_chest_object_mesh = "character_cap_uniform",
                part_chest_object_texture = "aottg_hero_annie_cap_uniform",
                body_texture = body_uniform_fb_texture[0],
                cape = false,
                hairInfo = CostumeHair.hairsF[5],
                eye_texture_id = 0,
                beard_texture_id = 33,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(1f, 0.9f, 0.5f),
                division = DIVISION.TraineesSquad,
                costumeId = 0
            };
            costume[2] = new HeroCostume
            {
                name = "annie",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.CasualB,
                part_chest_object_mesh = "character_cap_casual",
                part_chest_object_texture = "aottg_hero_annie_cap_causal",
                part_chest_1_object_mesh = "character_body_blade_keeper_f",
                part_chest_1_object_texture = body_casual_fb_texture[0],
                body_texture = body_casual_fb_texture[0],
                cape = false,
                hairInfo = CostumeHair.hairsF[5],
                eye_texture_id = 0,
                beard_texture_id = 33,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(1f, 0.9f, 0.5f),
                costumeId = 1
            };
            costume[3] = new HeroCostume
            {
                name = "mikasa",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.UniformB,
                body_texture = body_uniform_fb_texture[1],
                cape = true,
                hairInfo = CostumeHair.hairsF[7],
                eye_texture_id = 2,
                beard_texture_id = 33,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.15f, 0.15f, 0.145f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 2
            };
            costume[4] = new HeroCostume
            {
                name = "mikasa",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.UniformB,
                part_chest_skinned_cloth_mesh = "mikasa_asset_uni",
                part_chest_skinned_cloth_texture = body_uniform_fb_texture[1],
                body_texture = body_uniform_fb_texture[1],
                cape = false,
                hairInfo = CostumeHair.hairsF[7],
                eye_texture_id = 2,
                beard_texture_id = 33,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.15f, 0.15f, 0.145f),
                division = DIVISION.TraineesSquad,
                costumeId = 3
            };
            costume[5] = new HeroCostume
            {
                name = "mikasa",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.CasualB,
                part_chest_skinned_cloth_mesh = "mikasa_asset_cas",
                part_chest_skinned_cloth_texture = body_casual_fb_texture[1],
                part_chest_1_object_mesh = "character_body_blade_keeper_f",
                part_chest_1_object_texture = body_casual_fb_texture[1],
                body_texture = body_casual_fb_texture[1],
                cape = false,
                hairInfo = CostumeHair.hairsF[7],
                eye_texture_id = 2,
                beard_texture_id = 33,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.15f, 0.15f, 0.145f),
                costumeId = 4
            };
            costume[6] = new HeroCostume
            {
                name = "levi",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformB,
                body_texture = body_uniform_mb_texture[1],
                cape = true,
                hairInfo = CostumeHair.hairsM[7],
                eye_texture_id = 1,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.295f, 0.295f, 0.275f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 11
            };
            costume[7] = new HeroCostume
            {
                name = "levi",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.CasualB,
                body_texture = body_casual_mb_texture[1],
                part_chest_1_object_mesh = "character_body_blade_keeper_m",
                part_chest_1_object_texture = body_casual_mb_texture[1],
                cape = false,
                hairInfo = CostumeHair.hairsM[7],
                eye_texture_id = 1,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.295f, 0.295f, 0.275f),
                costumeId = 12
            };
            costume[8] = new HeroCostume
            {
                name = "eren",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformB,
                body_texture = body_uniform_mb_texture[0],
                cape = true,
                hairInfo = CostumeHair.hairsM[4],
                eye_texture_id = 3,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.295f, 0.295f, 0.275f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 13
            };
            costume[9] = new HeroCostume
            {
                name = "eren",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformB,
                body_texture = body_uniform_mb_texture[0],
                cape = false,
                hairInfo = CostumeHair.hairsM[4],
                eye_texture_id = 3,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.295f, 0.295f, 0.275f),
                division = DIVISION.TraineesSquad,
                costumeId = 13
            };
            costume[10] = new HeroCostume
            {
                name = "eren",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.CasualB,
                body_texture = body_casual_mb_texture[0],
                part_chest_1_object_mesh = "character_body_blade_keeper_m",
                part_chest_1_object_texture = body_casual_mb_texture[0],
                cape = false,
                hairInfo = CostumeHair.hairsM[4],
                eye_texture_id = 3,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.295f, 0.295f, 0.275f),
                costumeId = 14
            };
            costume[11] = new HeroCostume
            {
                name = "sasha",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_fa_texture[1],
                cape = true,
                hairInfo = CostumeHair.hairsF[10],
                eye_texture_id = 4,
                beard_texture_id = 33,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.45f, 0.33f, 0.255f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 5
            };
            costume[12] = new HeroCostume
            {
                name = "sasha",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_fa_texture[1],
                cape = false,
                hairInfo = CostumeHair.hairsF[10],
                eye_texture_id = 4,
                beard_texture_id = 33,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.45f, 0.33f, 0.255f),
                division = DIVISION.TraineesSquad,
                costumeId = 5
            };
            costume[13] = new HeroCostume
            {
                name = "sasha",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.CasualA,
                body_texture = body_casual_fa_texture[1],
                part_chest_1_object_mesh = "character_body_blade_keeper_f",
                part_chest_1_object_texture = body_casual_fa_texture[1],
                cape = false,
                hairInfo = CostumeHair.hairsF[10],
                eye_texture_id = 4,
                beard_texture_id = 33,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.45f, 0.33f, 0.255f),
                costumeId = 6
            };
            costume[14] = new HeroCostume
            {
                name = "hanji",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_fa_texture[2],
                cape = true,
                hairInfo = CostumeHair.hairsF[6],
                eye_texture_id = 5,
                beard_texture_id = 33,
                glass_texture_id = 49,
                skin_color = 1,
                hair_color = new Color(0.45f, 0.33f, 0.255f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 7
            };
            costume[15] = new HeroCostume
            {
                name = "hanji",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.CasualA,
                body_texture = body_casual_fa_texture[2],
                part_chest_1_object_mesh = "character_body_blade_keeper_f",
                part_chest_1_object_texture = body_casual_fa_texture[2],
                cape = false,
                hairInfo = CostumeHair.hairsF[6],
                eye_texture_id = 5,
                beard_texture_id = 33,
                glass_texture_id = 49,
                skin_color = 1,
                hair_color = new Color(0.295f, 0.23f, 0.17f),
                costumeId = 8
            };
            costume[16] = new HeroCostume
            {
                name = "rico",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_fa_texture[0],
                cape = true,
                hairInfo = CostumeHair.hairsF[9],
                eye_texture_id = 6,
                beard_texture_id = 33,
                glass_texture_id = 48,
                skin_color = 1,
                hair_color = new Color(1f, 1f, 1f),
                division = DIVISION.TheGarrison,
                costumeId = 9
            };
            costume[17] = new HeroCostume
            {
                name = "rico",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.CasualA,
                body_texture = body_casual_fa_texture[0],
                part_chest_1_object_mesh = "character_body_blade_keeper_f",
                part_chest_1_object_texture = body_casual_fa_texture[0],
                cape = false,
                hairInfo = CostumeHair.hairsF[9],
                eye_texture_id = 6,
                beard_texture_id = 33,
                glass_texture_id = 48,
                skin_color = 1,
                hair_color = new Color(1f, 1f, 1f),
                costumeId = 10
            };
            costume[18] = new HeroCostume
            {
                name = "jean",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_ma_texture[1],
                cape = true,
                hairInfo = CostumeHair.hairsM[6],
                eye_texture_id = 7,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.94f, 0.84f, 0.6f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 15
            };
            costume[19] = new HeroCostume
            {
                name = "jean",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_ma_texture[1],
                cape = false,
                hairInfo = CostumeHair.hairsM[6],
                eye_texture_id = 7,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.94f, 0.84f, 0.6f),
                division = DIVISION.TraineesSquad,
                costumeId = 15
            };
            costume[20] = new HeroCostume
            {
                name = "jean",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.CasualA,
                body_texture = body_casual_ma_texture[1],
                part_chest_1_object_mesh = "character_body_blade_keeper_m",
                part_chest_1_object_texture = body_casual_ma_texture[1],
                cape = false,
                hairInfo = CostumeHair.hairsM[6],
                eye_texture_id = 7,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.94f, 0.84f, 0.6f),
                costumeId = 16
            };
            costume[21] = new HeroCostume
            {
                name = "marco",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_ma_texture[2],
                cape = false,
                hairInfo = CostumeHair.hairsM[8],
                eye_texture_id = 8,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.295f, 0.295f, 0.275f),
                division = DIVISION.TraineesSquad,
                costumeId = 17
            };
            costume[22] = new HeroCostume
            {
                name = "marco",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.CasualA,
                body_texture = body_casual_ma_texture[2],
                cape = false,
                hairInfo = CostumeHair.hairsM[8],
                eye_texture_id = 8,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.295f, 0.295f, 0.275f),
                costumeId = 18
            };
            costume[23] = new HeroCostume
            {
                name = "mike",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformB,
                body_texture = body_uniform_mb_texture[3],
                cape = true,
                hairInfo = CostumeHair.hairsM[9],
                eye_texture_id = 9,
                beard_texture_id = 0x20,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.94f, 0.84f, 0.6f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 19
            };
            costume[24] = new HeroCostume
            {
                name = "mike",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.CasualB,
                body_texture = body_casual_mb_texture[3],
                part_chest_1_object_mesh = "character_body_blade_keeper_m",
                part_chest_1_object_texture = body_casual_mb_texture[3],
                cape = false,
                hairInfo = CostumeHair.hairsM[9],
                eye_texture_id = 9,
                beard_texture_id = 0x20,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.94f, 0.84f, 0.6f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 20
            };
            costume[25] = new HeroCostume
            {
                name = "connie",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformB,
                body_texture = body_uniform_mb_texture[2],
                cape = true,
                hairInfo = CostumeHair.hairsM[10],
                eye_texture_id = 10,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                division = DIVISION.TheSurveryCorps,
                costumeId = 21
            };
            costume[26] = new HeroCostume
            {
                name = "connie",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformB,
                body_texture = body_uniform_mb_texture[2],
                cape = false,
                hairInfo = CostumeHair.hairsM[10],
                eye_texture_id = 10,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                division = DIVISION.TraineesSquad,
                costumeId = 21
            };
            costume[27] = new HeroCostume
            {
                name = "connie",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.CasualB,
                body_texture = body_casual_mb_texture[2],
                part_chest_1_object_mesh = "character_body_blade_keeper_m",
                part_chest_1_object_texture = body_casual_mb_texture[2],
                cape = false,
                hairInfo = CostumeHair.hairsM[10],
                eye_texture_id = 10,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                costumeId = 22
            };
            costume[28] = new HeroCostume
            {
                name = "armin",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_ma_texture[0],
                cape = true,
                hairInfo = CostumeHair.hairsM[5],
                eye_texture_id = 11,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.95f, 0.8f, 0.5f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 23
            };
            costume[29] = new HeroCostume
            {
                name = "armin",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_ma_texture[0],
                cape = false,
                hairInfo = CostumeHair.hairsM[5],
                eye_texture_id = 11,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.95f, 0.8f, 0.5f),
                division = DIVISION.TraineesSquad,
                costumeId = 23
            };
            costume[30] = new HeroCostume
            {
                name = "armin",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.CasualA,
                body_texture = body_casual_ma_texture[0],
                part_chest_1_object_mesh = "character_body_blade_keeper_m",
                part_chest_1_object_texture = body_casual_ma_texture[0],
                cape = false,
                hairInfo = CostumeHair.hairsM[5],
                eye_texture_id = 11,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.95f, 0.8f, 0.5f),
                costumeId = 24
            };
            costume[31] = new HeroCostume
            {
                name = "petra",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_fa_texture[0],
                cape = true,
                hairInfo = CostumeHair.hairsF[8],
                eye_texture_id = 27,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(1f, 0.725f, 0.376f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 9
            };
            costume[32] = new HeroCostume
            {
                name = "petra",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.CasualA,
                body_texture = body_casual_fa_texture[0],
                part_chest_1_object_mesh = "character_body_blade_keeper_f",
                part_chest_1_object_texture = body_casual_fa_texture[0],
                cape = false,
                hairInfo = CostumeHair.hairsF[8],
                eye_texture_id = 27,
                beard_texture_id = -1,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(1f, 0.725f, 0.376f),
                division = DIVISION.TheSurveryCorps,
                costumeId = 10
            };
            costume[33] = new HeroCostume
            {
                name = "custom",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.CasualB,
                part_chest_skinned_cloth_mesh = "mikasa_asset_cas",
                part_chest_skinned_cloth_texture = body_casual_fb_texture[1],
                part_chest_1_object_mesh = "character_body_blade_keeper_f",
                part_chest_1_object_texture = body_casual_fb_texture[1],
                body_texture = body_casual_fb_texture[1],
                cape = false,
                hairInfo = CostumeHair.hairsF[2],
                eye_texture_id = 12,
                beard_texture_id = 33,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.15f, 0.15f, 0.145f),
                costumeId = 4
            };
            costume[34] = new HeroCostume
            {
                name = "custom",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.CasualA,
                body_texture = body_casual_ma_texture[0],
                part_chest_1_object_mesh = "character_body_blade_keeper_m",
                part_chest_1_object_texture = body_casual_ma_texture[0],
                cape = false,
                hairInfo = CostumeHair.hairsM[3],
                eye_texture_id = 26,
                beard_texture_id = 44,
                glass_texture_id = -1,
                skin_color = 1,
                hair_color = new Color(0.41f, 1f, 0f),
                costumeId = 24
            };
            costume[35] = new HeroCostume
            {
                name = "custom",
                sex = SEX.FEMALE,
                uniform_type = UNIFORM_TYPE.UniformA,
                body_texture = body_uniform_fa_texture[1],
                cape = false,
                hairInfo = CostumeHair.hairsF[4],
                eye_texture_id = 22,
                beard_texture_id = 33,
                glass_texture_id = 56,
                skin_color = 1,
                hair_color = new Color(0f, 1f, 0.874f),
                costumeId = 5
            };
            costume[36] = new HeroCostume
            {
                name = "feng",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.CasualB,
                body_texture = body_casual_mb_texture[3],
                part_chest_1_object_mesh = "character_body_blade_keeper_m",
                part_chest_1_object_texture = body_casual_mb_texture[3],
                cape = true,
                hairInfo = CostumeHair.hairsM[10],
                eye_texture_id = 25,
                beard_texture_id = 39,
                glass_texture_id = 53,
                skin_color = 1,
                division = DIVISION.TheSurveryCorps,
                costumeId = 20
            };
            costume[37] = new HeroCostume
            {
                name = "AHSS",
                sex = SEX.MALE,
                uniform_type = UNIFORM_TYPE.CasualAHSS,
                body_texture = body_casual_ma_texture[0] + "_ahss",
                cape = false,
                hairInfo = CostumeHair.hairsM[6],
                eye_texture_id = 25,
                beard_texture_id = 39,
                glass_texture_id = 53,
                skin_color = 3,
                division = DIVISION.TheMilitaryPolice,
                costumeId = 25
            };
            for (var i = 0; i < costume.Length; i++)
            {
                costume[i].stat = HeroStat.getInfo("CUSTOM_DEFAULT");
                costume[i].id = i;
                costume[i].setMesh();
                costume[i].setTexture();
            }
            costumeOption = new[]
            { 
                costume[0], costume[2], costume[3], costume[4], costume[5], costume[11], costume[13], costume[14], costume[15],
                costume[16], costume[17], costume[6], costume[7], costume[8], costume[10], costume[18], costume[19], costume[21],
                costume[22], costume[23], costume[24], costume[25], costume[27], costume[28], costume[30], costume[37]
             };
        }
    }

    public void setBodyByCostumeId(int id = -1)
    {
        if (id == -1)
            id = costumeId;
        costumeId = id;
        arm_l_mesh = costumeOption[id].arm_l_mesh;
        arm_r_mesh = costumeOption[id].arm_r_mesh;
        body_mesh = costumeOption[id].body_mesh;
        body_texture = costumeOption[id].body_texture;
        uniform_type = costumeOption[id].uniform_type;
        part_chest_1_object_mesh = costumeOption[id].part_chest_1_object_mesh;
        part_chest_1_object_texture = costumeOption[id].part_chest_1_object_texture;
        part_chest_object_mesh = costumeOption[id].part_chest_object_mesh;
        part_chest_object_texture = costumeOption[id].part_chest_object_texture;
        part_chest_skinned_cloth_mesh = costumeOption[id].part_chest_skinned_cloth_mesh;
        part_chest_skinned_cloth_texture = costumeOption[id].part_chest_skinned_cloth_texture;
    }

    public void setCape()
    {
        cape_mesh = cape ? "character_cape" : string.Empty;
    }

    public void setMesh()
    {
        brand1_mesh = string.Empty;
        brand2_mesh = string.Empty;
        brand3_mesh = string.Empty;
        brand4_mesh = string.Empty;
        hand_l_mesh = "character_hand_l";
        hand_r_mesh = "character_hand_r";
        mesh_3dmg = "character_3dmg";
        mesh_3dmg_belt = "character_3dmg_belt";
        mesh_3dmg_gas_l = "character_3dmg_gas_l";
        mesh_3dmg_gas_r = "character_3dmg_gas_r";
        weapon_l_mesh = "character_blade_l";
        weapon_r_mesh = "character_blade_r";
        switch (uniform_type)
        {
            case UNIFORM_TYPE.CasualAHSS:
                hand_l_mesh = "character_hand_l_ah";
                hand_r_mesh = "character_hand_r_ah";
                arm_l_mesh = "character_arm_casual_l_ah";
                arm_r_mesh = "character_arm_casual_r_ah";
                body_mesh = "character_body_casual_MA";
                mesh_3dmg = "character_3dmg_2";
                mesh_3dmg_belt = string.Empty;
                mesh_3dmg_gas_l = "character_gun_mag_l";
                mesh_3dmg_gas_r = "character_gun_mag_r";
                weapon_l_mesh = "character_gun_l";
                weapon_r_mesh = "character_gun_r";
                break;
            case UNIFORM_TYPE.UniformA:
                arm_l_mesh = "character_arm_uniform_l";
                arm_r_mesh = "character_arm_uniform_r";
                brand1_mesh = "character_brand_arm_l";
                brand2_mesh = "character_brand_arm_r";
                if (sex == SEX.FEMALE)
                {
                    body_mesh = "character_body_uniform_FA";
                    brand3_mesh = "character_brand_chest_f";
                    brand4_mesh = "character_brand_back_f";
                }
                else
                {
                    body_mesh = "character_body_uniform_MA";
                    brand3_mesh = "character_brand_chest_m";
                    brand4_mesh = "character_brand_back_m";
                }
                break;
            case UNIFORM_TYPE.UniformB:
                arm_l_mesh = "character_arm_uniform_l";
                arm_r_mesh = "character_arm_uniform_r";
                brand1_mesh = "character_brand_arm_l";
                brand2_mesh = "character_brand_arm_r";
                if (sex == SEX.FEMALE)
                {
                    body_mesh = "character_body_uniform_FB";
                    brand3_mesh = "character_brand_chest_f";
                    brand4_mesh = "character_brand_back_f";
                }
                else
                {
                    body_mesh = "character_body_uniform_MB";
                    brand3_mesh = "character_brand_chest_m";
                    brand4_mesh = "character_brand_back_m";
                }
                break;
            case UNIFORM_TYPE.CasualA:
                arm_l_mesh = "character_arm_casual_l";
                arm_r_mesh = "character_arm_casual_r";
                body_mesh = sex == SEX.FEMALE ? "character_body_casual_FA" : "character_body_casual_MA";
                break;
            case UNIFORM_TYPE.CasualB:
                arm_l_mesh = "character_arm_casual_l";
                arm_r_mesh = "character_arm_casual_r";
                body_mesh = sex == SEX.FEMALE ? "character_body_casual_FB" : "character_body_casual_MB";
                break;
        }
        if (hairInfo.hair.Length > 0)
            hair_mesh = hairInfo.hair;
        if (hairInfo.hasCloth)
            hair_1_mesh = hairInfo.hair_1;
        if (eye_texture_id >= 0)
            eye_mesh = "character_eye";
        beard_mesh = beard_texture_id >= 0 ? "character_face" : string.Empty;
        glass_mesh = glass_texture_id >= 0 ? "glass" : string.Empty;
        setCape();
    }

    public void setTexture()
    {
        _3dmg_texture = uniform_type == UNIFORM_TYPE.CasualAHSS ? "aottg_hero_AHSS_3dmg" : "AOTTG_HERO_3DMG";
        face_texture = "aottg_hero_eyes";
        if (division == DIVISION.TheMilitaryPolice)
            brand_texture = "aottg_hero_brand_mp";
        if (division == DIVISION.TheGarrison)
            brand_texture = "aottg_hero_brand_g";
        if (division == DIVISION.TheSurveryCorps)
            brand_texture = "aottg_hero_brand_sc";
        if (division == DIVISION.TraineesSquad)
            brand_texture = "aottg_hero_brand_ts";
        if (skin_color == 1)
            skin_texture = "aottg_hero_skin_1";
        else if (skin_color == 2)
            skin_texture = "aottg_hero_skin_2";
        else if (skin_color == 3)
            skin_texture = "aottg_hero_skin_3";
    }
}