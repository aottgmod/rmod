using System;
using System.Collections.Generic;
using System.Linq;
using NSpeex;
using UnityEngine;

namespace VoiceChat1
{
    [RequireComponent(typeof(AudioSource))]
    public class VoiceChatPlayer : MonoBehaviour
    {
        public event Action PlayerStarted;

        private float lastTime;
        private double played;
        private double received;
        private int index;
        private float[] data;
        private float playDelay;
        private bool shouldPlay;
        private float lastRecvTime;
        private SpeexDecoder speexDec = new SpeexDecoder(BandMode.Narrow);

        [SerializeField]
        [Range(.1f, 5f)]
        private float playbackDelay = .5f;

        [SerializeField]
        [Range(1, 32)]
        private int _packetBufferSize = 10;

        private SortedList<ulong, VoiceChatPacket> packetsToPlay = new SortedList<ulong, VoiceChatPacket>();

        public float LastRecvTime
        {
            get { return lastRecvTime; }
        }

        public int packetBufferSize
        {
            get { return _packetBufferSize; }
            set { _packetBufferSize = value; }
        }

        private void Start()
        {
            var size = VoiceChatSettings.Instance.Frequency * 10;

            GetComponent<AudioSource>().loop = true;
            GetComponent<AudioSource>().clip = AudioClip.Create("VoiceChat", size, 1, VoiceChatSettings.Instance.Frequency, true, false);
            data = new float[size];

            if (VoiceChatSettings.Instance.LocalDebug)
                VoiceChatRecorder.Instance.NewSample += OnNewSample;

            if (PlayerStarted != null)
                PlayerStarted();
        }

        private void Update()
        {
            if (GetComponent<AudioSource>().isPlaying)
            {
                // Wrapped around
                if (lastTime > GetComponent<AudioSource>().time)
                    played += GetComponent<AudioSource>().clip.length;

                lastTime = GetComponent<AudioSource>().time;

                // Check if we've played to far
                if (played + GetComponent<AudioSource>().time >= received)
                {
                    Stop();
                    shouldPlay = false;
                }
            }
            else
            {
                if (shouldPlay)
                {
                    playDelay -= Time.deltaTime;

                    if (playDelay <= 0)
                        GetComponent<AudioSource>().Play();
                }
            }
        }

        private void Stop()
        {
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().time = 0;
            index = 0;
            played = 0;
            received = 0;
            lastTime = 0;
        }

        public void OnNewSample(VoiceChatPacket newPacket)
        {
            // Set last time we got something
            lastRecvTime = Time.time;
            
            //packetsToPlay.Add(newPacket.PacketId, newPacket);
            packetsToPlay.Add(FengGameManagerMKII.packID++, newPacket);
            InRoomChat.AddLine("POST NEW SAMPLE");

            if (packetsToPlay.Count < 10)
                return;

            var pair = packetsToPlay.First();
            var packet = pair.Value;
            packetsToPlay.Remove(pair.Key);

            InRoomChat.AddLine("DECOMPRESS");
            // Decompress
            float[] sample = null;
            var length = VoiceChatUtils.Decompress(speexDec, packet, out sample);

            InRoomChat.AddLine("ADD TIME");
            // Add more time to received
            received += VoiceChatSettings.Instance.SampleTime;

            InRoomChat.AddLine("PUSH BUFFER");
            // Push data to buffer
            Array.Copy(sample, 0, data, index, length);

            InRoomChat.AddLine("INC IDX");
            // Increase index
            index += length;

            // Handle wrap-around
            if (index >= GetComponent<AudioSource>().clip.samples)
                index = 0;

            // Set data
            GetComponent<AudioSource>().clip.SetData(data, 0);

            // If we're not playing
            if (!GetComponent<AudioSource>().isPlaying)
            {
                // Set that we should be playing
                shouldPlay = true;

                // And if we have no delay set, set it.
                if (playDelay <= 0)
                    playDelay = (float)VoiceChatSettings.Instance.SampleTime * playbackDelay;
            }

            VoiceChatFloatPool.Instance.Return(sample);
        }
    }
}