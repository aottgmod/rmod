namespace VoiceChat1
{
    public enum VoiceChatCompression : byte
    {
        /*
        Raw, 
        RawZlib, 
        */
        Alaw,
        AlawZlib,
        Speex
    }
}