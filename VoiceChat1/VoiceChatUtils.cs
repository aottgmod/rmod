using System;
using System.IO;
using Ionic.Zlib;
using NAudio.Codecs;
using NSpeex;
using UnityEngine;

namespace VoiceChat1
{
    public static class VoiceChatUtils
    {
        private static void ToShortArray(this float[] input, short[] output)
        {
            if (output.Length < input.Length)
                throw new ArgumentException("in: " + input.Length + ", out: " + output.Length);

            for (var i = 0; i < input.Length; ++i)
                output[i] = (short)Mathf.Clamp((int)(input[i] * 32767.0f), short.MinValue, short.MaxValue);
        }

        private static void ToFloatArray(this short[] input, float[] output, int length)
        {
            if ((output.Length < length) || (input.Length < length))
                throw new ArgumentException();

            for (var i = 0; i < length; ++i)
                output[i] = input[i] / (float)short.MaxValue;
        }

        private static byte[] ZlibCompress(byte[] input, int length)
        {
            using (var ms = new MemoryStream())
            {
                using (var compressor = new ZlibStream(ms, CompressionMode.Compress, CompressionLevel.BestCompression))
                {
                    compressor.Write(input, 0, length);
                }

                return ms.ToArray();
            }
        }

        private static byte[] ZlibDecompress(byte[] input, int length)
        {
            using (var ms = new MemoryStream())
            {
                using (var compressor = new ZlibStream(ms, CompressionMode.Decompress, CompressionLevel.BestCompression))
                {
                    compressor.Write(input, 0, length);
                }

                return ms.ToArray();
            }
        }

        private static byte[] ALawCompress(float[] input)
        {
            var output = VoiceChatBytePool.Instance.Get();

            for (var i = 0; i < input.Length; ++i)
            {
                var scaled = (int)(input[i] * 32767.0f);
                var clamped = (short)Mathf.Clamp(scaled, short.MinValue, short.MaxValue);
                output[i] = ALawEncoder.LinearToALawSample(clamped);
            }

            return output;
        }

        private static float[] ALawDecompress(byte[] input, int length)
        {
            var output = VoiceChatFloatPool.Instance.Get();

            for (var i = 0; i < length; ++i)
            {
                var alaw = ALawDecoder.ALawToLinearSample(input[i]);
                output[i] = alaw / (float)short.MaxValue;
            }

            return output;
        }

        private static SpeexEncoder speexEnc = new SpeexEncoder(BandMode.Narrow);

        private static byte[] SpeexCompress(float[] input, out int length)
        {
            var shortBuffer = VoiceChatShortPool.Instance.Get();
            var encoded = VoiceChatBytePool.Instance.Get();
            input.ToShortArray(shortBuffer);
            length = speexEnc.Encode(shortBuffer, 0, input.Length, encoded, 0, encoded.Length);
            VoiceChatShortPool.Instance.Return(shortBuffer);
            return encoded;
        }

        private static float[] SpeexDecompress(SpeexDecoder speexDec, byte[] data, int dataLength)
        {
            var decoded = VoiceChatFloatPool.Instance.Get();
            var shortBuffer = VoiceChatShortPool.Instance.Get();
            speexDec.Decode(data, 0, dataLength, shortBuffer, 0, false);
            shortBuffer.ToFloatArray(decoded, shortBuffer.Length);
            VoiceChatShortPool.Instance.Return(shortBuffer);
            return decoded;
        }

        public static VoiceChatPacket Compress(float[] sample)
        {
            var packet = new VoiceChatPacket();
            packet.Compression = VoiceChatSettings.Instance.Compression;

            switch (packet.Compression)
            {
                /*
                case VoiceChatCompression.Raw:
                    {
                        short[] buffer = VoiceChatShortPool.Instance.Get();

                        packet.Length = sample.Length * 2;
                        sample.ToShortArray(shortBuffer);
                        Buffer.BlockCopy(shortBuffer, 0, byteBuffer, 0, packet.Length);
                    }
                    break;

                case VoiceChatCompression.RawZlib:
                    {
                        packet.Length = sample.Length * 2;
                        sample.ToShortArray(shortBuffer);
                        Buffer.BlockCopy(shortBuffer, 0, byteBuffer, 0, packet.Length);

                        packet.Data = ZlibCompress(byteBuffer, packet.Length);
                        packet.Length = packet.Data.Length;
                    }
                    break;
                */

                case VoiceChatCompression.Alaw:
                {
                    packet.Length = sample.Length;
                    packet.Data = ALawCompress(sample);
                }
                    break;

                case VoiceChatCompression.AlawZlib:
                {
                    var alaw = ALawCompress(sample);
                    packet.Data = ZlibCompress(alaw, sample.Length);
                    packet.Length = packet.Data.Length;

                    VoiceChatBytePool.Instance.Return(alaw);
                }
                    break;

                case VoiceChatCompression.Speex:
                {
                    packet.Data = SpeexCompress(sample, out packet.Length);
                }
                    break;
            }

            return packet;
        }

        public static int Decompress(VoiceChatPacket packet, out float[] data)
        {
            return Decompress(null, packet, out data);
        }

        public static int Decompress(SpeexDecoder speexDecoder, VoiceChatPacket packet, out float[] data)
        {
            switch (packet.Compression)
            {
                /*
                case VoiceChatCompression.Raw:
                    {
                        short[9 buffer 

                        Buffer.BlockCopy(packet.Data, 0, shortBuffer, 0, packet.Length);
                        shortBuffer.ToFloatArray(data, packet.Length / 2);
                        return packet.Length / 2;
                    }

                case VoiceChatCompression.RawZlib:
                    {
                        byte[] unzipedData = ZlibDecompress(packet.Data, packet.Length);

                        Buffer.BlockCopy(unzipedData, 0, shortBuffer, 0, unzipedData.Length);
                        shortBuffer.ToFloatArray(data, unzipedData.Length / 2);
                        return unzipedData.Length / 2;
                    }
                */

                case VoiceChatCompression.Speex:
                {
                    data = SpeexDecompress(speexDecoder, packet.Data, packet.Length);
                    return data.Length;
                }

                case VoiceChatCompression.Alaw:
                {
                    data = ALawDecompress(packet.Data, packet.Length);
                    return packet.Length;
                }

                case VoiceChatCompression.AlawZlib:
                {
                    var alaw = ZlibDecompress(packet.Data, packet.Length);
                    data = ALawDecompress(alaw, alaw.Length);
                    return alaw.Length;
                }
            }

            data = new float[0];
            return 0;
        }

        public static int ClosestPowerOfTwo(int value)
        {
            var i = 1;

            while (i < value)
                i <<= 1;

            return i;
        }
    }
}