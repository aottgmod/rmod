using System;
using Exocortex.DSP;
using mItems;
using UnityEngine;

namespace VoiceChat1
{
    public class VoiceChatRecorder : MonoBehaviour
    {
        #region Instance

        private static VoiceChatRecorder instance;

        public static VoiceChatRecorder Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType(typeof(VoiceChatRecorder)) as VoiceChatRecorder;

                return instance;
            }
        }

        public AudioClip audioClip
        {
            get { return clip; }
        }

        #endregion

        public event Action StartedRecording;

        [SerializeField]
        private KeyCode toggleToTalkKey = KeyCode.O;

        [SerializeField]
        private KeyCode pushToTalkKey = KeyCode.U;

        [SerializeField]
        private bool autoDetectSpeaking;

        [SerializeField]
        private int autoDetectIndex = 4;

        [SerializeField]
        private float forceTransmitTime = 2f;

        private ulong packetId;
        private int previousPosition;
        private int sampleIndex;
        private string device;
        private AudioClip clip;
        private bool transmitToggled;
        private bool recording;
        private float forceTransmit;
        private int recordFrequency;
        private int recordSampleSize;
        private int targetFrequency;
        private int targetSampleSize;
        private float[] fftBuffer;
        private float[] sampleBuffer;
        private VoiceChatCircularBuffer<float[]> previousSampleBuffer = new VoiceChatCircularBuffer<float[]>(5);

        public KeyCode PushToTalkKey
        {
            get { return pushToTalkKey; }
            set { pushToTalkKey = value; }
        }

        public KeyCode ToggleToTalkKey
        {
            get { return toggleToTalkKey; }
            set { toggleToTalkKey = value; }
        }

        public bool AutoDetectSpeech
        {
            get { return autoDetectSpeaking; }
            set { autoDetectSpeaking = value; }
        }

        public int NetworkId { get; set; }

        public string Device
        {
            get { return device; }
            set
            {
                if ((value != null) && !mExtensions.Contains(Microphone.devices, value))
                {
                    Debug.LogError(value + " is not a valid microphone device");
                    return;
                }

                device = value;
            }
        }

        public bool HasDefaultDevice
        {
            get { return device == null; }
        }

        public bool HasSpecificDevice
        {
            get { return device != null; }
        }

        public bool IsTransmitting
        {
            get { return transmitToggled || (forceTransmit > 0) || Input.GetKey(pushToTalkKey); }
        }

        public bool IsRecording
        {
            get { return recording; }
        }

        public string[] AvailableDevices
        {
            get { return Microphone.devices; }
        }

        public event Action<VoiceChatPacket> NewSample;

        private void Start()
        {
            if ((instance != null) && (instance != this))
            {
                Destroy(this);
                Debug.LogError("Only one instance of VoiceChatRecorder can exist");
                return;
            }

            Application.RequestUserAuthorization(UserAuthorization.Microphone);
            instance = this;
        }

        private void OnEnable()
        {
            if ((instance != null) && (instance != this))
            {
                Destroy(this);
                Debug.LogError("Only one instance of VoiceChatRecorder can exist");
                return;
            }

            Application.RequestUserAuthorization(UserAuthorization.Microphone);
            instance = this;
        }

        private void OnDisable()
        {
            if (instance == this)
                instance = null;
        }

        private void OnDestroy()
        {
            if (instance == this)
                instance = null;
        }

        private void Update()
        {
            if (!recording)
                return;

            forceTransmit -= Time.deltaTime;

            if (Input.GetKeyUp(toggleToTalkKey))
                transmitToggled = !transmitToggled;

            var transmit = transmitToggled || Input.GetKey(pushToTalkKey);
            var currentPosition = Microphone.GetPosition(Device);

            // This means we wrapped around
            if (currentPosition < previousPosition)
            {
                while (sampleIndex < recordFrequency)
                    ReadSample(transmit);

                sampleIndex = 0;
            }

            // Read non-wrapped samples
            previousPosition = currentPosition;

            while (sampleIndex + recordSampleSize <= currentPosition)
                ReadSample(transmit);
        }

        private void Resample(float[] src, float[] dst)
        {
            if (src.Length == dst.Length)
                Array.Copy(src, 0, dst, 0, src.Length);
            else
            {
                //TODO: Low-pass filter 
                var rec = 1.0f / dst.Length;

                for (var i = 0; i < dst.Length; ++i)
                {
                    var interp = rec * i * src.Length;
                    dst[i] = src[(int)interp];
                }
            }
        }

        private void ReadSample(bool transmit)
        {
            // Extract data
            clip.GetData(sampleBuffer, sampleIndex);

            // Grab a new sample buffer
            var targetSampleBuffer = VoiceChatFloatPool.Instance.Get();

            // Resample our real sample into the buffer
            Resample(sampleBuffer, targetSampleBuffer);

            // Forward index
            sampleIndex += recordSampleSize;

            // Highest auto-detected frequency
            var freq = float.MinValue;
            var index = -1;

            // Auto detect speech, but no need to do if we're pushing a key to transmit
            if (autoDetectSpeaking && !transmit)
            {
                // Clear FFT buffer
                for (var i = 0; i < fftBuffer.Length; ++i)
                    fftBuffer[i] = 0;

                // Copy to FFT buffer
                Array.Copy(targetSampleBuffer, 0, fftBuffer, 0, targetSampleBuffer.Length);

                // Apply FFT
                Fourier.FFT(fftBuffer, fftBuffer.Length / 2, FourierDirection.Forward);

                // Get highest frequency
                for (var i = 0; i < fftBuffer.Length; ++i)
                {
                    if (fftBuffer[i] > freq)
                    {
                        freq = fftBuffer[i];
                        index = i;
                    }
                }
            }

            // If we have an event, and 
            if ((NewSample != null) && (transmit || (forceTransmit > 0) || (index >= autoDetectIndex)))
            {
                // If we auto-detected a voice, force recording for a while
                if (index >= autoDetectIndex)
                {
                    if (forceTransmit <= 0)
                    {
                        while (previousSampleBuffer.Count > 0)
                            TransmitBuffer(previousSampleBuffer.Remove());
                    }

                    forceTransmit = forceTransmitTime;
                }

                TransmitBuffer(targetSampleBuffer);
            }
            else
            {
                if (previousSampleBuffer.Count == previousSampleBuffer.Capacity)
                    VoiceChatFloatPool.Instance.Return(previousSampleBuffer.Remove());

                previousSampleBuffer.Add(targetSampleBuffer);
            }
        }

        private void TransmitBuffer(float[] buffer)
        {
            // Compress into packet
            var packet = VoiceChatUtils.Compress(buffer);

            // Set networkid of packet
            packet.NetworkId = NetworkId;
            packet.PacketId = ++packetId;
            // Raise event
            NewSample(packet);
        }

        public bool StartRecording()
        {
            if ((NetworkId == 0) && !VoiceChatSettings.Instance.LocalDebug)
            {
                Debug.LogError("NetworkId is not set");
                return false;
            }

            if (recording)
            {
                Debug.LogError("Already recording");
                return false;
            }

            targetFrequency = VoiceChatSettings.Instance.Frequency;
            targetSampleSize = VoiceChatSettings.Instance.SampleSize;

            int minFreq;
            int maxFreq;
            Microphone.GetDeviceCaps(Device, out minFreq, out maxFreq);

            recordFrequency = (minFreq == 0) && (maxFreq == 0) ? 44100 : maxFreq;
            recordSampleSize = recordFrequency / (targetFrequency / targetSampleSize);

            clip = Microphone.Start(Device, true, 1, recordFrequency);
            sampleBuffer = new float[recordSampleSize];
            fftBuffer = new float[VoiceChatUtils.ClosestPowerOfTwo(targetSampleSize)];
            recording = true;

            if (StartedRecording != null)
                StartedRecording();

            return recording;
        }

        public void StopRecording()
        {
            clip = null;
            recording = false;
        }

        public void ToggleTransmit()
        {
            transmitToggled = !transmitToggled;
        }

        public void StartTransmit()
        {
            transmitToggled = true;
        }

        public void StopTransmit()
        {
            transmitToggled = false;
        }
    }
}