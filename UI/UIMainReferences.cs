using mItems;
using UnityEngine;

public class UIMainReferences : MonoBehaviour
{
    private static bool isGAMEFirstLaunch = true;
    public GameObject panelCredits;
    public GameObject PanelDisconnect;
    public GameObject panelMain;
    public GameObject PanelMultiJoinPrivate;
    public GameObject PanelMultiPWD;
    public GameObject panelMultiROOM;
    public GameObject panelMultiSet;
    public GameObject panelMultiStart;
    public GameObject PanelMultiWait;
    public GameObject panelOption;
    public GameObject panelSingleSet;
    public GameObject PanelSnapShot;
    public static string version = "01042015";

    private void Start()
    {
        NGUITools.SetActive(panelMain, true);
        CacheGameObject.Find<UILabel>("VERSION").text = version;
        if (isGAMEFirstLaunch)
        {
            isGAMEFirstLaunch = false;
            var target = (GameObject) Instantiate(CacheResources.Load("InputManagerController"));
            target.name = "InputManagerController";
            DontDestroyOnLoad(target);
        }
    }
}