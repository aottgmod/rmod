using UnityEngine;

public class LevelTriggerGas : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if (!other.gameObject.CompareTag("Player"))
            return;
        if (!IN_GAME_MAIN_CAMERA.IsSingleplayer && !other.gameObject.GetComponent<HERO>().photonView.isMine)
            return;
        other.gameObject.GetComponent<HERO>().fillGas();
        Destroy(gameObject);
    }

    private void Start() { }
}