using mItems;
using UnityEngine;
using MonoBehaviour = Photon.MonoBehaviour;

public class TITAN_SETUP : MonoBehaviour
{
    public GameObject eye;
    private CostumeHair hair;
    private GameObject hair_go_ref;
    private int hairType;
    private GameObject part_hair;

    private void Awake()
    {
        CostumeHair.init();
        CharacterMaterials.init();
        HeroCostume.init();
        hair_go_ref = new GameObject();
        eye.transform.parent = transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head").transform;
        hair_go_ref.transform.position = eye.transform.position + Vector3.up * 3.5f + transform.forward * 5.2f;
        hair_go_ref.transform.rotation = eye.transform.rotation;
        hair_go_ref.transform.RotateAround(eye.transform.position, transform.right, -20f);
        hair_go_ref.transform.localScale = new Vector3(210f, 210f, 210f);
        hair_go_ref.transform.parent = transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head").transform;
    }

    public void setFacialTexture(GameObject go, int id)
    {
        if (id >= 0)
        {
            var num = 0.25f;
            var num2 = 0.125f;
            var x = num2 * (int) (id / 8f);
            var y = -num * (id % 4);
            go.renderer.material.mainTextureOffset = new Vector2(x, y);
        }
    }

    public void setHair()
    {
        Destroy(part_hair);
        var index = Random.Range(0, CostumeHair.hairsM.Length);
        if (index == 3)
            index = 9;
        hairType = index;
        hair = CostumeHair.hairsM[index];
        if (hair.hair == string.Empty)
        {
            hair = CostumeHair.hairsM[9];
            hairType = 9;
        }
        part_hair = (GameObject) Instantiate(CacheResources.Load("Character/" + hair.hair));
        part_hair.transform.parent = hair_go_ref.transform.parent;
        part_hair.transform.position = hair_go_ref.transform.position;
        part_hair.transform.rotation = hair_go_ref.transform.rotation;
        part_hair.transform.localScale = hair_go_ref.transform.localScale;
        part_hair.renderer.material = CharacterMaterials.materials[hair.texture];
        part_hair.renderer.material.color = HeroCostume.costume[Random.Range(0, HeroCostume.costume.Length - 5)].hair_color;
        var id = Random.Range(1, 8);
        setFacialTexture(eye, id);
        if ((IN_GAME_MAIN_CAMERA.IsMultiplayer) && photonView.isMine)
        {
            var parameters = new object[] { hairType, id, part_hair.renderer.material.color.r, part_hair.renderer.material.color.g, part_hair.renderer.material.color.b };
            photonView.RPC("setHairPRC", PhotonTargets.OthersBuffered, parameters);
        }
    }

    [RPC]
    private void setHairPRC(int type, int eye_type, float c1, float c2, float c3)
    {
        Destroy(part_hair);
        hair = CostumeHair.hairsM[type];
        hairType = type;
        if (hair.hair != string.Empty)
        {
            var obj2 = (GameObject) Instantiate(CacheResources.Load("Character/" + hair.hair));
            obj2.transform.parent = hair_go_ref.transform.parent;
            obj2.transform.position = hair_go_ref.transform.position;
            obj2.transform.rotation = hair_go_ref.transform.rotation;
            obj2.transform.localScale = hair_go_ref.transform.localScale;
            obj2.renderer.material = CharacterMaterials.materials[hair.texture];
            obj2.renderer.material.color = new Color(c1, c2, c3);
            part_hair = obj2;
        }
        setFacialTexture(eye, eye_type);
    }

    public void setPunkHair()
    {
        Destroy(part_hair);
        hair = CostumeHair.hairsM[3];
        hairType = 3;
        var obj2 = (GameObject) Instantiate(CacheResources.Load("Character/" + hair.hair));
        obj2.transform.parent = hair_go_ref.transform.parent;
        obj2.transform.position = hair_go_ref.transform.position;
        obj2.transform.rotation = hair_go_ref.transform.rotation;
        obj2.transform.localScale = hair_go_ref.transform.localScale;
        obj2.renderer.material = CharacterMaterials.materials[hair.texture];
        switch (Random.Range(1, 4))
        {
            case 1:
                obj2.renderer.material.color = FengColor.hairPunk1;
                break;

            case 2:
                obj2.renderer.material.color = FengColor.hairPunk2;
                break;

            case 3:
                obj2.renderer.material.color = FengColor.hairPunk3;
                break;
        }
        part_hair = obj2;
        setFacialTexture(eye, 0);
        if ((IN_GAME_MAIN_CAMERA.IsMultiplayer) && photonView.isMine)
        {
            var parameters = new object[] { hairType, 0, part_hair.renderer.material.color.r, part_hair.renderer.material.color.g, part_hair.renderer.material.color.b };
            photonView.RPC("setHairPRC", PhotonTargets.OthersBuffered, parameters);
        }
    }
}