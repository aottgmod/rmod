﻿using UnityEngine;

public class TitanTrigger : MonoBehaviour
{
    //detects if player is near enough to justify colliders
    public bool isCollide;
    private void OnTriggerEnter(Collider other)
    {
        var obj = other.transform.root.gameObject;
        if (obj.layer == 8)
        {
            var myPlayer = Camera.main.GetComponent<IN_GAME_MAIN_CAMERA>().main_object;
            if ((myPlayer != null) && (myPlayer == obj))
                isCollide = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var obj = other.transform.root.gameObject;
        if (obj.layer == 8)
        {
            var myPlayer = Camera.main.GetComponent<IN_GAME_MAIN_CAMERA>().main_object;
            if ((myPlayer != null) && (myPlayer == obj))
                isCollide = false;
        }
    }
}