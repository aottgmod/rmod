using UnityEngine;

public class LevelTriggerRacingEnd : MonoBehaviour
{
    private bool disable;

    private void OnTriggerStay(Collider other)
    {
        if (disable || (!other.gameObject.CompareTag("Player")))
            return;
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
        {
            FengGameManagerMKII.MKII.gameWin();
            disable = true;
        }
        else if (other.gameObject.GetComponent<HERO>().photonView.isMine)
        {
            FengGameManagerMKII.MKII.multiplayerRacingFinsih();
            disable = true;
        }
    }

    private void Start()
    {
        disable = false;
    }
}