using UnityEngine;

public class OnClickInstantiate : MonoBehaviour
{
    public int InstantiateType;
    private readonly string[] InstantiateTypeNames = { "Mine", "Scene" };
    public GameObject Prefab;
    public bool showGui;

    private void OnClick()
    {
        if (PhotonNetwork.connectionStateDetailed != PeerState.Joined)
            return;
        if (InstantiateType == 0)
            PhotonNetwork.Instantiate(Prefab.name, InputToEvent.inputHitPos + new Vector3(0f, 5f, 0f), Quaternion.identity, 0);
        else if (InstantiateType == 1)
            PhotonNetwork.InstantiateSceneObject(Prefab.name, InputToEvent.inputHitPos + new Vector3(0f, 5f, 0f), Quaternion.identity, 0, null);
    }

    private void OnGUI()
    {
        if (!showGui)
            return;
        GUILayout.BeginArea(new Rect(Screen.width - 180, 0f, 180f, 50f));
        InstantiateType = GUILayout.Toolbar(InstantiateType, InstantiateTypeNames);
        GUILayout.EndArea();
    }
}