using UnityEngine;

public class OnClickLoadSomething : MonoBehaviour
{
    public string ResourceToLoad;
    public ResourceTypeOption ResourceTypeToLoad;

    public void OnClick()
    {
        if (ResourceTypeToLoad == ResourceTypeOption.Scene)
            Application.LoadLevel(ResourceToLoad);
        else if (ResourceTypeToLoad == ResourceTypeOption.Web)
            Application.OpenURL(ResourceToLoad);
    }

    public enum ResourceTypeOption : byte
    {
        Scene = 0,
        Web = 1
    }
}