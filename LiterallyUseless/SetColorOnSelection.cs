using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

[RequireComponent(typeof(UIWidget))][AddComponentMenu("NGUI/Examples/Set Color on Selection")][ExecuteInEditMode]
public class SetColorOnSelection : MonoBehaviour
{
    [CompilerGenerated]
    private static Dictionary<string, int> colors;
    private UIWidget mWidget;

    private void OnSelectionChange(string val)
    {
        if (mWidget == null)
            mWidget = GetComponent<UIWidget>();
        var key = val;
        if (key == null)
            return;
        int num;
        if (colors == null)
        {
            colors = new Dictionary<string, int>(7)
            {
                { "White", 0 }, { "Red", 1 }, { "Green", 2 }, { "Blue", 3 }, { "Yellow", 4 }, { "Cyan", 5 }, { "Magenta", 6 }
            };
        }
        if (!colors.TryGetValue(key, out num))
            return;
        switch (num)
        {
            case 0:
                mWidget.color = Color.white;
                break;
            case 1:
                mWidget.color = Color.red;
                break;
            case 2:
                mWidget.color = Color.green;
                break;
            case 3:
                mWidget.color = Color.blue;
                break;
            case 4:
                mWidget.color = Color.yellow;
                break;
            case 5:
                mWidget.color = Color.cyan;
                break;
            case 6:
                mWidget.color = Color.magenta;
                break;
        }
    }
}