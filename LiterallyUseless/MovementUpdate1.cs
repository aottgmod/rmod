using UnityEngine;

public class MovementUpdate1 : MonoBehaviour
{
    public bool disabled;

    private void Start()
    {
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
        {
            disabled = true;
            enabled = false;
        }
        else if (networkView.isMine)
            networkView.RPC("updateMovement1", RPCMode.OthersBuffered, transform.position, transform.rotation, transform.lossyScale);
        else
            enabled = false;
    }

    private void Update()
    {
        if (!disabled)
            networkView.RPC("updateMovement1", RPCMode.Others, transform.position, transform.rotation, transform.lossyScale);
    }

    [RPC]
    private void updateMovement1(Vector3 newPosition, Quaternion newRotation, Vector3 newScale)
    {
        transform.position = newPosition;
        transform.rotation = newRotation;
        transform.localScale = newScale;
    }
}