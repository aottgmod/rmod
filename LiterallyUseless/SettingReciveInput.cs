using mItems;
using UnityEngine;

public class SettingReciveInput : MonoBehaviour
{
    public int id;

    private void OnClick()
    {
        CacheGameObject.Find<FengCustomInputs>("InputManagerController").startListening(id);
        transform.Find("Label").gameObject.GetComponent<UILabel>().text = "*wait for input";
    }

    private void Start() { }

    private void Update() { }
}