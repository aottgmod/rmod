using UnityEngine;

public class MovementUpdate : MonoBehaviour
{
    public bool disabled;
    private Vector3 lastPosition;
    private Quaternion lastRotation;
    private Vector3 lastVelocity;
    private Vector3 targetPosition;

    private void Start()
    {
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
        {
            disabled = true;
            enabled = false;
        }
        else if (networkView.isMine)
            networkView.RPC("updateMovement", RPCMode.OthersBuffered, transform.position, transform.rotation, transform.localScale, Vector3.zero);
        else
            targetPosition = transform.position;
    }

    private void Update()
    {
        if (disabled || (Network.peerType == NetworkPeerType.Disconnected) || (Network.peerType == NetworkPeerType.Connecting))
            return;
        if (networkView.isMine)
        {
            if (Vector3.Distance(transform.position, lastPosition) >= 0.5f)
            {
                lastPosition = transform.position;
                networkView.RPC("updateMovement", RPCMode.Others, transform.position, transform.rotation, transform.localScale, rigidbody.velocity);
            }
            else if (Vector3.Distance(transform.rigidbody.velocity, lastVelocity) >= 0.1f)
            {
                lastVelocity = transform.rigidbody.velocity;
                networkView.RPC("updateMovement", RPCMode.Others, transform.position, transform.rotation, transform.localScale, rigidbody.velocity);
            }
            else if (Quaternion.Angle(transform.rotation, lastRotation) >= 1f)
            {
                lastRotation = transform.rotation;
                networkView.RPC("updateMovement", RPCMode.Others, transform.position, transform.rotation, transform.localScale, rigidbody.velocity);
            }
        }
        else
            transform.position = Vector3.Slerp(transform.position, targetPosition, Time.deltaTime * 2f);
    }

    [RPC]
    private void updateMovement(Vector3 newPosition, Quaternion newRotation, Vector3 newScale, Vector3 veloctiy)
    {
        targetPosition = newPosition;
        transform.rotation = newRotation;
        transform.localScale = newScale;
        rigidbody.velocity = veloctiy;
    }
}