# Code Style Conventions #
 
## Indentation ##
 
* The indent size is 4 spaces. 
 
``` 
#!c#
 
int main() 
{ 
    return 0; 
} 
``` 
 
 
## Spacing ##
 
* Do not place spaces around unary operators. 
``` 
#!c#
 
i++; 
``` 
 
  
 
* **Do** place spaces around binary and ternary operators. 
``` 
#!c#
 
y = m * x + b; 
f(a, b); 
c = a | b; 
return condition ? 1 : 0; 
``` 
 
  
 
* Do not place spaces before comma and semicolon. 
``` 
#!c#
 
for (int i = 0; i < 10; ++i) 
    doSomething(); 
 
f(a, b); 
``` 
 
  
 
* Place spaces between control statements and their parentheses. 
``` 
#!c#
 
if (condition) 
    doIt(); 
``` 
 
  
 
* Do not place spaces between a function and its parentheses, or between a parenthesis and its content. 
``` 
#!c#
 
f(a, b); 
``` 
 
  
 
 
## Line breaking ##
 
* Each statement should get its own line. 
``` 
#!c#
 
x++; 
y++; 
if (condition) 
    doIt(); 
``` 
 
  
 
 
## Braces ##
 
* Function definitions: place each brace on its own line. 
``` 
#!c#
 
int main() 
{ 
    ... 
} 
``` 
 
  
 
* One-line control clauses should not use braces unless comments are included or a single statement spans multiple lines. 
``` 
#!c#
 
if (condition) 
    doSomething(); 
else 
    doSomethingElse(); 
``` 
 
  
 
*  Control clauses without a body should use empty braces: 
``` 
#!c#
 
for ( ; current; current = current->next) { } 
``` 
 
  
 
 
## Null, false and zero ##
 
* Tests for true/false, null/non-null, and zero/non-zero should all be done without equality comparisons. 
``` 
#!c# 
 
if (condition) 
    doIt(); 
 
if (!ptr) 
    return; 
 
if (!count) 
    return; 
``` 
 
  
 
 
## Floating point literals ##
 
* Unless required in order to force floating point math, do not append .0, .f and .0f to floating point literals. 
``` 
#!c#
 
const double duration = 60; 
 
void setDiameter(float diameter) 
{ 
    radius = diameter / 2; 
} 
 
setDiameter(10); 
 
const int framesPerSecond = 12; 
double frameDuration = 1.0 / framesPerSecond; 
``` 
 
  
 
 
## Comments ##
 
* Use only one space before end of line comments and in between sentences in comments. 
``` 
#!c#
 
f(a, b); // This explains why the function call was done. This is another sentence. 
``` 
 
  
 
* Use FIXME: (without attribution) to denote items that need to be addressed in the future. 
``` 
#!c#
 
drawJpg(); // FIXME: Make this code handle jpg in addition to the png support.