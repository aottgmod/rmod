﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

using Modules;
using mItems;
using Season2;

public class FengGameManagerMKII : Photon.MonoBehaviour
{
    public static readonly string applicationId = "f1f6195c-df4a-40f9-bae5-4744c32901ef";
    private ArrayList chatContent;
    public GameObject checkpoint;
    private ArrayList cT;
    private float currentSpeed;
    public int difficulty;
    private bool endRacing;
    private ArrayList eT;
    private ArrayList fT;
    private float gameEndCD;
    private float gameEndTotalCDtime = 9f;
    public bool gameStart;
    private bool gameTimesUp;
    public static ArrayList heroes;
    private int highestwave = 1;
    private ArrayList hooks;
    private int humanScore;
    public FengCustomInputs inputManager;
    private bool isLosing;
    private bool isWinning;
    public bool justSuicide;
    private ArrayList kicklist;
    private ArrayList killInfoGO = new ArrayList();
    public static bool LAN;
    public static string level = string.Empty;
    private string localRacingResult;
    public IN_GAME_MAIN_CAMERA mainCamera;
    private float maxSpeed;
    private string myLastHero;
    private string myLastRespawnTag = "playerRespawn";
    public float myRespawnTime;
    public bool needChooseSide;
    public int PVPhumanScore;
    private int PVPhumanScoreMax = 200;
    public int PVPtitanScore;
    private int PVPtitanScoreMax = 200;
    private ArrayList racingResult;
    public float roundTime;
    private int single_kills;
    private int single_maxDamage;
    private int single_totalDamage;
    private bool startRacing;
    private int[] teamScores;
    private int teamWinner;
    public int time = 600;
    private float timeElapse;
    private float timeTotalServer;
    private static ArrayList titans;
    private int titanScore;
    private GameObject ui;
    public int wave = 1;

    public static int number_of_titans { get { return titans.Count; } }
    public static int number_of_heroes { get { return heroes.Count; } }

    private static GameObject playerPrefab;
    public static ulong packID = 1;
    
    public IDictionary<int, float> chatmessagecd = new Dictionary<int, float>();
    public IDictionary<int, IList<string>> chatmessage = new Dictionary<int, IList<string>>();

    public static FengGameManagerMKII MKII;

    public void SendChat(string content)
    {
        MKII.photonView.RPC("Chat", PhotonTargets.All, content, string.Empty);
    }

    public string getDictionary(string line, string file)
    {
        var dictionary = new Dictionary<string, string>();
        foreach (var str in File.ReadAllLines(file))
        {
            if (!str.StartsWith("!") && str.Contains("|"))
            {
                var strArray = str.Split('|');
                dictionary.Add(strArray[0], strArray[1]);
            }
        }
        return dictionary[line];
    }

    public void addCamera(IN_GAME_MAIN_CAMERA c)
    {
        mainCamera = c;
    }

    public void addCT(COLOSSAL_TITAN titan)
    {
        cT.Add(titan);
    }

    public void addET(TITAN_EREN hero)
    {
        eT.Add(hero);
    }

    public void addFT(FEMALE_TITAN titan)
    {
        fT.Add(titan);
    }

    public void addHero(HERO hero, int id = 0)
    {
        heroes.Add(hero);
    }

    public void addHook(Bullet h)
    {
        hooks.Add(h);
    }

    public void addTitan(TITAN titan, int id = 0)
    {
        titans.Add(titan);

        GlobalItems.titans.Add(new Pair<int, TITAN>(id, titan));
    }

    [RPC]
    private void Chat(string content, string sender, PhotonMessageInfo info)
    {
        if (!sender.IsNullOrEmpty())
        {
            if (!chatmessage.ContainsKey(info.sender.ID))
                chatmessage.Add(info.sender.ID, new List<string>());
            chatmessage[info.sender.ID].Add(content);
            if (!chatmessagecd.ContainsKey(info.sender.ID))
                chatmessagecd.Add(info.sender.ID, Time.time);
            chatmessagecd[info.sender.ID] = Time.time;
        }
        if ((content.Length > 7) && (content.Substring(0, 7) == "/kick #"))
        {
            if (PhotonNetwork.isMasterClient)
                kickPlayer(content.Remove(0, 7), sender);
        }
        else
        {
            if (sender != string.Empty)
                content = "<color=#FF7000>[" + info.sender.ID + "]</color>" + sender + " <color=#C9C3FF>||</color> " + content;
            CacheGameObject.Find<InRoomChat>("Chatroom").addLINE(content);
        }
    }

    private bool checkIsTitanAllDie()
    {
        foreach (var obj2 in GameObject.FindGameObjectsWithTag("titan"))
        {
            if ((obj2.GetComponent<TITAN>() != null) && !obj2.GetComponent<TITAN>().hasDie)
                return false;
            if (obj2.GetComponent<FEMALE_TITAN>() != null)
                return false;
        }
        return true;
    }

    public void checkPVPpts()
    {
        if (PVPtitanScore >= PVPtitanScoreMax)
        {
            PVPtitanScore = PVPtitanScoreMax;
            gameLose();
        }
        else
        {
            if (PVPhumanScore >= PVPhumanScoreMax)
            {
                PVPhumanScore = PVPhumanScoreMax;
                gameWin();
            }
        }
    }

    private void core()
    {
        if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) && needChooseSide)
        {
            if (CacheGameObject.Find<FengCustomInputs>("InputManagerController").isInputDown[InputCode.flare1])
            {
                if (NGUITools.GetActive(ui.GetComponent<UIReferArray>().panels[3]))
                {
                    Screen.lockCursor = true;
                    Screen.showCursor = true;
                    var uiref = ui.GetComponent<UIReferArray>();
                    NGUITools.SetActive(uiref.panels[0], true);
                    NGUITools.SetActive(uiref.panels[1], false);
                    NGUITools.SetActive(uiref.panels[2], false);
                    NGUITools.SetActive(uiref.panels[3], false);
                    CacheGameObject.Find<SpectatorMovement>("MainCamera").disable = false;
                    CacheGameObject.Find<MouseLook>("MainCamera").disable = false;
                }
                else
                {
                    Screen.lockCursor = false;
                    Screen.showCursor = true;
                    var uiref = ui.GetComponent<UIReferArray>();
                    NGUITools.SetActive(uiref.panels[0], false);
                    NGUITools.SetActive(uiref.panels[1], false);
                    NGUITools.SetActive(uiref.panels[2], false);
                    NGUITools.SetActive(uiref.panels[3], true);
                    CacheGameObject.Find<SpectatorMovement>("MainCamera").disable = true;
                    CacheGameObject.Find<MouseLook>("MainCamera").disable = true;
                }
            }
            if (CacheGameObject.Find<FengCustomInputs>("InputManagerController").isInputDown[15] && !NGUITools.GetActive(ui.GetComponent<UIReferArray>().panels[3]))
            {
                var uiref = ui.GetComponent<UIReferArray>();
                NGUITools.SetActive(uiref.panels[0], false);
                NGUITools.SetActive(uiref.panels[1], true);
                NGUITools.SetActive(uiref.panels[2], false);
                NGUITools.SetActive(uiref.panels[3], false);
                Screen.showCursor = true;
                Screen.lockCursor = false;
                CacheGameObject.Find<SpectatorMovement>("MainCamera").disable = true;
                CacheGameObject.Find<MouseLook>("MainCamera").disable = true;
                CacheGameObject.Find<FengCustomInputs>("InputManagerController").showKeyMap();
                CacheGameObject.Find<FengCustomInputs>("InputManagerController").justUPDATEME();
                CacheGameObject.Find<FengCustomInputs>("InputManagerController").menuOn = true;
            }
        }
        if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || (IN_GAME_MAIN_CAMERA.IsMultiplayer))
        {
            if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
            {
                if ((CacheGameObject.Find("MainCamera") != null) && (IN_GAME_MAIN_CAMERA.gamemode != GAMEMODE.RACING) && IN_GAME_MAIN_CAMERA.maincamera.gameOver && !needChooseSide)
                {
                    ShowHUDInfoCenter("Press [F7D358]" + CacheGameObject.Find<FengCustomInputs>("InputManagerController").inputString[InputCode.flare1] + "[-] to spectate the next player. \nPress [F7D358]"
                        + CacheGameObject.Find<FengCustomInputs>("InputManagerController").inputString[InputCode.flare2] + "[-] to spectate the previous player.\nPress [F7D358]"
                        + CacheGameObject.Find<FengCustomInputs>("InputManagerController").inputString[InputCode.attack1] + "[-] to enter the spectator mode.\n\n\n\n");
                    if (LevelInfo.getInfo(level).respawnMode == RespawnMode.DEATHMATCH)
                    {
                        myRespawnTime += Time.deltaTime;
                        var num2 = 10;
                        if ((int)PhotonNetwork.player.customProperties[PhotonPlayerProperty.isTitan] == 2)
                            num2 = 15;
                        ShowHUDInfoCenterADD("Respawn in " + (num2 - (int)myRespawnTime) + "s.");
                        if (myRespawnTime > num2)
                        {
                            myRespawnTime = 0f;
                            IN_GAME_MAIN_CAMERA.maincamera.gameOver = false;
                            if ((int)PhotonNetwork.player.customProperties[PhotonPlayerProperty.isTitan] == 2)
                                SpawnNonAITitan(myLastHero);
                            else
                                SpawnPlayer(myLastHero, myLastRespawnTag);
                            IN_GAME_MAIN_CAMERA.maincamera.gameOver = false;
                            ShowHUDInfoCenter(string.Empty);
                        }
                    }
                }
            }
            else if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            {
                if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.RACING)
                {
                    if (!isLosing)
                    {
                        currentSpeed = IN_GAME_MAIN_CAMERA.maincamera.main_object.rigidbody.velocity.magnitude;
                        maxSpeed = Mathf.Max(maxSpeed, currentSpeed);
                        ShowHUDInfoTopLeft(string.Concat("Current Speed : ", (int)currentSpeed, "\nMax Speed:", maxSpeed));
                    }
                }
                else
                    ShowHUDInfoTopLeft(string.Concat("Kills:", single_kills, "\nMax Damage:", single_maxDamage, "\nTotal Damage:", single_totalDamage));
            }
            if (isLosing && (IN_GAME_MAIN_CAMERA.gamemode != GAMEMODE.RACING))
            {
                if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                {
                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
                        ShowHUDInfoCenter(string.Concat("Survive ", wave, " Waves!\n Press ", CacheGameObject.Find<FengCustomInputs>("InputManagerController").inputString[InputCode.restart], " to Restart.\n\n\n"));
                    else
                        ShowHUDInfoCenter("Humanity Fail!\n Press " + CacheGameObject.Find<FengCustomInputs>("InputManagerController").inputString[InputCode.restart] + " to Restart.\n\n\n");
                }
                else
                {
                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
                        ShowHUDInfoCenter(string.Concat("Survive ", wave, " Waves!\nGame Restart in ", (int)gameEndCD, "s\n\n"));
                    else
                        ShowHUDInfoCenter("Humanity Fail!\nAgain!\nGame Restart in " + (int)gameEndCD + "s\n\n");
                    if (gameEndCD <= 0f)
                    {
                        gameEndCD = 0f;
                        if (PhotonNetwork.isMasterClient)
                            restartGame();
                        ShowHUDInfoCenter(string.Empty);
                    }
                    else
                        gameEndCD -= Time.deltaTime;
                }
            }
            if (isWinning)
            {
                if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                {
                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.RACING)
                        ShowHUDInfoCenter((int)(timeTotalServer * 10f) * 0.1f - 5f + "s !\n Press "
                            + CacheGameObject.Find<FengCustomInputs>("InputManagerController").inputString[InputCode.restart] + " to Restart.\n\n\n");
                    else
                    {
                        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
                            ShowHUDInfoCenter("Survive All Waves!\n Press " + CacheGameObject.Find<FengCustomInputs>("InputManagerController").inputString[InputCode.restart] + " to Restart.\n\n\n");
                        else
                            ShowHUDInfoCenter("Humanity Win!\n Press " + CacheGameObject.Find<FengCustomInputs>("InputManagerController").inputString[InputCode.restart] + " to Restart.\n\n\n");
                    }
                }
                else
                {
                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.RACING)
                        ShowHUDInfoCenter(string.Concat(localRacingResult, "\n\nGame Restart in ", (int)gameEndCD, "s"));
                    else
                    {
                        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
                            ShowHUDInfoCenter("Survive All Waves!\nGame Restart in " + (int)gameEndCD + "s\n\n");
                        else if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_AHSS)
                            ShowHUDInfoCenter(string.Concat("Team ", teamWinner, " Win!\nGame Restart in ", (int)gameEndCD, "s\n\n"));
                        else
                            ShowHUDInfoCenter("Humanity Win!\nGame Restart in " + (int)gameEndCD + "s\n\n");
                    }
                    if (gameEndCD <= 0f)
                    {
                        gameEndCD = 0f;
                        if (PhotonNetwork.isMasterClient)
                            restartGame();
                        ShowHUDInfoCenter(string.Empty);
                    }
                    else
                        gameEndCD -= Time.deltaTime;
                }
            }
            timeElapse += Time.deltaTime * 5;  //mod me refresh rate
            roundTime += Time.deltaTime;
            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            {
                if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.RACING)
                {
                    if (!isWinning)
                        timeTotalServer += Time.deltaTime;
                }
                else
                {
                    if (!isLosing && !isWinning)
                        timeTotalServer += Time.deltaTime;
                }
            }
            else
                timeTotalServer += Time.deltaTime;
            if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.RACING)
            {
                if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                {
                    if (!isWinning)
                        ShowHUDInfoTopCenter("Time : " + ((int)(timeTotalServer * 10f) * 0.1f - 5f));
                    if (timeTotalServer < 5f)
                        ShowHUDInfoCenter("RACE START IN " + (int)(5f - timeTotalServer));
                    else
                    {
                        if (!startRacing)
                        {
                            ShowHUDInfoCenter(string.Empty);
                            startRacing = true;
                            endRacing = false;
                            CacheGameObject.Find("door").SetActive(false);
                        }
                    }
                }
                else
                {
                    ShowHUDInfoTopCenter("Time : " + (roundTime >= 20f ? ((int)(roundTime * 10f) * 0.1f - 20f).ToString() : "WAITING"));
                    if (roundTime < 20f)
                        ShowHUDInfoCenter("RACE START IN " + (int)(20f - roundTime) + (localRacingResult != string.Empty ? "\nLast Round\n" + localRacingResult : "\n\n"));
                    else
                    {
                        if (!startRacing)
                        {
                            ShowHUDInfoCenter(string.Empty);
                            startRacing = true;
                            endRacing = false;
                            CacheGameObject.Find("door").SetActive(false);
                        }
                    }
                }
                if (IN_GAME_MAIN_CAMERA.maincamera.gameOver && !needChooseSide)
                {
                    myRespawnTime += Time.deltaTime;
                    if (myRespawnTime > 1.5f)
                    {
                        myRespawnTime = 0f;
                        IN_GAME_MAIN_CAMERA.maincamera.gameOver = false;
                        if (checkpoint != null)
                            SpawnPlayerAt(myLastHero, checkpoint);
                        else
                            SpawnPlayer(myLastHero, myLastRespawnTag);
                        IN_GAME_MAIN_CAMERA.maincamera.gameOver = false;
                        ShowHUDInfoCenter(string.Empty);
                    }
                }
            }
            if (timeElapse > 1f)
            {
                timeElapse--;
                var str2 = string.Empty;
                if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.ENDLESS_TITAN)
                    str2 = str2 + "Time : " + (time - (int)timeTotalServer);
                else
                {
                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.KILL_TITAN)
                    {
                        str2 = "Titan Left: ";
                        str2 = str2 + GameObject.FindGameObjectsWithTag("titan").Length + "  Time : ";
                        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                            str2 = str2 + (int)timeTotalServer;
                        else
                            str2 = str2 + (time - (int)timeTotalServer);
                    }
                    else
                    {
                        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
                        {
                            str2 = "Titan Left: ";
                            str2 = str2 + GameObject.FindGameObjectsWithTag("titan").Length + " Wave : " + wave;
                        }
                        else
                        {
                            if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.BOSS_FIGHT_CT)
                            {
                                str2 = "Time : ";
                                str2 = str2 + (time - (int)timeTotalServer) + "\nDefeat the Colossal Titan.\nPrevent abnormal titan from running to the north gate";
                            }
                            else
                            {
                                if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE)
                                {
                                    var str3 = "| ";
                                    for (var i = 0; i < PVPcheckPoint.chkPts.Count; i++)
                                        str3 = str3 + (PVPcheckPoint.chkPts[i] as PVPcheckPoint).getStateString() + " ";
                                    str3 = str3 + "|";
                                    str2 = string.Concat(PVPtitanScoreMax - PVPtitanScore, "  ", str3, "  ", PVPhumanScoreMax - PVPhumanScore, "\n") + "Time : " + (time - (int)timeTotalServer);
                                }
                            }
                        }
                    }
                }
                ShowHUDInfoTopCenter(str2);
                str2 = string.Empty;
                if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
                {
                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
                    {
                        str2 = "Time : ";
                        str2 = str2 + (int)timeTotalServer;
                    }
                }
                else
                {
                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.ENDLESS_TITAN)
                    {
                        object[] objArray10 = { "Humanity ", humanScore, " : Titan ", titanScore, " " };
                        str2 = string.Concat(objArray10);
                    }
                    else
                    {
                        if ((IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.KILL_TITAN) || (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.BOSS_FIGHT_CT) || (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE))
                        {
                            object[] objArray11 = { "Humanity ", humanScore, " : Titan ", titanScore, " " };
                            str2 = string.Concat(objArray11);
                        }
                        else
                        {
                            if (IN_GAME_MAIN_CAMERA.gamemode != GAMEMODE.CAGE_FIGHT)
                            {
                                if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
                                {
                                    str2 = "Time : ";
                                    str2 = str2 + (time - (int)timeTotalServer);
                                }
                                else
                                {
                                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_AHSS)
                                    {
                                        for (var j = 0; j < teamScores.Length; j++)
                                        {
                                            var str11 = str2;
                                            object[] objArray12 = { str11, j == 0 ? string.Empty : " : ", "Team", j + 1, " ", teamScores[j], string.Empty };
                                            str2 = string.Concat(objArray12);
                                        }
                                        str2 = str2 + "\nTime : " + (time - (int)timeTotalServer);
                                    }
                                }
                            }
                        }
                    }
                }
                ShowHUDInfoTopRight(str2);
                var str4 = IN_GAME_MAIN_CAMERA.difficulty >= 0 ? (IN_GAME_MAIN_CAMERA.difficulty != 0 ? (IN_GAME_MAIN_CAMERA.difficulty != 1 ? "Abnormal" : "Hard") : "Normal") : "Trainning";
                if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.CAGE_FIGHT)
                    ShowHUDInfoTopRightMAPNAME(string.Concat((int)roundTime, "s\n", level, " : ", str4));
                else
                    ShowHUDInfoTopRightMAPNAME("\n" + level + " : " + str4);
                ShowHUDInfoTopRightMAPNAME("\nCamera(" + CacheGameObject.Find<FengCustomInputs>("InputManagerController").inputString[InputCode.camera] + "):" + IN_GAME_MAIN_CAMERA.cameraMode);
                if ((IN_GAME_MAIN_CAMERA.IsMultiplayer) && needChooseSide)
                    ShowHUDInfoTopCenterADD("\n\nPRESS 1 TO ENTER GAME");
            }
            if ((IN_GAME_MAIN_CAMERA.IsMultiplayer) && (killInfoGO.Count > 0) && (killInfoGO[0] == null))
                killInfoGO.RemoveAt(0);
            if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) && PhotonNetwork.isMasterClient && (timeTotalServer > time))
            {
                string str10;
                IN_GAME_MAIN_CAMERA.gametype = GAMETYPE.STOP;
                gameStart = false;
                Screen.lockCursor = false;
                Screen.showCursor = true;
                var str5 = string.Empty;
                var str6 = string.Empty;
                var str7 = string.Empty;
                var str8 = string.Empty;
                var str9 = string.Empty;
                foreach (var player2 in PhotonNetwork.playerList)
                {
                    if (player2 != null)
                    {
                        str5 = str5 + player2.customProperties[PhotonPlayerProperty.name] + "\n";
                        str6 = str6 + player2.customProperties[PhotonPlayerProperty.kills] + "\n";
                        str7 = str7 + player2.customProperties[PhotonPlayerProperty.deaths] + "\n";
                        str8 = str8 + player2.customProperties[PhotonPlayerProperty.max_dmg] + "\n";
                        str9 = str9 + player2.customProperties[PhotonPlayerProperty.total_dmg] + "\n";
                    }
                }
                if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_AHSS)
                {
                    str10 = string.Empty;
                    for (var k = 0; k < teamScores.Length; k++)
                        str10 = str10 + (k == 0 ? string.Concat("Team", k + 1, " ", teamScores[k], " ") : " : ");
                }
                else
                {
                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
                        str10 = "Highest Wave : " + highestwave;
                    else
                    {
                        object[] objArray15 = { "Humanity ", humanScore, " : Titan ", titanScore };
                        str10 = string.Concat(objArray15);
                    }
                }
                object[] parameters = { str5, str6, str7, str8, str9, str10 };
                photonView.RPC("showResult", PhotonTargets.AllBuffered, parameters);
            }
        }
    }

    public void gameLose()
    {
        if (!isWinning && !isLosing)
        {
            isLosing = true;
            titanScore++;
            gameEndCD = gameEndTotalCDtime;
            if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
            {
                object[] parameters = { titanScore };
                photonView.RPC("netGameLose", PhotonTargets.Others, parameters);
            }
        }
    }

    public void gameWin()
    {
        if (!isLosing && !isWinning)
        {
            isWinning = true;
            humanScore++;
            if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.RACING)
            {
                gameEndCD = 20f;
                if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                {
                    object[] parameters = { 0 };
                    photonView.RPC("netGameWin", PhotonTargets.Others, parameters);
                }
            }
            else
            {
                if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_AHSS)
                {
                    gameEndCD = gameEndTotalCDtime;
                    if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                    {
                        object[] objArray2 = { teamWinner };
                        photonView.RPC("netGameWin", PhotonTargets.Others, objArray2);
                    }
                    teamScores[teamWinner - 1]++;
                }
                else
                {
                    gameEndCD = gameEndTotalCDtime;
                    if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                    {
                        object[] objArray3 = { humanScore };
                        photonView.RPC("netGameWin", PhotonTargets.Others, objArray3);
                    }
                }
            }
        }
    }

    [RPC]
    private void getRacingResult(string player, float time)
    {
        var result = new RacingResult
        {
            name = player,
            time = time
        };
        racingResult.Add(result);
        refreshRacingResult();
    }

    public bool isPlayerAllDead()
    {
        var num = 0;
        var num2 = 0;
        foreach (var player in PhotonNetwork.playerList)
        {
            if ((int)player.customProperties[PhotonPlayerProperty.isTitan] == 1)
            {
                num++;
                if ((bool)player.customProperties[PhotonPlayerProperty.dead])
                    num2++;
            }
        }
        return num == num2;
    }

    public bool isTeamAllDead(int team)
    {
        var num = 0;
        var num2 = 0;
        foreach (var player in PhotonNetwork.playerList)
        {
            if (((int)player.customProperties[PhotonPlayerProperty.isTitan] == 1) && ((int)player.customProperties[PhotonPlayerProperty.team] == team))
            {
                num++;
                if ((bool)player.customProperties[PhotonPlayerProperty.dead])
                    num2++;
            }
        }
        return num == num2;
    }

    private void kickPhotonPlayer(string name)
    {
        print("KICK " + name + "!!!");
        foreach (var player in PhotonNetwork.playerList)
        {
            if ((player.ID.ToString() == name) && !player.isMasterClient)
            {
                PhotonNetwork.CloseConnection(player);
                return;
            }
        }
    }

    private void kickPlayer(string kickPlayer, string kicker)
    {
        KickState state;
        var flag = false;
        for (var i = 0; i < kicklist.Count; i++)
        {
            if (((KickState)kicklist[i]).name == kickPlayer)
            {
                state = (KickState)kicklist[i];
                state.addKicker(kicker);
                tryKick(state);
                flag = true;
                break;
            }
        }
        if (!flag)
        {
            state = new KickState();
            state.init(kickPlayer);
            state.addKicker(kicker);
            kicklist.Add(state);
            tryKick(state);
        }
    }

    private void LateUpdate()
    {
        if (gameStart)
        {
            var enumerator = heroes.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                    ((HERO)enumerator.Current).lateUpdate();
            }
            finally
            {
                var disposable = enumerator as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
            var enumerator2 = eT.GetEnumerator();
            try
            {
                while (enumerator2.MoveNext())
                    ((TITAN_EREN)enumerator2.Current).lateUpdate();
            }
            finally
            {
                var disposable2 = enumerator2 as IDisposable;
                if (disposable2 != null)
                    disposable2.Dispose();
            }
            var enumerator3 = titans.GetEnumerator();
            try
            {
                while (enumerator3.MoveNext())
                    ((TITAN)enumerator3.Current).lateUpdate();
            }
            finally
            {
                var disposable3 = enumerator3 as IDisposable;
                if (disposable3 != null)
                    disposable3.Dispose();
            }
            var enumerator4 = fT.GetEnumerator();
            try
            {
                while (enumerator4.MoveNext())
                    ((FEMALE_TITAN)enumerator4.Current).lateUpdate();
            }
            finally
            {
                var disposable4 = enumerator4 as IDisposable;
                if (disposable4 != null)
                    disposable4.Dispose();
            }
            core();
        }
    }

    public void multiplayerRacingFinsih()
    {
        var time = roundTime - 20f;
        if (PhotonNetwork.isMasterClient)
            getRacingResult(LoginFengKAI.player.name, time);
        else
            photonView.RPC("getRacingResult", PhotonTargets.MasterClient, LoginFengKAI.player.name, time);
        gameWin();
    }

    [RPC]
    private void netGameLose(int score)
    {
        isLosing = true;
        titanScore = score;
        gameEndCD = gameEndTotalCDtime;
    }

    [RPC]
    private void netGameWin(int score)
    {
        humanScore = score;
        isWinning = true;
        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_AHSS)
        {
            teamWinner = score;
            teamScores[teamWinner - 1]++;
            gameEndCD = gameEndTotalCDtime;
        }
        else
        {
            if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.RACING)
                gameEndCD = 20f;
            else
                gameEndCD = gameEndTotalCDtime;
        }
    }

    [RPC]
    private void netRefreshRacingResult(string tmp)
    {
        localRacingResult = tmp;
    }

    [RPC]
    public void netShowDamage(int speed)
    {
        CacheGameObject.Find<StylishComponent>("Stylish").Style(speed);
        var target = CacheGameObject.Find("LabelScore");
        if (target != null)
        {
            target.GetComponent<UILabel>().text = speed.ToString();
            target.transform.localScale = Vector3.zero;
            speed = (int)(speed * 0.1f);
            speed = Mathf.Max(40, speed);
            speed = Mathf.Min(150, speed);
            iTween.Stop(target);
            object[] args = { "x", speed, "y", speed, "z", speed, "easetype", iTween.EaseType.easeOutElastic, "time", 1f };
            iTween.ScaleTo(target, iTween.Hash(args));
            object[] objArray2 = { "x", 0, "y", 0, "z", 0, "easetype", iTween.EaseType.easeInBounce, "time", 0.5f, "delay", 2f };
            iTween.ScaleTo(target, iTween.Hash(objArray2));
        }
    }

    public void NOTSpawnNonAITitan(string id)
    {
        myLastHero = id.ToUpper();
        var propertiesToSet = new ExitGames.Client.Photon.Hashtable
        {
            { "dead", true },
            { PhotonPlayerProperty.isTitan, 2 }
        };
        PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        Screen.lockCursor = IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS;
        Screen.showCursor = true;
        ShowHUDInfoCenter("the game has started for 60 seconds.\n please wait for next round.\n Click Right Mouse Key to Enter or Exit the Spectator Mode.");
        IN_GAME_MAIN_CAMERA.maincamera.enabled = true;
        IN_GAME_MAIN_CAMERA.maincamera.setMainObject(null);
        IN_GAME_MAIN_CAMERA.maincamera.setSpectorMode(true);
        IN_GAME_MAIN_CAMERA.maincamera.gameOver = true;
    }

    public void NOTSpawnPlayer(string id)
    {
        myLastHero = id.ToUpper();
        var propertiesToSet = new ExitGames.Client.Photon.Hashtable
        {
            { "dead", true },
            { PhotonPlayerProperty.isTitan, 1 }
        };
        PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        Screen.lockCursor = IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS;
        Screen.showCursor = false;
        ShowHUDInfoCenter("the game has started for 60 seconds.\n please wait for next round.\n Click Right Mouse Key to Enter or Exit the Spectator Mode.");
        IN_GAME_MAIN_CAMERA.maincamera.enabled = true;
        IN_GAME_MAIN_CAMERA.maincamera.setMainObject(null);
        IN_GAME_MAIN_CAMERA.maincamera.setSpectorMode(true);
        IN_GAME_MAIN_CAMERA.maincamera.gameOver = true;
    }

    public void OnConnectedToMaster()
    {
        print("OnConnectedToMaster");
    }

    public void OnConnectedToPhoton()
    {
        print("OnConnectedToPhoton");
    }

    public void OnConnectionFail(DisconnectCause cause)
    {
        print("OnConnectionFail : " + cause);
        Screen.lockCursor = false;
        Screen.showCursor = true;
        IN_GAME_MAIN_CAMERA.gametype = GAMETYPE.STOP;
        gameStart = false;
        var uiref = ui.GetComponent<UIReferArray>();
        NGUITools.SetActive(uiref.panels[0], false);
        NGUITools.SetActive(uiref.panels[1], false);
        NGUITools.SetActive(uiref.panels[2], false);
        NGUITools.SetActive(uiref.panels[3], false);
        NGUITools.SetActive(uiref.panels[4], true);
        CacheGameObject.Find<UILabel>("LabelDisconnectInfo").text = "OnConnectionFail : " + cause;
    }

    public void OnCreatedRoom()
    {
        kicklist = new ArrayList();
        racingResult = new ArrayList();
        teamScores = new int[2];
        print("OnCreatedRoom");
    }

    public void OnCustomAuthenticationFailed()
    {
        print("OnCustomAuthenticationFailed");
    }

    public void OnDisconnectedFromPhoton()
    {
        print("OnDisconnectedFromPhoton");
        Screen.lockCursor = false;
        Screen.showCursor = true;
    }

    [RPC]
    public void oneTitanDown(string name1 = "", bool onPlayerLeave = false)
    {
        if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || PhotonNetwork.isMasterClient)
        {
            if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE)
            {
                if (name1 != string.Empty)
                {
                    if (name1 == "Titan")
                        PVPhumanScore++;
                    else if (name1 == "Aberrant")
                        PVPhumanScore += 2;
                    else if (name1 == "Jumper")
                        PVPhumanScore += 3;
                    else if (name1 == "Crawler")
                        PVPhumanScore += 4;
                    else if (name1 == "Female Titan")
                        PVPhumanScore += 10;
                    else
                        PVPhumanScore += 3;
                }
                checkPVPpts();
                object[] parameters = { PVPhumanScore, PVPtitanScore };
                photonView.RPC("refreshPVPStatus", PhotonTargets.Others, parameters);
            }
            else
            {
                if (IN_GAME_MAIN_CAMERA.gamemode != GAMEMODE.CAGE_FIGHT)
                {
                    if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.KILL_TITAN)
                    {
                        if (checkIsTitanAllDie())
                        {
                            gameWin();
                            IN_GAME_MAIN_CAMERA.maincamera.gameOver = true;
                        }
                    }
                    else
                    {
                        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
                        {
                            if (checkIsTitanAllDie())
                            {
                                wave++;
                                if ((LevelInfo.getInfo(level).respawnMode == RespawnMode.NEWROUND) && (IN_GAME_MAIN_CAMERA.IsMultiplayer))
                                    photonView.RPC("respawnHeroInNewRound", PhotonTargets.All);
                                if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
                                    sendChatContentInfo("<color=#A8FF24>Wave : " + wave + "</color>");
                                if (wave > highestwave)
                                    highestwave = wave;
                                if (PhotonNetwork.isMasterClient)
                                    RequireStatus();
                                if (wave > 20)
                                    gameWin();
                                else
                                {
                                    var rate = 90;
                                    if (difficulty == 1)
                                        rate = 70;
                                    if (!LevelInfo.getInfo(level).punk)
                                        randomSpawnTitan("titanRespawn", rate, wave + 2);
                                    else if (wave == 5)
                                        randomSpawnTitan("titanRespawn", rate, 1, true);
                                    else if (wave == 10)
                                        randomSpawnTitan("titanRespawn", rate, 2, true);
                                    else if (wave == 15)
                                        randomSpawnTitan("titanRespawn", rate, 3, true);
                                    else if (wave == 20)
                                        randomSpawnTitan("titanRespawn", rate, 4, true);
                                    else
                                        randomSpawnTitan("titanRespawn", rate, wave + 2);
                                }
                            }
                        }
                        else
                        {
                            if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.ENDLESS_TITAN)
                            {
                                if (!onPlayerLeave)
                                {
                                    humanScore++;
                                    var num2 = 90;
                                    if (difficulty == 1)
                                        num2 = 70;
                                    randomSpawnTitan("titanRespawn", num2, 1);
                                }
                            }
                            else
                            {
                                if (LevelInfo.getInfo(level).enemyNumber == -1)
                                { }
                            }
                        }
                    }
                }
            }
        }
    }

    public void OnFailedToConnectToPhoton()
    {
        print("OnFailedToConnectToPhoton");
    }

    public void OnJoinedLobby()
    {
        print("OnJoinedLobby");
        NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").panelMultiStart, false);
        NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").panelMultiROOM, true);
    }

    public void OnJoinedRoom()
    {
        var roomInfo = PhotonNetwork.room.name.Split('`');
        print("OnJoinedRoom` " + PhotonNetwork.room.name.Replace('`', '/') + " >>> " + LevelInfo.getInfo(roomInfo[1]).mapName);
        gameTimesUp = false;
        level = roomInfo[1];
        if (roomInfo[2] == "normal")
            difficulty = 0;
        else if (roomInfo[2] == "hard")
            difficulty = 1;
        else if (roomInfo[2] == "abnormal")
            difficulty = 2;
        IN_GAME_MAIN_CAMERA.difficulty = difficulty;
        time = int.Parse(roomInfo[3]);
        time *= 60;
        if (roomInfo[4] == "day")
            IN_GAME_MAIN_CAMERA.dayLight = DayLight.Day;
        else if (roomInfo[4] == "dawn")
            IN_GAME_MAIN_CAMERA.dayLight = DayLight.Dawn;
        else if (roomInfo[4] == "night")
             IN_GAME_MAIN_CAMERA.dayLight = DayLight.Night;
        IN_GAME_MAIN_CAMERA.gamemode = LevelInfo.getInfo(level).type;
        PhotonNetwork.LoadLevel(LevelInfo.getInfo(level).mapName);
        var propertiesToSet = new ExitGames.Client.Photon.Hashtable
        {
            { PhotonPlayerProperty.name, LoginFengKAI.player.name },
            { PhotonPlayerProperty.guildName, LoginFengKAI.player.guildname },
            { PhotonPlayerProperty.kills, 0 },
            { PhotonPlayerProperty.max_dmg, 0 },
            { PhotonPlayerProperty.total_dmg, 0 },
            { PhotonPlayerProperty.deaths, 0 },
            { PhotonPlayerProperty.dead, true },
            { PhotonPlayerProperty.isTitan, 0 }
        };
        PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        humanScore = titanScore = PVPtitanScore = PVPhumanScore = 0;
        wave = highestwave = 1;
        localRacingResult = string.Empty;
        needChooseSide = true;
        chatContent = new ArrayList();
        killInfoGO = new ArrayList();
        InRoomChat.messages = new Dictionary<int, string>();
        if (!PhotonNetwork.isMasterClient)
            photonView.RPC("RequireStatus", PhotonTargets.MasterClient);
    }

    public void OnLeftLobby()
    {
        print("OnLeftLobby");
    }

    public void OnLeftRoom()
    {
        print("OnLeftRoom");
        if (Application.loadedLevel != 0)
        {
            Time.timeScale = 1f;
            if (PhotonNetwork.connected)
                PhotonNetwork.Disconnect();
            IN_GAME_MAIN_CAMERA.gametype = GAMETYPE.STOP;
            gameStart = false;
            Screen.lockCursor = false;
            Screen.showCursor = true;
            CacheGameObject.Find<FengCustomInputs>("InputManagerController").menuOn = false;
            Destroy(CacheGameObject.Find("MultiplayerManager"));
            Application.LoadLevel("menu");
        }
        GlobalItems.reset();
    }

    

    private void OnLevelWasLoaded(int level)
    {
        if ((level == 0) || (Application.loadedLevelName == "characterCreation") || (Application.loadedLevelName == "SnapShot"))
            return;
        ChangeQuality.setCurrentQuality();
        foreach (var obj2 in GameObject.FindGameObjectsWithTag("titan"))
        {
            if ((obj2.GetPhotonView() == null) || !obj2.GetPhotonView().owner.isMasterClient)
                Destroy(obj2);
        }
        GlobalItems.reset();
        isWinning = false;
        gameStart = true;
        ShowHUDInfoCenter(string.Empty);
        var main_camera = (GameObject)Instantiate(CacheResources.Load("MainCamera_mono"), CacheGameObject.Find("cameraDefaultPosition").transform.position, CacheGameObject.Find("cameraDefaultPosition").transform.rotation);
        Destroy(CacheGameObject.Find("cameraDefaultPosition"));
        main_camera.name = "MainCamera";
        Screen.lockCursor = true;
        Screen.showCursor = true;
        ui = (GameObject)Instantiate(CacheResources.Load("UI_IN_GAME"));
        ui.name = "UI_IN_GAME";
        ui.SetActive(true);
        var uirefer = ui.GetComponent<UIReferArray>();
        NGUITools.SetActive(uirefer.panels[0], true);
        NGUITools.SetActive(uirefer.panels[1], false);
        NGUITools.SetActive(uirefer.panels[2], false);
        NGUITools.SetActive(uirefer.panels[3], false);
        IN_GAME_MAIN_CAMERA.maincamera.setHUDposition();
        IN_GAME_MAIN_CAMERA.maincamera.setDayLight(IN_GAME_MAIN_CAMERA.dayLight);
        var info = LevelInfo.getInfo(FengGameManagerMKII.level);
        LoadAsset.loadSkills();
        HERO.InitUIElements();
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
        {
            single_kills = 0;
            single_maxDamage = 0;
            single_totalDamage = 0;
            IN_GAME_MAIN_CAMERA.maincamera.enabled = true;
            CacheGameObject.Find<SpectatorMovement>("MainCamera").disable = true;
            CacheGameObject.Find<MouseLook>("MainCamera").disable = true;
            IN_GAME_MAIN_CAMERA.gamemode = LevelInfo.getInfo(FengGameManagerMKII.level).type;
            SpawnPlayer(IN_GAME_MAIN_CAMERA.singleCharacter.ToUpper());
            Screen.lockCursor = IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS;
            Screen.showCursor = false;
            var rate = difficulty == 1 ? 70 : 90;
            randomSpawnTitan("titanRespawn", rate, info.enemyNumber);
        }
        else
        {
            PVPcheckPoint.chkPts = new ArrayList();
            IN_GAME_MAIN_CAMERA.maincamera.enabled = false;
            CacheGameObject.Find<CameraShake>("MainCamera").enabled = false;
            IN_GAME_MAIN_CAMERA.gametype = GAMETYPE.MULTIPLAYER;
            if (info.type == GAMEMODE.TROST)
            {
                CacheGameObject.Find("playerRespawn").SetActive(false);
                Destroy(CacheGameObject.Find("playerRespawn"));
                CacheGameObject.Find("rock").animation["lift"].speed = 0f;
                CacheGameObject.Find("door_fine").SetActive(false);
                CacheGameObject.Find("door_broke").SetActive(true);
                Destroy(CacheGameObject.Find("ppl"));
            }
            else if (info.type == GAMEMODE.BOSS_FIGHT_CT)
            {
                CacheGameObject.Find("playerRespawnTrost").SetActive(false);
                Destroy(CacheGameObject.Find("playerRespawnTrost"));
            }
            if (needChooseSide)
                ShowHUDInfoTopCenterADD("\n\nPRESS 1 TO ENTER GAME");
            else
            {
                Screen.lockCursor = IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS;
                if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE)
                    checkpoint = CacheGameObject.Find((int)PhotonNetwork.player.customProperties[PhotonPlayerProperty.isTitan] == 2 ? "PVPchkPtT" : "PVPchkPtH");
                if ((int)PhotonNetwork.player.customProperties[PhotonPlayerProperty.isTitan] == 2)
                    SpawnNonAITitan(myLastHero);
                else
                    SpawnPlayer(myLastHero, myLastRespawnTag);
            }
            if (info.type == GAMEMODE.BOSS_FIGHT_CT)
                Destroy(CacheGameObject.Find("rock"));
            if (PhotonNetwork.isMasterClient)
            {
                if (info.type == GAMEMODE.TROST)
                {
                    if (!isPlayerAllDead())
                    {
                        PhotonNetwork.Instantiate("TITAN_EREN_trost", new Vector3(-200f, 0f, -194f), Quaternion.Euler(0f, 180f, 0f), 0).GetComponent<TITAN_EREN>().rockLift = true;
                        var rate = difficulty == 1 ? 70 : 90;
                        var objArray3 = GameObject.FindGameObjectsWithTag("titanRespawn");
                        var obj6 = CacheGameObject.Find("titanRespawnTrost");
                        if (obj6 != null)
                        {
                            foreach (var obj7 in objArray3)
                            {
                                if (obj7.transform.parent.gameObject == obj6)
                                    spawnTitan(rate, obj7.transform.position, obj7.transform.rotation);
                            }
                        }
                    }
                }
                else
                {
                    if (info.type == GAMEMODE.BOSS_FIGHT_CT)
                    {
                        if (!isPlayerAllDead())
                            PhotonNetwork.Instantiate("COLOSSAL_TITAN", -Vector3.up * 10000f, Quaternion.Euler(0f, 180f, 0f), 0);
                    }
                    else if ((info.type == GAMEMODE.KILL_TITAN) || (info.type == GAMEMODE.ENDLESS_TITAN) || (info.type == GAMEMODE.SURVIVE_MODE))
                    {
                        if ((info.name == "Annie") || (info.name == "Annie II"))
                            PhotonNetwork.Instantiate("FEMALE_TITAN", GameObject.Find("titanRespawn").transform.position,
                                GameObject.Find("titanRespawn").transform.rotation, 0);
                        else
                        {
                            var rate = difficulty == 1 ? 70 : 90;
                            randomSpawnTitan("titanRespawn", rate, info.enemyNumber);
                        }
                    }
                    else if ((info.type != GAMEMODE.TROST) && (info.type == GAMEMODE.PVP_CAPTURE) && (LevelInfo.getInfo(FengGameManagerMKII.level).mapName == "OutSide"))
                    {
                        var spawns = GameObject.FindGameObjectsWithTag("titanRespawn");
                        if (spawns.Length <= 0)
                            return;
                        for (var i = 0; i < spawns.Length; i++)
                            spawnTitanRaw(spawns[i].transform.position, spawns[i].transform.rotation).GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_CRAWLER, true);
                    }
                }
            }
            if (!info.supply)
                Destroy(CacheGameObject.Find("aot_supply"));
            if (!PhotonNetwork.isMasterClient)
                photonView.RPC("RequireStatus", PhotonTargets.MasterClient);
            if (LevelInfo.getInfo(FengGameManagerMKII.level).lavaMode)
            {
                Instantiate(CacheResources.Load("levelBottom"), new Vector3(0f, -29.5f, 0f), Quaternion.Euler(0f, 0f, 0f));
                CacheGameObject.Find("aot_supply").transform.position = CacheGameObject.Find("aot_supply_lava_position").transform.position;
                CacheGameObject.Find("aot_supply").transform.rotation = CacheGameObject.Find("aot_supply_lava_position").transform.rotation;
            }
        }
    }

    public void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        print("OnMasterClientSwitched");
        if (!gameTimesUp && PhotonNetwork.isMasterClient)
            restartGame(true);
    }

    public void OnPhotonCreateRoomFailed()
    {
        print("OnPhotonCreateRoomFailed");
    }

    public void OnPhotonCustomRoomPropertiesChanged()
    {
        print("OnPhotonCustomRoomPropertiesChanged");
    }

    public void OnPhotonInstantiate()
    {
        print("OnPhotonInstantiate");
    }

    public void OnPhotonJoinRoomFailed()
    {
        print("OnPhotonJoinRoomFailed");
    }

    public void OnPhotonMaxCccuReached()
    {
        print("OnPhotonMaxCccuReached");
    }

    public void OnPhotonPlayerConnected()
    {
        refreshPlayerlist();
        print("OnPhotonPlayerConnected");
    }

    public void OnPhotonPlayerDisconnected()
    {
        print("OnPhotonPlayerDisconnected");
        if (!gameTimesUp)
        {
            oneTitanDown(string.Empty, true);
            someOneIsDead(0);
        }
        refreshPlayerlist();
    }

    public void OnPhotonPlayerPropertiesChanged()
    {
        popPlayerListAlpha();
        refreshPlayerlist();
    }

    public void OnPhotonRandomJoinFailed()
    {
        print("OnPhotonRandomJoinFailed");
    }

    public void OnPhotonSerializeView()
    {
        print("OnPhotonSerializeView");
    }

    public void OnReceivedRoomListUpdate() { }

    public void OnUpdatedFriendList()
    {
        print("OnUpdatedFriendList");
    }

    public void playerKillInfoSingleUpdate(int dmg)
    {
        single_kills++;
        single_maxDamage = Mathf.Max(dmg, single_maxDamage);
        single_totalDamage += dmg;
    }

    public void playerKillInfoUpdate(PhotonPlayer player, int dmg)
    {
        var propertiesToSet = new ExitGames.Client.Photon.Hashtable
        {
            { PhotonPlayerProperty.kills, (int)player.customProperties[PhotonPlayerProperty.kills] + 1 },
            { PhotonPlayerProperty.max_dmg, Mathf.Max(dmg, (int)player.customProperties[PhotonPlayerProperty.max_dmg]) },
            { PhotonPlayerProperty.total_dmg, (int)player.customProperties[PhotonPlayerProperty.total_dmg] + dmg }
        };
        player.SetCustomProperties(propertiesToSet);
    }

    public GameObject randomSpawnOneTitan(string place, int rate)
    {
        var objArray = GameObject.FindGameObjectsWithTag(place);
        var index = UnityEngine.Random.Range(0, objArray.Length);
        var obj2 = objArray[index];
        while (objArray[index] == null)
        {
            index = UnityEngine.Random.Range(0, objArray.Length);
            obj2 = objArray[index];
        }
        objArray[index] = null;
        return spawnTitan(rate, obj2.transform.position, obj2.transform.rotation);
    }

    public void randomSpawnTitan(string place, int rate, int num, bool punk = false)
    {
        if (num == -1)
            num = 1;
        var objArray = GameObject.FindGameObjectsWithTag(place);
        if (objArray.Length > 0)
        {
            for (var i = 0; i < num; i++)
            {
                var index = UnityEngine.Random.Range(0, objArray.Length);
                var obj2 = objArray[index];
                while (objArray[index] == null)
                {
                    index = UnityEngine.Random.Range(0, objArray.Length);
                    obj2 = objArray[index];
                }
                objArray[index] = null;
                spawnTitan(rate, obj2.transform.position, obj2.transform.rotation, punk);
            }
        }
    }

    [RPC]
    private void refreshPVPStatus(int score1, int score2)
    {
        PVPhumanScore = score1;
        PVPtitanScore = score2;
    }

    [RPC]
    private void refreshPVPStatus_AHSS(int[] score1)
    {
        print("FMKII - score1: " + score1);
        teamScores = score1;
    }

    private void refreshRacingResult()
    {
        this.localRacingResult = "Result\n";
        IComparer comparer = new IComparerRacingResult();
        racingResult.Sort(comparer);
        var num = Mathf.Min(racingResult.Count, 6);
        for (var i = 0; i < num; i++)
        {
            var localRacingResult = this.localRacingResult;
            object[] objArray1 = { localRacingResult, "Rank ", i + 1, " : " };
            this.localRacingResult = string.Concat(objArray1);
            this.localRacingResult = this.localRacingResult + (racingResult[i] as RacingResult).name;
            this.localRacingResult = this.localRacingResult + "   " + (int)((racingResult[i] as RacingResult).time * 100f) * 0.01f + "s";
            this.localRacingResult = this.localRacingResult + "\n";
        }
        object[] parameters = { this.localRacingResult };
        photonView.RPC("netRefreshRacingResult", PhotonTargets.All, parameters);
    }

    [RPC]
    private void refreshStatus(int score1, int score2, int wav, int highestWav, float time1, float time2, bool startRacin, bool endRacin)
    {
        humanScore = score1;
        titanScore = score2;
        wave = wav;
        highestwave = highestWav;
        roundTime = time1;
        timeTotalServer = time2;
        startRacing = startRacin;
        endRacing = endRacin;
        if (startRacing && (CacheGameObject.Find("door") != null))
            CacheGameObject.Find("door").SetActive(false);
    }

    public void removeCT(COLOSSAL_TITAN titan)
    {
        cT.Remove(titan);
    }

    public void removeET(TITAN_EREN hero)
    {
        eT.Remove(hero);
    }

    public void removeFT(FEMALE_TITAN titan)
    {
        fT.Remove(titan);
    }

    public void removeHero(HERO hero)
    {
        heroes.Remove(hero);
    }

    public void removeHook(Bullet h)
    {
        hooks.Remove(h);
    }

    public void removeTitan(TITAN titan)
    {
        titans.Remove(titan);
        GlobalItems.removeTitan(titan);
    }

    [RPC]
    private void RequireStatus()  //todo send this only to the player who requested it
    {
        photonView.RPC("refreshStatus", PhotonTargets.Others, humanScore, titanScore, wave, highestwave, roundTime, timeTotalServer, startRacing, endRacing);
        photonView.RPC("refreshPVPStatus", PhotonTargets.Others, PVPhumanScore, PVPtitanScore);
        photonView.RPC("refreshPVPStatus_AHSS", PhotonTargets.Others, teamScores);
    }

    [RPC]
    private void respawnHeroInNewRound()
    {
        if (!needChooseSide && IN_GAME_MAIN_CAMERA.maincamera.gameOver)
        {
            SpawnPlayer(myLastHero, myLastRespawnTag);
            IN_GAME_MAIN_CAMERA.maincamera.gameOver = false;
            ShowHUDInfoCenter(string.Empty);
        }
    }

    public void restartGame(bool masterclientSwitched = false)
    {
        print("reset game: " + gameTimesUp);
        if (!gameTimesUp)
        {
            PVPtitanScore = 0;
            PVPhumanScore = 0;
            startRacing = false;
            endRacing = false;
            checkpoint = null;
            timeElapse = 0f;
            roundTime = 0f;
            isWinning = false;
            isLosing = false;
            wave = 1;
            myRespawnTime = 0f;
            kicklist = new ArrayList();
            killInfoGO = new ArrayList();
            racingResult = new ArrayList();
            ShowHUDInfoCenter(string.Empty);
            PhotonNetwork.DestroyAll();
            //HideNSeek.Restart();
            photonView.RPC("RPCLoadLevel", PhotonTargets.All);
            if (masterclientSwitched)
                sendChatContentInfo("<color=#A8FF24>MasterClient has switched to </color>" + PhotonNetwork.player.customProperties[PhotonPlayerProperty.name]);
        }
    }

    [RPC]
    private void restartGameByClient()
    {
        restartGame();
    }

    public void restartGameSingle()
    {
        startRacing = false;
        endRacing = false;
        checkpoint = null;
        single_kills = 0;
        single_maxDamage = 0;
        single_totalDamage = 0;
        timeElapse = 0f;
        roundTime = 0f;
        timeTotalServer = 0f;
        isWinning = false;
        isLosing = false;
        wave = 1;
        myRespawnTime = 0f;
        ShowHUDInfoCenter(string.Empty);
        Application.LoadLevel(Application.loadedLevel);
    }

    [RPC]
    private void RPCLoadLevel()
    {
        //HideNSeek.Restart();
        PhotonNetwork.LoadLevel(LevelInfo.getInfo(level).mapName);
        refreshPlayerlist();
    }

    public void sendChatContentInfo(string content)
    {
        object[] parameters = { content, string.Empty };
        photonView.RPC("Chat", PhotonTargets.All, parameters);
    }

    public void sendKillInfo(bool t1, string killer, bool t2, string victim, int dmg = 0)
    {
        object[] parameters = { t1, killer, t2, victim, dmg };
        photonView.RPC("updateKillInfo", PhotonTargets.All, parameters);
    }

    [RPC]
    private void showChatContent(string content)  //obsolete
    {
        chatContent.Add(content);
        if (chatContent.Count > 10)
            chatContent.RemoveAt(0);
        CacheGameObject.Find<UILabel>("LabelChatContent").text = string.Empty;
        for (var i = 0; i < chatContent.Count; i++)
        {
            var component = GameObject.Find("LabelChatContent").GetComponent<UILabel>();
            component.text = component.text + chatContent[i];
        }
    }

    public void ShowHUDInfoCenter(string content)
    {
        var obj = CacheGameObject.Find("LabelInfoCenter");
        if (obj != null)
            obj.GetComponent<UILabel>().text = content;
    }

    public void ShowHUDInfoCenterADD(string content)
    {
        var obj = CacheGameObject.Find("LabelInfoCenter");
        if (obj != null)
        {
            var component = obj.GetComponent<UILabel>();
            component.text = component.text + content;
        }
    }

    private void ShowHUDInfoTopCenter(string content)
    {
        var obj = CacheGameObject.Find("LabelInfoTopCenter");
        if (obj != null)
            obj.GetComponent<UILabel>().text = content;
    }

    private void ShowHUDInfoTopCenterADD(string content)
    {
        var obj = CacheGameObject.Find("LabelInfoTopCenter");
        if (obj != null)
        {
            var component = obj.GetComponent<UILabel>();
            component.text = component.text + content;
        }
    }

    private static float playerListAlpha = 0.5f;
    public void ShowHUDInfoTopLeft(string content)
    {
        var obj = CacheGameObject.Find("LabelInfoTopLeft");
        if (obj != null)
        {
            var label = obj.GetComponent<UILabel>();
            label.text = content;
            label.alpha = playerListAlpha;
            //make alpha pop with 1f when edits occur and slowly fade back down
        }
    }

    private static void popPlayerListAlpha()
    {
        MKII.StopCoroutine("playerStatsChangedFX");
        MKII.StartCoroutine("playerStatsChangedFX");
    }

    private IEnumerator playerStatsChangedFX()
    {
        playerListAlpha = 1f;
        refreshPlayerlist();
        yield return new WaitForSeconds(1f);
        var wait = new WaitForSeconds(0.07f);
        while (playerListAlpha > 0.5f)
        {
            refreshPlayerlist();
            yield return wait;
            playerListAlpha -= 0.05f;
        }
    }
    
    private void ShowHUDInfoTopRight(string content)
    {
        var obj = CacheGameObject.Find("LabelInfoTopRight");
        if (obj != null)
            obj.GetComponent<UILabel>().text = content;
    }

    private void ShowHUDInfoTopRightMAPNAME(string content)
    {
        var obj = CacheGameObject.Find("LabelInfoTopRight");
        if (obj != null)
        {
            var component = obj.GetComponent<UILabel>();
            component.text = component.text + content;
        }
    }

    [RPC]
    private void showResult(string text0, string text1, string text2, string text3, string text4, string text6, PhotonMessageInfo t)
    {
        if (!gameTimesUp)
        {
            gameTimesUp = true;
            var obj2 = CacheGameObject.Find("UI_IN_GAME");
            NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[0], false);
            NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[1], false);
            NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[2], true);
            NGUITools.SetActive(obj2.GetComponent<UIReferArray>().panels[3], false);
            CacheGameObject.Find<UILabel>("LabelName").text = text0;
            CacheGameObject.Find<UILabel>("LabelKill").text = text1;
            CacheGameObject.Find<UILabel>("LabelDead").text = text2;
            CacheGameObject.Find<UILabel>("LabelMaxDmg").text = text3;
            CacheGameObject.Find<UILabel>("LabelTotalDmg").text = text4;
            CacheGameObject.Find<UILabel>("LabelResultTitle").text = text6;
            Screen.lockCursor = false;
            Screen.showCursor = true;
            IN_GAME_MAIN_CAMERA.gametype = GAMETYPE.STOP;
            gameStart = false;
        }
    }

    private void SingleShowHUDInfoTopCenter(string content)
    {
        var obj2 = CacheGameObject.Find("LabelInfoTopCenter");
        if (obj2 != null)
            obj2.GetComponent<UILabel>().text = content;
    }

    private void SingleShowHUDInfoTopLeft(string content)
    {
        var obj2 = CacheGameObject.Find("LabelInfoTopLeft");
        if (obj2 != null)
        {
            content = content.Replace("[0]", "[*^_^*]");
            obj2.GetComponent<UILabel>().text = content;
        }
    }

    [RPC]
    public void someOneIsDead(int id = -1)
    {
        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE)
        {
            if (id != 0)
                PVPtitanScore += 2;
            checkPVPpts();
            object[] parameters = { PVPhumanScore, PVPtitanScore };
            photonView.RPC("refreshPVPStatus", PhotonTargets.Others, parameters);
        }
        else if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.ENDLESS_TITAN)
            titanScore++;
        else if ((IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.KILL_TITAN) || (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.SURVIVE_MODE)
                 || (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.BOSS_FIGHT_CT) || (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.TROST))
        {
            if (isPlayerAllDead())
                gameLose();
        }
        else if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_AHSS)
        {
            if (isPlayerAllDead())
            {
                gameLose();
                teamWinner = 0;
            }
            if (isTeamAllDead(1))
            {
                teamWinner = 2;
                gameWin();
            }
            if (isTeamAllDead(2))
            {
                teamWinner = 1;
                gameWin();
            }
        }
    }

    public void SpawnNonAITitan(string id, string tag = "titanRespawn")
    {
        var spawns = GameObject.FindGameObjectsWithTag(tag);
        var obj2 = spawns[UnityEngine.Random.Range(0, spawns.Length)];
        myLastHero = id.ToUpper();
        var titan = IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE
            ? PhotonNetwork.Instantiate("TITAN_VER3.1", checkpoint.transform.position + new Vector3(UnityEngine.Random.Range(-20, 20), 2f, UnityEngine.Random.Range(-20, 20)), checkpoint.transform.rotation, 0)
            : PhotonNetwork.Instantiate("TITAN_VER3.1", obj2.transform.position, obj2.transform.rotation, 0);
        IN_GAME_MAIN_CAMERA.maincamera.setMainObjectASTITAN(titan);
        titan.GetComponent<TITAN>().nonAI = true;
        titan.GetComponent<TITAN>().speed = 30f;
        titan.GetComponent<TITAN_CONTROLLER>().enabled = true;
        if ((id == "RANDOM") && (UnityEngine.Random.Range(0, 100) < 7))
            titan.GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_CRAWLER, true);
        IN_GAME_MAIN_CAMERA.maincamera.enabled = true;
        CacheGameObject.Find<SpectatorMovement>("MainCamera").disable = true;
        CacheGameObject.Find<MouseLook>("MainCamera").disable = true;
        IN_GAME_MAIN_CAMERA.maincamera.gameOver = false;
        var propertiesToSet = new ExitGames.Client.Photon.Hashtable
        {
            { "dead", false },
            { PhotonPlayerProperty.isTitan, 2 }
        };
        PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        Screen.lockCursor = IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS;
        Screen.showCursor = true;
        ShowHUDInfoCenter(string.Empty);
    }

    public void SpawnPlayer(string id, string tag = "playerRespawn")
    {
        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE)
            SpawnPlayerAt(id, checkpoint);
        else
        {
            myLastRespawnTag = tag;
            var spawns = GameObject.FindGameObjectsWithTag(tag);
            var pos = spawns[UnityEngine.Random.Range(0, spawns.Length)];
            SpawnPlayerAt(id, pos);
        }
    }

    public void SpawnPlayerAt(string id, GameObject pos)
    {
        var component = IN_GAME_MAIN_CAMERA.maincamera;
        myLastHero = id.ToUpper();
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
        {
            if (IN_GAME_MAIN_CAMERA.singleCharacter == "TITAN_EREN")
                component.setMainObject((GameObject)Instantiate(CacheResources.Load("TITAN_EREN"), pos.transform.position, pos.transform.rotation));
            else
            {
                component.setMainObject((GameObject)Instantiate(CacheResources.Load("AOTTG_HERO 1"), pos.transform.position, pos.transform.rotation));
                if ((IN_GAME_MAIN_CAMERA.singleCharacter == "SET 1") || (IN_GAME_MAIN_CAMERA.singleCharacter == "SET 2") || (IN_GAME_MAIN_CAMERA.singleCharacter == "SET 3"))
                {
                    var costume = CostumeConverter.LocalDataToHeroCostume(IN_GAME_MAIN_CAMERA.singleCharacter);
                    costume.checkstat();
                    CostumeConverter.HeroCostumeToLocalData(costume, IN_GAME_MAIN_CAMERA.singleCharacter);
                    var hero = component.main_object.GetComponent<HERO>();
                    hero.GetComponent<HERO_SETUP>().init();
                    if (costume != null)
                    {
                        hero.GetComponent<HERO_SETUP>().myCostume = costume;
                        hero.GetComponent<HERO_SETUP>().myCostume.stat = costume.stat;
                    }
                    else
                    {
                        costume = HeroCostume.costumeOption[3];
                        hero.GetComponent<HERO_SETUP>().myCostume = costume;
                        hero.GetComponent<HERO_SETUP>().myCostume.stat = HeroStat.getInfo(costume.name.ToUpper());
                    }
                    hero.GetComponent<HERO_SETUP>().setCharacterComponent();
                    hero.setStat();
                    hero.setSkillHUDPosition();
                }
                else
                {
                    for (var i = 0; i < HeroCostume.costume.Length; i++)
                    {
                        if (HeroCostume.costume[i].name.ToUpper() != IN_GAME_MAIN_CAMERA.singleCharacter.ToUpper())
                            continue;
                        var index = HeroCostume.costume[i].id + CheckBoxCostume.costumeSet - 1;
                        if (HeroCostume.costume[index].name != HeroCostume.costume[i].name)
                            index = HeroCostume.costume[i].id + 1;
                        var hero = component.main_object.GetComponent<HERO>();
                        hero.GetComponent<HERO_SETUP>().init();
                        hero.GetComponent<HERO_SETUP>().myCostume = HeroCostume.costume[index];
                        hero.GetComponent<HERO_SETUP>().myCostume.stat = HeroStat.getInfo(HeroCostume.costume[index].name.ToUpper());
                        hero.GetComponent<HERO_SETUP>().setCharacterComponent();
                        hero.setStat();
                        hero.setSkillHUDPosition();
                        break;
                    }
                }
            }
        }
        else
        {
            component.setMainObject(PhotonNetwork.Instantiate("AOTTG_HERO 1", pos.transform.position, pos.transform.rotation, 0));
            id = id.ToUpper();
            if ((id == "SET 1") || (id == "SET 2") || (id == "SET 3"))
            {
                var costume2 = CostumeConverter.LocalDataToHeroCostume(id);
                costume2.checkstat();
                CostumeConverter.HeroCostumeToLocalData(costume2, id);
                var hero = component.main_object.GetComponent<HERO>();
                hero.GetComponent<HERO_SETUP>().init();
                if (costume2 != null)
                {
                    hero.GetComponent<HERO_SETUP>().myCostume = costume2;
                    hero.GetComponent<HERO_SETUP>().myCostume.stat = costume2.stat;
                }
                else
                {
                    costume2 = HeroCostume.costumeOption[3];
                    hero.GetComponent<HERO_SETUP>().myCostume = costume2;
                    hero.GetComponent<HERO_SETUP>().myCostume.stat = HeroStat.getInfo(costume2.name.ToUpper());
                }
                hero.GetComponent<HERO_SETUP>().setCharacterComponent();
                hero.setStat();
                hero.setSkillHUDPosition();
            }
            else
            {
                for (var j = 0; j < HeroCostume.costume.Length; j++)
                {
                    if (HeroCostume.costume[j].name.ToUpper() != id.ToUpper())
                        continue;
                    var num4 = HeroCostume.costume[j].id;
                    if (id.ToUpper() != "AHSS")
                        num4 += CheckBoxCostume.costumeSet - 1;
                    if (HeroCostume.costume[num4].name != HeroCostume.costume[j].name)
                        num4 = HeroCostume.costume[j].id + 1;
                    var hero = component.main_object.GetComponent<HERO>();
                    hero.GetComponent<HERO_SETUP>().init();
                    hero.GetComponent<HERO_SETUP>().myCostume = HeroCostume.costume[num4];
                    hero.GetComponent<HERO_SETUP>().myCostume.stat = HeroStat.getInfo(HeroCostume.costume[num4].name.ToUpper());
                    hero.GetComponent<HERO_SETUP>().setCharacterComponent();
                    hero.setStat();
                    hero.setSkillHUDPosition();
                    break;
                }
            }
            CostumeConverter.HeroCostumeToPhotonData(component.main_object.GetComponent<HERO>().GetComponent<HERO_SETUP>().myCostume, PhotonNetwork.player);
            if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE)
            {
                var transform = component.main_object.transform;
                transform.position += new Vector3(UnityEngine.Random.Range(-20, 20), 2f, UnityEngine.Random.Range(-20, 20));
            }
            var propertiesToSet = new ExitGames.Client.Photon.Hashtable();
            propertiesToSet.Add("dead", false);
            propertiesToSet.Add(PhotonPlayerProperty.isTitan, 1);
            PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        }
        component.enabled = true;
        IN_GAME_MAIN_CAMERA.maincamera.setHUDposition();
        CacheGameObject.Find<SpectatorMovement>("MainCamera").disable = true;
        CacheGameObject.Find<MouseLook>("MainCamera").disable = true;
        component.gameOver = false;
        Screen.lockCursor = IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS;
        Screen.showCursor = false;
        isLosing = false;
        ShowHUDInfoCenter(string.Empty);

        LoadAsset.myHero = component.main_object;
    }

    public GameObject spawnTitan(int rate, Vector3 position, Quaternion rotation, bool punk = false)
    {
        var titan = spawnTitanRaw(position, rotation).GetComponent<TITAN>();
        if (punk)
            titan.setAbnormalType(AbnormalType.TYPE_PUNK);
        else
        {
            if (UnityEngine.Random.Range(0, 100) < rate)
            {
                if (IN_GAME_MAIN_CAMERA.difficulty == 2)
                {
                    if ((UnityEngine.Random.Range(0f, 1f) < 0.7f) || LevelInfo.getInfo(level).noCrawler)
                        titan.setAbnormalType(AbnormalType.TYPE_JUMPER);
                    else
                        titan.setAbnormalType(AbnormalType.TYPE_CRAWLER);
                }
            }
            else
            {
                if (IN_GAME_MAIN_CAMERA.difficulty == 2)
                {
                    if ((UnityEngine.Random.Range(0f, 1f) < 0.7f) || LevelInfo.getInfo(level).noCrawler)
                        titan.setAbnormalType(AbnormalType.TYPE_JUMPER);
                    else
                        titan.setAbnormalType(AbnormalType.TYPE_CRAWLER);
                }
                else
                {
                    if (UnityEngine.Random.Range(0, 100) < rate)
                    {
                        if ((UnityEngine.Random.Range(0f, 1f) < 0.8f) || LevelInfo.getInfo(level).noCrawler)
                            titan.setAbnormalType(AbnormalType.TYPE_I);
                        else
                            titan.setAbnormalType(AbnormalType.TYPE_CRAWLER);
                    }
                    else
                    {
                        if ((UnityEngine.Random.Range(0f, 1f) < 0.8f) || LevelInfo.getInfo(level).noCrawler)
                            titan.setAbnormalType(AbnormalType.TYPE_JUMPER);
                        else
                            titan.setAbnormalType(AbnormalType.TYPE_CRAWLER);
                    }
                }
            }
        }
        GameObject obj3;
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            obj3 = (GameObject)Instantiate(CacheResources.Load("FX/FXtitanSpawn"), titan.gameObject.transform.position, Quaternion.Euler(-90f, 0f, 0f));
        else
            obj3 = PhotonNetwork.Instantiate("FX/FXtitanSpawn", titan.gameObject.transform.position, Quaternion.Euler(-90f, 0f, 0f), 0);
        obj3.transform.localScale = titan.gameObject.transform.localScale;
        return titan.gameObject;
    }

    private GameObject spawnTitanRaw(Vector3 position, Quaternion rotation)
    {
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            return (GameObject)Instantiate(CacheResources.Load("TITAN_VER3.1"), position, rotation);
        return PhotonNetwork.Instantiate("TITAN_VER3.1", position, rotation, 0);
    }

    private static bool red;

    public static void refreshPlayerlist()
    {
        if (PhotonNetwork.connectionStateDetailed != PeerState.Joined)
            return;
        red = !red;
        ModControl.playerlist_speedup = false;
        var content = new StringBuilder();
        foreach (var player in PhotonNetwork.playerList)
        {
            if (player == null)
                continue;
            //Player ID
            //Give guests another color
            //Start the tab playerlist
            var id_color = Season2.BanHandler.ignoredPlayes.Contains(player.ID)
                ? "[FF0000]"
                : player.isModerator || player.isModerator ? "[892EFE]" : "[3EEB32]";
            content.Append(id_color).Append("[").Append(player.ID).Append("][-] ");
            if (player.isMasterClient)
                content.Append("[FFFFFF][MC][-]");

            if (player.info.isDead)
                content.Append("[FF0000]*dead█*[-]");
            else if (player.info.isHuman && player.grabbed)
            {
                ModControl.playerlist_speedup = true;
                content.Append(red ? "[FF0000]" : "[FFC300]").Append("*grabbed*[-]");
            }
            if (player.isModerator)
                content.Append("[892EFE]");
            if (player.info.isHuman)
                content.Append(player.info.isAHSS ? "[82FA58]" : "[82BCF4]");
            else if (player.info.isTitan)
                content.Append("[EFD16E]");
            else
                content.Append("[EC2700]");
            var name = player.info.name/*.FixColorCodes()*/;
            content.Append(name/*.RemoveColorCodes().RemoveNonAlphanumericFast()*/);
            content.Append("/").Append(player.kills).Append("/")
                .Append(player.deaths).Append("/").Append(player.max_dmg).Append("/").Append(player.total_dmg).Append("\n");
            /*if (player.Ping > 0)
                content.Append(" | " + player.Ping);*/
        }
        MKII.ShowHUDInfoTopLeft(content.ToString());
    }

    public static bool spawnahss;
    public static bool spawnblade;

    /*public void FixedUpdate()
    {
        if (spawnahss)
        {
            PhotonNetwork.Destroy(CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").main_object);
            SpawnPlayer("AHSS");
            spawnahss = false;
        }
        else if (spawnblade)
        {
            PhotonNetwork.Destroy(CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").main_object);
            SpawnPlayer("SET 1");
            spawnblade = false;
        }
    }*/

    private void Start()
    {
        MKII = this;
        gameObject.name = "MultiplayerManager";
        HeroCostume.init();
        CharacterMaterials.init();
        DontDestroyOnLoad(gameObject);
        heroes = new ArrayList();
        eT = new ArrayList();
        titans = new ArrayList();
        fT = new ArrayList();
        cT = new ArrayList();
        hooks = new ArrayList();

        GlobalItems.reset();
        
        Application.targetFrameRate = Screen.currentResolution.refreshRate;
        gameObject.AddComponent<PhotonStatsGui>();
        gameObject.AddComponent<PhotonLagSimulationGui>();

        PhotonNetwork.player.SetModProperties(new ExitGames.Client.Photon.Hashtable { { ModProperties.ping, PhotonNetwork.GetPing().ToString() } });
        
        //gameObject.AddComponent<rMicMod>();

        //gameObject.AddComponent<HideNSeek>();
        //gameObject.AddComponent<rGameHall>();
        gameObject.AddComponent<LoadAsset>();
        gameObject.AddComponent<Skins>();

        gameObject.AddComponent<Wallhack>();

        //gameObject.AddComponent<kPluginManager>();
    }

    [RPC]
    public void titanGetKill(PhotonPlayer player, int damage, string name)
    {
        damage = Mathf.Max(10, damage);
        photonView.RPC("netShowDamage", player, damage);
        photonView.RPC("oneTitanDown", PhotonTargets.MasterClient, name, false);
        sendKillInfo(false, (string)player.customProperties[PhotonPlayerProperty.name], true, name, damage);
        playerKillInfoUpdate(player, damage);
    }

    public void titanGetKillbyServer(int damage, string name)
    {
        damage = Mathf.Max(10, damage);
        sendKillInfo(false, LoginFengKAI.player.name, true, name, damage);
        netShowDamage(damage);
        oneTitanDown(name);
        playerKillInfoUpdate(PhotonNetwork.player, damage);
    }

    private void tryKick(KickState tmp)
    {
        sendChatContentInfo(string.Concat("kicking #", tmp.name, ", ", tmp.getKickCount(), "/", (int)(PhotonNetwork.playerList.Length * 0.5f), "vote"));
        if (tmp.getKickCount() >= (int)(PhotonNetwork.playerList.Length * 0.5f))
            kickPhotonPlayer(tmp.name);
    }

    private void Update()
    {
        if ((!IN_GAME_MAIN_CAMERA.IsSingleplayer) && (CacheGameObject.Find("LabelNetworkStatus") != null))
        {
            CacheGameObject.Find<UILabel>("LabelNetworkStatus").text = PhotonNetwork.connectionStateDetailed.ToString();
            if (PhotonNetwork.connected)
            {
                var component = CacheGameObject.Find<UILabel>("LabelNetworkStatus");
                component.text = component.text + " ping:" + PhotonNetwork.GetPing();
            }
        }
        if (gameStart)
        {
            var heroes_enum = heroes.GetEnumerator();
            try
            {
                while (heroes_enum.MoveNext())
                    ((HERO)heroes_enum.Current).update();
            }
            finally
            {
                var disposable = heroes_enum as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
            var bullets = hooks.GetEnumerator();
            try
            {
                while (bullets.MoveNext())
                    ((Bullet)bullets.Current).update();
            }
            finally
            {
                var disposable2 = bullets as IDisposable;
                if (disposable2 != null)
                    disposable2.Dispose();
            }
            if (mainCamera != null)
                mainCamera.snapShotUpdate();
            var titan_erens = eT.GetEnumerator();
            try
            {
                while (titan_erens.MoveNext())
                    ((TITAN_EREN)titan_erens.Current).update();
            }
            finally
            {
                var disposable3 = titan_erens as IDisposable;
                if (disposable3 != null)
                    disposable3.Dispose();
            }
            var titans_enum = titans.GetEnumerator();
            try
            {
                while (titans_enum.MoveNext())
                    ((TITAN)titans_enum.Current).update();
            }
            finally
            {
                var disposable4 = titans_enum as IDisposable;
                if (disposable4 != null)
                    disposable4.Dispose();
            }
            var female_titans = fT.GetEnumerator();
            try
            {
                while (female_titans.MoveNext())
                    ((FEMALE_TITAN)female_titans.Current).update();
            }
            finally
            {
                var disposable5 = female_titans as IDisposable;
                if (disposable5 != null)
                    disposable5.Dispose();
            }
            var colossal_titans = cT.GetEnumerator();
            try
            {
                while (colossal_titans.MoveNext())
                    ((COLOSSAL_TITAN)colossal_titans.Current).update();
            }
            finally
            {
                var disposable6 = colossal_titans as IDisposable;
                if (disposable6 != null)
                    disposable6.Dispose();
            }
            if (mainCamera != null)
                mainCamera.update();
        }
    }

    [RPC]
    private void updateKillInfo(bool t1, string killer, bool t2, string victim, int dmg)
    {
        GameObject obj4;
        var obj2 = CacheGameObject.Find("UI_IN_GAME");
        var obj3 = (GameObject)Instantiate(CacheResources.Load("UI/KillInfo"));
        for (var i = 0; i < killInfoGO.Count; i++)
        {
            obj4 = (GameObject)killInfoGO[i];
            if (obj4 != null)
                obj4.GetComponent<KillInfoComponent>().moveOn();
        }
        if (killInfoGO.Count > 4)
        {
            obj4 = (GameObject)killInfoGO[0];
            if (obj4 != null)
                obj4.GetComponent<KillInfoComponent>().destory();
            killInfoGO.RemoveAt(0);
        }
        obj3.transform.parent = obj2.GetComponent<UIReferArray>().panels[0].transform;
        obj3.GetComponent<KillInfoComponent>().show(t1, killer, t2, victim, dmg);
        killInfoGO.Add(obj3);
    }
}