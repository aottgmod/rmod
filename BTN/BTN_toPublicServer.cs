using mItems;
using UnityEngine;

public class BTN_toPublicServer : MonoBehaviour
{
    private void OnClick()
    {
        NGUITools.SetActive(transform.parent.gameObject, false);
        NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").panelMultiROOM, true);
    }
}