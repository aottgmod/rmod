using mItems;
using UnityEngine;

public class BTN_ToCredit : MonoBehaviour
{
    private void OnClick()
    {
        NGUITools.SetActive(transform.parent.gameObject, false);
        NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").panelCredits, true);
    }
}