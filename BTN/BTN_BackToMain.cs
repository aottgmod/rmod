using mItems;
using UnityEngine;

public class BTN_BackToMain : MonoBehaviour
{
    private void OnClick()
    {
        NGUITools.SetActive(transform.parent.gameObject, false);
        NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").panelMain, true);
        CacheGameObject.Find<FengCustomInputs>("InputManagerController").menuOn = false;
        PhotonNetwork.Disconnect();
    }
}