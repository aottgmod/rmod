using mItems;
using UnityEngine;

public class BTN_START_MULTI_SERVER : MonoBehaviour
{
    private void OnClick()
    {
        var text = CacheGameObject.Find<UIInput>("InputServerName").label.text;
        var maxPlayers = int.Parse(CacheGameObject.Find<UIInput>("InputMaxPlayer").label.text);
        var time = int.Parse(CacheGameObject.Find<UIInput>("InputMaxTime").label.text);
        var selection = CacheGameObject.Find<UIPopupList>("PopupListMap").selection;
        var difficulty = !GameObject.Find("CheckboxHard").GetComponent<UICheckbox>().isChecked
            ? (!CacheGameObject.Find<UICheckbox>("CheckboxAbnormal").isChecked ? "normal" : "abnormal") : "hard";
        var daytime = string.Empty;
        if (IN_GAME_MAIN_CAMERA.dayLight == DayLight.Day)
            daytime = "day";
        if (IN_GAME_MAIN_CAMERA.dayLight == DayLight.Dawn)
            daytime = "dawn";
        if (IN_GAME_MAIN_CAMERA.dayLight == DayLight.Night)
            daytime = "night";
        var unencrypted = CacheGameObject.Find<UIInput>("InputStartServerPWD").label.text;
        if (unencrypted.Length > 0)
            unencrypted = new SimpleAES().Encrypt(unencrypted);

        mString str = text + "`" + selection + "`" + difficulty + "`" + time + "`" + daytime + "`" + unencrypted + "`" + Random.Range(0, 0xc350);
        var roomOptions = new RoomOptions
        {
            isVisible = true,
            isOpen = true,
            maxPlayers = maxPlayers
        };
        PhotonNetwork.CreateRoom(str, roomOptions, null);
    }
}