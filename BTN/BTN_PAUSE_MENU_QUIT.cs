using mItems;
using UnityEngine;

public class BTN_PAUSE_MENU_QUIT : MonoBehaviour
{
    private void OnClick()
    {
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            Time.timeScale = 1f;
        else
            PhotonNetwork.Disconnect();
        Screen.lockCursor = false;
        Screen.showCursor = true;
        IN_GAME_MAIN_CAMERA.gametype = GAMETYPE.STOP;
        FengGameManagerMKII.MKII.gameStart = false;
        CacheGameObject.Find<FengCustomInputs>("InputManagerController").menuOn = false;
        Destroy(CacheGameObject.Find("MultiplayerManager"));
        Application.LoadLevel("menu");
    }
}