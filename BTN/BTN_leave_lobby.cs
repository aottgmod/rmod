using mItems;
using UnityEngine;

public class BTN_leave_lobby : MonoBehaviour
{
    private void OnClick()
    {
        NGUITools.SetActive(transform.parent.gameObject, false);
        NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").panelMultiStart, true);
        PhotonNetwork.Disconnect();
    }
}