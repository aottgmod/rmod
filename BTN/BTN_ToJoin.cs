using mItems;
using UnityEngine;

public class BTN_ToJoin : MonoBehaviour
{
    private void OnClick()
    {
        NGUITools.SetActive(transform.parent.gameObject, false);
        NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").PanelMultiJoinPrivate, true);
        CacheGameObject.Find<UILabel>("LabelJoinInfo").text = string.Empty;
        if (PlayerPrefs.HasKey("lastIP"))
            CacheGameObject.Find<UIInput>("InputIP").label.text = PlayerPrefs.GetString("lastIP");
        if (PlayerPrefs.HasKey("lastPort"))
            CacheGameObject.Find<UIInput>("InputPort").label.text = PlayerPrefs.GetString("lastPort");
    }

    private void Start()
    {
        gameObject.GetComponent<UIButton>().isEnabled = false;
    }
}