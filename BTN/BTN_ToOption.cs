using mItems;
using UnityEngine;

public class BTN_ToOption : MonoBehaviour
{
    private void OnClick()
    {
        NGUITools.SetActive(transform.parent.gameObject, false);
        NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").panelOption, true);
        CacheGameObject.Find<FengCustomInputs>("InputManagerController").showKeyMap();
        CacheGameObject.Find<FengCustomInputs>("InputManagerController").menuOn = true;
    }
}