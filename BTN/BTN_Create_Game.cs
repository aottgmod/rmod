using mItems;
using UnityEngine;

public class BTN_Create_Game : MonoBehaviour
{
    private void OnClick()
    {
        NGUITools.SetActive(transform.parent.gameObject, false);
        NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").panelMultiSet, true);
    }
}