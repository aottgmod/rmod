using ExitGames.Client.Photon;
using mItems;
using UnityEngine;

public class BTN_choose_titan : MonoBehaviour
{
    private void OnClick()
    {
        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_AHSS)
        {
            var id = "AHSS";
            NGUITools.SetActive(CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0], true);
            FengGameManagerMKII.MKII.needChooseSide = false;
            if (!PhotonNetwork.isMasterClient && (FengGameManagerMKII.MKII.roundTime > 60f))
            {
                FengGameManagerMKII.MKII.NOTSpawnPlayer(id);
                FengGameManagerMKII.MKII.photonView.RPC("restartGameByClient", PhotonTargets.MasterClient);
            }
            else
                FengGameManagerMKII.MKII.SpawnPlayer(id, "playerRespawn2");
            NGUITools.SetActive(CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[1], false);
            NGUITools.SetActive(CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[2], false);
            NGUITools.SetActive(CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[3], false);
            IN_GAME_MAIN_CAMERA.usingTitan = false;
            IN_GAME_MAIN_CAMERA.maincamera.setHUDposition();
            var hashtable2 = new Hashtable();
            hashtable2.Add(PhotonPlayerProperty.character, id);
            var propertiesToSet = hashtable2;
            PhotonNetwork.player.SetCustomProperties(propertiesToSet);
        }
        else
        {
            if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE)
                FengGameManagerMKII.MKII.checkpoint = CacheGameObject.Find("PVPchkPtT");
            var selection = CacheGameObject.Find<UIPopupList>("PopupListCharacterTITAN").selection;
            NGUITools.SetActive(transform.parent.gameObject, false);
            NGUITools.SetActive(CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0], true);
            if ((!PhotonNetwork.isMasterClient && (FengGameManagerMKII.MKII.roundTime > 60f)) || FengGameManagerMKII.MKII.justSuicide)
            {
                FengGameManagerMKII.MKII.justSuicide = false;
                FengGameManagerMKII.MKII.NOTSpawnNonAITitan(selection);
            }
            else
                FengGameManagerMKII.MKII.SpawnNonAITitan(selection, "titanRespawn");
            FengGameManagerMKII.MKII.needChooseSide = false;
            NGUITools.SetActive(CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[1], false);
            NGUITools.SetActive(CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[2], false);
            NGUITools.SetActive(CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[3], false);
            IN_GAME_MAIN_CAMERA.usingTitan = true;
            IN_GAME_MAIN_CAMERA.maincamera.setHUDposition();
        }
    }

    private void Start()
    {
        if (!LevelInfo.getInfo(FengGameManagerMKII.level).teamTitan)
            gameObject.GetComponent<UIButton>().isEnabled = false;
    }
}