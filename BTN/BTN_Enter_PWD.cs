using mItems;
using UnityEngine;

public class BTN_Enter_PWD : MonoBehaviour
{
    private void OnClick()
    {
        var text = CacheGameObject.Find<UIInput>("InputEnterPWD").label.text;
        var eaes = new SimpleAES();
        if (text == eaes.Decrypt(PanelMultiJoinPWD.Password))
            PhotonNetwork.JoinRoom(PanelMultiJoinPWD.roomName);
        else
        {
            NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").PanelMultiPWD, false);
            NGUITools.SetActive(CacheGameObject.Find<UIMainReferences>("UIRefer").panelMultiROOM, true);
            CacheGameObject.Find<PanelMultiJoin>("PanelMultiROOM").refresh();
        }
    }
}