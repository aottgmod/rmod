using mItems;
using UnityEngine;

public class BTN_PAUSE_MENU_CONTINUE : MonoBehaviour
{
    private void OnClick()
    {
        if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            Time.timeScale = 1f;
        var obj2 = CacheGameObject.Find("UI_IN_GAME");
        var uirefer = obj2.GetComponent<UIReferArray>();
        NGUITools.SetActive(uirefer.panels[0], true);
        NGUITools.SetActive(uirefer.panels[1], false);
        NGUITools.SetActive(uirefer.panels[2], false);
        NGUITools.SetActive(uirefer.panels[3], false);
        if (!IN_GAME_MAIN_CAMERA.maincamera.enabled)
        {
            Screen.showCursor = true;
            Screen.lockCursor = true;
            CacheGameObject.Find<FengCustomInputs>("InputManagerController").menuOn = false;
            CacheGameObject.Find<SpectatorMovement>("MainCamera").disable = false;
            CacheGameObject.Find<MouseLook>("MainCamera").disable = false;
        }
        else
        {
            IN_GAME_MAIN_CAMERA.isPausing = false;
            if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
            {
                Screen.showCursor = false;
                Screen.lockCursor = true;
            }
            else
            {
                Screen.showCursor = false;
                Screen.lockCursor = false;
            }
            CacheGameObject.Find<FengCustomInputs>("InputManagerController").menuOn = false;
            CacheGameObject.Find<FengCustomInputs>("InputManagerController").justUPDATEME();
        }
    }
}