using mItems;
using UnityEngine;

public class BTN_RESULT_TO_MAIN : MonoBehaviour
{
    private void OnClick()
    {
        Time.timeScale = 1f;
        if (PhotonNetwork.connected)
            PhotonNetwork.Disconnect();
        IN_GAME_MAIN_CAMERA.gametype = GAMETYPE.STOP;
        FengGameManagerMKII.MKII.gameStart = false;
        Screen.lockCursor = false;
        Screen.showCursor = true;
        CacheGameObject.Find<FengCustomInputs>("InputManagerController").menuOn = false;
        Destroy(CacheGameObject.Find("MultiplayerManager"));
        Application.LoadLevel("menu");
    }
}