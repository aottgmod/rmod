using mItems;
using UnityEngine;

public class Btn_to_Main_from_CC : MonoBehaviour
{
    private void OnClick()
    {
        PhotonNetwork.Disconnect();
        Screen.lockCursor = false;
        Screen.showCursor = true;
        IN_GAME_MAIN_CAMERA.gametype = GAMETYPE.STOP;
        FengGameManagerMKII.MKII.gameStart = false;
        CacheGameObject.Find<FengCustomInputs>("InputManagerController").menuOn = false;
        Destroy(CacheGameObject.Find("MultiplayerManager"));
        Application.LoadLevel("menu");
    }
}