using mItems;
using UnityEngine;

public class BTN_SetDefault : MonoBehaviour
{
    private void OnClick()
    {
        CacheGameObject.Find<FengCustomInputs>("InputManagerController").setToDefault();
        CacheGameObject.Find<FengCustomInputs>("InputManagerController").showKeyMap();
    }
}