using ExitGames.Client.Photon;
using mItems;
using UnityEngine;

public class BTN_choose_human : MonoBehaviour
{
    public bool isPlayerAllDead()
    {
        var num = 0;
        var num2 = 0;
        foreach (var player in PhotonNetwork.playerList)
        {
            if ((int) player.customProperties[PhotonPlayerProperty.isTitan] == 1)
            {
                num++;
                if ((bool) player.customProperties[PhotonPlayerProperty.dead])
                    num2++;
            }
        }
        return num == num2;
    }

    private void OnClick()
    {
        var selection = CacheGameObject.Find<UIPopupList>("PopupListCharacterHUMAN").selection;
        NGUITools.SetActive(CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0], true);
        FengGameManagerMKII.MKII.needChooseSide = false;
        if (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE)
            FengGameManagerMKII.MKII.checkpoint = CacheGameObject.Find("PVPchkPtH");
        if (!PhotonNetwork.isMasterClient && (FengGameManagerMKII.MKII.roundTime > 60f))
        {
            if (!isPlayerAllDead())
                FengGameManagerMKII.MKII.NOTSpawnPlayer(selection);
            else
            {
                FengGameManagerMKII.MKII.NOTSpawnPlayer(selection);
                FengGameManagerMKII.MKII.photonView.RPC("restartGameByClient", PhotonTargets.MasterClient);
            }
        }
        else
        {
            if ((IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.BOSS_FIGHT_CT) || (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.TROST) || (IN_GAME_MAIN_CAMERA.gamemode == GAMEMODE.PVP_CAPTURE))
            {
                if (isPlayerAllDead())
                {
                    FengGameManagerMKII.MKII.NOTSpawnPlayer(selection);
                    FengGameManagerMKII.MKII.photonView.RPC("restartGameByClient", PhotonTargets.MasterClient);
                }
                else
                    FengGameManagerMKII.MKII.SpawnPlayer(selection);
            }
            else
                FengGameManagerMKII.MKII.SpawnPlayer(selection);
        }
        UIReferArray uirefer = CacheGameObject.Find<UIReferArray>("UI_IN_GAME");
        NGUITools.SetActive(uirefer.panels[1], false);
        NGUITools.SetActive(uirefer.panels[2], false);
        NGUITools.SetActive(uirefer.panels[3], false);
        IN_GAME_MAIN_CAMERA.usingTitan = false;
        IN_GAME_MAIN_CAMERA.maincamera.setHUDposition();
        PhotonNetwork.player.SetCustomProperties(new Hashtable { { PhotonPlayerProperty.character, selection } });
    }
}