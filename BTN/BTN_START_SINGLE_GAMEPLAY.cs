using mItems;
using UnityEngine;

public class BTN_START_SINGLE_GAMEPLAY : MonoBehaviour
{
    private void OnClick()
    {
        var selection = CacheGameObject.Find<UIPopupList>("PopupListMap").selection;
        var str2 = CacheGameObject.Find<UIPopupList>("PopupListCharacter").selection;
        var num = !CacheGameObject.Find<UICheckbox>("CheckboxHard").isChecked ? (!CacheGameObject.Find<UICheckbox>("CheckboxAbnormal").isChecked ? 0 : 2) : 1;
        IN_GAME_MAIN_CAMERA.difficulty = num;
        IN_GAME_MAIN_CAMERA.gametype = GAMETYPE.SINGLE;
        IN_GAME_MAIN_CAMERA.singleCharacter = str2.ToUpper();
        if (IN_GAME_MAIN_CAMERA.cameraMode == CAMERA_TYPE.TPS)
            Screen.lockCursor = true;
        Screen.showCursor = false;
        if (selection == "trainning_0")
            IN_GAME_MAIN_CAMERA.difficulty = -1;
        FengGameManagerMKII.level = selection;
        Application.LoadLevel(LevelInfo.getInfo(selection).mapName);
    }
}