﻿using System.Collections.Generic;
using UnityEngine;

namespace mItems
{
    public static class CacheGameObject
    {
        private static readonly Dictionary<string, GameObject> cache = new Dictionary<string, GameObject>();
        private static readonly Dictionary<string, Component> cacheType = new Dictionary<string, Component>();

        public static T Find<T>(string name) where T : Component
        {
            var typename = name + typeof(T).FullName;
            if (cacheType.ContainsKey(typename))
            {
                var component = cacheType[typename];
                if (component != null)
                {
                    var cached = component as T;
                    if (cached != null)
                        return cached;
                    return (T)(cacheType[typename] = component.GetComponent<T>());
                }
            }
            var find = Find(name);
            if (find != null)
                return (T)(cacheType[typename] = find.GetComponent<T>());
            if (find = GameObject.Find(name))
                return (T)(cacheType[typename] = find.GetComponent<T>());
            return null;
        }

        public static GameObject Find(string name)
        {
            var lower = name.ToLower().Trim();
            switch (lower)
            {
                case "maincamera":
                {
                    if (cache.ContainsKey(name))
                    {
                        if (cache[name] != null)
                            return cache[name];
                    }
                    return cache[name] = GameObject.Find(name);
                }
                case "aottg_hero1":
                case "aottg_hero1(clone)":
                case "colossal_titan":
                case "femaletitan":
                case "female_titan":
                case "crawler":
                case "punk":
                case "abberant":
                case "jumper":
                case "titan":
                case "tree":
                case "cube001":
                    return GameObject.Find(name);
            }
            GameObject obj;
            if (cache.ContainsKey(name))
            {
                if (obj = cache[name])
                {
                    if (obj.activeInHierarchy || lower.StartsWith("ui") || lower.StartsWith("label") || lower.StartsWith("ngui"))
                        return obj;
                }
            }
            if (obj = GameObject.Find(name))
                return cache[name] = obj;
            return obj;
        }
    }
}