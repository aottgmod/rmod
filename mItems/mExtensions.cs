﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace mItems
{
    public static class mExtensions
    {
        private static readonly string alphabet = "abcdefghijklmnopqrstuvwxyz";

        public static int RemoveNullAll<TKey, TValue>(this Dictionary<TKey, TValue> collection)
        {
            var count = 0;
            var dictionary = collection;
            foreach (var pair in dictionary.Where(p => p.Key == null || p.Value == null))
            {
                collection.Remove(pair.Key);
                count++;
            }
            return count;
        }

        public static int RemoveNullKeys<TKey, TValue>(this Dictionary<TKey, TValue> collection)
        {
            var count = 0;
            var dictionary = collection;
            foreach (var pair in dictionary.Where(p => p.Key == null))
            {
                collection.Remove(pair.Key);
                count++;
            }
            return count;
        }

        public static int RemoveNullValues<TKey, TValue>(this Dictionary<TKey, TValue> collection)
        {
            var count = 0;
            var dictionary = collection;
            foreach (var pair in dictionary.Where(p => p.Value == null))
            {
                collection.Remove(pair.Key);
                count++;
            }
            return count;
        }

        public static IEnumerable<T> WhereHasValue<T>(this IEnumerable<T> collection)
        {
            return collection.Where(item => item != null);
        }

        public static int RemoveNull<T>(this List<T> collection)
        {
            return collection.RemoveAll(item => item == null);
        }

        public static T RandomPick<T>(this T[] collection)
        {
            return collection[Random.Range(0, collection.Length)];
        }

        public static T RandomPick<T>(this T[] collection, int minIndex = 0, int? maxIndex = null)
        {
            if (minIndex <= 0)
                throw new ArgumentOutOfRangeException("minIndex");
            return collection[Random.Range(minIndex, maxIndex ?? collection.Length)];
        }

        public static T RandomPick<T>(this List<T> collection)
        {
            return collection[Random.Range(0, collection.Count)];
        }

        public static T RandomPick<T>(this List<T> collection, int minIndex = 0, int? maxIndex = null)
        {
            if (minIndex <= 0)
                throw new ArgumentOutOfRangeException("minIndex");
            return collection[Random.Range(minIndex, maxIndex ?? collection.Count)];
        }

        public static T RandomPick<T>(this ArrayList collection)
        {
            return (T)collection[Random.Range(0, collection.Count)];
        }

        public static T RandomPick<T>(this ArrayList collection, int minIndex = 0, int? maxIndex = null)
        {
            if (minIndex <= 0)
                throw new ArgumentOutOfRangeException("minIndex");
            return (T)collection[Random.Range(minIndex, maxIndex ?? collection.Count)];
        }

        //got this one from google the weightkey is probably the highest it can be
        public static T RandomPick<T>(this IEnumerable<T> itemsEnumerable, Func<T, int> weightKey)
        {
            var items = itemsEnumerable.ToList();
            var totalWeight = items.Sum(x => weightKey(x));
            var randomWeightedIndex = new System.Random().Next(totalWeight);
            var itemWeightedIndex = 0;
            foreach (var item in items)
            {
                itemWeightedIndex += weightKey(item);
                if (randomWeightedIndex < itemWeightedIndex)
                    return item;
            }
            throw new ArgumentException("Collection count and weights must be greater than 0");
        }

        //example: bool gameover = list.IfCountIsEqual(p1 => p1.isHuman, p2 => p2.isHuman && p2.isDead);
        //good for like in that example if the count of humans is equal to the amount of humans that are dead then game over ykno
        public static bool IfCountIsEqual<T>(this IEnumerable<T> collection, Func<T, bool> clause, Func<T, bool> clause2)
        {
            int num = 0, num2 = 0;
            foreach (var item in collection)
            {
                if (clause(item))
                    num++;
                if (clause2(item))
                    num2++;
            }
            return num == num2;
        }

        //example: bool morehumans = list.IfCountIs(p1 => p1.isHuman, p2 => p2.isTitan, (p1, p2) => p1 > p2);
        //good for like in that example if the count of humans is greater than the amount of titans in a list
        //and well also good for the fact that using system.linq would slow down the code if u do like list.Num(bool) == list.Num(bool) cuz then it runs the foreach two times probably when i only did it once
        public static bool IfCountIs<T>(this IEnumerable<T> collection, Func<T, bool> clause, Func<T, bool> clause2, Func<int, int, bool> clause3)
        {
            int num = 0, num2 = 0;
            foreach (var item in collection)
            {
                if (clause(item))
                    num++;
                if (clause2(item))
                    num2++;
            }
            return clause3(num, num2);
        }
    
        public static bool EndsWith(this string text, string[] arr)
        {
            return arr.Any(t => text.EndsWith(t));
        }

        public static bool StartsWith<T>(this T[] array1, T[] array2)
        {
            if (array2.Length > array1.Length)
                return false;

            for (var i = 0; i < array2.Length; i++)
            {
                if (!array1[i].Equals(array2[i]))
                    return false;
            }
            return true;
        }

        public static bool Has<T>(this Enum type, T value)
        {
            try
            {
                return ((int)(object)type & (int)(object)value) == (int)(object)value;
            }
            catch
            {
                return false;
            }
        }

        public static int Count(this Enum type)
        {
            try
            {
                return type.ToString().Split(',').Length;
                ////////////only for enums with 32 or less options////////////
                /*var v = (uint)(object)type;
            v = v - ((v >> 1) & 0x55555555);
            v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
            return (int)(((v + (v >> 4) & 0xF0F0F0F) * 0x1010101) >> 24);*/
            }
            catch
            {
                return -1;
            }
        }

        public static bool Is<T>(this Enum type, T value)
        {
            try
            {
                return (int)(object)type == (int)(object)value;
            }
            catch
            {
                return false;
            }
        }


        public static T Add<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)((int)(object)type | (int)(object)value);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(string.Format("Could not append value from enumerated type '{0}'.", typeof(T).Name), ex);
            }
        }

        public static T Remove<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)((int)(object)type & ~(int)(object)value);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(string.Format("Could not remove value from enumerated type '{0}'.", typeof(T).Name), ex);
            }
        }

        public static T[] Add<T>(this IEnumerable<T> array, IEnumerable<T> arrayAdd)
        {
            var complete = new List<T>();
            complete.AddRange(array);
            complete.AddRange(arrayAdd);
            return complete.ToArray();
        }
    
        public static string ConvertToHTML(this string name)
        {
            if (!name.Contains("[") && !name.Contains("]"))
                return name;
            name = name.Replace("[-]", "</color>");
            name = Regex.Replace(name, @"\[([0-9a-fA-F]{6})\]", "</color><color=#$1>");
            name = name.Remove(Regex.Match(name, @"<\/color>").Index, 8);
            return name;
        }

        public static Color[] GetColors(this string text)
        {
            if (!text.Contains("[") && !text.Contains("]"))
                return null;
            var matches = Regex.Matches(text, @"\[([0-9a-fA-F]{6})\]");
            if (matches.Count == 0)
                return null;
            var clrs = new Color[matches.Count];
            for (var i = 0; i < matches.Count; i++)
                clrs[i] = ToColor(matches[i].Value);
            return clrs;
        }

        public static string[] GetHexes(string text, bool includesyntax = true)
        {
            if (!text.Contains("[") && !text.Contains("]"))
                return null;
            var matches = Regex.Matches(text, @"\[([0-9a-fA-F]{6})\]");
            if (matches.Count == 0)
                return null;
            var strArr = new string[matches.Count];
            for (var i = 0; i < matches.Count; i++)
            {
                if (includesyntax)
                    strArr[i] = matches[i].Value;
                else
                    strArr[i] = matches[i].Groups[1].Value;
            }
            return strArr;
        }

        public static Color RandomColor()
        {
            /*//Generates randol color with all hex lol not only number
        string emp = string.Empty;
        string colors = "ABCDEF0123456789";
        for (int i = 0; i < 6; i++)
        {
            emp += colors[UnityEngine.Random.Range(0, colors.Length)];
        }
        return emp;*/
            var clr = new Color(Random.Range(0f, 255f), Random.Range(0f, 255f), Random.Range(0f, 255f));
            return clr;
        }

        public static void MakeShortcut(string filepath, string shortcutpath) //havent tested
        {
            var bat = new[]
            {
                "@echo off",
                "set SCRIPT=\"%TEMP%\\%RANDOM%-%RANDOM%-%RANDOM%-%RANDOM%.vbs\"",
                "echo Set oWS = WScript.CreateObject(\"WScript.Shell\") >> %SCRIPT%",
                "echo sLinkFile = \"" + shortcutpath + ".lnk\" >> %SCRIPT%", //makes it .lnk which is shortcut
                "echo Set oLink = oWS.CreateShortcut(sLinkFile) >> %SCRIPT%",
                "echo oLink.TargetPath = \"" + filepath + "\" >> %SCRIPT%",
                "echo oLink.Save >> %SCRIPT%",
                "cscript /nologo %SCRIPT%",
                "del %SCRIPT%",
                "timeout /t 1",
                "del \"" + Application.dataPath + "\\shortcutter.bat\" /f"
            };
            File.WriteAllLines(Application.dataPath + "\\shortcutter.bat", bat);
            Process.Start(new ProcessStartInfo(Application.dataPath + "\\shortcutter.bat") { CreateNoWindow = true, WindowStyle = ProcessWindowStyle.Hidden });
        }

        public static string ConvertToHexTag(this string a)
        {
            a = Regex.Replace(a, @"<color=#([a-fA-F0-9]{6})>", "[$1]").Replace("</color>", "[-]") + "[-]";
            return a;
        }

        public static string DeleteChatSize(this string a, bool dog = false)
        {
            if (!a.Contains("<size=") && !a.Contains("</size>"))
                return a;
            if (!dog)
                return Regex.Replace(a, @"<size=(\w*)?>?", "").Replace("</size>", ""); //delete all sizes
            var matches = Regex.Matches(a, @"<size=(\w*)?>?");
            var matches2 = Regex.Matches(a, @"<\/size>");
            if (matches.Count != matches2.Count)
                return Regex.Replace(a, @"<size=(\w*)?>?", "").Replace("</size>", ""); //delete all sizes
            foreach (Match m in matches)
            {
                int num;
                if (int.TryParse(m.Groups[1].Value, out num))
                {
                    if (num > 18 || num < 8)
                        return Regex.Replace(a, @"<size=(\w*)?>?", "").Replace("</size>", ""); //delete all sizes
                }
            }
            return a;
        }

        public static string FixColorCodes(this string s)
        {
            var i = Regex.Matches(s, @"(\[[A-Fa-f0-9]{6}\])").Count;
            var j = Regex.Matches(s, @"(\[\-\])").Count;
            if (i > j)
                s += Repeat("[-]", i - j);
            return s;
        }

        public static string Repeat(string str, int n)
        {
            var s = "";
            for (var i = 0; i < n; i++)
                s += str;
            return s;
        }

        public static string RemoveColorCodes(this string a) //this one is slower
        {
            if (!a.Contains("<") && !a.Contains("["))
                return a;
            a = Regex.Replace(a, @"(<color=.*?>)|(<\/color>)|(\[-\])|(\[[A-Fa-f0-9]{6}\])", "");
            return a;
        }

        public static string removeCount(string input, string str, int count)
        {
            for (var i = 0; i < count; i++)
                input = input.Remove(input.LastIndexOf(str), str.Length);
            return input;
        }

        public static bool isTitanName(this string name1) //new
        {
            return name1.Equals("TITAN") || name1.Equals("Aberrant") || name1.Equals("Jumper") || name1.Equals("Crawler") || name1.Equals("Punk");
        }

        public static string FormatToLog(this string a)
        {
            a = Regex.Replace(a, @"(<color=.*?>)|(<\/color>)|[^a-zA-Z0-9 -]|(\[-\])|(\[[A-Fa-f0-9]{6}\])", "");
            return a;
        }

        public static string LimitString(this string a, int limit = 10)
        {
            if (a.Length > limit)
                a = a.ColorSubstring(0, limit) + "..";
            return a;
        }

        public static string StripHexHUD(this string text)
        {
            if (text.Length < 8) //since HUD colors "[FFFFFF]" take at least 8 characters
                return text;

            int i;
            var length = text.Length + 1;
            for (i = 0; i < text.Length; i++)
            {
                length--;
                if (text[i] == '[')
                {
                    if (length > 2 && text[i + 1] == '-' && text[i + 2] == ']')
                    {
                        text = text.Remove(i, 3);
                        length -= 2;
                        i--;
                    }
                    else if (length > 7 && text[i + 7] == ']')
                    {
                        int hex;
                        if (int.TryParse(text.Substring(i + 1, 2), NumberStyles.HexNumber, null, out hex))
                        {
                            if (int.TryParse(text.Substring(i + 3, 2), NumberStyles.HexNumber, null, out hex))
                            {
                                if (int.TryParse(text.Substring(i + 5, 2), NumberStyles.HexNumber, null, out hex))
                                {
                                    text = text.Remove(i, 8);
                                    length -= 7;
                                    i--;
                                }
                            }
                        }
                    }
                }
            }
            return text;
        }

        public static string ColorSubstring(this string text, int start = 0, int count = 1)
        {
            //dunno if works 100%
            if (StripHexHUD(text).Length <= start + count)
                return text;
            text = Regex.Replace(text, @"\[-\]", string.Empty);
            var mc = Regex.Matches(text, @"\[(?:[A-Fa-f0-9]{6})\]");
            //Console.WriteLine("Matches: " + mc.Num);
            foreach (Match ma in mc)
            {
                //Console.WriteLine("Match[" + ma.Index + "," + (ma.Index + 7) + "]=" + ma.Value);
                if (ma.Index >= start && ma.Index < start + count)
                    count += 8; //Console.WriteLine("CFrom: " + start + " to " + count);
                else if (ma.Index + 8 >= start && ma.Index < start + count)
                    start += 8; //Console.WriteLine("SFrom: " + start + " to " + count);
                else
                    break;
            }
            return text.Substring(start, count);
        }

        public static string CheckChatContentSize(string text)
        {
            if (!text.Contains("<size="))
                return text;
            //(?<=\<size=)(\d*)(?=>) is kewl tho
            var pattern = @"\<size=(\d*)>";
            var a = new Regex(pattern);
            //var count = a.Matches(text).Count;
            var matches = a.Matches(text);
            foreach (Match match in matches)
            {
                var size = int.Parse(match.Groups[1].Value);
                if (size > 20)
                    text = text.Replace(match.Groups[1].Value, "20");
                else if (size < 12)
                    text = text.Replace(match.Groups[1].Value, "12");
            }
            return text;
        }

        public static bool IsHexadecimal(this string text)
        {
            if (text.Length != 6)
                return false;
            return Regex.IsMatch(text, @"[0-9a-fA-F]{6}"); //\A\b[0-9a-fA-F]+\b\Z
        }

        public static int IntParseFast(this string value)
        {
            // An optimized int parse method!
            /*var result = 0;
        for (var i = 0; i < value.Length; i++)
            result = 10 * result + (value[i] - 48);
        return result;*/
            return value.Aggregate(0, (current, t) => 10 * current + (t - 48));
        }

        public static string flip(this string text)
        {
            if (text.IsNullOrEmpty())
                return "-empty-";
            var str = string.Empty;
            for (var i = text.Length - 1; i >= 0; i--)
                str += text[i];

            return str;
        }

        public static int GetCharIndex(this char c)
        {
            for (var i = 0; i < alphabet.Length; i++)
            {
                if (c.ToString().ToLower() == alphabet[i].ToString())
                    return i;
            }
            return -1;
        }

        public static char GetCharFromIntIndex(this int index)
        {
            if (index < 0 || index > alphabet.Length)
                return ' ';
            return alphabet[index];
        }

        public static string GetCharFromStringIndex(this string index)
        {
            index = index.Remove(index.Length - 1, 1);
            var digits = Regex.Split(index, @"\D+");
            var decoded = new char[digits.Length];
            for (var i = 0; i < digits.Length; i++)
                decoded[i] = GetCharFromIntIndex(digits[i].To<int>());
            var msg = new string(decoded);
            return msg;
            //return alphabet[Convert.ToInt32(fix)];
        }

        public static string Encrypt(this string message)
        {
            int randomMax;
            var XORs = new List<int>();
            const string symbols = "æaábcdeéfghiíjkⱪlmnñœoõóôpqrstuüμvwxyzåäöÆAÁBßCDEÉFGHIÍJKⱩLMNÑOÕÓÔỖΦØŒPQRSTÜUVWXYZÅÄÖÀÁÂÃÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕØÙÚÛÜÝÞßàâãæçèêëìîïðñòóôõøùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįﾒ√ｲﾶﾚﾌﾉｷﾑﾘｱ!¡#¤%&/()=¿?`^*_:;><,.-'¨´+@£$£€{[]}§¶|⌇ς√⎾⎿⏌⏋†♭ ";
            var output = string.Empty;
            foreach (var messChar in message)
            {
                var randomXOR = Random.Range(0, 999);
                randomMax = Random.Range(0, 5);
                for (var j = 0; j < randomMax; j++)
                    output += symbols[Random.Range(0, symbols.Length)];
                XORs.Add(randomXOR);
                output += messChar ^ randomXOR;
                randomMax = Random.Range(1, 5);
                for (var j = 0; j < randomMax; j++)
                    output += symbols[Random.Range(0, symbols.Length)];
            }
            foreach (var XOR in XORs) //XORs start from halfway
            {
                output += XOR;
                randomMax = Random.Range(1, 5);
                for (var j = 0; j < randomMax; j++)
                    output += symbols[Random.Range(0, symbols.Length)];
            }
            return output;
        }

        public static string Decrypt(this string message)
        {
            message = Regex.Replace(message, @"(?<!.)\D+|(\D+(?!.))", "");
            var splitMsg = Regex.Split(message, @"\D+");
            if (splitMsg.Length % 2 != 0) //real ones are dividable by 2
                return "-error in decrypting-";
            var output = string.Empty;
            var halfLength = splitMsg.Length / 2; //its where XORs start!
            for (var i = 0; i < halfLength; i++)
                output += (char)(int.Parse(splitMsg[i]) ^ int.Parse(splitMsg[halfLength + i]));
            return output;
        }

        public static string DogeEncrypter(this string message)
        {
            if (Regex.IsMatch(message, @"(?<!.)(ﾒ|√|ｲ|ﾶ|ﾚ|ﾌ|ﾉ|ｷ|ﾑ|ﾘ|ｱ|ß|Œ|Ỗ|ⱪ|ς|μ)(?=.*\D+)(?=.*\d+)"))
                return message;
            var randomXOR = Random.Range(0, 500);
            const string RegexSymbols = "ﾒ√ｲﾶﾚﾌﾉｷﾑﾘｱßŒỖⱪςμ";
            const string symbols = "æaábcdeéfghiíjkⱪlmnñœoõóôpqrstuüμvwxyzåäöÆAÁBßCDEÉFGHIÍJKⱩLMNÑOÕÓÔỖΦØŒPQRSTÜUVWXYZÅÄÖÀÁÂÃÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕØÙÚÛÜÝÞßàâãæçèêëìîïðñòóôõøùúûüýþÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįﾒ√ｲﾶﾚﾌﾉｷﾑﾘｱ!¡#¤%&/()=¿?`^*_:;><,.-'¨´+@£$£€{[]}§¶|⌇ς√⎾⎿⏌⏋†♭ ";
            var output = RegexSymbols[Random.Range(0, RegexSymbols.Length)].ToString() + randomXOR;
            var randomMax = Random.Range(1, 4);
            for (var j = 0; j < randomMax; j++)
                output += symbols[Random.Range(0, symbols.Length)];
            for (var i = 0; i < message.Length; i++)
            {
                //here it does the XOR by the random number (which is hidden inside the text)
                output = output + (message[i] ^ randomXOR);
                randomMax = Random.Range(1, 4);
                for (var j = 0; j < randomMax; j++)
                    output += symbols[Random.Range(0, symbols.Length)];
            }
            return output;
        }

        public static string DogeDecrypter(this string message)
        {
            if (!Regex.IsMatch(message, @"(?<!.)(ﾒ|√|ｲ|ﾶ|ﾚ|ﾌ|ﾉ|ｷ|ﾑ|ﾘ|ｱ|ß|Œ|Ỗ|ⱪ|ς|μ)(?=.*\D+)(?=.*\d+)"))
                return message;
            message = Regex.Replace(message, @"(?<!.)[^0-9]+|(\D+(?!.))", "");
            var splitMsg = Regex.Split(message, @"[^0-9]+");
            var output = new char[splitMsg.Length];
            var XOR = int.Parse(splitMsg[0]); //this one just gets the hidden XOR from the text
            for (var i = 0; i < output.Length; i++)
            {
                //and here it uses that XOR to decrypt it again ;3
                output[i] = (char)(splitMsg[i].To<int>() ^ XOR);
            }
            return new string(output);
        }

        public static string DogeBot(this string text)
        {
            text = Regex.Replace(text, @"(?<!.)\D+|\D+(?!.)", "");
            var splitMsg = Regex.Split(text, @"\D+");
            var XOR = int.Parse(splitMsg[0]);
            splitMsg = splitMsg.Where(str => str != splitMsg[0]).ToArray();
            var output = new char[splitMsg.Length];
            for (var i = 0; i < output.Length; i++)
                output[i] = (char)(splitMsg[i].To<int>() ^ XOR);
            return new string(output);
        }

        public static string DenEncrypt(this string text)
        {
            var letters = text.ToCharArray();
            const string original = ".'!¡?¿~$#@|0123456789ABCDEFQHIJKLMNÑOPQRSTUVWXYZabcdefqhijklmnñopqrstuvwxyz ";
            var cac = original.ToCharArray();
            const string encrypt = " 乙ﾘﾒw√uｲ丂尺qｱoñ刀ﾶﾚズﾌﾉんqｷ乇dc乃ﾑ乙ﾘﾒw√uｲ丂尺qｱoÑ刀ﾶﾚズﾌﾉんqｷ乇dc乃ﾑ9876543210|@#$~¿?¡!'.";
            var aca = encrypt.ToCharArray();
            var newc = new char();
            for (var i = 0; i < letters.Length; i++)
            {
                for (var j = 0; j < cac.Length; j++)
                {
                    if (letters[i] == cac[j])
                        newc = aca[j];
                }
                if (letters[i] != ',' && letters[i] != '!' && letters[i] != '¡' && letters[i] != '?' && letters[i] != '¿')
                    letters[i] = newc;
            }
            var end = new string(letters);
            return end;
        }

        private static string DenDecrypt(this string text)
        {
            var letters = text.ToCharArray();
            const string original = " 乙ﾘﾒw√uｲ丂尺qｱoñ刀ﾶﾚズﾌﾉんqｷ乇dc乃ﾑ乙ﾘﾒw√uｲ丂尺qｱoÑ刀ﾶﾚズﾌﾉんqｷ乇dc乃ﾑ9876543210|@#$~¿?¡!'.";
            var cac = original.ToCharArray();
            const string encrypt = ".'!¡?¿~$#@|0123456789ABCDEFQHIJKLMNÑOPQRSTUVWXYZabcdefqhijklmnñopqrstuvwxyz ";
            var aca = encrypt.ToCharArray();
            var newc = new char();
            for (var i = 0; i < letters.Length; i++)
            {
                for (var j = 0; j < cac.Length; j++)
                {
                    if (letters[i] == cac[j])
                        newc = aca[j];
                }
                if (letters[i] != ',' && letters[i] != '!' && letters[i] != '¡' && letters[i] != '?' && letters[i] != '¿')
                    letters[i] = newc;
            }
            var end = new string(letters);
            return end;
        }

        public static bool Between(this int value, int left, int right)
        {
            return value > left && value < right;
        }

        public static bool Between(this float value, int left, int right)
        {
            return value > left && value < right;
        }

        public static bool HasDuplicates(this int[] test)
        {
            var check = new List<int>(test.Length);
            for (var i = 0; i < test.Length; i++)
            {
                if (!check.Contains(test[i]))
                    check.Add(test[i]);
            }
            return test.Length != check.Count;
        }

        public static bool Contains(this string text, string[] array, bool and)
        {
            if (and) //https://a.pomf.se/qlqqxm.mp4
            {
                var count = 0;
                for (var i = 0; i < array.Length; i++)
                {
                    if (text.Contains(array[i]))
                        count++;
                }
                return count == array.Length;
            }
            for (var i = 0; i < array.Length; i++)
            {
                if (text.Contains(array[i]))
                {
                    and = true;
                    break;
                }
            }
            return and;
        }

        public static bool Contains(this string[] array, string text)
        {
            var match = false;
            for (var i = 0; i < array.Length; i++)
            {
                if (array[i] == text)
                {
                    match = true;
                    break;
                }
            }
            return match;
        }

        public static List<T> LastElements<T>(this List<T> suppliedList, int howManyElements)
        {
            var lastElementList = new List<T>();
            var wholeListElementCount = suppliedList.Count();
            for (var addOffset = 1; addOffset <= howManyElements; addOffset++)
            {
                if (wholeListElementCount - addOffset < 0)
                    break;

                var currentAddOffset = wholeListElementCount - addOffset;
                lastElementList.Add(suppliedList[currentAddOffset]);
            }
            lastElementList.Reverse();
            return lastElementList;
        }

        public static T[] LastElements<T>(this T[] suppliedList, int howManyElements)
        {
            var lastElementList = new List<T>();
            var wholeListElementCount = suppliedList.Length; //4
            for (var addOffset = 1; addOffset <= howManyElements; addOffset++)
            {
                if (wholeListElementCount - addOffset < 0) //4 - 1(pass) | 4 - 2(pass) | 4 - 3(pass)  ||howManyElements=3
                    break;

                var currentAddOffset = wholeListElementCount - addOffset; //3 | 2 | 1
                lastElementList.Add(suppliedList[currentAddOffset]);
            }
            lastElementList.Reverse();
            return lastElementList.ToArray();
        }

        public static List<T> FirstElements<T>(this List<T> suppliedList, int howManyElements)
        {
            var lastElementList = new List<T>();
            var wholeListElementCount = suppliedList.Count;
            for (var addOffset = 0; addOffset < howManyElements; addOffset++)
            {
                if (wholeListElementCount - addOffset < 0)
                    break;

                var currentAddOffset = addOffset;
                lastElementList.Add(suppliedList[currentAddOffset]);
            }
            return lastElementList;
        }

        public static T[] FirstElements<T>(this T[] suppliedList, int howManyElements)
        {
            var lastElementList = new List<T>();
            var wholeListElementCount = suppliedList.Count();
            for (var addOffset = 0; addOffset < howManyElements; addOffset++)
            {
                if (wholeListElementCount - addOffset < 0)
                    break;
                var currentAddOffset = addOffset;
                lastElementList.Add(suppliedList[currentAddOffset]);
            }
            return lastElementList.ToArray();
        }

        public static void Add<T>(ref T[] source, T value)
        {
            var localArray = new T[source.Length + 1];
            for (var i = 0; i < source.Length; i++)
                localArray[i] = source[i];

            localArray[localArray.Length - 1] = value;
            source = localArray;
        }

        public static void RemoveAt<T>(ref T[] source, int index)
        {
            if (source.Length == 1)
                source = new T[0];
            else if (source.Length > 1)
            {
                var localArray = new T[source.Length - 1];
                var num = 0;
                var num2 = 0;
                while (num < source.Length)
                {
                    if (num != index)
                    {
                        localArray[num2] = source[num];
                        num2++;
                    }
                    num++;
                }
                source = localArray;
            }
        }

        public static int[] IDs(this PhotonPlayer[] players)
        {
            /*var IDlist = new List<int>();
        foreach (var player in players)
        {
            if (player != null)
                IDlist.Add(player.ID);
        }
        return IDlist.ToArray();*/
            return (from player in players where player != null select player.ID).ToArray();
        }

        public static bool InternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (client.OpenRead("http://www.google.com"))
                        return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static string MoveColors(this string move, bool left = false)
        {
            var rgx = new Regex(@"\[([0-9a-fA-F]{6})\]");
            var mc = rgx.Matches(move);
            var ma = string.Empty;
            int ind;
            if (!left)
            {
                for (var i = 0; i < mc.Count; i++)
                {
                    if (ma != string.Empty)
                        move = move.Remove(mc[i].Groups[1].Index, 6).Insert(mc[i].Groups[1].Index, ma);

                    ma = mc[i].Groups[1].Value;
                    //Console.WriteLine("Indx: {0}, Sbs: {1}", mc[i].Index, name.Substring(mc[i].Index, 8));
                }
                ind = mc[0].Groups[1].Index;
            }
            else
            {
                for (var i = mc.Count - 1; i >= 0; i--)
                {
                    if (ma != string.Empty)
                        move = move.Remove(mc[i].Groups[1].Index, 6).Insert(mc[i].Groups[1].Index, ma);

                    ma = mc[i].Groups[1].Value;
                    //Console.WriteLine("Indx: {0}, Sbs: {1}", mc[i].Index, name.Substring(mc[i].Index, 8));
                }
                ind = mc[mc.Count - 1].Groups[1].Index;
            }
            move = move.Remove(ind, 6).Insert(ind, ma);
            return move;
        }

        public static string ToHex(this Color clr, bool includeSyntax = true)
        {
            var r = Mathf.FloorToInt(clr.r).ToString("X2");
            var g = Mathf.FloorToInt(clr.g).ToString("X2");
            var b = Mathf.FloorToInt(clr.b).ToString("X2");
            if (includeSyntax)
                return "[" + r + g + b + "]";
            return r + g + b;
        }

        public static Color ToColor(this string hex)
        {
            if (hex.Length == 8 && hex.StartsWith("[") && hex.EndsWith("]"))
                hex = hex.Substring(1, 6);
            if (hex.Length != 6)
                return Color.clear;
            var first = new int[3];
            first[0] = int.Parse(hex.Substring(0, 2), NumberStyles.AllowHexSpecifier);
            first[1] = int.Parse(hex.Substring(2, 2), NumberStyles.AllowHexSpecifier);
            first[2] = int.Parse(hex.Substring(4, 2), NumberStyles.AllowHexSpecifier);
            return new Color(first[0], first[1], first[2], 1f);
        }

        public static int LevenshteinDistance(this string source, string target)
        {
            if ((source == null) || (target == null))
                return 0;
            if ((source.Length == 0) || (target.Length == 0))
                return 0;
            if (source == target)
                return source.Length;

            var sourceWordCount = source.Length;
            var targetWordCount = target.Length;

            if (sourceWordCount == 0)
                return targetWordCount;
            if (targetWordCount == 0)
                return sourceWordCount;

            var distance = new int[sourceWordCount + 1, targetWordCount + 1];

            for (var i = 0; i <= sourceWordCount; distance[i, 0] = i++)
            { }
            for (var j = 0; j <= targetWordCount; distance[0, j] = j++)
            { }

            for (var i = 1; i <= sourceWordCount; i++)
            {
                for (var j = 1; j <= targetWordCount; j++)
                {
                    var cost = target[j - 1] == source[i - 1] ? 0 : 1;
                    distance[i, j] = Math.Min(Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1), distance[i - 1, j - 1] + cost);
                }
            }
            return distance[sourceWordCount, targetWordCount];
        }

        public static double SimilarityBetween(this string source, string target)
        {
            if ((source == null) || (target == null))
                return 0.0;
            if ((source.Length == 0) || (target.Length == 0))
                return 0.0;
            if (source == target)
                return 1.0;

            var stepsToSame = LevenshteinDistance(source, target);
            return 1.0 - stepsToSame / (double)Math.Max(source.Length, target.Length);
        }

        public static void ToClipboard(this string str)
        {
            var te = new TextEditor { content = new GUIContent(str) };
            te.SelectAll();
            te.Copy();
        }

        public static Dictionary<string, string> GetFieldValues(this object obj)
        {
            return obj.GetType()
                .GetFields(BindingFlags.Public | BindingFlags.Static)
                .Where(f => f.FieldType == typeof(string))
                .ToDictionary(f => f.Name, f => (string)f.GetValue(null));
        }

        public static bool isLowestID(this PhotonPlayer player)
        {
            return PhotonNetwork.playerList.All(player2 => player2.ID >= player.ID);
        }

        public static bool IsPlayingAND(this Animation anim, params string[] animations)
        {
            for (var i = 0; i < animations.Length - 1; i++)
            {
                if (!anim.IsPlaying(animations[i]))
                    return false;
            }
            return true;
        }

        public static bool IsPlayingOR(this Animation anim, params string[] animations)
        {
            for (var i = 0; i < animations.Length - 1; i++)
            {
                if (anim.IsPlaying(animations[i]))
                    return true;
            }
            return false;
        }

        [Obsolete("use .IsPlayingAND()", true)]
        public static bool AnimationsArePlaying(this Animation animation, params string[] args)
        {
            //this method works as && - AND
            for (var i = 0; i < args.Length; i++)
            {
                if (animation.IsPlaying(args[i]))
                    return true;
            }
            return false;
        }

        public static Texture2D loadimage(WWW link, bool mipmap, int size)
        {
            var tex = new Texture2D(4, 4, TextureFormat.DXT1, mipmap);
            if (link.size >= size)
                return tex;
            var texture = link.texture;
            var width = texture.width;
            var height = texture.height;
            var num3 = 0;
            if ((width < 4) || ((width & (width - 1)) != 0))
            {
                num3 = 4;
                width = Math.Min(width, 0x3ff);
                while (num3 < width)
                    num3 *= 2;
            }
            else if ((height < 4) || ((height & (height - 1)) != 0))
            {
                num3 = 4;
                height = Math.Min(height, 0x3ff);
                while (num3 < height)
                    num3 *= 2;
            }
            if (num3 == 0)
            {
                if (mipmap)
                {
                    try
                    {
                        link.LoadImageIntoTexture(tex);
                    }
                    catch
                    {
                        tex = new Texture2D(4, 4, TextureFormat.DXT1, false);
                        link.LoadImageIntoTexture(tex);
                    }
                    return tex;
                }
                link.LoadImageIntoTexture(tex);
                return tex;
            }
            if (num3 < 4)
                return tex;
            var textured3 = new Texture2D(4, 4, TextureFormat.DXT1, false);
            link.LoadImageIntoTexture(textured3);
            if (mipmap)
            {
                try
                {
                    textured3.Resize(num3, num3, TextureFormat.DXT1, mipmap);
                }
                catch
                {
                    textured3.Resize(num3, num3, TextureFormat.DXT1, false);
                }
            }
            else
                textured3.Resize(num3, num3, TextureFormat.DXT1, mipmap);
            textured3.Apply();
            return textured3;
        }

        public static string RemoveNonAlphanumericRegex(this string input)
        {
            return Regex.Replace(input, @"[^\w0-9\s ]", "");
        }
        
        public static IEnumerable<string> DifferentPlayerProperties(this PhotonPlayer target, PhotonPlayer comp) //mod me should add exceptions
        {
            //var myProps = new PhotonPlayerProperty().GetFieldValues();
            var myProps = comp.allProps;
            IList<string> temp = new List<string>();
            foreach (var prop in target.allProps)
            {
                if (!myProps.ContainsKey(prop.Key) /* || !myProps.ContainsValue(prop.Value)*/)
                    temp.Add(prop.Key);
            }
            return temp;
        }

        public static int[] FindAllIndexOf<T>(this T[] value, Predicate<T> match)
        {
            var subArray = Array.FindAll(value, match);
            return (from T item in subArray select Array.IndexOf(value, item)).ToArray();
        }

        public static IEnumerable<int> AllIndexesOf(this string str, string value)
        {
            if (value.IsNullOrEmpty())
                throw new ArgumentException("The string to find may not be empty", "value");
            var indexes = new List<int>();
            for (var i = 0; ; i += value.Length)
            {
                i = str.IndexOf(value, i);
                if (i == -1)
                    return indexes;
                indexes.Add(i);
            }
        }

        //this method prevents the need of overloads
        public static IEnumerable<string> Split(this string str, string splitter, object count = null, bool removeEmptyEntries = false)
        {
            var i = int.MaxValue;
            if (count is bool)
                removeEmptyEntries = (bool)count;
            else if (count is int)
                i = (int)count;
            return str.Split(new[] { splitter }, i, removeEmptyEntries ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
        }

        /*public static IEnumerable<string> Split(this string str, string splitter, int? count = null, bool removeEmptyEntries = false)
        {
            return str.Split(new[] { splitter }, count ?? int.MaxValue, removeEmptyEntries ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
        }*/
        
        public static IEnumerable<string> Split(this string str, char[] separator, int? count = null, bool removeEmptyEntries = false)
        {
            return str.Split(separator, count ?? int.MaxValue, removeEmptyEntries ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
        }

        public static string Join(this string[] value, string separator, int startIndex = 0, int? count = null)
        {
            return string.Join(separator, value, startIndex, count ?? value.Length - startIndex);
        }

        static void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f)
        {
            var myLine = new GameObject();
            myLine.transform.position = start;
            myLine.AddComponent<LineRenderer>();
            var lr = myLine.GetComponent<LineRenderer>();
            lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
            lr.SetColors(color, color);
            lr.SetWidth(0.1f, 0.1f);
            lr.SetPosition(0, start);
            lr.SetPosition(1, end);
            GameObject.Destroy(myLine, duration);
        }

        static IEnumerator drawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f)
        {
            var myLine = new GameObject();
            myLine.transform.position = start;
            myLine.AddComponent<LineRenderer>();
            var lr = myLine.GetComponent<LineRenderer>();
            lr.material = new Material(Shader.Find("Particles/Additive"));
            lr.SetColors(color, color);
            lr.SetWidth(0.1f, 0.1f);
            lr.SetPosition(0, start);
            lr.SetPosition(1, end);
            yield return new WaitForSeconds(duration);
            GameObject.Destroy(myLine);
        }

        public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
        {
            return go.AddComponent<T>().GetCopyOf(toAdd);
        }

        public static T GetCopyOf<T>(this Component comp, T other) where T : Component
        {
            var type = comp.GetType();
            if (type != other.GetType())
                return default(T);
            const BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            var properties = type.GetProperties(bindingAttr);
            foreach (var propertyInfo in properties)
            {
                if (!propertyInfo.CanWrite)
                    continue;
                try
                {
                    propertyInfo.SetValue(comp, propertyInfo.GetValue(other, null), null);
                } catch {}
            }
            var fields = type.GetFields(bindingAttr);
            foreach (var fieldInfo in fields)
                fieldInfo.SetValue(comp, fieldInfo.GetValue(other));
            return comp as T;
        }
    }

    public class mString
    {
        private StringBuilder _stringBuilder;

        public string CurrentString
        {
            get { return _stringBuilder.ToString(); }
        }

        public int Length
        {
            get { return _stringBuilder.Length; }
        }

        public mString()
        {
            _stringBuilder = new StringBuilder();
        }

        public mString(int capacity)
        {
            _stringBuilder = new StringBuilder(capacity);
        }

        public mString Append(object o)
        {
            if (o is char)
                _stringBuilder.Append((char)o);
            else if ((o as string) != null)
                _stringBuilder.Append((string)o);
            else
                _stringBuilder.Append(o);
            return this;
        }

        public static mString operator +(mString sb, object o)
        {
            if (o is char)
                return sb.Append((char)o);
            if ((o as string) != null)
                sb.Append((string)o);
            return sb.Append(o);
        }

        public static implicit operator string(mString sb)
        {
            return sb.CurrentString;
        }

        public static implicit operator mString(string s)
        {
            return new mString().Append(s);
        }

        public override string ToString()
        {
            return CurrentString;
        }

        public string ToString(int startIndex, int length)
        {
            return _stringBuilder.ToString(startIndex, length);
        }
    }
}