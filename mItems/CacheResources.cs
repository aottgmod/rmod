﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Object = UnityEngine.Object;

namespace mItems
{
    internal static class CacheResources
    {
        //todo add load for RC assets
        private static readonly Dictionary<string, Object> cacheLocal = new Dictionary<string, Object>
        {
            { "ROPE", Resources.Load("ROPE") },
            { "HOOK", Resources.Load("HOOK") },
            { "HITMEAT", Resources.Load("HITMEAT") },
            { "REDCROSS", Resources.Load("REDCROSS") },
            { "REDCROSS1", Resources.Load("REDCROSS1") },
            { "UI_IN_GAME", Resources.Load("UI_IN_GAME") },
            { "FLASHLIGHT", Resources.Load("FLASHLIGHT") },
            { "AOT_SUPPLY", Resources.Load("AOT_SUPPLY") },
            { "FX/THUNDER", Resources.Load("FX/THUNDER") },
            { "TITAN_EREN", Resources.Load("TITAN_EREN") },
            { "UI/KILLINFO", Resources.Load("UI/KILLINFO") },
            { "FEMALE_TITAN", Resources.Load("FEMALE_TITAN") },
            { "TITANNAPEMEAT", Resources.Load("TITANNAPEMEAT") },
            { "FX/FXTITANDIE", Resources.Load("FX/FXTITANDIE") },
            { "FX/FXTITANDIE1", Resources.Load("FX/FXTITANDIE1") },
            { "COLOSSAL_TITAN", Resources.Load("COLOSSAL_TITAN") },
            { "MAINCAMERA_MONO", Resources.Load("MAINCAMERA_MONO") }
        };

        private static readonly Dictionary<string, Object> cacheGO = new Dictionary<string, Object>
        {
            { "HOOKUnityEngine.GameObject", Resources.Load<GameObject>("HOOK") },
            { "ROPEUnityEngine.GameObject", Resources.Load<GameObject>("ROPE") },
            { "HORSEUnityEngine.GameObject", Resources.Load<GameObject>("HORSE") },
            { "HITMEATUnityEngine.GameObject", Resources.Load<GameObject>("HITMEAT") },
            { "HITMEAT2UnityEngine.GameObject", Resources.Load<GameObject>("HITMEAT2") },
            { "REDCROSSUnityEngine.GameObject", Resources.Load<GameObject>("REDCROSS") },
            { "REDCROSS1UnityEngine.GameObject", Resources.Load<GameObject>("REDCROSS1") },
            { "TITAN_ERENUnityEngine.GameObject", Resources.Load<GameObject>("TITAN_EREN") },
            { "FX/THUNDERUnityEngine.GameObject", Resources.Load<GameObject>("FX/THUNDER") },
            { "UI_IN_GAMEUIReferArray", Resources.Load<UIReferArray>("UI_IN_GAME") },
            { "FEMALE_TITANUnityEngine.GameObject", Resources.Load<GameObject>("FEMALE_TITAN") },
            { "FX/ROCKTHROWUnityEngine.GameObject", Resources.Load<GameObject>("FX/ROCKTHROW") },
            { "FX/JUSTSMOKEUnityEngine.GameObject", Resources.Load<GameObject>("FX/JUSTSMOKE") },
            { "FX/FXTITANDIEUnityEngine.GameObject", Resources.Load<GameObject>("FX/FXTITANDIE") },
            { "FX/FXTITANDIE1UnityEngine.GameObject", Resources.Load<GameObject>("FX/FXTITANDIE1") },
            { "FX/BOOST_SMOKEUnityEngine.GameObject", Resources.Load<GameObject>("FX/BOOST_SMOKE") },
            { "COLOSSAL_TITANUnityEngine.GameObject", Resources.Load<GameObject>("COLOSSAL_TITAN") },
            { "FX/FLAREBULLETUnityEngine.GameObject", Resources.Load<GameObject>("FX/FLAREBULLET") },
            { "UI/KILLINFOKillInfoComponent", Resources.Load<KillInfoComponent>("UI/KILLINFO") },
            { "FX/FXTITANSPAWNUnityEngine.GameObject", Resources.Load<GameObject>("FX/FXTITANSPAWN") },
            { "TITAN_EREN_TROSTUnityEngine.GameObject", Resources.Load<GameObject>("TITAN_EREN_TROST") },
            { "FX/COLOSSAL_STEAMUnityEngine.GameObject", Resources.Load<GameObject>("FX/COLOSSAL_STEAM") },
            { "FX/COLOSSAL_STEAM_DMGUnityEngine.GameObject", Resources.Load<GameObject>("FX/COLOSSAL_STEAM_DMG") }
        };

        internal static Object Load(string path)
        {
            path = path.ToUpper();
            if (cacheLocal.ContainsKey(path) && cacheLocal[path] != null)
                return cacheLocal[path];
            return cacheLocal[path] = Resources.Load(path);
        }

        internal static Object Load(string path, Type type)
        {
            var keyname = path.ToUpper() + type.FullName;
            if (cacheGO.ContainsKey(keyname) && cacheGO[keyname] != null)
                return cacheGO[keyname];
            return cacheGO[keyname] = Resources.Load(path, type);
        }

        internal static T Load<T>(string path) where T : Object
        {
            var keyname = path.ToUpper() + typeof(T).FullName;
            if (cacheGO.ContainsKey(keyname) && cacheGO[keyname] != null && cacheGO[keyname] is T)
                return (T)cacheGO[keyname];
            return (T)(cacheGO[keyname] = Resources.Load<T>(path));
        }

        internal static bool Load<T>(string path, out T value) where T : Object
        {
            var keyname = path.ToUpper() + typeof(T).FullName;
            if (cacheGO.ContainsKey(keyname) && cacheGO[keyname] != null && cacheGO[keyname] is T)
            {
                value = (T)cacheGO[keyname];
                return value != null;
            }
            cacheGO[keyname] = value = Resources.Load<T>(path);
            return value != null;
        }
    }
}
