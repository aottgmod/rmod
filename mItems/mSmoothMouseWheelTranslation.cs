﻿using UnityEngine;

namespace mItems
{
    public class mSmoothMouseWheelTranslation : MonoBehaviour
    {
        private float timer;
        private float translation;
        private float position;  //hero
        private float target;  //incrementally to hook
        private float falloff;
        private float input;
        private static float[] inf;

        //increases the target translation
        public float speed = 100.0f;
        //maximum acceleration possible by scrolling the wheel faster 5.0f
        public float maxAcceleration = 3f;
        //how quickly to follow the target
        public float followSpeed = 10.0f;
        //whether or not to use the scrollwheel acceleration
        public bool scrollWheelAcceleration = true;

        public static float[] Inf
        {
            get
            {
                var pos = inf[0];
                if (pos < 0)
                    pos = Mathf.Clamp(pos * 1.8f, -0.8f, 3f);
                return new[] { pos, inf[1] };
            }
            set { inf = value; }
        }

        private void Start()
        {
            inf = new float[2];
        }

        private void LateUpdate()
        {
            timer += Time.deltaTime;
            input = Input.GetAxis("Mouse ScrollWheel");

            //acceleration according to the time difference between the 'clicks' of the mousewheel
            //"300" - lower means larger steps, stronger acceleration
            if (scrollWheelAcceleration)
            {
                if (input != 0)
                {
                    target += Mathf.Clamp((input / (timer * 600)) * speed, maxAcceleration * -1, maxAcceleration);
                    timer = 0;
                }
            }
            else  //larger discrete steps but smooth follow
            {
                target += Mathf.Clamp(input * speed, maxAcceleration * -1, maxAcceleration);
            }
            // As a falloff we use the distance between position and target
            // results in faster Movement at higher distances
            falloff = Mathf.Abs(position - target);

            // Determine the amount of translation for this frame
            translation = Time.deltaTime * falloff * followSpeed;

            // 0.001 is our deadzone
            if (position + 0.001 < target)
            {
                position += translation;
                inf[0] = translation;
                inf[1] = falloff;
            }
            if (position - 0.001 > target)
            {
                position -= translation;
                inf[0] = -translation;
                inf[1] = falloff;
                //InRoomChat.AddLine("scroll down:\t" + inf[0]);
            }
        }
    }
}