﻿using System.Collections.Generic;
using mItems;
using UnityEngine;

public class GlobalItems
{
    public static readonly string AssetsPath = Application.dataPath + @"\Assets\";

    //TTMonoBehaviour in mau' dogemod
    private static HERO mHero;
    private static TITAN mTitan;
    
    public static List<Pair<int, TITAN>> titans = new List<Pair<int, TITAN>>();

    public static GameObject focusedGO
    {
        get { return CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").main_object; }
        //mau: add set if you ever need it
    }

    public static HERO myHero
    {
        get
        {
            if (!IN_GAME_MAIN_CAMERA.IsMultiplayer)
                return focusedGO.GetComponent<HERO>();  //mod me hacky way
            if (mHero == null)
            {
                Debug.Log("=Will find hero");
                foreach (var hero in GameObject.FindGameObjectsWithTag("Player"))
                {
                    if (hero == null || hero.GetPhotonView().owner != PhotonNetwork.player)
                        continue;
                    mHero = hero.GetComponent<HERO>();
                    break;
                }
            }
            return mHero;
        }
        set { mHero = value; }
    }

    public static TITAN myTitan
    {
        get
        {
            if (mTitan == null)
            {
                foreach (var titan in GameObject.FindGameObjectsWithTag("titan"))
                {
                    if (titan == null || titan.GetPhotonView().owner != PhotonNetwork.player)
                        continue;
                    mTitan = titan.GetComponent<TITAN>();
                    break;
                }
            }
            return mTitan;
        }
        set { mTitan = value; }
    }

    public static void reset()
    {
        myHero = null;
        myTitan = null;
        titans.Clear();
    }

    public static void removeTitan(object t)
    {
        if (t is TITAN)
        {
            var tt = (TITAN)t;
            for (var i = 0; i < titans.Count; i++)
            {
                if (tt.GetHashCode() == titans[i].Second.GetHashCode())
                {
                    titans.Remove(titans[i]);
                    break;
                }
            }
        }
        else if (t is int)
        {
            var n = (int)t;
            for (var i = 0; i < titans.Count; i++)
            {
                if (n == titans[i].First)
                {
                    titans.Remove(titans[i]);
                    break;
                }
            }
        }
    }
}