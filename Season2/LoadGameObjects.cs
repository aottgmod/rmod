﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Season2
{
    internal static class LoadGameObjects
    {
        private static readonly Dictionary<string, GameObject> GameObjects = new Dictionary<string, GameObject>();

        public static GameObject Fetch(string name)
        {
            return GameObjects.ContainsKey(name) ? GameObjects[name] : null;
        }
        
        public static void Add(string name, GameObject prefab)
        {
            foreach (Transform transform in prefab.GetComponentsInChildren<Transform>())
            {
                transform.gameObject.hideFlags = HideFlags.HideAndDontSave;
            }
            prefab.SetActive(false);
            GameObjects.Add(name, prefab);
        }
        
        public static GameObject Instantiate(GameObject prefab)
        {
            GameObject gameObject = UnityEngine.Object.Instantiate(prefab) as GameObject;
            if (gameObject == null)
                return null;
            gameObject.name = prefab.name;
            gameObject.SetActive(true);
            foreach (Transform transform in gameObject.GetComponentsInChildren<Transform>())
            {
                transform.gameObject.hideFlags = HideFlags.None;
            }
            return gameObject;
        }
        
        public static GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation)
        {
            GameObject gameObject = UnityEngine.Object.Instantiate(prefab, position, rotation) as GameObject;
            if (gameObject == null)
            {
                return null;
            }
            gameObject.name = prefab.name;
            gameObject.SetActive(true);
            foreach (Transform transform in gameObject.GetComponentsInChildren<Transform>())
            {
                transform.gameObject.hideFlags = HideFlags.None;
            }
            return gameObject;
        }
        
        public static GameObject InstantiateBRS(string prefabName, int group)
        {
            GameObject gameObject = PhotonNetwork.Instantiate("BRS.Prefab/" + prefabName, Vector3.zero, Quaternion.identity, group);
            if (gameObject == null)
            {
                return null;
            }
            gameObject.name = Fetch(prefabName).name;
            gameObject.SetActive(true);
            foreach (Transform transform in gameObject.GetComponentsInChildren<Transform>())
            {
                transform.gameObject.hideFlags = HideFlags.None;
            }
            return gameObject;
        }
        
        public static GameObject InstantiateBRS(string prefabName, Vector3 position, Quaternion rotation, int group)
        {
            GameObject gameObject = PhotonNetwork.Instantiate("BRS.Prefab/" + prefabName, position, rotation, group);
            if (gameObject == null)
            {
                return null;
            }
            gameObject.name = Fetch(prefabName).name;
            gameObject.SetActive(true);
            foreach (Transform transform in gameObject.GetComponentsInChildren<Transform>())
            {
                transform.gameObject.hideFlags = HideFlags.None;
            }
            return gameObject;
        }
    }
}