﻿using ExitGames.Client.Photon;

partial class PhotonPlayer
{
    public bool isModerator { get; set; }
    public Hashtable modProperties { get; private set; }

    internal void SetModProperties(Hashtable propertiesToSet)
    {
        if (propertiesToSet != null)
        {
            modProperties.MergeStringKeys(propertiesToSet);
            modProperties.HashNullFix();
            if ((actorID > 0) && !PhotonNetwork.offlineMode)
                PhotonNetwork.networkingPeer.OpSetModPropertiesOfActor(actorID, propertiesToSet, true, 0); //mod me to mod users only
            NetworkingPeer.SendMonoMessage(PhotonNetworkingMessage.OnPhotonPlayerPropertiesChanged, this, propertiesToSet);
        }
    }

    public int Ping
    {
        get
        {
            SetModProperties(new Hashtable { { ModProperties.ping, PhotonNetwork.GetPing() } });
            var obj = modProperties[ModProperties.ping];
            if (obj != null)
                return obj.To<int>();
            return -1;
        }
    }

    public bool isRM
    {
        get
        {
            var obj = modProperties[ModProperties.ping];
            return obj != null;
        }
    }
}