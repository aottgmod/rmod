﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Wallhack : MonoBehaviour  //shouldnt be a behaviour | move to Moderation.dll
{
    private static Material titan_material;

    private const float red = 1f;
    private const float green = 0.39f;
    private const float blue = 0f;
    private const float alpha = 0.9f;
    
    private readonly IDictionary<int, Material[]> skinnedMeshes = new Dictionary<int, Material[]>();
    private readonly IDictionary<int, Material[]> meshes = new Dictionary<int, Material[]>();

    public void Awake()
    {
        titan_material = new Material("Shader \"Chams\" { SubShader { Fog { Mode Off } Pass { Color(" + String.Format("{0},{1},{2},{3}", red, green, blue, alpha) + ") Blend SrcAlpha OneMinusSrcAlpha ZTest Always ZWrite Off } Pass { Color(" + String.Format("{0},{1},{2},{3}", 1.0f - red, 1.0f - green, 1.0f - blue, alpha) + ") Blend SrcAlpha OneMinusSrcAlpha } } }");
    }

    public void ReplaceTitanShaders()
    {
        if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || (IN_GAME_MAIN_CAMERA.IsMultiplayer))
        {
            foreach (var titan in GameObject.FindGameObjectsWithTag("titan"))
            {
                var renderers = titan.transform.root.GetComponentsInChildren<SkinnedMeshRenderer>();
                var renderers2 = titan.transform.root.GetComponentsInChildren<MeshRenderer>();

                var n = titan.GetInstanceID();

                var mat = new Material[renderers.Length];
                for (var i = 0; i < renderers.Length; i++)
                    mat[i] = renderers[i].material;
                if (!skinnedMeshes.ContainsKey(n))
                    skinnedMeshes.Add(n, mat);

                mat = new Material[renderers2.Length];
                for (var i = 0; i < renderers2.Length; i++)
                    mat[i] = renderers2[i].material;
                if (!meshes.ContainsKey(n))
                    meshes.Add(n, mat);

                for (var i = 0; i < renderers.Length; i++)
                    renderers[i].material = titan_material;
                for (var i = 0; i < renderers2.Length; i++)
                    renderers2[i].material = titan_material;
            }
        }
    }

    public void RevertTitanShaders()
    {
        if ((IN_GAME_MAIN_CAMERA.IsSingleplayer) || (IN_GAME_MAIN_CAMERA.IsMultiplayer))
        {
            foreach (var titan in GameObject.FindGameObjectsWithTag("titan"))
            {
                var renderers = titan.transform.root.GetComponentsInChildren<SkinnedMeshRenderer>();
                var renderers2 = titan.transform.root.GetComponentsInChildren<MeshRenderer>();

                var n = titan.GetInstanceID();
                for (var i = 0; i < renderers.Length; i++)
                {
                    if (skinnedMeshes.ContainsKey(n))
                        renderers[i].material = skinnedMeshes[n][i];
                }
                for (var i = 0; i < renderers2.Length; i++)
                {
                    if (meshes.ContainsKey(n))
                        renderers2[i].material = meshes[n][i];
                }
            }
        }
    }
}