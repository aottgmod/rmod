﻿using System.IO;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using mItems;
using Modules;
using Season2;

public class LoadAsset : Photon.MonoBehaviour
{
    public static AssetBundle Wings;
    public static AssetBundle Tree;
    public static AssetBundle Cloud;

    public static AssetBundle Black;
    public static GameObject myHero;

    public static Texture rModLogo;
    public static Texture loading;

    public static void loadSkills()
    {
        GameObject obj = new GameObject();
        UIAtlas uiatlas = obj.AddComponent<UIAtlas>();
        uiatlas.coordinates = UIAtlas.Coordinates.Pixels;
        List<UIAtlas.Sprite> spriteList = new List<UIAtlas.Sprite>
        {
            new UIAtlas.Sprite
            {
                inner = new Rect(0f, 0f, 32f, 32f),
                name = "skill_cd_horse",
                outer = new Rect(0f, 0f, 32f, 32f)
            }
        };
        uiatlas.spriteList = spriteList;
        Material spriteMaterial = new Material(GameObject.Find("skill_cd_eren").GetComponent<UISprite>().atlas.spriteMaterial)
        {
            mainTexture = Import.ImportTexture("horse_skill.png")
        };
        uiatlas.spriteMaterial = spriteMaterial;
        if (LoadGameObjects.Fetch("skill_cd_horse") == null)
        {
            GameObject o = (GameObject)Instantiate(GameObject.Find("skill_cd_eren"));
            o.name = "skill_cd_horse";
            o.AddComponent(uiatlas);
            UISprite component = o.GetComponent<UISprite>();
            component.atlas = o.GetComponent<UIAtlas>();
            component.spriteName = "skill_cd_horse";
            LoadGameObjects.Add("skill_cd_horse", o);
        }
    }

    private IEnumerator Load()
    {
        AssetBundleCreateRequest assetReq = AssetBundle.CreateFromMemory(File.ReadAllBytes(GlobalItems.AssetsPath + @"Wings.unity3d"));
        yield return assetReq;
        Wings = assetReq.assetBundle;
    }

    private static IEnumerator LoadBlack()
    {
        AssetBundleCreateRequest assetReq = AssetBundle.CreateFromMemory(File.ReadAllBytes(GlobalItems.AssetsPath + @"Black.unity3d"));
        yield return assetReq;
        Black = assetReq.assetBundle;
    }

    private static IEnumerator LoadTree()
    {
        AssetBundleCreateRequest assetReq = AssetBundle.CreateFromMemory(File.ReadAllBytes(GlobalItems.AssetsPath + @"Tree.unity3d"));
        yield return assetReq;
        Tree = assetReq.assetBundle;
    }

    private static IEnumerator LoadCloud()
    {
        AssetBundleCreateRequest assetReq = AssetBundle.CreateFromMemory(File.ReadAllBytes(GlobalItems.AssetsPath + @"Cloud.unity3d"));
        yield return assetReq;
        Cloud = assetReq.assetBundle;
    }

    private IEnumerator LoadLogo()
    {
        WWW temp = new WWW("https://vgy.me/FxuhBD.png");
        yield return temp;
        rModLogo = temp.texture;
    }

    public IEnumerator LoadLoading()
    {
        WWW temp = new WWW("https://vgy.me/JrPdiP.png");
        yield return temp;
        loading = temp.texture;
    }

    public void Start()
    {
        StartCoroutine(Load());
        if (Black == null)
            StartCoroutine(LoadBlack());
        if (Tree == null)
            StartCoroutine(LoadTree());
        if (Cloud == null)
            StartCoroutine(LoadCloud());
        if (rModLogo == null)
            StartCoroutine(LoadLogo());
        if (loading == null)
            StartCoroutine(LoadLoading());
        //loadSkills();
    }
}