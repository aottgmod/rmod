﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Pair<TKey, TValue>
{
    public TKey First;
    public TValue Second;

    public Pair(TKey first, TValue second)
    {
        First = first;
        Second = second;
    }

    public Pair(KeyValuePair<TKey, TValue> pair)
    {
        First = pair.Key;
        Second = pair.Value;
    }

    public Pair(DictionaryEntry entry)
    {
        First = (TKey)entry.Key;
        Second = (TValue)entry.Value;
    }

    public Pair<TKey, TValue> Clone()
    {
        return new Pair<TKey, TValue>(First, Second);
    }

    public bool AnyEquals(object obj)
    {
        bool result;
        if (obj == null)
            result = First == null || Second == null;
        else
            result = this == obj || (obj is TKey && First.Equals((TKey)obj)) || (obj is TValue && Second.Equals((TValue)obj)) || this == obj || First.Equals((TKey)obj) || Second.Equals((TValue)obj);
        return result;
    }

    public static bool operator ==(Pair<TKey, TValue> one, KeyValuePair<TKey, TValue> two)
    {
        return !ReferenceEquals(one, null) && Equals(one.First, two.Key) && Equals(one.Second, two.Value);
    }
    
    public static bool operator !=(Pair<TKey, TValue> one, KeyValuePair<TKey, TValue> two)
    {
        return ReferenceEquals(one, null) || !Equals(one.First, two.Key) || !Equals(one.Second, two.Value);
    }
    
    public static bool operator ==(KeyValuePair<TKey, TValue> one, Pair<TKey, TValue> two)
    {
        return !ReferenceEquals(two, null) && Equals(two.First, one.Key) && Equals(two.Second, one.Value);
    }
    
    public static bool operator !=(KeyValuePair<TKey, TValue> one, Pair<TKey, TValue> two)
    {
        return !ReferenceEquals(two, null) && (!Equals(two.First, one.Key) || !Equals(two.Second, one.Value));
    }
    
    public static implicit operator KeyValuePair<TKey, TValue>(Pair<TKey, TValue> pair)
    {
        return new KeyValuePair<TKey, TValue>(pair.First, pair.Second);
    }
    
    public static implicit operator Pair<TKey, TValue>(KeyValuePair<TKey, TValue> pair)
    {
        return new Pair<TKey, TValue>(pair);
    }
    
    public static implicit operator DictionaryEntry(Pair<TKey, TValue> pair)
    {
        return new DictionaryEntry(pair.First, pair.Second);
    }
    
    public static implicit operator Pair<TKey, TValue>(DictionaryEntry entry)
    {
        return new Pair<TKey, TValue>(entry);
    }
    
    public override string ToString()
    {
        return string.Format("[{0}, {1}]", new object[]
        {
            First != null ? First.ToString() : string.Empty,
            Second != null ? Second.ToString() : string.Empty
        });
    }
    
    public string ToString(Func<TKey, string> clause1)
    {
        return string.Format("[{0}, {1}]", new object[]
        {
            First != null ? clause1(First) : string.Empty,
            Second != null ? Second.ToString() : string.Empty
        });
    }
    
    public string ToString(Func<TKey, string> clause1, Func<TValue, string> clause2)
    {
        return string.Format("[{0}, {1}]", new object[]
        {
            First != null ? clause1(First) : string.Empty,
            Second != null ? clause2(Second) : string.Empty
        });
    }
    
    public override bool Equals(object obj)
    {
        bool result;
        if (obj is Pair<TKey, TValue>)
        {
            Pair<TKey, TValue> pair = (Pair<TKey, TValue>)obj;
            result = (pair != null && First.Equals(pair.First) && Second.Equals(pair.Second));
        }
        else
        {
            result = false;
        }
        return result;
    }
    
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}