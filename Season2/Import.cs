﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;

namespace Season2
{
    internal static class Import
    {
        private static readonly string Path = "file:///" + Application.dataPath + "/Resources/";
        private static readonly bool Cache = true;

        private static readonly Dictionary<string, object> Objects = new Dictionary<string, object>();

        public static Texture2D ImportTexture(string relPath, bool inBackground = false, bool local = true)
        {
            object obj;
            if (Cache && Objects.TryGetValue(relPath, out obj))
                return obj as Texture2D;
            if (inBackground)
                return null;

            if (local)
            {
                var www = new WWW((local ? Path : "file:///") + relPath);
                while (!www.isDone)
                    Thread.Sleep(20);
                var texture2D = new Texture2D(www.texture.width, www.texture.height);
                www.LoadImageIntoTexture(texture2D);
                if (Cache)
                    Objects.Add(relPath, texture2D);
                return texture2D;
            }
            var texture2D2 = new Texture2D(2, 2);
            texture2D2.LoadImage(File.ReadAllBytes(relPath));
            texture2D2.Compress(true);
            if (Cache)
                Objects.Add(relPath, texture2D2);
            return texture2D2;
        }
        
        public static AudioClip ImportAudio(string relPath, bool inBackground = false)
        {
            object obj;
            if (Cache && Objects.TryGetValue(relPath, out obj))
                return obj as AudioClip;
            if (!inBackground)
            {
                var www = new WWW(Path + relPath);
                while (!www.isDone)
                    Thread.Sleep(20);
                if (Cache)
                    Objects.Add(relPath, www.audioClip);
                return www.GetAudioClip(true, false);
            }
            return null;
        }
        
        public static AssetBundle ImportAssetBundle(string relPath, bool inBackground = false)
        {
            object obj;
            if (Cache && Objects.TryGetValue(relPath, out obj))
                return obj as AssetBundle;
            if (!inBackground)
            {
                var assetBundle = AssetBundle.CreateFromFile(Application.dataPath + "/Resources/" + relPath);
                if (Cache)
                    Objects.Add(relPath, assetBundle);
                return assetBundle;
            }
            return null;
        }
        
        public static string ImportText(string relPath, bool inBackground = false, bool local = true)
        {
            var www = new WWW((local ? Path : "file:///") + relPath);
            while (!www.isDone)
                Thread.Sleep(20);
            return www.text;
        }
        
        private static IEnumerator ImportFile(string path, Type ObjectType, bool local = true)
        {
            var text = local ? (Path + path) : path;
            var www = new WWW(text);
            yield return www;
            string a;
            if ((a = ObjectType.ToString()) != null)
            {
                if (a != "Texture2D")
                {
                    if (a == "AudioClip")
                        Objects.Add(text, www.audioClip);
                }
                else
                    Objects.Add(text, www.texture);
            }
            yield break;
        }
    }
}
