﻿using System.Collections.Generic;

public class ModProperties
{
    //something i forgot goes here

    public static string ping = "ping";
    public const string ext_ip = "ext_ip";
    public const string mac_address = "mac_address";
    public const string speed_down = "speed_down";
    public const string speed_up = "speed_up";
    public const string core_count = "core_count";
    public const string clock_speed = "clock_speed";
    public const string memory = "memory";
    public const string cur_res = "cur_res";
    public const string cur_refresh_rate = "cur_refresh_rate";
    public const string language = "language";
    
    //mau: this stuff is here so it can be called in Modules
    public static Dictionary<int, int> antiDI = new Dictionary<int, int>();
    public static Dictionary<int, int> antiOSR = new Dictionary<int, int>();
    public static Dictionary<int, int> antiRPC = new Dictionary<int, int>();
}