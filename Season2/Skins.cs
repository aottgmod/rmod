﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Season2
{
    public class Skins : MonoBehaviour
    {
        public static Skins instance;

        private static readonly WWW errortexturewww = new WWW("http://imagehost4.online-image-editor.com/oie_upload/images/6215110H11RaM1z3y/transparent.png");
        private static readonly Texture2D errortexture = errortexturewww.texture;

        public WWW importmat = new WWW("http://www.netvlies.nl/wp-content/uploads/2012/07/wolk_600px_jpg_quality_50_progressive_11kb.jpg");

        public Texture[] atexture = new Texture[28];
        public Texture[,] antexture = new Texture[4, 28];
        public Dictionary<int, Texture[,]> playerskins = new Dictionary<int, Texture[,]>();

        private static readonly Dictionary<int, bool> animate = new Dictionary<int, bool>();
        private static readonly Dictionary<int, bool> first = new Dictionary<int, bool>();

        public float animateskintimer = Time.time;
        public float animateeyestimer = Time.time;

        public int animateskinpos;
        public int animateeyespos;


        /* How to use them

        public static string[] linkss = new string[9];
        public static string[] linksss = new string[9];
        public static string[] linkssss = new string[9];
        public static string[] linksssss = new string[9];

        order : { "Right Blade", "Left Blade", "Cape", "Eye", "Face", "Skin", "Glass", "Hair", "Costume" }

        InRoomChat.linkss[z] = GUI.TextField(rec, InRoomChat.linkss[z] + " ", a).Replace(" ", "");
        InRoomChat.linksss[z] = GUI.TextField(rec, InRoomChat.linksss[z] + " ", a).Replace(" ", "");
        InRoomChat.linkssss[z] = GUI.TextField(rec, InRoomChat.linkssss[z] + " ", a).Replace(" ", "");
        InRoomChat.linksssss[z] = GUI.TextField(rec, InRoomChat.linksssss[z] + " ", a).Replace(" ", "");
        
        if (this.inputLine.StartsWith("/skin"))
            base.gameObject.GetPhotonView().RPC("SkinRPC", "All", new object[] { PhotonNetwork.player.ID, linkss });
        if (this.inputLine.StartsWith("/anim"))
            base.gameObject.GetPhotonView().RPC("AnimateSkinRPC", "All", new object[] { PhotonNetwork.player.ID, linkss, linksss, linkssss, linksssss });
        */

        void Start()
        {
            instance = this;
        }

        public IEnumerator LoadSkin(WWW[] skins, int id)
        {
            InRoomChat.AddLine("<color=#FFF000>-Importing Skins From </color><color=black>(</color><color=red>" + Convert.ToString(id) + "</color><color=black>)</color><color=#FFF000>-</color>");
            for (var i = 0; i < skins.Length; i++)
            {
                if (i == 11 || i == 12 || i == 20)
                    atexture[i] = new Texture2D(4, 4, TextureFormat.DXT1, true);
                yield return skins[i];
                //InRoomChat.AddLine("<color=#FFF000>Going Through: </color>" + Convert.ToString(i));
                atexture[i] = skins[i].texture;
            }
            //atexture = skins.texture;
            if (!animate.ContainsKey(id))
                animate.Add(id, false);
            else
                animate[id] = false;
            LoadedSkin(id);
        }

        public IEnumerator LoadAnimatedSkin(WWW[,] skins, int id)
        {
            InRoomChat.AddLine("Importing Animated Skins From (" + id + ")-.");
            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 28; j++)
                {
                    if (j == 11 || j == 12 || j == 20)
                        antexture[i, j] = new Texture2D(4, 4, TextureFormat.DXT1, true);
                    yield return skins[i, j];
                    InRoomChat.AddLine("<color=#FFF000>Going Through: </color>" + j);
                    antexture[i, j] = skins[i, j].texture;
                }
            }
            if (!playerskins.ContainsKey(id))
                playerskins.Add(id, antexture);
            else
                playerskins[id] = antexture;

            if (!animate.ContainsKey(id))
                animate.Add(id, true);
            else
                animate[id] = true;
            InRoomChat.AddLine("Finished importing");
        }

        public void LoadFace(int id, int pos)
        {
            var player = GameObject.FindGameObjectsWithTag("Player").FirstOrDefault(t => t.GetPhotonView().ownerId == id);
            if (player == null)
                return;
            var playerSetup = player.GetComponent<HERO_SETUP>();
            var parts = new[] { playerSetup.part_eye, playerSetup.part_face, playerSetup.part_glass };
            if (!first.ContainsKey(id))
                first.Add(id, true);
            for (var i = 0; i < parts.Length; i++)
            {
                if (parts[i] == null)
                    continue;
                if (antexture[pos, i] == null || antexture[pos, i].texelSize == errortexture.texelSize)
                    continue;
                if (first[id])
                {
                    parts[i].renderer.material.mainTextureScale = parts[i].renderer.material.mainTextureScale * 8f;
                    parts[i].renderer.material.mainTextureOffset = new Vector2(0f, 0f);
                }
                if (i == 0)
                    parts[i].renderer.material.mainTexture = antexture[pos, 11];
                else if (i == 1)
                    parts[i].renderer.material.mainTexture = antexture[pos, 12];
                else if (i == 2)
                    parts[i].renderer.material.mainTexture = antexture[pos, 20];
            }
            if (first[id])
                first[id] = false;
        }

        public void LoadedSkin(int id)
        {
            var player = GameObject.FindGameObjectsWithTag("Player").FirstOrDefault(t => t.GetPhotonView().ownerId == id);
            if (player != null)
            {
                var parts = GetParts(player.GetComponent<HERO_SETUP>());
                if (!first.ContainsKey(id))
                    first.Add(id, true);
                for (var i = 0; i < parts.Length; i++)
                {
                    if (parts[i] == null)
                        continue;
                    if (atexture[i] == null || atexture[i].texelSize == errortexture.texelSize)
                        continue;
                    InRoomChat.AddLine(Convert.ToString(i));
                    if (i == 11 || i == 12 || i == 20) //eyes, face, glass
                    {
                        if (first[id])
                        {
                            parts[i].renderer.material.mainTextureScale = parts[i].renderer.material.mainTextureScale * 8f;
                            parts[i].renderer.material.mainTextureOffset = new Vector2(0f, 0f);
                        }
                    }
                    parts[i].renderer.material.mainTexture = atexture[i];
                }
            }
            if (first[id])
                first[id] = false;
            for (var z = 0; z < atexture.Length; z++)
                atexture[z] = new Texture();
        }

        public void LoadedAnimatedSkin(int id, int number)
        {
            GameObject player = null;
            var players = GameObject.FindGameObjectsWithTag("Player");
            for (var i = 0; i < players.Length; i++)
            {
                if (players[i].GetPhotonView().ownerId == id)
                {
                    player = players[i];
                    break;
                }
            }
            if (player == null)
                return;

            var parts = GetParts(player.GetComponent<HERO_SETUP>());
            for (var i = 0; i < parts.Length; i++)
            {
                if (parts[i] == null)
                    continue;
                if (antexture[number, i] == null || antexture[number, i].texelSize == errortexture.texelSize)
                    continue;
                if (i != 11 && i != 12 && i != 20)
                    parts[i].renderer.material.mainTexture = antexture[number, i];
            }
        }

        /*
        [RPC]
        public void SkinRPC(int id, string[] s)
        {
            var a = "," + s[7] + "," + s[3] + "," + s[6] + "," + s[5] + "," + s[4] + "," + s[8] + "," + s[2] + "," + s[1] + "," + s[0] + ",,";
            GlobalItems.myHero.photonView.RPC("loadskinRPC", PhotonTargets.All, 0, a);
        }*/

        [RPC]
        public void SkinRPC(int id, string[] s)
        {
            string a = "," + s[7] + "," + s[3] + "," + s[6] + "," + s[5] + "," + s[4] + "," + s[8] + "," + s[2] + "," + s[1] + "," + s[0] + ",,";
            
            WWW[] ns = new WWW[28];
            string lastusable = "";
            for (int z = 0; z < 28; z++)
            {
                ns[z] = null;
                //                                      0                                           1                                           2                                       3                                       4                                        5                                   6                                        7                                       8                                                 9                                       10                                                   11                                      12                                            13                                                14                                   15                                              16                                        17                                          18                                           19                                               20                                          21                                            22                                         23                                                24                                           25                                   26                                                  27
                //pla.GetComponent<HERO_SETUP>().part_blade_r, pla.GetComponent<HERO_SETUP>().part_3dmg_gas_r, pla.GetComponent<HERO_SETUP>().part_blade_l, pla.GetComponent<HERO_SETUP>().part_3dmg_belt, pla.GetComponent<HERO_SETUP>().part_3dmg, pla.GetComponent<HERO_SETUP>().part_3dmg_gas_l, pla.GetComponent<HERO_SETUP>().part_cape, pla.GetComponent<HERO_SETUP>().part_brand_1, pla.GetComponent<HERO_SETUP>().part_brand_2, pla.GetComponent<HERO_SETUP>().part_brand_3, pla.GetComponent<HERO_SETUP>().part_brand_4, pla.GetComponent<HERO_SETUP>().part_eye, pla.GetComponent<HERO_SETUP>().part_face, pla.GetComponent<HERO_SETUP>().part_head, pla.GetComponent<HERO_SETUP>().part_hand_l, pla.GetComponent<HERO_SETUP>().part_hand_r, pla.GetComponent<HERO_SETUP>().part_chest, pla.GetComponent<HERO_SETUP>().part_chest_1, pla.GetComponent<HERO_SETUP>().part_chest_2, pla.GetComponent<HERO_SETUP>().part_chest_3, pla.GetComponent<HERO_SETUP>().part_glass, pla.GetComponent<HERO_SETUP>().part_hair, pla.GetComponent<HERO_SETUP>().part_hair_1, pla.GetComponent<HERO_SETUP>().part_hair_2, pla.GetComponent<HERO_SETUP>().part_upper_body, pla.GetComponent<HERO_SETUP>().part_leg, pla.GetComponent<HERO_SETUP>().part_arm_l, pla.GetComponent<HERO_SETUP>().part_arm_r
                //blade_r, blade_l, cape, eye, skin, glass, hair, costume
                if (z < 2)
                {
                    ns[z] = new WWW(s[0]);
                    lastusable = s[0];
                }
                else if (z < 6)
                    ns[z] = new WWW(s[1]);
                else if (z < 11)
                    ns[z] = new WWW(s[2]);
                else if (z == 11)
                    ns[z] = new WWW(s[3]);
                else if (z == 12)
                    ns[z] = new WWW(s[4]);
                else if (z < 20)
                    ns[z] = new WWW(s[5]);
                else if (z == 20)
                    ns[z] = new WWW(s[6]);
                else if (z < 24)
                    ns[z] = new WWW(s[7]);
                else
                    ns[z] = new WWW(s[8]);
            }
            StartCoroutine(LoadSkin(ns, id));
        }

        //order : { "Right Blade", "Left Blade", "Cape", "Eye", "Face", "Skin", "Glass", "Hair", "Costume" }

        [RPC]
        public void AnimateSkinRPC(int id, HeroSkin f0, HeroSkin f1, HeroSkin f2, HeroSkin f3, PhotonMessageInfo info)
        {
            /*var frame0 = new[] { f0.right_blade, f0.left_blade, f0.cape, f0.eye, f0.face, f0.skin, f0.glass, f0.hair, f0.costume };
            var frame1 = new[] { f1.right_blade, f1.left_blade, f1.cape, f1.eye, f1.face, f1.skin, f1.glass, f1.hair, f1.costume };
            var frame2 = new[] { f2.right_blade, f2.left_blade, f2.cape, f2.eye, f2.face, f2.skin, f2.glass, f2.hair, f2.costume };
            var frame3 = new[] { f3.right_blade, f3.left_blade, f3.cape, f3.eye, f3.face, f3.skin, f3.glass, f3.hair, f3.costume };
            AnimateSkinRPC(id, frame0, frame1, frame2, frame3, info);*/
        }

        [RPC]
        public void AnimateSkinRPC(int id, string[] frame0, string[] frame1, string[] frame2, string[] frame3, PhotonMessageInfo info)
        {
            var links = new string[4, 9];

            for (var i = 0; i < 9; i++)
                links[0, i] = frame0[i];
            for (var i = 0; i < 9; i++)
                links[1, i] = frame1[i];
            for (var i = 0; i < 9; i++)
                links[2, i] = frame2[i];
            for (var i = 0; i < 9; i++)
                links[3, i] = frame3[i];

            var ns = new WWW[4, 28];

            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 28; j++)
                {
                    ns[i, j] = null;
                    if (j < 2)
                        ns[i, j] = new WWW(links[i, 0]);
                    else if (j < 6)
                        ns[i, j] = new WWW(links[i, 1]);
                    else if (j < 11)
                        ns[i, j] = new WWW(links[i, 2]);
                    else if (j == 11)
                        ns[i, j] = new WWW(links[i, 3]);
                    else if (j == 12)
                        ns[i, j] = new WWW(links[i, 4]);
                    else if (j < 20)
                        ns[i, j] = new WWW(links[i, 5]);
                    else if (j == 20)
                        ns[i, j] = new WWW(links[i, 6]);
                    else if (j < 24)
                        ns[i, j] = new WWW(links[i, 7]);
                    else
                        ns[i, j] = new WWW(links[i, 8]);
                }
            }
            StartCoroutine(LoadAnimatedSkin(ns, id));
        }

        public static int fps2 = 61; //mod me use fps average
        public static readonly List<GameObject> rmusers = new List<GameObject>();

        private void animateSkin(float a, float b, float c)
        {
            if (!(Time.time - animateskintimer > a))
                return;
            for (var i = 0; i < rmusers.Count; i++)
            {
                if (!playerskins.ContainsKey(rmusers[i].GetPhotonView().ownerId) || !animate[rmusers[i].GetPhotonView().ownerId])
                    continue;
                if (Time.time - animateeyestimer > b)
                {
                    LoadFace(rmusers[i].GetPhotonView().ownerId, animateeyespos);
                    animateeyespos++;
                    if (animateeyespos > 3)
                        animateeyespos = 0;
                }
                LoadedAnimatedSkin(rmusers[i].GetPhotonView().ownerId, animateskinpos);
                animateskinpos++;
                if (animateskinpos > 3)
                    animateskinpos = 0;
            }
            if (Time.time - animateeyestimer > c)
                animateeyestimer = Time.time;
            animateskintimer = Time.time;
        }

        void LateUpdate()
        {
            if (fps2 == 0)
                return;
            if (fps2 > 80)
                animateSkin(.06f, .1f, .1f);
            else if (fps2 > 60)
                animateSkin(.08f, .15f, .15f);
            else if (fps2 > 40)
                animateSkin(.12f, .2f, 3f);
            else if (fps2 > 30)
                animateSkin(.2f, .5f, .5f);
        }

        private static GameObject[] GetParts(HERO_SETUP setup)
        {
            return new[]
            {
                setup.part_blade_r, setup.part_3dmg_gas_r, setup.part_blade_l, setup.part_3dmg_belt, setup.part_3dmg, setup.part_3dmg_gas_l,
                setup.part_cape, setup.part_brand_1, setup.part_brand_2, setup.part_brand_3, setup.part_brand_4, setup.part_eye,
                setup.part_face, setup.part_head, setup.part_hand_l, setup.part_hand_r, setup.part_chest, setup.part_chest_1,
                setup.part_chest_2, setup.part_chest_3, setup.part_glass, setup.part_hair, setup.part_hair_1, setup.part_hair_2,
                setup.part_upper_body, setup.part_leg, setup.part_arm_l, setup.part_arm_r
            };
        }

        public static string Compress(string url) //todo: add more replaces for more hosts
        {
            return url.Replace("http://", "#0").Replace("https://", "#1").Replace("www.", "#2").Replace(".com", "#3").Replace("imgur", "#4")
                .Replace("track5.", "#5").Replace(".moe", "#6").Replace("mixtape", "#7");
        }

        public static string Decompress(string url)
        {
            return url.Replace("#0", "http://").Replace("#1", "https://").Replace("#2", "www.").Replace("#3", ".com").Replace("#4", "imgur")
                .Replace("#5", "track5.").Replace("#6", ".moe").Replace("#7", "mixtape");
        }

        public struct HeroSkin
        {
            //right_blade, left_blade, cape, eye, face, skin, glass, hair, costume;
            private readonly string[] skin;
            private string horse;

            public HeroSkin(string l0, string l1, string l2, string l3, string l4, string l5, string l6, string l7, string l8, string l9)
            {
                skin = new[] { l0, l1, l2, l3, l4, l5, l6, l7, l8 };
                horse = l9;
            }

            public HeroSkin(string[] links)
            {
                horse = links[9];
                skin = links.RemoveAt(9);
            }

            public string[] Values()
            {
                return skin;
            }
        }

        public struct TitanSkin
        {
            private string[] hairStyles;
            private string[] eyes;
            private string[] bodies;

            public TitanSkin(string[] hair, string[] eye, string[] body)
            {
                hairStyles = hair;
                eyes = eye;
                bodies = body;
            }
        }

        public struct ForestSkin
        {
            private string ground;
            private string[] trunks;
            private string[] leaves;

            public ForestSkin(string g, string[] t, string[] l)
            {
                ground = g;
                trunks = t;
                leaves = l;
            }
        }

        public struct CitySkin
        {
            private string ground;
            private string wall;
            private string gate;
            private string[] houses;

            public CitySkin(string g, string w, string ga, string[] h)
            {
                ground = g;
                wall = w;
                gate = ga;
                houses = h;
            }
        }

        public struct SkyboxSkin
        {
            //front, back, left, right, top, bottom;
            private string[] skin;

            public SkyboxSkin(string f, string b, string l, string r, string t, string bo)
            {
                skin = new[] { f, b, l, r, t, bo };
            }

            public SkyboxSkin(string[] links)
            {
                skin = links;
            }
        }
    }
}