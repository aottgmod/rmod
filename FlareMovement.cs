using mItems;
using UnityEngine;

public class FlareMovement : MonoBehaviour
{
    public string color;
    private GameObject hero;
    private GameObject hint;
    private bool nohint;
    private Vector3 offY;
    private float timer;

    public void dontShowHint()
    {
        Destroy(hint);
        nohint = true;
    }

    private void Start()
    {
        hero = IN_GAME_MAIN_CAMERA.maincamera.main_object;
        if (!nohint && (hero != null))
        {
            hint = (GameObject) Instantiate(CacheResources.Load("UI/" + color + "FlareHint"));
            offY = color == "Black" ? Vector3.up * 0.4f : Vector3.up * 0.5f;
            hint.transform.parent = transform.root;
            hint.transform.position = hero.transform.position + offY;
            var vector = transform.position - hint.transform.position;
            var num = Mathf.Atan2(-vector.z, vector.x) * 57.29578f;
            hint.transform.rotation = Quaternion.Euler(-90f, num + 180f, 0f);
            hint.transform.localScale = Vector3.zero;
            iTween.ScaleTo(hint, iTween.Hash("x", 1f, "y", 1f, "z", 1f, "easetype", iTween.EaseType.easeOutElastic, "time", 1f));
            iTween.ScaleTo(hint, iTween.Hash("x", 0, "y", 0, "z", 0, "easetype", iTween.EaseType.easeInBounce, "time", 0.5f, "delay", 2.5f));
        }
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (hint != null)
        {
            if (timer < 6f)
            {
                hint.transform.position = hero.transform.position + offY;
                var vector = transform.position - hint.transform.position;
                var num = Mathf.Atan2(-vector.z, vector.x) * 57.29578f;
                hint.transform.rotation = Quaternion.Euler(-90f, num + 180f, 0f);
            }
            else if (hint != null)
                Destroy(hint);
        }
        if (timer < 4f)
            rigidbody.AddForce((transform.forward + transform.up * 5f) * Time.deltaTime * 5f, ForceMode.VelocityChange);
        else
            rigidbody.AddForce(-transform.up * Time.deltaTime * 7f, ForceMode.Acceleration);
    }
}