using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

public class BetterList<T>
{
    public T[] buffer;
    public int size;

    public void Add(T item)
    {
        if ((buffer == null) || (size == buffer.Length))
            AllocateMore();
        buffer[size++] = item;
    }

    private void AllocateMore()
    {
        var array = buffer == null ? new T[0x20] : new T[Mathf.Max(buffer.Length << 1, 0x20)];
        if ((buffer != null) && (size > 0))
            buffer.CopyTo(array, 0);
        buffer = array;
    }

    public void Clear()
    {
        size = 0;
    }

    public bool Contains(T item)
    {
        if (buffer != null)
        {
            for (var i = 0; i < size; i++)
            {
                if (buffer[i].Equals(item))
                    return true;
            }
        }
        return false;
    }

    [DebuggerHidden]
    public IEnumerator<T> GetEnumerator()
    {
        return new c__Iterator9 { f__this = this };
    }

    public void Insert(int index, T item)
    {
        if ((buffer == null) || (size == buffer.Length))
            AllocateMore();
        if (index < size)
        {
            for (var i = size; i > index; i--)
                buffer[i] = buffer[i - 1];
            buffer[index] = item;
            size++;
        }
        else
            Add(item);
    }

    public T Pop()
    {
        if ((buffer != null) && (size != 0))
        {
            var local = buffer[--size];
            buffer[size] = default(T);
            return local;
        }
        return default(T);
    }

    public void Release()
    {
        size = 0;
        buffer = null;
    }

    public bool Remove(T item)
    {
        if (buffer != null)
        {
            var comparer = EqualityComparer<T>.Default;
            for (var i = 0; i < size; i++)
            {
                if (comparer.Equals(buffer[i], item))
                {
                    size--;
                    buffer[i] = default(T);
                    for (var j = i; j < size; j++)
                        buffer[j] = buffer[j + 1];
                    return true;
                }
            }
        }
        return false;
    }

    public void RemoveAt(int index)
    {
        if ((buffer != null) && (index < size))
        {
            size--;
            buffer[index] = default(T);
            for (var i = index; i < size; i++)
                buffer[i] = buffer[i + 1];
        }
    }

    public void Sort(Comparison<T> comparer)
    {
        var flag = true;
        while (flag)
        {
            flag = false;
            for (var i = 1; i < size; i++)
            {
                if (comparer(buffer[i - 1], buffer[i]) > 0)
                {
                    var local = buffer[i];
                    buffer[i] = buffer[i - 1];
                    buffer[i - 1] = local;
                    flag = true;
                }
            }
        }
    }

    public T[] ToArray()
    {
        Trim();
        return buffer;
    }

    private void Trim()
    {
        if (size > 0)
        {
            if (size < buffer.Length)
            {
                var localArray = new T[size];
                for (var i = 0; i < size; i++)
                    localArray[i] = buffer[i];
                buffer = localArray;
            }
        }
        else
            buffer = null;
    }

    public T this[int i]
    {
        get
        {
            return buffer[i];
        }
        set
        {
            buffer[i] = value;
        }
    }

    [CompilerGenerated]
    private sealed class c__Iterator9 : IEnumerator, IDisposable, IEnumerator<T>
    {
        internal T current;
        internal int PC;
        internal BetterList<T> f__this;
        internal int i__0;

        [DebuggerHidden]
        public void Dispose()
        {
            PC = -1;
        }

        public bool MoveNext()
        {
            var num = (uint) PC;
            PC = -1;
            switch (num)
            {
                case 0:
                    if (f__this.buffer == null)
                        goto Label_0089;
                    i__0 = 0;
                    break;

                case 1:
                    i__0++;
                    break;

                default:
                    goto Label_0090;
            }
            if (i__0 < f__this.size)
            {
                current = f__this.buffer[i__0];
                PC = 1;
                return true;
            }
        Label_0089:
            PC = -1;
        Label_0090:
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        T IEnumerator<T>.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }
    }
}