using System.Collections.Generic;
using System.Web.UI.WebControls;
using dItems;
using mItems;
using Modules;
using UnityEngine;
using UnityEngineInternal;

public class IN_GAME_MAIN_CAMERA : MonoBehaviour
{
    public RotationAxes axes;
    public AudioSource bgmusic;
    public static float cameraDistance = 0.6f;
    public static CAMERA_TYPE cameraMode;
    public static int cameraTilt = 1;
    public static int character = 1;
    private float closestDistance;
    private int currentPeekPlayerIndex;
    public static DayLight dayLight = DayLight.Dawn;
    private float decay;
    public static int difficulty;
    private float distance = 10f;
    private float distanceMulti;
    private float distanceOffsetMulti;
    private float duration;
    private float flashDuration;
    private bool flip;
    public static GAMEMODE gamemode;
    public bool gameOver;
    public static GAMETYPE gametype = GAMETYPE.STOP;
    private bool hasSnapShot;
    private Transform head;
    private float heightMulti;
    public FengCustomInputs inputManager;
    public static int invertY = 1;
    public static bool isCheating;
    public static bool isPausing;
    public static bool isTyping;
    public float justHit;
    public int lastScore;
    public static int level;
    private bool lockAngle;
    private Vector3 lockCameraPosition;
    private GameObject locker;
    private GameObject lockTarget;
    public GameObject main_object;
    public float maximumX = 360f;
    public float maximumY = 60f;
    public float minimumX = -360f;
    public float minimumY = -60f;
    private bool needSetHUD;
    private float R;
    public int score;
    public static float sensitivityMulti = 0.5f;
    public static string singleCharacter;
    public Material skyBoxDAWN;
    public Material skyBoxDAY;
    public Material skyBoxNIGHT;
    private Texture2D snapshot1;
    private Texture2D snapshot2;
    private Texture2D snapshot3;
    public GameObject snapShotCamera;
    private int snapShotCount;
    private float snapShotCountDown;
    private int snapShotDmg;
    private float snapShotInterval = 0.02f;
    private float snapShotStartCountDownTime;
    private GameObject snapShotTarget;
    private Vector3 snapShotTargetPosition;
    public bool spectatorMode;
    private bool startSnapShotFrameCount;
    public static STEREO_3D_TYPE stereoType;
    public Texture texture;
    public float timer;
    public static bool triggerAutoLock;
    public static bool usingTitan;
    private Vector3 verticalHeightOffset = Vector3.zero;

    public static float headX;
    public static float headY;

    public static IN_GAME_MAIN_CAMERA maincamera;
    private static bool disabledRenderers;
    private static float x_rotation;
    private static float y_rotation;

    private static GameObject fogLight;

    private static bool debug = false;

    public static bool IsSingleplayer
    {
        get { return gametype == GAMETYPE.SINGLE; }
    }

    public static bool IsMultiplayer
    {
        get { return gametype == GAMETYPE.MULTIPLAYER; }
    }

    private void Awake()
    {
        maincamera = this;
        x_rotation = 0f;
        //main_objectT = CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").main_object.transform;
        //mainT = Camera.main.transform;
        isTyping = false;
        isPausing = false;
        name = "MainCamera";
        if (PlayerPrefs.HasKey("GameQuality"))
            GetComponent<TiltShift>().enabled = PlayerPrefs.GetFloat("GameQuality") >= 0.9f;
        else
            GetComponent<TiltShift>().enabled = true;
    }

    public void fog()  //mod me shouldnt be here
    {
        camera.farClipPlane = 120;
        gameObject.GetComponent<Skybox>().material = skyBoxNIGHT;
        Debug.Log("=fog111111111");
        Camera.main.GetComponent<Skybox>().material = (Material)LoadAsset.Black.Load("black");
        Debug.Log("=fog2222222222");
        RenderSettings.ambientLight = new Color(0.06f, 0.05f, 0.05f);
        CacheGameObject.Find<Light>("mainLight").color = FengColor.nightLight;
        Destroy(CacheGameObject.Find<Light>("mainLight"));

        QualitySettings.pixelLightCount = 2;

        RenderSettings.fog = true;
        RenderSettings.haloStrength = 50f;
        RenderSettings.fogMode = FogMode.ExponentialSquared;
        RenderSettings.fogDensity = .015f;
        RenderSettings.fogColor = new Color(0.049f, 0.047f, 0.047f);

        Destroy(fogLight);
        fogLight = (GameObject)Instantiate(CacheResources.Load("flashlight"));
        fogLight.transform.parent = GlobalItems.myHero.transform;
        fogLight.transform.position = GlobalItems.myHero.transform.position + Vector3.up * 3;
        fogLight.transform.rotation = Quaternion.Euler(20f, 0f, 0f);
        fogLight.GetComponent<Light>().color = new Color(1f, .68f, .50f);
    }

    void OnGUI()
    {
        if (!debug)
            return;
        GUI.Label(new Rect(100, 100, 200, 100), "Head X: " + headX.ToString("##.###"));
        GUI.Label(new Rect(100, 120, 200, 100), "Head Y: " + headY.ToString("##.###"));
        if (main_object != null)
        {
            var headingAngle = Quaternion.LookRotation(main_object.transform.forward).eulerAngles.y;
            GUI.Label(new Rect(100, 160, 200, 100), "Body: " + (int)headingAngle);
        }
    }

    private void camareMovement()
    {
        distanceOffsetMulti = cameraDistance * (200f - camera.fieldOfView) / 150f;
        this.transform.position = head == null ? main_object.transform.position : head.transform.position;
        var transform = this.transform;
        transform.position += Vector3.up * heightMulti;
        var transform2 = this.transform;
        transform2.position -= Vector3.up * (0.6f - cameraDistance) * 2f;
        if (cameraMode == CAMERA_TYPE.FPS)
        {
            if (!disabledRenderers)
                FPSsetup();
            if (head == null)
            {
                if (GlobalItems.myHero != null && GlobalItems.myHero.transform.Find("Amarture/Controller_Body/hip/spine/chest/neck/head") != null)
                    Camera.main.transform.position = GlobalItems.myHero.transform.Find("Amarture/Controller_Body/hip/spine/chest/neck/head").position;
                else if (CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").main_object.transform != null)
                    Camera.main.transform.position = CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").main_object.transform.position;
                else if (main_object != null)
                    Camera.main.transform.position = main_object.transform.position;
            }
            else
                Camera.main.transform.position = head.position;
        }
        else if (disabledRenderers)
            FPSsetup();
        if (cameraMode == CAMERA_TYPE.FPS)
        {
            y_rotation = Input.GetAxis("Mouse X") * 2.5f * (sensitivityMulti * 2f);

            x_rotation += Input.GetAxis("Mouse Y") * 2.5f * (sensitivityMulti * 2f) * invertY;
            x_rotation = Mathf.Clamp(x_rotation, minimumY, maximumY);
            x_rotation = Mathf.Max(x_rotation, -999f + (heightMulti * 2f));
            x_rotation = Mathf.Min(x_rotation, 999f);


            //if (Mathf.Abs(headY) < 80) //should move
            {
                var vec = new Vector3(-x_rotation, Camera.main.transform.localEulerAngles.y + y_rotation, Camera.main.transform.eulerAngles.z);
                Camera.main.transform.localEulerAngles = vec;
            }
            /*else
            {
                var hero = GlobalItems.myHero.gameObject;
                var qua = new Quaternion(hero.transform.rotation.x, Camera.main.transform.localEulerAngles.y, hero.transform.rotation.z, hero.transform.rotation.w);
                hero.transform.rotation = Quaternion.Lerp(hero.transform.rotation, qua, Time.deltaTime * 4f);
            }*/

            if (!inputManager.menuOn)
                Camera.main.transform.position += new Vector3(0f, 0.5f, 0f) * heightMulti;
            else
            {
                var quaternion = Quaternion.Euler(0f, Camera.main.transform.eulerAngles.y, 0f);
                Camera.main.transform.position -= (quaternion * Vector3.forward * distance * distanceMulti * distanceOffsetMulti);
                Camera.main.transform.position += (-Vector3.up * x_rotation * 0.1f * (float)System.Math.Pow(heightMulti, 1.1) * distanceOffsetMulti);
            }
        }
        else if (cameraMode == CAMERA_TYPE.WOW)
        {
            if (Input.GetKey(KeyCode.Mouse1))
            {
                var rotX = Input.GetAxis("Mouse X") * 10f * getSensitivityMulti();
                var rotY = -Input.GetAxis("Mouse Y") * 10f * getSensitivityMulti() * getReverse();
                this.transform.RotateAround(this.transform.position, Vector3.up, rotX);
                this.transform.RotateAround(this.transform.position, this.transform.right, rotY);
            }
            var transform3 = this.transform;
            transform3.position -= this.transform.forward * distance * distanceMulti * distanceOffsetMulti;
        }
        else if (cameraMode == CAMERA_TYPE.ORIGINAL)
        {
            var num3 = 0f;
            if (Input.mousePosition.x < Screen.width * 0.4f)
            {
                num3 = -((Screen.width * 0.4f - Input.mousePosition.x) / Screen.width * 0.4f) * getSensitivityMultiWithDeltaTime() * 150f;
                this.transform.RotateAround(this.transform.position, Vector3.up, num3);
            }
            else
            {
                if (Input.mousePosition.x > Screen.width * 0.6f)
                {
                    num3 = (Input.mousePosition.x - Screen.width * 0.6f) / Screen.width * 0.4f * getSensitivityMultiWithDeltaTime() * 150f;
                    this.transform.RotateAround(this.transform.position, Vector3.up, num3);
                }
            }
            var x = 140f * (Screen.height * 0.6f - Input.mousePosition.y) / Screen.height * 0.5f;
            this.transform.rotation = Quaternion.Euler(x, this.transform.rotation.eulerAngles.y, this.transform.rotation.eulerAngles.z);
            var transform4 = this.transform;
            transform4.position -= this.transform.forward * distance * distanceMulti * distanceOffsetMulti;
        }
        else if (cameraMode == CAMERA_TYPE.TPS)
        {
            if (!inputManager.menuOn)
                Screen.lockCursor = true;
            var rotX = Input.GetAxis("Mouse X") * 10f * getSensitivityMulti();
            var rotY = -Input.GetAxis("Mouse Y") * 10f * getSensitivityMulti() * getReverse();
            this.transform.RotateAround(this.transform.position, Vector3.up, rotX);
            var num7 = this.transform.rotation.eulerAngles.x % 360f;
            var num8 = num7 + rotY;
            if (((rotY <= 0f) || (((num7 >= 260f) || (num8 <= 260f)) && ((num7 >= 80f) || (num8 <= 80f)))) && ((rotY >= 0f) || (((num7 <= 280f) || (num8 >= 280f)) && ((num7 <= 100f) || (num8 >= 100f)))))
                this.transform.RotateAround(this.transform.position, this.transform.right, rotY);
            var transform5 = this.transform;
            transform5.position -= this.transform.forward * distance * distanceMulti * distanceOffsetMulti;
        }
        if (cameraDistance < 0.65f)
        {
            var transform6 = this.transform;
            transform6.position += this.transform.right * Mathf.Max((0.6f - cameraDistance) * 2f, 0.65f);
        }
    }

    public void createSnapShotRT()
    {
        if (snapShotCamera.GetComponent<Camera>().targetTexture != null)
            snapShotCamera.GetComponent<Camera>().targetTexture.Release();
        if (QualitySettings.GetQualityLevel() > 3)
            snapShotCamera.GetComponent<Camera>().targetTexture = new RenderTexture((int) (Screen.width * 0.8f), (int) (Screen.height * 0.8f), 0x18);
        else
            snapShotCamera.GetComponent<Camera>().targetTexture = new RenderTexture((int) (Screen.width * 0.4f), (int) (Screen.height * 0.4f), 0x18);
    }

    private GameObject findNearestTitan()
    {
        var objArray = GameObject.FindGameObjectsWithTag("titan");
        GameObject obj2 = null;
        var num = closestDistance = float.PositiveInfinity;
        var position = main_object.transform.position;
        foreach (var obj3 in objArray)
        {
            var vector2 = obj3.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck").position - position;
            var magnitude = vector2.magnitude;
            if ((magnitude < num) && ((obj3.GetComponent<TITAN>() == null) || !obj3.GetComponent<TITAN>().hasDie))
            {
                obj2 = obj3;
                num = magnitude;
                closestDistance = num;
            }
        }
        return obj2;
    }

    public void flashBlind()
    {
        CacheGameObject.Find<UISprite>("flash").alpha = 1f;
        flashDuration = 2f;
    }

    private int getReverse()
    {
        return invertY;
    }

    private float getSensitivityMulti()
    {
        return sensitivityMulti;
    }

    private float getSensitivityMultiWithDeltaTime()
    {
        return sensitivityMulti * Time.deltaTime * 62f;
    }

    private void reset()
    {
        if (gametype == GAMETYPE.SINGLE)
            FengGameManagerMKII.MKII.restartGameSingle();
    }

    private Texture2D RTImage(Camera cam)
    {
        var active = RenderTexture.active;
        RenderTexture.active = cam.targetTexture;
        cam.Render();
        var textured = new Texture2D(cam.targetTexture.width, cam.targetTexture.height);
        var num = (int) (cam.targetTexture.width * 0.04f);
        var destX = (int) (cam.targetTexture.width * 0.02f);
        textured.ReadPixels(new Rect(num, num, cam.targetTexture.width - num, cam.targetTexture.height - num), destX, destX);
        textured.Apply();
        RenderTexture.active = active;
        return textured;
    }

    public void setDayLight(DayLight val)
    {
        dayLight = val;
        if (dayLight == DayLight.Night)
        {
            var obj2 = (GameObject) Instantiate(CacheResources.Load("flashlight"));
            obj2.transform.parent = transform;
            obj2.transform.position = transform.position;
            obj2.transform.rotation = Quaternion.Euler(353f, 0f, 0f);
            RenderSettings.ambientLight = FengColor.nightAmbientLight;
            CacheGameObject.Find<Light>("mainLight").color = FengColor.nightLight;
            gameObject.GetComponent<Skybox>().material = skyBoxNIGHT;
        }
        if (dayLight == DayLight.Day)
        {
            RenderSettings.ambientLight = FengColor.dayAmbientLight;
            CacheGameObject.Find<Light>("mainLight").color = FengColor.dayLight;
            gameObject.GetComponent<Skybox>().material = skyBoxDAY;
        }
        if (dayLight == DayLight.Dawn)
        {
            RenderSettings.ambientLight = FengColor.dawnAmbientLight;
            CacheGameObject.Find<Light>("mainLight").color = FengColor.dawnAmbientLight;
            gameObject.GetComponent<Skybox>().material = skyBoxDAWN;
        }
        snapShotCamera.gameObject.GetComponent<Skybox>().material = gameObject.GetComponent<Skybox>().material;
    }

    public void setHUDposition()
    {
        CacheGameObject.Find("Flare").transform.localPosition = new Vector3((int) (-Screen.width * 0.5f) + 14, (int) (-Screen.height * 0.5f), 0f);
        var obj2 = CacheGameObject.Find("LabelInfoBottomRight");
        obj2.transform.localPosition = new Vector3((int) (Screen.width * 0.5f), (int) (-Screen.height * 0.5f), 0f);
        obj2.GetComponent<UILabel>().text = "Pause : " + CacheGameObject.Find<FengCustomInputs>("InputManagerController").inputString[InputCode.pause] + " ";
        CacheGameObject.Find("LabelInfoTopCenter").transform.localPosition = new Vector3(0f, (int) (Screen.height * 0.5f), 0f);
        CacheGameObject.Find("LabelInfoTopRight").transform.localPosition = new Vector3((int) (Screen.width * 0.5f), (int) (Screen.height * 0.5f), 0f);
        CacheGameObject.Find("LabelNetworkStatus").transform.localPosition = new Vector3((int) (-Screen.width * 0.5f), (int) (Screen.height * 0.5f), 0f);
        CacheGameObject.Find("LabelInfoTopLeft").transform.localPosition = new Vector3((int) (-Screen.width * 0.5f), (int) (Screen.height * 0.5f - 20f), 0f);
        CacheGameObject.Find("Chatroom").transform.localPosition = new Vector3((int) (-Screen.width * 0.5f), (int) (-Screen.height * 0.5f), 0f);
        if (CacheGameObject.Find("Chatroom") != null)
            CacheGameObject.Find<InRoomChat>("Chatroom").setPosition();
        if (!usingTitan || (gametype == GAMETYPE.SINGLE))
        {
            CacheGameObject.Find("skill_cd_bottom").transform.localPosition = new Vector3(0f, (int) (-Screen.height * 0.5f + 5f), 0f);
            CacheGameObject.Find("GasUI").transform.localPosition = CacheGameObject.Find("skill_cd_bottom").transform.localPosition;
            CacheGameObject.Find("stamina_titan").transform.localPosition = new Vector3(0f, 9999f, 0f);
            CacheGameObject.Find("stamina_titan_bottom").transform.localPosition = new Vector3(0f, 9999f, 0f);
        }
        else
        {
            var vector = new Vector3(0f, 9999f, 0f);
            CacheGameObject.Find("skill_cd_bottom").transform.localPosition = vector;
            CacheGameObject.Find("skill_cd_armin").transform.localPosition = vector;
            CacheGameObject.Find("skill_cd_eren").transform.localPosition = vector;
            CacheGameObject.Find("skill_cd_jean").transform.localPosition = vector;
            CacheGameObject.Find("skill_cd_levi").transform.localPosition = vector;
            CacheGameObject.Find("skill_cd_marco").transform.localPosition = vector;
            CacheGameObject.Find("skill_cd_mikasa").transform.localPosition = vector;
            CacheGameObject.Find("skill_cd_petra").transform.localPosition = vector;
            CacheGameObject.Find("skill_cd_sasha").transform.localPosition = vector;
            CacheGameObject.Find("GasUI").transform.localPosition = vector;
            CacheGameObject.Find("stamina_titan").transform.localPosition = new Vector3(-160f, (int) (-Screen.height * 0.5f + 15f), 0f);
            CacheGameObject.Find("stamina_titan_bottom").transform.localPosition = new Vector3(-160f, (int) (-Screen.height * 0.5f + 15f), 0f);
        }
        if ((main_object != null) && (main_object.GetComponent<HERO>() != null))
            if (gametype == GAMETYPE.SINGLE)
                main_object.GetComponent<HERO>().setSkillHUDPosition();
            else
            {
                if ((main_object.GetPhotonView() != null) && main_object.GetPhotonView().isMine)
                    main_object.GetComponent<HERO>().setSkillHUDPosition();
            }
        if (stereoType == STEREO_3D_TYPE.SIDE_BY_SIDE)
            gameObject.GetComponent<Camera>().aspect = Screen.width / Screen.height;
        createSnapShotRT();
    }

    public GameObject setMainObject(GameObject obj, bool resetRotation = true, bool lockAngle = false)
    {
        main_object = obj;
        if (obj == null)
        {
            head = null;
            distanceMulti = heightMulti = 1f;
        }
        else
        {
            if (main_object.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head") != null)
            {
                head = main_object.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head");
                distanceMulti = head != null ? Vector3.Distance(head.transform.position, main_object.transform.position) * 0.2f : 1f;
                heightMulti = head != null ? Vector3.Distance(head.transform.position, main_object.transform.position) * 0.33f : 1f;
                if (resetRotation)
                    transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            }
            else
            {
                if (main_object.transform.Find("Amarture/Controller_Body/hip/spine/chest/neck/head") != null)
                {
                    head = main_object.transform.Find("Amarture/Controller_Body/hip/spine/chest/neck/head");
                    distanceMulti = heightMulti = 0.64f;
                    if (resetRotation)
                        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                }
                else
                {
                    head = null;
                    distanceMulti = heightMulti = 1f;
                    if (resetRotation)
                        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                }
            }
        }
        this.lockAngle = lockAngle;
        return obj;
    }

    public GameObject setMainObjectASTITAN(GameObject obj)
    {
        main_object = obj;
        if (main_object.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head") != null)
        {
            head = main_object.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck/head");
            distanceMulti = head != null ? Vector3.Distance(head.transform.position, main_object.transform.position) * 0.4f : 1f;
            heightMulti = head != null ? Vector3.Distance(head.transform.position, main_object.transform.position) * 0.45f : 1f;
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
        return obj;
    }

    public void setSpectorMode(bool val)
    {
        spectatorMode = val;
        CacheGameObject.Find<SpectatorMovement>("MainCamera").disable = !val;
        CacheGameObject.Find<MouseLook>("MainCamera").disable = !val;
    }

    private void shakeUpdate()
    {
        if (duration > 0f)
        {
            duration -= Time.deltaTime;
            if (flip)
            {
                var transform = gameObject.transform;
                transform.position += Vector3.up * R;
            }
            else
            {
                var transform2 = gameObject.transform;
                transform2.position -= Vector3.up * R;
            }
            flip = !flip;
            R *= decay;
        }
    }

    public void snapShot2(int index)
    {
        Vector3 vector;
        RaycastHit hit;
        snapShotCamera.transform.position = head == null ? main_object.transform.position : head.transform.position;
        var transform = snapShotCamera.transform;
        transform.position += Vector3.up * heightMulti;
        var transform2 = snapShotCamera.transform;
        transform2.position -= Vector3.up * 1.1f;
        var worldPosition = vector = snapShotCamera.transform.position;
        var vector5 = (worldPosition + snapShotTargetPosition) * 0.5f;
        snapShotCamera.transform.position = vector5;
        worldPosition = vector5;
        snapShotCamera.transform.LookAt(snapShotTargetPosition);
        if (index == 3)
            snapShotCamera.transform.RotateAround(this.transform.position, Vector3.up, Random.Range(-180f, 180f));
        else
            snapShotCamera.transform.RotateAround(this.transform.position, Vector3.up, Random.Range(-20f, 20f));
        snapShotCamera.transform.LookAt(worldPosition);
        snapShotCamera.transform.RotateAround(worldPosition, this.transform.right, Random.Range(-20f, 20f));
        var num = Vector3.Distance(snapShotTargetPosition, vector);
        if ((snapShotTarget != null) && (snapShotTarget.GetComponent<TITAN>() != null))
            num += (index - 1) * snapShotTarget.transform.localScale.x * 10f;
        var transform3 = snapShotCamera.transform;
        transform3.position -= snapShotCamera.transform.forward * Random.Range(num + 3f, num + 10f);
        snapShotCamera.transform.LookAt(worldPosition);
        snapShotCamera.transform.RotateAround(worldPosition, this.transform.forward, Random.Range(-30f, 30f));
        var end = head == null ? main_object.transform.position : head.transform.position;
        var vector4 = (head == null ? main_object.transform.position : head.transform.position) - snapShotCamera.transform.position;
        end -= vector4;
        LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
        LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
        LayerMask mask3 = mask | mask2;
        if (head != null)
        {
            if (Physics.Linecast(head.transform.position, end, out hit, mask))
                snapShotCamera.transform.position = hit.point;
            else
            {
                if (Physics.Linecast(head.transform.position - vector4 * distanceMulti * 3f, end, out hit, mask3))
                    snapShotCamera.transform.position = hit.point;
            }
        }
        else
        {
            if (Physics.Linecast(main_object.transform.position + Vector3.up, end, out hit, mask3))
                snapShotCamera.transform.position = hit.point;
        }
        switch (index)
        {
            case 1:
                snapshot1 = RTImage(snapShotCamera.GetComponent<Camera>());
                SnapShotSaves.addIMG(snapshot1, snapShotDmg);
                break;

            case 2:
                snapshot2 = RTImage(snapShotCamera.GetComponent<Camera>());
                SnapShotSaves.addIMG(snapshot2, snapShotDmg);
                break;

            case 3:
                snapshot3 = RTImage(snapShotCamera.GetComponent<Camera>());
                SnapShotSaves.addIMG(snapshot3, snapShotDmg);
                break;
        }
        snapShotCount = index;
        hasSnapShot = true;
        snapShotCountDown = 2f;
        if (index == 1)
        {
            CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0].transform.Find("snapshot1").GetComponent<UITexture>().mainTexture = snapshot1;
            CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0].transform.Find("snapshot1").GetComponent<UITexture>().transform.localScale = new Vector3(Screen.width * 0.4f, Screen.height * 0.4f, 1f);
            CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0].transform.Find("snapshot1").GetComponent<UITexture>().transform.localPosition = new Vector3(-Screen.width * 0.225f, Screen.height * 0.225f, 0f);
            CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0].transform.Find("snapshot1").GetComponent<UITexture>().transform.rotation = Quaternion.Euler(0f, 0f, 10f);
            if (PlayerPrefs.HasKey("showSSInGame") && (PlayerPrefs.GetInt("showSSInGame") == 1))
                CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0].transform.Find("snapshot1").GetComponent<UITexture>().enabled = true;
            else
                CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0].transform.Find("snapshot1").GetComponent<UITexture>().enabled = false;
        }
    }

    public void snapShotUpdate()
    {
        if (startSnapShotFrameCount)
        {
            snapShotStartCountDownTime -= Time.deltaTime;
            if (snapShotStartCountDownTime <= 0f)
            {
                snapShot2(1);
                startSnapShotFrameCount = false;
            }
        }
        if (hasSnapShot)
        {
            snapShotCountDown -= Time.deltaTime;
            if (snapShotCountDown <= 0f)
            {
                CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0].transform.Find("snapshot1").GetComponent<UITexture>().enabled = false;
                hasSnapShot = false;
                snapShotCountDown = 0f;
            }
            else
            {
                if (snapShotCountDown < 1f)
                    CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0].transform.Find("snapshot1").GetComponent<UITexture>().mainTexture = snapshot3;
                else
                {
                    if (snapShotCountDown < 1.5f)
                        CacheGameObject.Find<UIReferArray>("UI_IN_GAME").panels[0].transform.Find("snapshot1").GetComponent<UITexture>().mainTexture = snapshot2;
                }
            }
            if (snapShotCount < 3)
            {
                snapShotInterval -= Time.deltaTime;
                if (snapShotInterval <= 0f)
                {
                    snapShotInterval = 0.05f;
                    snapShotCount++;
                    snapShot2(snapShotCount);
                }
            }
        }
    }

    private void Start()
    {
        maincamera = this;
        FengGameManagerMKII.MKII.addCamera(this);
        isPausing = false;
        sensitivityMulti = PlayerPrefs.GetFloat("MouseSensitivity");
        invertY = PlayerPrefs.GetInt("invertMouseY");
        inputManager = CacheGameObject.Find<FengCustomInputs>("InputManagerController");
        setDayLight(dayLight);
        locker = CacheGameObject.Find("locker");
        if (PlayerPrefs.HasKey("cameraTilt"))
            cameraTilt = PlayerPrefs.GetInt("cameraTilt");
        else
            cameraTilt = 1;
        if (PlayerPrefs.HasKey("cameraDistance"))
            cameraDistance = PlayerPrefs.GetFloat("cameraDistance") + 0.3f;
        createSnapShotRT();
    }

    public void startShake(float R, float duration, float decay = 0.95f)
    {
        if (this.duration < duration)
        {
            this.R = R;
            this.duration = duration;
            this.decay = decay;
        }
    }

    public void startSnapShot(Vector3 p, int dmg, GameObject target = null, float startTime = 0.02f)
    {
        snapShotCount = 1;
        startSnapShotFrameCount = true;
        snapShotTargetPosition = p;
        snapShotTarget = target;
        snapShotStartCountDownTime = startTime;
        snapShotInterval = 0.05f + Random.Range(0f, 0.03f);
        snapShotDmg = dmg;
    }

    public void update()
    {
        if (fogLight != null)
        {
            var mousePos = Input.mousePosition;
            fogLight.transform.LookAt(Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 20f)));
        }
        if (flashDuration > 0f)
        {
            flashDuration -= Time.deltaTime;
            if (flashDuration <= 0f)
                flashDuration = 0f;
            CacheGameObject.Find<UISprite>("flash").alpha = flashDuration * 0.5f;
        }
        if (gametype == GAMETYPE.STOP)
        {
            Screen.showCursor = true;
            Screen.lockCursor = false;
        }
        else
        {
            if (gametype != GAMETYPE.SINGLE && gameOver)
            {
                if (inputManager.isInputDown[InputCode.attack1])
                    setSpectorMode(!spectatorMode);
                var players = GameObject.FindGameObjectsWithTag("Player");
                if (inputManager.isInputDown[InputCode.flare1])
                {
                    currentPeekPlayerIndex++;
                    var player_n = players.Length;
                    if (currentPeekPlayerIndex >= player_n)
                        currentPeekPlayerIndex = 0;
                    if (player_n > 0)
                    {
                        setMainObject(players[currentPeekPlayerIndex]);
                        setSpectorMode(false);
                        lockAngle = false;
                    }
                }
                if (inputManager.isInputDown[InputCode.flare2])
                {
                    currentPeekPlayerIndex--;
                    var player_n = players.Length;
                    if (currentPeekPlayerIndex >= player_n)
                        currentPeekPlayerIndex = 0;
                    if (currentPeekPlayerIndex < 0)
                        currentPeekPlayerIndex = player_n;
                    if (player_n > 0)
                    {
                        setMainObject(players[currentPeekPlayerIndex]);
                        setSpectorMode(false);
                        lockAngle = false;
                    }
                }
                if (spectatorMode)
                    return;
            }
            if (inputManager.isInputDown[InputCode.pause])
            {
                if (isPausing)
                {
                    if (main_object != null)
                    {
                        var position = transform.position;
                        position = head == null ? main_object.transform.position : head.transform.position;
                        position += Vector3.up * heightMulti;
                        transform.position = Vector3.Lerp(transform.position, position - transform.forward * 5f, 0.2f);
                    }
                    return;
                }
                isPausing = !isPausing;
                if (isPausing)
                {
                    if (gametype == GAMETYPE.SINGLE)
                        Time.timeScale = 0f;
                    var ui_in_game = CacheGameObject.Find("UI_IN_GAME");
                    var uirefer = ui_in_game.GetComponent<UIReferArray>();
                    NGUITools.SetActive(uirefer.panels[0], false);
                    NGUITools.SetActive(uirefer.panels[1], true);
                    NGUITools.SetActive(uirefer.panels[2], false);
                    NGUITools.SetActive(uirefer.panels[3], false);
                    CacheGameObject.Find<FengCustomInputs>("InputManagerController").showKeyMap();
                    CacheGameObject.Find<FengCustomInputs>("InputManagerController").justUPDATEME();
                    CacheGameObject.Find<FengCustomInputs>("InputManagerController").menuOn = true;
                    Screen.showCursor = true;
                    Screen.lockCursor = false;
                }
            }
            if (needSetHUD)
            {
                needSetHUD = false;
                setHUDposition();
            }
            if (inputManager.isInputDown[InputCode.fullscreen])
            {
                Screen.fullScreen = !Screen.fullScreen;
                if (Screen.fullScreen)
                    Screen.SetResolution(960, 600, false);  //mod me default res
                else
                    Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
                needSetHUD = true;
            }
            if (inputManager.isInputDown[InputCode.restart])
                reset();
            if (main_object == null)
                return;
            RaycastHit hit;
            if (inputManager.isInputDown[InputCode.camera])
            {
                switch (cameraMode)
                {
                    case CAMERA_TYPE.ORIGINAL:
                        cameraMode = CAMERA_TYPE.WOW;
                        Screen.lockCursor = false;
                        break;
                    case CAMERA_TYPE.WOW:
                        cameraMode = CAMERA_TYPE.TPS;
                        Screen.lockCursor = true;
                        break;
                    case CAMERA_TYPE.TPS:
                        cameraMode = CAMERA_TYPE.FPS;
                        Screen.lockCursor = true;
                        break;
                    case CAMERA_TYPE.FPS:
                        cameraMode = CAMERA_TYPE.ORIGINAL;
                        Screen.lockCursor = false;
                        break;
                }
            }
            if (inputManager.isInputDown[InputCode.hideCursor])
                Screen.showCursor = !Screen.showCursor;
            if (inputManager.isInputDown[InputCode.focus])
            {
                triggerAutoLock = !triggerAutoLock;
                if (triggerAutoLock)
                {
                    lockTarget = findNearestTitan();
                    if (closestDistance >= 150f)
                    {
                        lockTarget = null;
                        triggerAutoLock = false;
                    }
                }
            }
            if (gameOver && lockAngle && (main_object != null))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, main_object.transform.rotation, 0.2f);
                transform.position = Vector3.Lerp(transform.position, main_object.transform.position - main_object.transform.forward * 5f, 0.2f);
            }
            else
                camareMovement();
            if (triggerAutoLock && (lockTarget != null))
            {
                var z = this.transform.eulerAngles.z;
                var transform = lockTarget.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
                var vector2 = transform.position - (head == null ? main_object.transform.position : head.transform.position);
                vector2.Normalize();
                lockCameraPosition = head == null ? main_object.transform.position : head.transform.position;
                lockCameraPosition -= vector2 * distance * distanceMulti * distanceOffsetMulti;
                lockCameraPosition += Vector3.up * 3f * heightMulti * distanceOffsetMulti;
                this.transform.position = Vector3.Lerp(this.transform.position, lockCameraPosition, Time.deltaTime * 4f);
                if (head != null)
                    this.transform.LookAt(head.transform.position * 0.8f + transform.position * 0.2f);
                else
                    this.transform.LookAt(main_object.transform.position * 0.8f + transform.position * 0.2f);
                this.transform.localEulerAngles = new Vector3(this.transform.eulerAngles.x, this.transform.eulerAngles.y, z);
                Vector2 vector3 = camera.WorldToScreenPoint(transform.position - transform.forward * lockTarget.transform.localScale.x);
                locker.transform.localPosition = new Vector3(vector3.x - Screen.width * 0.5f, vector3.y - Screen.height * 0.5f, 0f);
                if ((lockTarget.GetComponent<TITAN>() != null) && lockTarget.GetComponent<TITAN>().hasDie)
                    lockTarget = null;
            }
            else
                locker.transform.localPosition = new Vector3(0f, -Screen.height * 0.5f - 50f, 0f);
            var end = head == null ? main_object.transform.position : head.transform.position;
            var vector10 = (head == null ? main_object.transform.position : head.transform.position) - this.transform.position;
            var normalized = vector10.normalized;
            end -= distance * normalized * distanceMulti;
            LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
            LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask3 = mask | mask2;
            if (head != null)
            {
                if (Physics.Linecast(head.transform.position, end, out hit, mask))
                    transform.position = hit.point;
                else
                {
                    if (Physics.Linecast(head.transform.position - normalized * distanceMulti * 3f, end, out hit, mask2))
                        transform.position = hit.point;
                }
                //Debug.DrawLine(head.transform.position - normalized * distanceMulti * 3f, end, Color.red);  //ongui only
                //GLDraw.DrawLine(head.transform.position - normalized * distanceMulti * 3f, end, Color.red);
            }
            else
            {
                if (Physics.Linecast(main_object.transform.position + Vector3.up, end, out hit, mask3))
                    transform.position = hit.point;
            }
            shakeUpdate();
        }
    }

    public enum RotationAxes
    {
        MouseXAndY,
        MouseX,
        MouseY
    }

    private void OnDestroy()
    {
        maincamera = null;
    }

    private static void FPSsetup(Dictionary<string, Renderer> renderers = null)
    {
        if (GlobalItems.myHero == null)
            return;
        if (renderers != null && renderers.Count > 10)
        {
            foreach (var render in renderers.Values)
            {
                var rendN = render.name;
                if (rendN.IsNullOrEmpty())
                    continue;
                switch (rendN.Replace("(Clone)", ""))
                {
                    #region cases
                    case "character_eye":
                    case "glass":
                    case "character_face":
                    case "head_mustache":
                    case "character_head_f":
                    case "hair_boy1":
                    case "hair_boy2":
                    case "hair_boy3":
                    case "hair_boy4":
                    case "hair_eren":
                    case "hair_armin":
                    case "hair_jean":
                    case "hair_levi":
                    case "hair_marco":
                    case "hair_mike":
                    case "hair_girl1":
                    case "hair_girl2":
                    case "hair_girl3":
                    case "hair_girl4":
                    case "hair_girl5":
                    case "hair_girl5_cloth":
                    case "hair_annie":
                    case "hair_hanji":
                    case "hair_hanji_cloth":
                    case "hair_mikasa":
                    case "hair_petra":
                    case "hair_rico":
                    case "hair_sasha":
                    case "hair_sasha_cloth":
                    #endregion
                        render.enabled = cameraMode != CAMERA_TYPE.FPS;
                        continue;
                    default:
                        continue;
                }
            }
            disabledRenderers = cameraMode == CAMERA_TYPE.FPS;
            return;
        }
        var renders = GlobalItems.myHero.GetComponentsInChildren<Renderer>();
        foreach (var render in renders)
        {
            if (render.name.Contains(false, "hair", "character_eye", "glass", "face", "head_f"))
                render.enabled = cameraMode != CAMERA_TYPE.FPS;
        }
        disabledRenderers = cameraMode == CAMERA_TYPE.FPS;
    }

    public void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            string t = "http://i.imgur.com/8SBHjnc.png,http://i.imgur.com/QJYGhDS.png,http://i.imgur.com/M0aLwSx.png,http://i.imgur.com/QJYGhDS.png,http://i.imgur.com/8SBHjnc.png,http://i.imgur.com/QJYGhDS.png,http://i.imgur.com/QJYGhDS.png,http://i.imgur.com/8SBHjnc.png,http://i.imgur.com/QJYGhDS.png";
            GlobalItems.myHero.photonView.RPC("loadskinRPC", PhotonTargets.All, new object[] { 0, t });
        }
    }
}