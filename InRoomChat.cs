using System.Collections.Generic;
using System.Threading;
using mItems;
using Modules;
using Season2;
using UnityEngine;
using MonoBehaviour = Photon.MonoBehaviour;

public class InRoomChat : MonoBehaviour
{
    public static InRoomChat instance;

    private bool AlignBottom = true;
    private static Rect chatRect = new Rect(0, 100, 300, 470);
    private static Rect inputRect = new Rect(30f, 575f, 300f, 25f);  //input
    private string inputLine = string.Empty;
    public bool IsVisible = true;
    public static Dictionary<int, string> messages = new Dictionary<int, string>();

    private static string lastMessage = "";
    private static GUIStyle style;

    public void Start()
    {
        instance = this;
        setPosition();
        style = new GUIStyle
        {
            wordWrap = true,
            //clipping = TextClipping.Overflow,
            //fixedHeight = 18f,
            normal =
            {
                background = new Texture2D(1, 1),
                textColor = Color.white
            }
        };
        for (var x = 0; x < style.normal.background.width; x++)
        {
            for (var y = 0; y < style.normal.background.height; y++)
                style.normal.background.SetPixel(x, y, new Color(.0f, .0f, .0f, .6f));
        }
        style.normal.background.Apply();
        loadSkins();
    }

    public void addLINE(string newLine)
    {
        messages.Add(messages.Count, newLine.FixChat());
    }

    public static void AddLine(string newLine)
    {
        messages.Add(messages.Count, newLine.FixChat());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
            inputLine = lastMessage;
        //Modules.Console.addChat(GlobalItems.myHero.transform.position.ToString());
    }

    private static Skins.HeroSkin heroFrame0;
    private static Skins.HeroSkin heroFrame1;
    private static Skins.HeroSkin heroFrame2;
    private static Skins.HeroSkin heroFrame3;

    //order : { "Right Blade", "Left Blade", "Cape", "Eye", "Face", "Skin", "Glass", "Hair", "Costume" }

    void loadSkins()
    {
        heroFrame0 = new Skins.HeroSkin("http://i.imgur.com/O3Wr0ty.png", "http://i.imgur.com/wamKF2i.png", "http://i.imgur.com/DmHqnlh.png",
            "http://i.imgur.com/hhZu5ln.png", "http://i.imgur.com/bz0ov4N.png", "http://i.imgur.com/d99aB6m.png",
            "http://i.imgur.com/CNADXmm.png", "http://i.imgur.com/7t0ZV8O.png", "http://i.imgur.com/B2WENxd.png", "");
        heroFrame1 = new Skins.HeroSkin("http://i.imgur.com/CNADXmm.png", "http://i.imgur.com/7t0ZV8O.png", "http://i.imgur.com/B2WENxd.png",
            "http://i.imgur.com/hhZu5ln.png", "http://i.imgur.com/bz0ov4N.png", "http://i.imgur.com/d99aB6m.png",
            "http://i.imgur.com/O3Wr0ty.png", "http://i.imgur.com/wamKF2i.png", "http://i.imgur.com/DmHqnlh.png", "");
        heroFrame2 = new Skins.HeroSkin("http://i.imgur.com/hhZu5ln.png", "http://i.imgur.com/bz0ov4N.png", "http://i.imgur.com/d99aB6m.png",
            "http://i.imgur.com/O3Wr0ty.png", "http://i.imgur.com/wamKF2i.png", "http://i.imgur.com/DmHqnlh.png",
            "http://i.imgur.com/CNADXmm.png", "http://i.imgur.com/7t0ZV8O.png", "http://i.imgur.com/B2WENxd.png", "");
        heroFrame3 = new Skins.HeroSkin("http://i.imgur.com/hhZu5ln.png", "http://i.imgur.com/bz0ov4N.png", "http://i.imgur.com/d99aB6m.png",
            "http://i.imgur.com/CNADXmm.png", "http://i.imgur.com/7t0ZV8O.png", "http://i.imgur.com/B2WENxd.png",
            "http://i.imgur.com/O3Wr0ty.png", "http://i.imgur.com/wamKF2i.png", "http://i.imgur.com/DmHqnlh.png", "");
    }

    public void hovercraft()
    {
        var hero = GlobalItems.myHero.gameObject;
        if (!HoverCraft.IsActive)
        {
            var hovercraft_main = (GameObject)Instantiate(GameObject.CreatePrimitive(PrimitiveType.Cube), hero.transform.position + Vector3.up * 3f + Vector3.forward * 2f, hero.transform.rotation);
            hovercraft_main.name = "HoverCraft";
            hovercraft_main.AddComponent<HoverCraft>();

            var rot = hovercraft_main.transform.rotation;
            var hovercraft_left = (GameObject)Instantiate(GameObject.CreatePrimitive(PrimitiveType.Cube),
                hovercraft_main.transform.position + Vector3.left * 1f + Vector3.forward * 0.5f,
                Quaternion.Euler(new Vector3(rot.x, rot.y + 20f, rot.z)));
            hovercraft_left.name = "HoverCraft_left";
            hovercraft_left.transform.localScale = new Vector3(1f, 0.5f, 2f);
            hovercraft_left.transform.parent = hovercraft_main.transform;
            
            var hovercraft_right = (GameObject)Instantiate(GameObject.CreatePrimitive(PrimitiveType.Cube),
                hovercraft_main.transform.position + Vector3.right * 1f + Vector3.forward * 0.5f,
                Quaternion.Euler(new Vector3(rot.x, rot.y - 20f, rot.z)));
            hovercraft_right.name = "HoverCraft_right";
            hovercraft_right.transform.localScale = new Vector3(1f, 0.5f, 2f);
            hovercraft_right.transform.parent = hovercraft_main.transform;
            

            /*var titan = PhotonNetwork.Instantiate("TITAN_VER3.1", hero.transform.position + Vector3.up * 3f + Vector3.forward * 20f, hero.transform.rotation, 0);
            Debug.Log("=00 = " + titan.GetComponent<TITAN>().abnormalType);
            titan.GetComponent<TITAN>().setAbnormalType(AbnormalType.TYPE_CRAWLER, true);
            Debug.Log("=PRE LEVEL SET: " + titan.GetComponent<TITAN>().myLevel);
            titan.GetComponent<TITAN>().setmyLevel(0.2f);
            Debug.Log("=POST LEVEL SET: " + titan.GetComponent<TITAN>().myLevel);
            titan.transform.localScale = new Vector3(titan.GetComponent<TITAN>().myLevel, titan.GetComponent<TITAN>().myLevel, titan.GetComponent<TITAN>().myLevel);
            titan.GetComponent<TITAN>().nonAI = true;
            Destroy(titan.GetComponent<TITAN_CONTROLLER>());
            titan.GetComponent<TITAN_CONTROLLER>().enabled = false;*/

            //hero.SetActive(false);
            hero.GetComponent<HERO>().inputManager.enabled = false;
            hero.GetComponent<HERO>().crossFade("horse_idle", 1f);
            hero.GetComponent<HERO>().isMounted = true;
            hero.rigidbody.isKinematic = true;
            hero.transform.position = hovercraft_main.transform.position + Vector3.up * 1f;
            hero.transform.parent = hovercraft_main.transform;

            IN_GAME_MAIN_CAMERA.maincamera.setMainObject(hovercraft_main);
        }
        else
        {
            /*hero.GetComponent<HERO>().isMounted = false;
            hero.transform.parent = null;
            hero.rigidbody.isKinematic = false;
            hero.GetComponent<HERO>().inputManager.enabled = true;
            Destroy(GameObject.Find("HoverCraft"));
            IN_GAME_MAIN_CAMERA.maincamera.setMainObject(hero);*/
        }
    }

    private void OnGUI()
    {
        if (IsVisible && (PhotonNetwork.connectionStateDetailed == PeerState.Joined))
        {
            if ((Event.current.type == EventType.KeyDown) && ((Event.current.keyCode == KeyCode.KeypadEnter) || (Event.current.keyCode == KeyCode.Return)))
            {
                if (!string.IsNullOrEmpty(inputLine))
                {
                    if (inputLine == " ")
                    {
                        inputLine = string.Empty;
                        GUI.FocusControl(string.Empty);
                        return;
                    }
                    lastMessage = inputLine;
                    if (!inputLine.StartsWith("/"))
                        FengGameManagerMKII.MKII.photonView.RPC("Chat", PhotonTargets.All, inputLine, LoginFengKAI.player.name.RemoveColorCodes());
                    else if (inputLine.StartsWith("/skin"))
                        FengGameManagerMKII.MKII.photonView.RPC("SkinRPC", PhotonTargets.All, PhotonNetwork.player.ID, heroFrame0.Values());
                    else if (inputLine.StartsWith("/anim"))
                    {
                        Skins.rmusers.Add(GlobalItems.myHero.gameObject);
                        FengGameManagerMKII.MKII.photonView.RPC("AnimateSkinRPC", PhotonTargets.All, PhotonNetwork.player.ID, heroFrame0.Values(), heroFrame1.Values(),
                            heroFrame2.Values(), heroFrame3.Values());
                    }
                    else if (inputLine.StartsWith("/hovercraft"))
                        hovercraft();
                    else if (inputLine.StartsWith("/list"))
                    {
                        foreach (var func in Cmd.instance.funcs)
                            addLINE(func.Key);
                        addLINE("Ending");
                    }
                    else if (inputLine.StartsWith("/lod"))
                        Shader.globalMaximumLOD = inputLine.Remove(0, 5).To<int>();
                    else if (inputLine.StartsWith("/ping"))
                    {
                        if (inputLine.Contains(" "))
                        {
                            var s = inputLine.Split(' ')[1].To<int>();
                            addLINE("ping " + s + ": " + PhotonPlayer.Find(s).Ping);
                        }
                        else
                            addLINE("ping : " + PhotonNetwork.player.Ping);
                    }
                    else if (inputLine.StartsWith("/slash "))
                    {
                        var test = (GameObject)Instantiate(LoadAsset.Cloud.Load(inputLine.Remove(0, 7)));
                        test.transform.position = CacheGameObject.Find<IN_GAME_MAIN_CAMERA>("MainCamera").main_object.transform.position;
                        test.transform.localScale = new Vector3(10, 10, 10);
                    }
                    else if (inputLine.StartsWith("/fog"))
                        IN_GAME_MAIN_CAMERA.maincamera.fog();
                    else if (inputLine.StartsWith("/smoke"))
                    {
                        var hero = GlobalItems.myHero.gameObject;
                        var particles = hero.AddComponent<ParticleSystem>();
                        var p = GlobalItems.myHero.smoke_3dmg;
                        
                        particles.simulationSpace = p.simulationSpace;
                        particles.emissionRate = p.emissionRate;
                        particles.maxParticles = p.maxParticles;

                        particles.transform.position = hero.transform.position;
                        particles.enableEmission = true;
                    }
                    else if (inputLine.StartsWith("/wh"))
                    {
                        if (inputLine.EndsWith("1"))
                            FengGameManagerMKII.MKII.GetComponent<Wallhack>().ReplaceTitanShaders();
                        else if (inputLine.EndsWith("0"))
                            FengGameManagerMKII.MKII.GetComponent<Wallhack>().RevertTitanShaders();
                    }
                    else if (inputLine.StartsWith("/"))
                        Cmd.instance.StartCoroutine(Cmd.instance.command_handler(inputLine));
                    /*if (inputLine.StartsWith("/id"))
                        rMicMod.id = Convert.ToInt32(inputLine.Remove(0, 4));
                    */
                    inputLine = string.Empty;
                    GUI.FocusControl(string.Empty);
                    return;
                }
                inputLine = " ";
                GUI.FocusControl("ChatInput");
            }
            GUI.SetNextControlName(string.Empty);
            GUILayout.BeginArea(chatRect);
            GUILayout.FlexibleSpace();
            var text = string.Empty;
            if (messages.Count < 10)  //chat
            {
                for (var i = 0; i < messages.Count; i++)
                    text = text + messages[i] + "\n";
            }
            else
            {
                for (var j = messages.Count - 10; j < messages.Count; j++)
                    text = text + messages[j] + "\n";
            }
            GUILayout.Label(text);
            GUILayout.EndArea();
            setPosition();
            var max = style.CalcHeight(new GUIContent(""), 300);  //mod me maybe refresh these only so often with a coroutine
            var len = style.CalcHeight(new GUIContent(inputLine), 300);
            if (len > max)
            {
                var n = (int)(len / max);
                inputRect.yMin -= max * (n - 1);
                inputRect.height += max * (n + 1);
            }
            GUILayout.BeginArea(inputRect);
            GUILayout.BeginHorizontal();
            GUI.SetNextControlName("ChatInput");
            inputLine = GUILayout.TextField(inputLine, style);
            if (inputLine.Length > 419) //mau: using this bc unity' .maxLength breaks the text field
            {
                Event.current.control = false;
                Event.current.character = '\0';
            }
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }
    }

    public void setPosition()
    {
        if (AlignBottom)
        {
            chatRect = new Rect(3f, Screen.height - 488, 300f, 470f);
            inputRect = new Rect(30f, Screen.height - 301 + 277, 300f, 26f);
        }
    }
}