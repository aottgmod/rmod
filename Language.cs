﻿using mItems;
using UnityEngine;

public class Language
{
    public static readonly string[] abnormal = new string[0x19];
    public static readonly string[] btn_back = new string[0x19];
    public static readonly string[] btn_continue = new string[0x19];
    public static readonly string[] btn_create_game = new string[0x19];
    public static readonly string[] btn_credits = new string[0x19];
    public static readonly string[] btn_default = new string[0x19];
    public static readonly string[] btn_join = new string[0x19];
    public static readonly string[] btn_LAN = new string[0x19];
    public static readonly string[] btn_multiplayer = new string[0x19];
    public static readonly string[] btn_option = new string[0x19];
    public static readonly string[] btn_QUICK_MATCH = new string[0x19];
    public static readonly string[] btn_quit = new string[0x19];
    public static readonly string[] btn_ready = new string[0x19];
    public static readonly string[] btn_refresh = new string[0x19];
    public static readonly string[] btn_server_ASIA = new string[0x19];
    public static readonly string[] btn_server_EU = new string[0x19];
    public static readonly string[] btn_server_JAPAN = new string[0x19];
    public static readonly string[] btn_server_US = new string[0x19];
    public static readonly string[] btn_single = new string[0x19];
    public static readonly string[] btn_start = new string[0x19];
    public static readonly string[] camera_info = new string[0x19];
    public static readonly string[] camera_original = new string[0x19];
    public static readonly string[] camera_tilt = new string[0x19];
    public static readonly string[] camera_tps = new string[0x19];
    public static readonly string[] camera_type = new string[0x19];
    public static readonly string[] camera_wow = new string[0x19];
    public static readonly string[] change_quality = new string[0x19];
    public static readonly string[] choose_character = new string[0x19];
    public static readonly string[] choose_map = new string[0x19];
    public static readonly string[] choose_region_server = new string[0x19];
    public static readonly string[] difficulty = new string[0x19];
    public static readonly string[] game_time = new string[0x19];
    public static readonly string[] hard = new string[0x19];
    public static readonly string[] invert_mouse = new string[0x19];
    public static readonly string[] key_set_info_1 = new string[0x19];
    public static readonly string[] key_set_info_2 = new string[0x19];
    public static readonly string[] max_player = new string[0x19];
    public static readonly string[] max_Time = new string[0x19];
    public static readonly string[] mouse_sensitivity = new string[0x19];
    public static readonly string[] normal = new string[0x19];
    public static readonly string[] port = new string[0x19];
    public static readonly string[] select_titan = new string[0x19];
    public static readonly string[] server_ip = new string[0x19];
    public static readonly string[] server_name = new string[0x19];
    public static readonly string[] soldier = new string[0x19];
    public static readonly string[] titan = new string[0x19];
    public static int type = -1;
    public static readonly string[] waiting_for_input = new string[0x19];

    public static string GetLang(int id)
    {
        if (id == 0)
            return "ENGLISH";
        switch (id)
        {
            case 1:
                return "简体中文";
            case 2:
                return "SPANISH";
            case 3:
                return "POLSKI";
            case 4:
                return "ITALIANO";
            case 5:
                return "NORWEGIAN";
            case 6:
                return "PORTUGUESE";
            case 7:
                return "PORTUGUESE_BR";
            case 8:
                return "繁體中文_台";
            case 9:
                return "繁體中文_港";
            case 10:
                return "SLOVAK";
            case 11:
                return "GERMAN";
            case 12:
                return "FRANCAIS";
            case 13:
                return "T\x00dcRK\x00c7E";
            case 14:
                return "ARABIC";
            case 15:
                return "Thai";
            case 0x10:
                return "Русский";
            case 0x11:
                return "NEDERLANDS";
            case 0x12:
                return "Hebrew";
            case 0x13:
                return "DANSK";
        }
        return "ENGLISH";
    }

    public static int GetLangIndex(string txt)
    {
        if (txt == "ENGLISH")
            return 0;
        switch (txt)
        {
            case "SPANISH":
                return 2;
            case "POLSKI":
                return 3;
            case "ITALIANO":
                return 4;
            case "NORWEGIAN":
                return 5;
            case "PORTUGUESE":
                return 6;
            case "PORTUGUESE_BR":
                return 7;
            case "SLOVAK":
                return 10;
            case "GERMAN":
                return 11;
            case "FRANCAIS":
                return 12;
            case "T\x00dcRK\x00c7E":
                return 13;
            case "ARABIC":
                return 14;
            case "Thai":
                return 15;
            case "Русский":
                return 0x10;
            case "NEDERLANDS":
                return 0x11;
            case "Hebrew":
                return 0x12;
            case "DANSK":
                return 0x13;
            case "简体中文":
                return 1;
            case "繁體中文_台":
                return 8;
            case "繁體中文_港":
                return 9;
        }
        return 0;
    }

    public static void init()
    {
        var strArray = ((TextAsset)CacheResources.Load("lang")).text.Split("\n"[0]);
        var txt = string.Empty;
        var index = 0;
        for (var i = 0; i < strArray.Length; i++)
        {
            var str2 = strArray[i];
            if (str2.Contains("//"))
                continue;
            if (str2.Contains("#START"))
            {
                txt = str2.Split('@')[1];
                index = GetLangIndex(txt);
            }
            else if (str2.Contains("#END"))
                txt = string.Empty;
            else if ((txt != string.Empty) && str2.Contains("@"))
            {
                var original = str2.Split('@')[0];
                var translated = str2.Split('@')[1];
                switch (original)
                {
                    case "btn_single":
                        btn_single[index] = translated;
                        break;
                    case "btn_multiplayer":
                        btn_multiplayer[index] = translated;
                        break;
                    case "btn_option":
                        btn_option[index] = translated;
                        break;
                    case "btn_credits":
                        btn_credits[index] = translated;
                        break;
                    case "btn_back":
                        btn_back[index] = translated;
                        break;
                    case "btn_refresh":
                        btn_refresh[index] = translated;
                        break;
                    case "btn_join":
                        btn_join[index] = translated;
                        break;
                    case "btn_start":
                        btn_start[index] = translated;
                        break;
                    case "btn_create_game":
                        btn_create_game[index] = translated;
                        break;
                    case "btn_LAN":
                        btn_LAN[index] = translated;
                        break;
                    case "btn_server_US":
                        btn_server_US[index] = translated;
                        break;
                    case "btn_server_EU":
                        btn_server_EU[index] = translated;
                        break;
                    case "btn_server_ASIA":
                        btn_server_ASIA[index] = translated;
                        break;
                    case "btn_server_JAPAN":
                        btn_server_JAPAN[index] = translated;
                        break;
                    case "btn_QUICK_MATCH":
                        btn_QUICK_MATCH[index] = translated;
                        break;
                    case "btn_default":
                        btn_default[index] = translated;
                        break;
                    case "btn_ready":
                        btn_ready[index] = translated;
                        break;
                    case "server_name":
                        server_name[index] = translated;
                        break;
                    case "server_ip":
                        server_ip[index] = translated;
                        break;
                    case "port":
                        port[index] = translated;
                        break;
                    case "choose_map":
                        choose_map[index] = translated;
                        break;
                    case "choose_character":
                        choose_character[index] = translated;
                        break;
                    case "camera_type":
                        camera_type[index] = translated;
                        break;
                    case "camera_original":
                        camera_original[index] = translated;
                        break;
                    case "camera_wow":
                        camera_wow[index] = translated;
                        break;
                    case "camera_tps":
                        camera_tps[index] = translated;
                        break;
                    case "max_player":
                        max_player[index] = translated;
                        break;
                    case "max_Time":
                        max_Time[index] = translated;
                        break;
                    case "game_time":
                        game_time[index] = translated;
                        break;
                    case "difficulty":
                        difficulty[index] = translated;
                        break;
                    case "normal":
                        normal[index] = translated;
                        break;
                    case "hard":
                        hard[index] = translated;
                        break;
                    case "abnormal":
                        abnormal[index] = translated;
                        break;
                    case "mouse_sensitivity":
                        mouse_sensitivity[index] = translated;
                        break;
                    case "change_quality":
                        change_quality[index] = translated;
                        break;
                    case "camera_tilt":
                        camera_tilt[index] = translated;
                        break;
                    case "invert_mouse":
                        invert_mouse[index] = translated;
                        break;
                    case "waiting_for_input":
                        waiting_for_input[index] = translated;
                        break;
                    case "key_set_info_1":
                        key_set_info_1[index] = translated;
                        break;
                    case "key_set_info_2":
                        key_set_info_2[index] = translated;
                        break;
                    case "soldier":
                        soldier[index] = translated;
                        break;
                    case "titan":
                        titan[index] = translated;
                        break;
                    case "select_titan":
                        select_titan[index] = translated;
                        break;
                    case "camera_info":
                        camera_info[index] = translated;
                        break;
                    case "btn_continue":
                        btn_continue[index] = translated;
                        break;
                    case "btn_quit":
                        btn_quit[index] = translated;
                        break;
                    case "choose_region_server":
                        choose_region_server[index] = translated;
                        break;
                }
            }
        }
    }
}