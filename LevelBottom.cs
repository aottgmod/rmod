using UnityEngine;

public class LevelBottom : MonoBehaviour
{
    public GameObject link;
    public BottomType type;

    private void OnTriggerStay(Collider other)
    {
        if (!other.gameObject.CompareTag("Player"))
            return;
        if (type == BottomType.Die)
        {
            if (other.gameObject.GetComponent<HERO>() == null)
                return;
            if (IN_GAME_MAIN_CAMERA.IsMultiplayer)
            {
                if (other.gameObject.GetPhotonView().isMine)
                    other.gameObject.GetComponent<HERO>().netDie(rigidbody.velocity * 50f, false, -1, string.Empty, true);
            }
            else
                other.gameObject.GetComponent<HERO>().die(other.gameObject.rigidbody.velocity * 50f, false);
        }
        else if (type == BottomType.Teleport)
            other.gameObject.transform.position = link != null ? link.transform.position : Vector3.zero;
    }

    private void Start() { }

    private void Update() { }
}