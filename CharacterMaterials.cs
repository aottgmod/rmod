using System.Collections.Generic;
using mItems;
using UnityEngine;

public class CharacterMaterials
{
    private static bool inited;
    public static Dictionary<string, Material> materials;

    public static void init()
    {
        if (!inited)
        {
            inited = true;
            materials = new Dictionary<string, Material>();
            newMaterial
            (
                "AOTTG_HERO_3DMG", "aottg_hero_AHSS_3dmg", "aottg_hero_annie_cap_causal", "aottg_hero_annie_cap_uniform", "aottg_hero_brand_sc",
                "aottg_hero_brand_mp", "aottg_hero_brand_g", "aottg_hero_brand_ts", "aottg_hero_skin_1", "aottg_hero_skin_2", "aottg_hero_skin_3",
                "aottg_hero_casual_fa_1", "aottg_hero_casual_fa_2", "aottg_hero_casual_fa_3", "aottg_hero_casual_fb_1", "aottg_hero_casual_fb_2",
                "aottg_hero_casual_ma_1", "aottg_hero_casual_ma_1_ahss", "aottg_hero_casual_ma_2", "aottg_hero_casual_ma_3", "aottg_hero_casual_mb_1",
                "aottg_hero_casual_mb_2", "aottg_hero_casual_mb_3", "aottg_hero_casual_mb_4", "aottg_hero_uniform_fa_1", "aottg_hero_uniform_fa_2",
                "aottg_hero_uniform_fa_3", "aottg_hero_uniform_fb_1", "aottg_hero_uniform_fb_2", "aottg_hero_uniform_ma_1", "aottg_hero_uniform_ma_2",
                "aottg_hero_uniform_ma_3", "aottg_hero_uniform_mb_1", "aottg_hero_uniform_mb_2", "aottg_hero_uniform_mb_3", "aottg_hero_uniform_mb_4", "hair_annie",
                "hair_armin", "hair_boy1", "hair_boy2", "hair_boy3", "hair_boy4", "hair_eren", "hair_girl1", "hair_girl2", "hair_girl3", "hair_girl4",
                "hair_girl5", "hair_hanji", "hair_jean", "hair_levi", "hair_marco", "hair_mike", "hair_petra", "hair_rico", "hair_sasha", "hair_mikasa"
            );
            var texture = (Texture) Object.Instantiate(CacheResources.Load("NewTexture/aottg_hero_eyes"));
            var material = (Material)Object.Instantiate(CacheResources.Load("NewTexture/MaterialGLASS"));
            material.mainTexture = texture;
            materials.Add("aottg_hero_eyes", material);
        }
    }

    private static void newMaterial(params string[] prefs)
    {
        foreach (var pref in prefs)
        {
            var texture = (Texture)Object.Instantiate(CacheResources.Load("NewTexture/" + pref));
            var material = (Material)Object.Instantiate(CacheResources.Load("NewTexture/MaterialCharacter"));
            material.mainTexture = texture;
            materials.Add(pref, material);
        }
    }
}