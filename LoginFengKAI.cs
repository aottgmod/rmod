using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

public class LoginFengKAI : MonoBehaviour
{
    private string ChangeGuildURL = "http://fenglee.com/game/aog/change_guild_name.php";
    private string ChangePasswordURL = "http://fenglee.com/game/aog/change_password.php";
    private string CheckUserURL = "http://fenglee.com/game/aog/login_check.php";
    private string ForgetPasswordURL = "http://fenglee.com/game/aog/forget_password.php";
    public string formText = string.Empty;
    private string GetInfoURL = "http://fenglee.com/game/aog/require_user_info.php";
    public PanelLoginGroupManager loginGroup;
    public GameObject output;
    public GameObject output2;
    public GameObject panelChangeGUILDNAME;
    public GameObject panelChangePassword;
    public GameObject panelForget;
    public GameObject panelLogin;
    public GameObject panelRegister;
    public GameObject panelStatus;
    public static PlayerInfoPHOTON player;
    private static string playerGUILDName = string.Empty;
    private static string playerName = string.Empty;
    private static string playerPassword = string.Empty;
    private string RegisterURL = "http://fenglee.com/game/aog/signup_check.php";

    public void cGuild(string name)
    {
        if (playerName == string.Empty)
        {
            logout();
            NGUITools.SetActive(panelChangeGUILDNAME, false);
            NGUITools.SetActive(panelLogin, true);
            output.GetComponent<UILabel>().text = "Please sign in.";
        }
        else
            StartCoroutine(changeGuild(name));
    }

    [DebuggerHidden]
    private IEnumerator changeGuild(string name)
    {
        return new c__Iterator5 { name = name, f__this = this};
    }

    [DebuggerHidden]
    private IEnumerator changePassword(string oldpassword, string password, string password2)
    {
        return new c__Iterator4 { oldpassword = oldpassword, password = password, password2 = password2, f__this = this};
    }

    private void clearCOOKIE()
    {
        playerName = string.Empty;
        playerPassword = string.Empty;
    }

    public void cpassword(string oldpassword, string password, string password2)
    {
        if (playerName == string.Empty)
        {
            logout();
            NGUITools.SetActive(panelChangePassword, false);
            NGUITools.SetActive(panelLogin, true);
            output.GetComponent<UILabel>().text = "Please sign in.";
        }
        else
            StartCoroutine(changePassword(oldpassword, password, password2));
    }

    [DebuggerHidden]
    private IEnumerator ForgetPassword(string email)
    {
        return new c__Iterator6 { email = email, f__this = this};
    }

    [DebuggerHidden]
    private IEnumerator getInfo()
    {
        return new c__Iterator2 { f__this = this };
    }

    public void login(string name, string password)
    {
        StartCoroutine(Login(name, password));
    }

    [DebuggerHidden]
    private IEnumerator Login(string name, string password)
    {
        return new c__Iterator1 { name = name, password = password, f__this = this};
    }

    public void logout()
    {
        clearCOOKIE();
        player = new PlayerInfoPHOTON();
        player.initAsGuest();
        output.GetComponent<UILabel>().text = "Welcome," + player.name;
    }

    [DebuggerHidden]
    private IEnumerator Register(string name, string password, string password2, string email)
    {
        return new c__Iterator3 { name = name, password = password, password2 = password2, email = email, f__this = this};
    }

    public void resetPassword(string email)
    {
        StartCoroutine(ForgetPassword(email));
    }

    public void signup(string name, string password, string password2, string email)
    {
        StartCoroutine(Register(name, password, password2, email));
    }

    private void Start()
    {
        if (player == null)
        {
            player = new PlayerInfoPHOTON();
            player.initAsGuest();
        }
        if (playerName != string.Empty)
        {
            NGUITools.SetActive(panelLogin, false);
            NGUITools.SetActive(panelStatus, true);
            StartCoroutine(getInfo());
        }
        else
            output.GetComponent<UILabel>().text = "Welcome," + player.name;
    }

    private void Update()
    {
    }

    [CompilerGenerated]
    private sealed class c__Iterator5 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object current;
        internal int PC;
//        internal string name; //RD:That line was detected as a duplicated variable declaration
        internal LoginFengKAI f__this;
        internal WWWForm form__0;
        internal WWW w__1;
        internal string name;

        [DebuggerHidden]
        public void Dispose()
        {
            PC = -1;
        }

        public bool MoveNext()
        {
            var num = (uint) PC;
            PC = -1;
            switch (num)
            {
                case 0:
                    form__0 = new WWWForm();
                    form__0.AddField("name", playerName);
                    form__0.AddField("guildname", name);
                    w__1 = new WWW(f__this.ChangeGuildURL, form__0);
                    current = w__1;
                    PC = 1;
                    return true;

                case 1:
                    if (w__1.error == null)
                    {
                        f__this.output.GetComponent<UILabel>().text = w__1.text;
                        if (w__1.text.Contains("Guild name set."))
                        {
                            NGUITools.SetActive(f__this.panelChangeGUILDNAME, false);
                            NGUITools.SetActive(f__this.panelStatus, true);
                            f__this.StartCoroutine(f__this.getInfo());
                        }
                        w__1.Dispose();
                        break;
                    }
                    print(w__1.error);
                    break;

                default:
                    goto Label_0135;
            }
            PC = -1;
        Label_0135:
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }
    }

    [CompilerGenerated]
    private sealed class c__Iterator4 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object current;
        internal int PC;
//        internal string oldpassword; //RD:That line was detected as a duplicated variable declaration
//        internal string password; //RD:That line was detected as a duplicated variable declaration
//        internal string password2; //RD:That line was detected as a duplicated variable declaration
        internal LoginFengKAI f__this;
        internal WWWForm form__0;
        internal WWW w__1;
        internal string oldpassword;
        internal string password;
        internal string password2;

        [DebuggerHidden]
        public void Dispose()
        {
            PC = -1;
        }

        public bool MoveNext()
        {
            var num = (uint) PC;
            PC = -1;
            switch (num)
            {
                case 0:
                    form__0 = new WWWForm();
                    form__0.AddField("userid", playerName);
                    form__0.AddField("old_password", oldpassword);
                    form__0.AddField("password", password);
                    form__0.AddField("password2", password2);
                    w__1 = new WWW(f__this.ChangePasswordURL, form__0);
                    current = w__1;
                    PC = 1;
                    return true;

                case 1:
                    if (w__1.error == null)
                    {
                        f__this.output.GetComponent<UILabel>().text = w__1.text;
                        if (w__1.text.Contains("Thanks, Your password changed successfully"))
                        {
                            NGUITools.SetActive(f__this.panelChangePassword, false);
                            NGUITools.SetActive(f__this.panelLogin, true);
                        }
                        w__1.Dispose();
                        break;
                    }
                    print(w__1.error);
                    break;

                default:
                    goto Label_014A;
            }
            PC = -1;
        Label_014A:
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }
    }

    [CompilerGenerated]
    private sealed class c__Iterator6 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object current;
        internal int PC;
//        internal string email; //RD:That line was detected as a duplicated variable declaration
        internal LoginFengKAI f__this;
        internal WWWForm form__0;
        internal WWW w__1;
        internal string email;

        [DebuggerHidden]
        public void Dispose()
        {
            PC = -1;
        }

        public bool MoveNext()
        {
            var num = (uint) PC;
            PC = -1;
            switch (num)
            {
                case 0:
                    form__0 = new WWWForm();
                    form__0.AddField("email", email);
                    w__1 = new WWW(f__this.ForgetPasswordURL, form__0);
                    current = w__1;
                    PC = 1;
                    return true;

                case 1:
                    if (w__1.error == null)
                    {
                        f__this.output.GetComponent<UILabel>().text = w__1.text;
                        w__1.Dispose();
                        NGUITools.SetActive(f__this.panelForget, false);
                        NGUITools.SetActive(f__this.panelLogin, true);
                        break;
                    }
                    print(w__1.error);
                    break;

                default:
                    goto Label_00FA;
            }
            f__this.clearCOOKIE();
            PC = -1;
        Label_00FA:
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }
    }

    [CompilerGenerated]
    private sealed class c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object current;
        internal int PC;
        internal LoginFengKAI f__this;
        internal WWWForm form__0;
        internal string[] result__2;
        internal WWW w__1;

        [DebuggerHidden]
        public void Dispose()
        {
            PC = -1;
        }

        public bool MoveNext()
        {
            var num = (uint) PC;
            PC = -1;
            switch (num)
            {
                case 0:
                    form__0 = new WWWForm();
                    form__0.AddField("userid", playerName);
                    form__0.AddField("password", playerPassword);
                    w__1 = new WWW(f__this.GetInfoURL, form__0);
                    current = w__1;
                    PC = 1;
                    return true;

                case 1:
                    if (w__1.error == null)
                    {
                        if (w__1.text.Contains("Error,please sign in again."))
                        {
                            NGUITools.SetActive(f__this.panelLogin, true);
                            NGUITools.SetActive(f__this.panelStatus, false);
                            f__this.output.GetComponent<UILabel>().text = w__1.text;
                            playerName = string.Empty;
                            playerPassword = string.Empty;
                        }
                        else
                        {
                            var separator = new[] { '|' };
                            result__2 = w__1.text.Split(separator);
                            playerGUILDName = result__2[0];
                            f__this.output2.GetComponent<UILabel>().text = result__2[1];
                            player.name = playerName;
                            player.guildname = playerGUILDName;
                        }
                        w__1.Dispose();
                        break;
                    }
                    print(w__1.error);
                    break;

                default:
                    goto Label_019F;
            }
            PC = -1;
        Label_019F:
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }
    }

    [CompilerGenerated]
    private sealed class c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object current;
        internal int PC;
//        internal string name; //RD:That line was detected as a duplicated variable declaration
//        internal string password; //RD:That line was detected as a duplicated variable declaration
        internal LoginFengKAI f__this;
        internal WWWForm form__0;
        internal WWW w__1;
        internal string name;
        internal string password;

        [DebuggerHidden]
        public void Dispose()
        {
            PC = -1;
        }

        public bool MoveNext()
        {
            var num = (uint) PC;
            PC = -1;
            switch (num)
            {
                case 0:
                    form__0 = new WWWForm();
                    form__0.AddField("userid", name);
                    form__0.AddField("password", password);
                    form__0.AddField("version", UIMainReferences.version);
                    w__1 = new WWW(f__this.CheckUserURL, form__0);
                    current = w__1;
                    PC = 1;
                    return true;

                case 1:
                    f__this.clearCOOKIE();
                    if (w__1.error == null)
                    {
                        f__this.output.GetComponent<UILabel>().text = w__1.text;
                        f__this.formText = w__1.text;
                        w__1.Dispose();
                        if (f__this.formText.Contains("Welcome back") && f__this.formText.Contains("(^o^)/~"))
                        {
                            NGUITools.SetActive(f__this.panelLogin, false);
                            NGUITools.SetActive(f__this.panelStatus, true);
                            playerName = name;
                            playerPassword = password;
                            f__this.StartCoroutine(f__this.getInfo());
                        }
                        break;
                    }
                    print(w__1.error);
                    break;

                default:
                    goto Label_019C;
            }
            PC = -1;
        Label_019C:
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }
    }

    [CompilerGenerated]
    private sealed class c__Iterator3 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal object current;
        internal int PC;
//        internal string email; //RD:That line was detected as a duplicated variable declaration
//        internal string name; //RD:That line was detected as a duplicated variable declaration
//        internal string password; //RD:That line was detected as a duplicated variable declaration
//        internal string password2; //RD:That line was detected as a duplicated variable declaration
        internal LoginFengKAI f__this;
        internal WWWForm form__0;
        internal WWW w__1;
        internal string email;
        internal string name;
        internal string password;
        internal string password2;

        [DebuggerHidden]
        public void Dispose()
        {
            PC = -1;
        }

        public bool MoveNext()
        {
            var num = (uint) PC;
            PC = -1;
            switch (num)
            {
                case 0:
                    form__0 = new WWWForm();
                    form__0.AddField("userid", name);
                    form__0.AddField("password", password);
                    form__0.AddField("password2", password2);
                    form__0.AddField("email", email);
                    w__1 = new WWW(f__this.RegisterURL, form__0);
                    current = w__1;
                    PC = 1;
                    return true;

                case 1:
                    if (w__1.error == null)
                    {
                        f__this.output.GetComponent<UILabel>().text = w__1.text;
                        if (w__1.text.Contains("Final step,to activate your account, please click the link in the activation email"))
                        {
                            NGUITools.SetActive(f__this.panelRegister, false);
                            NGUITools.SetActive(f__this.panelLogin, true);
                        }
                        w__1.Dispose();
                        break;
                    }
                    print(w__1.error);
                    break;

                default:
                    goto Label_0156;
            }
            f__this.clearCOOKIE();
            PC = -1;
        Label_0156:
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }

        object IEnumerator.Current
        {
            [DebuggerHidden]
            get
            {
                return current;
            }
        }
    }
}