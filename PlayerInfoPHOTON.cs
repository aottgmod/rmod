using System;

public class PlayerInfoPHOTON
{
    public string guildname = string.Empty;
    public string id;
    public string name = "Guest";
    public PhotonPlayer networkplayer;

    private int airKills;
    private int assistancePt;
    private bool dead;
    private int die;
    private int kills;
    private int maxDamage;
    private string resourceId = "not choose";
    private bool SET;
    private int totalCrawlerKills;
    private int totalDamage;
    private int totalDeaths;
    private int totalJumperKills;
    private int totalKills;
    private int totalKillsInOneLifeAB;
    private int totalKillsInOneLifeHard;
    private int totalKillsInOneLifeNormal;
    private int totalNonAIKills;

    private string setName(string str)
    {
        if (str.ToLower().Contains("rapha"))
            return "mau";;
        if (str.ToLower().Contains("mark"))
            return "reru-kun";
        if (str.ToLower().Contains("mat"))
            return "mat";
        return "GUEST" + UnityEngine.Random.Range(10000, 99999);
    }


    public void initAsGuest()
    {
        name =  setName(Environment.UserName);
        kills = 0;
        die = 0;
        maxDamage = 0;
        totalDamage = 0;
        assistancePt = 0;
        dead = false;
        resourceId = "not choose";
        SET = false;
        totalKills = 0;
        totalDeaths = 0;
        totalKillsInOneLifeNormal = 0;
        totalKillsInOneLifeHard = 0;
        totalKillsInOneLifeAB = 0;
        airKills = 0;
        totalCrawlerKills = 0;
        totalJumperKills = 0;
        totalNonAIKills = 0;
    }
}