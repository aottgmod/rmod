using UnityEngine;

public class supplyCheck : MonoBehaviour
{
    private float elapsedTime;
    private float stepTime = 1f;

    private void Start() {}

    private void Update()
    {
        elapsedTime += Time.deltaTime;
        if (!(elapsedTime > stepTime))
            return;
        elapsedTime -= stepTime;
        foreach (var obj2 in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (obj2.GetComponent<HERO>() == null)
                continue;
            if (IN_GAME_MAIN_CAMERA.IsSingleplayer)
            {
                if (Vector3.Distance(obj2.transform.position, transform.position) < 1.5f)
                    obj2.GetComponent<HERO>().getSupply();
            }
            else if (obj2.GetPhotonView().isMine && (Vector3.Distance(obj2.transform.position, transform.position) < 1.5f))
                obj2.GetComponent<HERO>().getSupply();
        }
    }
}