using UnityEngine;

public class PopListCamera : MonoBehaviour
{
    private void Awake()
    {
        GetComponent<UIPopupList>().selection = PlayerPrefs.HasKey("cameraType") ? PlayerPrefs.GetString("cameraType") : "TPS";
        setItems();
    }

    private void OnSelectionChange()
    {
        if (GetComponent<UIPopupList>().selection == "ORIGINAL")
            IN_GAME_MAIN_CAMERA.cameraMode = CAMERA_TYPE.ORIGINAL;
        if (GetComponent<UIPopupList>().selection == "WOW")
            IN_GAME_MAIN_CAMERA.cameraMode = CAMERA_TYPE.WOW;
        if (GetComponent<UIPopupList>().selection == "TPS")
            IN_GAME_MAIN_CAMERA.cameraMode = CAMERA_TYPE.TPS;
        if (GetComponent<UIPopupList>().selection == "FPS")
            IN_GAME_MAIN_CAMERA.cameraMode = CAMERA_TYPE.FPS;
        PlayerPrefs.SetString("cameraType", GetComponent<UIPopupList>().selection);
    }

    private void setItems()
    {
        if (!GetComponent<UIPopupList>().items.Contains("FPS"))
            GetComponent<UIPopupList>().items.Add("FPS");
    }
}