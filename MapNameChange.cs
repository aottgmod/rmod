using mItems;
using UnityEngine;

public class MapNameChange : MonoBehaviour
{
    private void OnSelectionChange()
    {
        var info = LevelInfo.getInfo(GetComponent<UIPopupList>().selection);
        if (info != null)
            CacheGameObject.Find<UILabel>("LabelLevelInfo").text = info.desc;
    }
}